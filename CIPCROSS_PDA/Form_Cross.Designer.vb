<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Form_Cross
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Cross))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.TabControl = New System.Windows.Forms.TabControl
        Me.TabPage_User = New System.Windows.Forms.TabPage
        Me.Panel_SelectCrossPlan = New System.Windows.Forms.Panel
        Me.Button_Cancel = New System.Windows.Forms.Button
        Me.ComboBox_SelectCrossingPlan = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lblID = New System.Windows.Forms.Label
        Me.ListBox_SelectPlan = New System.Windows.Forms.ListBox
        Me.TextBox_CrossingPlanSelectedHide = New System.Windows.Forms.TextBox
        Me.TextBox_CrossingPlanSelected = New System.Windows.Forms.TextBox
        Me.Button_SelectPlan = New System.Windows.Forms.Button
        Me.ListBox_FullName = New System.Windows.Forms.ListBox
        Me.ListBox_Password = New System.Windows.Forms.ListBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.ListBox_Username = New System.Windows.Forms.ListBox
        Me.Button_SignIn = New System.Windows.Forms.Button
        Me.ComboBox_Username = New System.Windows.Forms.ComboBox
        Me.ComboBox_Location = New System.Windows.Forms.ComboBox
        Me.Label_Location = New System.Windows.Forms.Label
        Me.TextBox_Password = New System.Windows.Forms.TextBox
        Me.Label_Password = New System.Windows.Forms.Label
        Me.Label_Username = New System.Windows.Forms.Label
        Me.Label_SignIn = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label_CrossingPlan = New System.Windows.Forms.Label
        Me.TextBox_CrossingPlan = New System.Windows.Forms.TextBox
        Me.Button_CrossingPlan = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label_Languaje = New System.Windows.Forms.Label
        Me.ComboBox_Language = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.TabPage_Parentals = New System.Windows.Forms.TabPage
        Me.Label_ParentalsMales = New System.Windows.Forms.Label
        Me.Label_ParentalsFemales = New System.Windows.Forms.Label
        Me.Label_CountMales = New System.Windows.Forms.Label
        Me.ListBox_FemaleLabels = New System.Windows.Forms.ListBox
        Me.Button_SearchFemale = New System.Windows.Forms.Button
        Me.TextBox_SearchFemale = New System.Windows.Forms.TextBox
        Me.Label_CountFemales = New System.Windows.Forms.Label
        Me.C1FlexGrid_Female = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.DateTimePicker_Female = New System.Windows.Forms.DateTimePicker
        Me.Button_PrintFemales = New System.Windows.Forms.Button
        Me.TextBox_SearchMale = New System.Windows.Forms.TextBox
        Me.ListBox_MaleLabels = New System.Windows.Forms.ListBox
        Me.Button_SearchMale = New System.Windows.Forms.Button
        Me.C1FlexGrid_Male = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.DateTimePicker_Male = New System.Windows.Forms.DateTimePicker
        Me.Label_Parentals = New System.Windows.Forms.Label
        Me.Button_PrintMales = New System.Windows.Forms.Button
        Me.TabPage_View = New System.Windows.Forms.TabPage
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.C1FlexGrid1 = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.TabPage_Crossing = New System.Windows.Forms.TabPage
        Me.ComboBox_GenMale = New System.Windows.Forms.ComboBox
        Me.ComboBox_GenFemale = New System.Windows.Forms.ComboBox
        Me.TextBox_SearchValueCross = New System.Windows.Forms.TextBox
        Me.Button_Search_ValueCross = New System.Windows.Forms.Button
        Me.lblPor = New System.Windows.Forms.Label
        Me.lblSearchCrossing = New System.Windows.Forms.Label
        Me.lblMale = New System.Windows.Forms.Label
        Me.lblFemale = New System.Windows.Forms.Label
        Me.C1FlexGrid_ValuesCross = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.TextBox_NroRepetitions = New System.Windows.Forms.TextBox
        Me.Label_CountMalesCross = New System.Windows.Forms.Label
        Me.Label_CountFemalesCross = New System.Windows.Forms.Label
        Me.Label_CrossingMales = New System.Windows.Forms.Label
        Me.Label_CrossingFemales = New System.Windows.Forms.Label
        Me.Button_SearchMaleCross = New System.Windows.Forms.Button
        Me.Button_SearchFemaleCross = New System.Windows.Forms.Button
        Me.TextBox_SearchMaleCross = New System.Windows.Forms.TextBox
        Me.TextBox_SearchFemaleCross = New System.Windows.Forms.TextBox
        Me.Label_CrossingTitle = New System.Windows.Forms.Label
        Me.TextBox_Data3 = New System.Windows.Forms.TextBox
        Me.TextBox_Data2 = New System.Windows.Forms.TextBox
        Me.Label_Order = New System.Windows.Forms.Label
        Me.C1FlexGrid_MaleCross = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.C1FlexGrid_FemaleCross = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.DateTimePicker_Cross = New System.Windows.Forms.DateTimePicker
        Me.ComboBox_TypeCross = New System.Windows.Forms.ComboBox
        Me.Label_TypeCross = New System.Windows.Forms.Label
        Me.ComboBox_Flowers = New System.Windows.Forms.ComboBox
        Me.TextBox_Order = New System.Windows.Forms.TextBox
        Me.TextBox_Cross = New System.Windows.Forms.TextBox
        Me.Button_PrintCross = New System.Windows.Forms.Button
        Me.Label_CrossingDate = New System.Windows.Forms.Label
        Me.Label_Flowers = New System.Windows.Forms.Label
        Me.Label_Crossing = New System.Windows.Forms.Label
        Me.Button_SelectCross = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.TabPage_Harvest = New System.Windows.Forms.TabPage
        Me.txtHarvest_Potato = New System.Windows.Forms.TextBox
        Me.ComboBox_TotalFlowers = New System.Windows.Forms.ComboBox
        Me.lblNroFloresG = New System.Windows.Forms.Label
        Me.TextBox_HideNroRepetitions = New System.Windows.Forms.TextBox
        Me.TextBox_IndexHarvest = New System.Windows.Forms.TextBox
        Me.Label_CountHarvest = New System.Windows.Forms.Label
        Me.Button_LoadHarvest = New System.Windows.Forms.Button
        Me.TextBox_Harvest = New System.Windows.Forms.TextBox
        Me.Button_PrintHarvest = New System.Windows.Forms.Button
        Me.Label_HarvestDate = New System.Windows.Forms.Label
        Me.Label_FruitSize = New System.Windows.Forms.Label
        Me.DateTimePicker_Harvest = New System.Windows.Forms.DateTimePicker
        Me.ComboBox_FruitSize = New System.Windows.Forms.ComboBox
        Me.ComboBox_NroFruits = New System.Windows.Forms.ComboBox
        Me.Button_SelectHarvest = New System.Windows.Forms.Button
        Me.Button_SearchHarvest = New System.Windows.Forms.Button
        Me.TextBox_SearchHarvest = New System.Windows.Forms.TextBox
        Me.C1FlexGrid_Harvest = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Results_DataSet1 = New DeviceApplication1.Results_Dataset
        Me.Label_HarvestTitle = New System.Windows.Forms.Label
        Me.Label_Fruits = New System.Windows.Forms.Label
        Me.TabPage_Harvest_Tuber = New System.Windows.Forms.TabPage
        Me.Label12 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtCosechaTuber = New System.Windows.Forms.TextBox
        Me.lblColector = New System.Windows.Forms.Label
        Me.lblNroTuber = New System.Windows.Forms.Label
        Me.ComboBox_NroTubers = New System.Windows.Forms.ComboBox
        Me.cboProcedence = New System.Windows.Forms.ComboBox
        Me.lblProcedence = New System.Windows.Forms.Label
        Me.cboFemaleTuber = New System.Windows.Forms.ComboBox
        Me.TextBox_SearchValue = New System.Windows.Forms.TextBox
        Me.btnSearchTuberculusHarvest = New System.Windows.Forms.Button
        Me.lblValueTuberculusHarvest = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.C1FlexGrid_ValuesTubHarv = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.lblTuberHarvestTitle = New System.Windows.Forms.Label
        Me.btnPrintTuberHarvest = New System.Windows.Forms.Button
        Me.btnSelectTuberHarv = New System.Windows.Forms.Button
        Me.TabPage_Maceration = New System.Windows.Forms.TabPage
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox_Weight1 = New System.Windows.Forms.TextBox
        Me.TextBox_Weight = New System.Windows.Forms.TextBox
        Me.TextBox_Comments = New System.Windows.Forms.TextBox
        Me.TextBox_Quantity = New System.Windows.Forms.TextBox
        Me.TextBox_HideNroRepetitions2 = New System.Windows.Forms.TextBox
        Me.TextBox_IndexMaceration = New System.Windows.Forms.TextBox
        Me.Button_PrintMaceration = New System.Windows.Forms.Button
        Me.Label_MacerationDate = New System.Windows.Forms.Label
        Me.DateTimePicker_Maceration = New System.Windows.Forms.DateTimePicker
        Me.Label_CountMaceration = New System.Windows.Forms.Label
        Me.TextBox_Maceration = New System.Windows.Forms.TextBox
        Me.Button_SelectMaceration = New System.Windows.Forms.Button
        Me.C1FlexGrid_Maceration = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.Button_LoadMaceration = New System.Windows.Forms.Button
        Me.Button_SearchMaceration = New System.Windows.Forms.Button
        Me.TextBox_SearchMaceration = New System.Windows.Forms.TextBox
        Me.Label_MacerationTitle = New System.Windows.Forms.Label
        Me.TabPage_Options = New System.Windows.Forms.TabPage
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.Label_CrossingOptions = New System.Windows.Forms.Label
        Me.cboBarcodeForCross = New System.Windows.Forms.ComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label_TypeCrossOption = New System.Windows.Forms.Label
        Me.ComboBox_TypeCrossOption = New System.Windows.Forms.ComboBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.ComboBox_Printer = New System.Windows.Forms.ComboBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.ComboBox_PrintType = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.ListBox_BarcodeType = New System.Windows.Forms.ListBox
        Me.ListBox_BarcodeTypeID = New System.Windows.Forms.ListBox
        Me.Label_PrintOptions = New System.Windows.Forms.Label
        Me.Label_BarcodeType = New System.Windows.Forms.Label
        Me.ComboBox_BarcodeType = New System.Windows.Forms.ComboBox
        Me.ListBox_Hide = New System.Windows.Forms.ListBox
        Me.Label_Port = New System.Windows.Forms.Label
        Me.ComboBox_Port = New System.Windows.Forms.ComboBox
        Me.Label_OptionsTitle = New System.Windows.Forms.Label
        Me.TabPage_Options2 = New System.Windows.Forms.TabPage
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextBox_PasswordDB = New System.Windows.Forms.TextBox
        Me.Label_ExportDB = New System.Windows.Forms.Label
        Me.Button_DeleteDB = New System.Windows.Forms.Button
        Me.Label_DBOptions = New System.Windows.Forms.Label
        Me.Button_ExportDB = New System.Windows.Forms.Button
        Me.chkPrintHarvestGeneral = New System.Windows.Forms.CheckBox
        Me.ChkDateHarvest = New System.Windows.Forms.CheckBox
        Me.dtpHarvestFin = New System.Windows.Forms.DateTimePicker
        Me.dtpHarvestIni = New System.Windows.Forms.DateTimePicker
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Panel_Message = New System.Windows.Forms.Panel
        Me.Label_Message = New System.Windows.Forms.Label
        Me.Button_Close = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.Tuber = New System.Windows.Forms.TabPage
        Me.TextBox_SearchValueTuberHarvest = New System.Windows.Forms.TextBox
        Me.btnSearchTubHarv = New System.Windows.Forms.Button
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.ResultsTableAdapter1 = New DeviceApplication1.Results_DatasetTableAdapters.resultsTableAdapter
        Me.TabControl.SuspendLayout()
        Me.TabPage_User.SuspendLayout()
        Me.Panel_SelectCrossPlan.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage_Parentals.SuspendLayout()
        Me.TabPage_View.SuspendLayout()
        Me.TabPage_Crossing.SuspendLayout()
        Me.TabPage_Harvest.SuspendLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Results_DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage_Harvest_Tuber.SuspendLayout()
        Me.TabPage_Maceration.SuspendLayout()
        Me.TabPage_Options.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.TabPage_Options2.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel_Message.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl
        '
        Me.TabControl.Controls.Add(Me.TabPage_User)
        Me.TabControl.Controls.Add(Me.TabPage_Parentals)
        Me.TabControl.Controls.Add(Me.TabPage_View)
        Me.TabControl.Controls.Add(Me.TabPage_Crossing)
        Me.TabControl.Controls.Add(Me.TabPage_Harvest)
        Me.TabControl.Controls.Add(Me.TabPage_Harvest_Tuber)
        Me.TabControl.Controls.Add(Me.TabPage_Maceration)
        Me.TabControl.Controls.Add(Me.TabPage_Options)
        Me.TabControl.Controls.Add(Me.TabPage_Options2)
        Me.TabControl.Location = New System.Drawing.Point(0, 0)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(240, 268)
        Me.TabControl.TabIndex = 0
        '
        'TabPage_User
        '
        Me.TabPage_User.Controls.Add(Me.Panel_SelectCrossPlan)
        Me.TabPage_User.Controls.Add(Me.Panel3)
        Me.TabPage_User.Controls.Add(Me.Panel2)
        Me.TabPage_User.Controls.Add(Me.Panel1)
        Me.TabPage_User.Controls.Add(Me.Label23)
        Me.TabPage_User.Controls.Add(Me.Label15)
        Me.TabPage_User.Controls.Add(Me.PictureBox1)
        Me.TabPage_User.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_User.Name = "TabPage_User"
        Me.TabPage_User.Size = New System.Drawing.Size(240, 245)
        Me.TabPage_User.Text = "User Login"
        '
        'Panel_SelectCrossPlan
        '
        Me.Panel_SelectCrossPlan.BackColor = System.Drawing.SystemColors.Info
        Me.Panel_SelectCrossPlan.Controls.Add(Me.Button_Cancel)
        Me.Panel_SelectCrossPlan.Controls.Add(Me.ComboBox_SelectCrossingPlan)
        Me.Panel_SelectCrossPlan.Controls.Add(Me.Label2)
        Me.Panel_SelectCrossPlan.Location = New System.Drawing.Point(0, 1)
        Me.Panel_SelectCrossPlan.Name = "Panel_SelectCrossPlan"
        Me.Panel_SelectCrossPlan.Size = New System.Drawing.Size(1, 1)
        Me.Panel_SelectCrossPlan.Visible = False
        '
        'Button_Cancel
        '
        Me.Button_Cancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_Cancel.ForeColor = System.Drawing.Color.White
        Me.Button_Cancel.Location = New System.Drawing.Point(84, 223)
        Me.Button_Cancel.Name = "Button_Cancel"
        Me.Button_Cancel.Size = New System.Drawing.Size(72, 20)
        Me.Button_Cancel.TabIndex = 21
        Me.Button_Cancel.Text = "Cancel"
        '
        'ComboBox_SelectCrossingPlan
        '
        Me.ComboBox_SelectCrossingPlan.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular)
        Me.ComboBox_SelectCrossingPlan.Location = New System.Drawing.Point(48, 98)
        Me.ComboBox_SelectCrossingPlan.Name = "ComboBox_SelectCrossingPlan"
        Me.ComboBox_SelectCrossingPlan.Size = New System.Drawing.Size(147, 27)
        Me.ComboBox_SelectCrossingPlan.TabIndex = 18
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(0, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(240, 22)
        Me.Label2.Text = "Select crossing plan"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.Panel3.Controls.Add(Me.lblID)
        Me.Panel3.Controls.Add(Me.ListBox_SelectPlan)
        Me.Panel3.Controls.Add(Me.TextBox_CrossingPlanSelectedHide)
        Me.Panel3.Controls.Add(Me.TextBox_CrossingPlanSelected)
        Me.Panel3.Controls.Add(Me.Button_SelectPlan)
        Me.Panel3.Controls.Add(Me.ListBox_FullName)
        Me.Panel3.Controls.Add(Me.ListBox_Password)
        Me.Panel3.Controls.Add(Me.Label25)
        Me.Panel3.Controls.Add(Me.ListBox_Username)
        Me.Panel3.Controls.Add(Me.Button_SignIn)
        Me.Panel3.Controls.Add(Me.ComboBox_Username)
        Me.Panel3.Controls.Add(Me.ComboBox_Location)
        Me.Panel3.Controls.Add(Me.Label_Location)
        Me.Panel3.Controls.Add(Me.TextBox_Password)
        Me.Panel3.Controls.Add(Me.Label_Password)
        Me.Panel3.Controls.Add(Me.Label_Username)
        Me.Panel3.Controls.Add(Me.Label_SignIn)
        Me.Panel3.Location = New System.Drawing.Point(4, 104)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(233, 141)
        '
        'lblID
        '
        Me.lblID.Location = New System.Drawing.Point(4, 108)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(16, 20)
        Me.lblID.Visible = False
        '
        'ListBox_SelectPlan
        '
        Me.ListBox_SelectPlan.Location = New System.Drawing.Point(213, 55)
        Me.ListBox_SelectPlan.Name = "ListBox_SelectPlan"
        Me.ListBox_SelectPlan.Size = New System.Drawing.Size(20, 16)
        Me.ListBox_SelectPlan.TabIndex = 51
        Me.ListBox_SelectPlan.Visible = False
        '
        'TextBox_CrossingPlanSelectedHide
        '
        Me.TextBox_CrossingPlanSelectedHide.Location = New System.Drawing.Point(223, 71)
        Me.TextBox_CrossingPlanSelectedHide.Name = "TextBox_CrossingPlanSelectedHide"
        Me.TextBox_CrossingPlanSelectedHide.Size = New System.Drawing.Size(10, 21)
        Me.TextBox_CrossingPlanSelectedHide.TabIndex = 45
        Me.TextBox_CrossingPlanSelectedHide.Visible = False
        '
        'TextBox_CrossingPlanSelected
        '
        Me.TextBox_CrossingPlanSelected.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.TextBox_CrossingPlanSelected.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox_CrossingPlanSelected.Font = New System.Drawing.Font("Tahoma", 6.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_CrossingPlanSelected.Location = New System.Drawing.Point(102, 0)
        Me.TextBox_CrossingPlanSelected.Multiline = True
        Me.TextBox_CrossingPlanSelected.Name = "TextBox_CrossingPlanSelected"
        Me.TextBox_CrossingPlanSelected.ReadOnly = True
        Me.TextBox_CrossingPlanSelected.Size = New System.Drawing.Size(89, 20)
        Me.TextBox_CrossingPlanSelected.TabIndex = 39
        '
        'Button_SelectPlan
        '
        Me.Button_SelectPlan.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SelectPlan.Font = New System.Drawing.Font("Tahoma", 7.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SelectPlan.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Button_SelectPlan.Location = New System.Drawing.Point(195, 0)
        Me.Button_SelectPlan.Name = "Button_SelectPlan"
        Me.Button_SelectPlan.Size = New System.Drawing.Size(32, 20)
        Me.Button_SelectPlan.TabIndex = 38
        Me.Button_SelectPlan.Text = "Plan"
        '
        'ListBox_FullName
        '
        Me.ListBox_FullName.Location = New System.Drawing.Point(213, 108)
        Me.ListBox_FullName.Name = "ListBox_FullName"
        Me.ListBox_FullName.Size = New System.Drawing.Size(20, 16)
        Me.ListBox_FullName.TabIndex = 32
        Me.ListBox_FullName.Visible = False
        '
        'ListBox_Password
        '
        Me.ListBox_Password.Location = New System.Drawing.Point(213, 124)
        Me.ListBox_Password.Name = "ListBox_Password"
        Me.ListBox_Password.Size = New System.Drawing.Size(20, 16)
        Me.ListBox_Password.TabIndex = 31
        Me.ListBox_Password.Visible = False
        '
        'Label25
        '
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 6.0!, System.Drawing.FontStyle.Regular)
        Me.Label25.Location = New System.Drawing.Point(0, 129)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(233, 11)
        Me.Label25.Text = "Integrated IT and Computational Research"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ListBox_Username
        '
        Me.ListBox_Username.Location = New System.Drawing.Point(213, 92)
        Me.ListBox_Username.Name = "ListBox_Username"
        Me.ListBox_Username.Size = New System.Drawing.Size(20, 16)
        Me.ListBox_Username.TabIndex = 30
        Me.ListBox_Username.Visible = False
        '
        'Button_SignIn
        '
        Me.Button_SignIn.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SignIn.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Button_SignIn.Location = New System.Drawing.Point(69, 101)
        Me.Button_SignIn.Name = "Button_SignIn"
        Me.Button_SignIn.Size = New System.Drawing.Size(96, 24)
        Me.Button_SignIn.TabIndex = 16
        Me.Button_SignIn.Text = "Sign in"
        '
        'ComboBox_Username
        '
        Me.ComboBox_Username.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.ComboBox_Username.Location = New System.Drawing.Point(102, 28)
        Me.ComboBox_Username.Name = "ComboBox_Username"
        Me.ComboBox_Username.Size = New System.Drawing.Size(104, 20)
        Me.ComboBox_Username.TabIndex = 15
        '
        'ComboBox_Location
        '
        Me.ComboBox_Location.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.ComboBox_Location.Location = New System.Drawing.Point(102, 79)
        Me.ComboBox_Location.Name = "ComboBox_Location"
        Me.ComboBox_Location.Size = New System.Drawing.Size(104, 20)
        Me.ComboBox_Location.TabIndex = 14
        '
        'Label_Location
        '
        Me.Label_Location.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Location.Location = New System.Drawing.Point(24, 78)
        Me.Label_Location.Name = "Label_Location"
        Me.Label_Location.Size = New System.Drawing.Size(72, 17)
        Me.Label_Location.Text = "Location"
        '
        'TextBox_Password
        '
        Me.TextBox_Password.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_Password.Location = New System.Drawing.Point(102, 52)
        Me.TextBox_Password.Name = "TextBox_Password"
        Me.TextBox_Password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBox_Password.Size = New System.Drawing.Size(104, 19)
        Me.TextBox_Password.TabIndex = 11
        '
        'Label_Password
        '
        Me.Label_Password.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Password.Location = New System.Drawing.Point(24, 53)
        Me.Label_Password.Name = "Label_Password"
        Me.Label_Password.Size = New System.Drawing.Size(72, 17)
        Me.Label_Password.Text = "Password"
        '
        'Label_Username
        '
        Me.Label_Username.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Username.Location = New System.Drawing.Point(24, 28)
        Me.Label_Username.Name = "Label_Username"
        Me.Label_Username.Size = New System.Drawing.Size(71, 20)
        Me.Label_Username.Text = "Username"
        '
        'Label_SignIn
        '
        Me.Label_SignIn.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_SignIn.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_SignIn.Location = New System.Drawing.Point(3, 1)
        Me.Label_SignIn.Name = "Label_SignIn"
        Me.Label_SignIn.Size = New System.Drawing.Size(145, 22)
        Me.Label_SignIn.Text = "Sign in"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.Panel2.Controls.Add(Me.Label_CrossingPlan)
        Me.Panel2.Controls.Add(Me.TextBox_CrossingPlan)
        Me.Panel2.Controls.Add(Me.Button_CrossingPlan)
        Me.Panel2.Location = New System.Drawing.Point(4, 66)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(233, 32)
        '
        'Label_CrossingPlan
        '
        Me.Label_CrossingPlan.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CrossingPlan.Location = New System.Drawing.Point(3, 9)
        Me.Label_CrossingPlan.Name = "Label_CrossingPlan"
        Me.Label_CrossingPlan.Size = New System.Drawing.Size(82, 16)
        Me.Label_CrossingPlan.Text = "Crossing Plan"
        '
        'TextBox_CrossingPlan
        '
        Me.TextBox_CrossingPlan.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_CrossingPlan.Location = New System.Drawing.Point(87, 8)
        Me.TextBox_CrossingPlan.Name = "TextBox_CrossingPlan"
        Me.TextBox_CrossingPlan.ReadOnly = True
        Me.TextBox_CrossingPlan.Size = New System.Drawing.Size(104, 19)
        Me.TextBox_CrossingPlan.TabIndex = 6
        '
        'Button_CrossingPlan
        '
        Me.Button_CrossingPlan.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_CrossingPlan.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Button_CrossingPlan.ForeColor = System.Drawing.Color.White
        Me.Button_CrossingPlan.Location = New System.Drawing.Point(195, 8)
        Me.Button_CrossingPlan.Name = "Button_CrossingPlan"
        Me.Button_CrossingPlan.Size = New System.Drawing.Size(32, 20)
        Me.Button_CrossingPlan.TabIndex = 7
        Me.Button_CrossingPlan.Text = "..."
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label_Languaje)
        Me.Panel1.Controls.Add(Me.ComboBox_Language)
        Me.Panel1.Location = New System.Drawing.Point(71, 37)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(166, 24)
        '
        'Label_Languaje
        '
        Me.Label_Languaje.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Languaje.Location = New System.Drawing.Point(4, 4)
        Me.Label_Languaje.Name = "Label_Languaje"
        Me.Label_Languaje.Size = New System.Drawing.Size(66, 18)
        Me.Label_Languaje.Text = "Language"
        '
        'ComboBox_Language
        '
        Me.ComboBox_Language.Font = New System.Drawing.Font("Tahoma", 7.0!, System.Drawing.FontStyle.Regular)
        Me.ComboBox_Language.Items.Add("English (Ingles)")
        Me.ComboBox_Language.Items.Add("Spanish (Espa�ol)")
        Me.ComboBox_Language.Location = New System.Drawing.Point(70, 5)
        Me.ComboBox_Language.Name = "ComboBox_Language"
        Me.ComboBox_Language.Size = New System.Drawing.Size(93, 19)
        Me.ComboBox_Language.TabIndex = 1
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.Label23.Location = New System.Drawing.Point(66, 20)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(171, 12)
        Me.Label23.Text = "CIPCROSS"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(66, 1)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(177, 20)
        Me.Label15.Text = "International Potato Center"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(4, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(61, 57)
        '
        'TabPage_Parentals
        '
        Me.TabPage_Parentals.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.TabPage_Parentals.Controls.Add(Me.Label_ParentalsMales)
        Me.TabPage_Parentals.Controls.Add(Me.Label_ParentalsFemales)
        Me.TabPage_Parentals.Controls.Add(Me.Label_CountMales)
        Me.TabPage_Parentals.Controls.Add(Me.ListBox_FemaleLabels)
        Me.TabPage_Parentals.Controls.Add(Me.Button_SearchFemale)
        Me.TabPage_Parentals.Controls.Add(Me.TextBox_SearchFemale)
        Me.TabPage_Parentals.Controls.Add(Me.Label_CountFemales)
        Me.TabPage_Parentals.Controls.Add(Me.C1FlexGrid_Female)
        Me.TabPage_Parentals.Controls.Add(Me.DateTimePicker_Female)
        Me.TabPage_Parentals.Controls.Add(Me.Button_PrintFemales)
        Me.TabPage_Parentals.Controls.Add(Me.TextBox_SearchMale)
        Me.TabPage_Parentals.Controls.Add(Me.ListBox_MaleLabels)
        Me.TabPage_Parentals.Controls.Add(Me.Button_SearchMale)
        Me.TabPage_Parentals.Controls.Add(Me.C1FlexGrid_Male)
        Me.TabPage_Parentals.Controls.Add(Me.DateTimePicker_Male)
        Me.TabPage_Parentals.Controls.Add(Me.Label_Parentals)
        Me.TabPage_Parentals.Controls.Add(Me.Button_PrintMales)
        Me.TabPage_Parentals.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_Parentals.Name = "TabPage_Parentals"
        Me.TabPage_Parentals.Size = New System.Drawing.Size(232, 242)
        Me.TabPage_Parentals.Text = "Parentals"
        '
        'Label_ParentalsMales
        '
        Me.Label_ParentalsMales.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_ParentalsMales.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_ParentalsMales.Location = New System.Drawing.Point(173, 5)
        Me.Label_ParentalsMales.Name = "Label_ParentalsMales"
        Me.Label_ParentalsMales.Size = New System.Drawing.Size(60, 17)
        Me.Label_ParentalsMales.Text = "Males"
        Me.Label_ParentalsMales.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label_ParentalsFemales
        '
        Me.Label_ParentalsFemales.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_ParentalsFemales.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_ParentalsFemales.Location = New System.Drawing.Point(7, 5)
        Me.Label_ParentalsFemales.Name = "Label_ParentalsFemales"
        Me.Label_ParentalsFemales.Size = New System.Drawing.Size(60, 17)
        Me.Label_ParentalsFemales.Text = "Females"
        '
        'Label_CountMales
        '
        Me.Label_CountMales.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_CountMales.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CountMales.Location = New System.Drawing.Point(123, 203)
        Me.Label_CountMales.Name = "Label_CountMales"
        Me.Label_CountMales.Size = New System.Drawing.Size(44, 20)
        '
        'ListBox_FemaleLabels
        '
        Me.ListBox_FemaleLabels.Location = New System.Drawing.Point(97, 225)
        Me.ListBox_FemaleLabels.Name = "ListBox_FemaleLabels"
        Me.ListBox_FemaleLabels.Size = New System.Drawing.Size(20, 16)
        Me.ListBox_FemaleLabels.TabIndex = 48
        Me.ListBox_FemaleLabels.Visible = False
        '
        'Button_SearchFemale
        '
        Me.Button_SearchFemale.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SearchFemale.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SearchFemale.ForeColor = System.Drawing.Color.White
        Me.Button_SearchFemale.Location = New System.Drawing.Point(95, 22)
        Me.Button_SearchFemale.Name = "Button_SearchFemale"
        Me.Button_SearchFemale.Size = New System.Drawing.Size(22, 20)
        Me.Button_SearchFemale.TabIndex = 47
        Me.Button_SearchFemale.Text = "S"
        '
        'TextBox_SearchFemale
        '
        Me.TextBox_SearchFemale.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_SearchFemale.Location = New System.Drawing.Point(7, 22)
        Me.TextBox_SearchFemale.Name = "TextBox_SearchFemale"
        Me.TextBox_SearchFemale.Size = New System.Drawing.Size(84, 19)
        Me.TextBox_SearchFemale.TabIndex = 46
        '
        'Label_CountFemales
        '
        Me.Label_CountFemales.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_CountFemales.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CountFemales.Location = New System.Drawing.Point(7, 203)
        Me.Label_CountFemales.Name = "Label_CountFemales"
        Me.Label_CountFemales.Size = New System.Drawing.Size(44, 20)
        '
        'C1FlexGrid_Female
        '
        Me.C1FlexGrid_Female.AllowEditing = False
        Me.C1FlexGrid_Female.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.C1FlexGrid_Female.AutoResize = True
        Me.C1FlexGrid_Female.AutoSearchDelay = 1
        Me.C1FlexGrid_Female.ClipSeparators = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.C1FlexGrid_Female.Col = 0
        Me.C1FlexGrid_Female.ColSel = 0
        Me.C1FlexGrid_Female.ComboList = Nothing
        Me.C1FlexGrid_Female.EditMask = Nothing
        Me.C1FlexGrid_Female.ExtendLastCol = False
        Me.C1FlexGrid_Female.LeftCol = 1
        Me.C1FlexGrid_Female.Location = New System.Drawing.Point(7, 47)
        Me.C1FlexGrid_Female.Name = "C1FlexGrid_Female"
        Me.C1FlexGrid_Female.Redraw = True
        Me.C1FlexGrid_Female.Row = -2
        Me.C1FlexGrid_Female.RowSel = -2
        Me.C1FlexGrid_Female.ScrollPosition = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid_Female.ScrollTrack = True
        Me.C1FlexGrid_Female.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
        Me.C1FlexGrid_Female.ShowCursor = False
        Me.C1FlexGrid_Female.ShowSort = True
        Me.C1FlexGrid_Female.Size = New System.Drawing.Size(110, 150)
        Me.C1FlexGrid_Female.StyleInfo = resources.GetString("C1FlexGrid_Female.StyleInfo")
        Me.C1FlexGrid_Female.TabIndex = 45
        Me.C1FlexGrid_Female.TopRow = 1
        '
        'DateTimePicker_Female
        '
        Me.DateTimePicker_Female.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.DateTimePicker_Female.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_Female.Location = New System.Drawing.Point(57, 199)
        Me.DateTimePicker_Female.Name = "DateTimePicker_Female"
        Me.DateTimePicker_Female.Size = New System.Drawing.Size(60, 20)
        Me.DateTimePicker_Female.TabIndex = 44
        '
        'Button_PrintFemales
        '
        Me.Button_PrintFemales.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_PrintFemales.ForeColor = System.Drawing.Color.White
        Me.Button_PrintFemales.Location = New System.Drawing.Point(7, 225)
        Me.Button_PrintFemales.Name = "Button_PrintFemales"
        Me.Button_PrintFemales.Size = New System.Drawing.Size(110, 20)
        Me.Button_PrintFemales.TabIndex = 43
        Me.Button_PrintFemales.Text = "Print Females"
        '
        'TextBox_SearchMale
        '
        Me.TextBox_SearchMale.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_SearchMale.Location = New System.Drawing.Point(123, 22)
        Me.TextBox_SearchMale.Name = "TextBox_SearchMale"
        Me.TextBox_SearchMale.Size = New System.Drawing.Size(84, 19)
        Me.TextBox_SearchMale.TabIndex = 41
        '
        'ListBox_MaleLabels
        '
        Me.ListBox_MaleLabels.Location = New System.Drawing.Point(213, 225)
        Me.ListBox_MaleLabels.Name = "ListBox_MaleLabels"
        Me.ListBox_MaleLabels.Size = New System.Drawing.Size(20, 16)
        Me.ListBox_MaleLabels.TabIndex = 35
        Me.ListBox_MaleLabels.Visible = False
        '
        'Button_SearchMale
        '
        Me.Button_SearchMale.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SearchMale.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SearchMale.ForeColor = System.Drawing.Color.White
        Me.Button_SearchMale.Location = New System.Drawing.Point(211, 22)
        Me.Button_SearchMale.Name = "Button_SearchMale"
        Me.Button_SearchMale.Size = New System.Drawing.Size(22, 20)
        Me.Button_SearchMale.TabIndex = 30
        Me.Button_SearchMale.Text = "S"
        '
        'C1FlexGrid_Male
        '
        Me.C1FlexGrid_Male.AllowEditing = False
        Me.C1FlexGrid_Male.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.C1FlexGrid_Male.AutoResize = True
        Me.C1FlexGrid_Male.AutoSearchDelay = 1
        Me.C1FlexGrid_Male.ClipSeparators = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.C1FlexGrid_Male.Col = 0
        Me.C1FlexGrid_Male.ColSel = 0
        Me.C1FlexGrid_Male.ComboList = Nothing
        Me.C1FlexGrid_Male.EditMask = Nothing
        Me.C1FlexGrid_Male.ExtendLastCol = False
        Me.C1FlexGrid_Male.LeftCol = 1
        Me.C1FlexGrid_Male.Location = New System.Drawing.Point(123, 47)
        Me.C1FlexGrid_Male.Name = "C1FlexGrid_Male"
        Me.C1FlexGrid_Male.Redraw = True
        Me.C1FlexGrid_Male.Row = -2
        Me.C1FlexGrid_Male.RowSel = -2
        Me.C1FlexGrid_Male.ScrollPosition = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid_Male.ScrollTrack = True
        Me.C1FlexGrid_Male.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
        Me.C1FlexGrid_Male.ShowCursor = False
        Me.C1FlexGrid_Male.ShowSort = True
        Me.C1FlexGrid_Male.Size = New System.Drawing.Size(110, 150)
        Me.C1FlexGrid_Male.StyleInfo = resources.GetString("C1FlexGrid_Male.StyleInfo")
        Me.C1FlexGrid_Male.TabIndex = 25
        Me.C1FlexGrid_Male.TopRow = 1
        '
        'DateTimePicker_Male
        '
        Me.DateTimePicker_Male.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.DateTimePicker_Male.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_Male.Location = New System.Drawing.Point(173, 199)
        Me.DateTimePicker_Male.Name = "DateTimePicker_Male"
        Me.DateTimePicker_Male.Size = New System.Drawing.Size(60, 20)
        Me.DateTimePicker_Male.TabIndex = 9
        '
        'Label_Parentals
        '
        Me.Label_Parentals.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_Parentals.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Parentals.Location = New System.Drawing.Point(7, 1)
        Me.Label_Parentals.Name = "Label_Parentals"
        Me.Label_Parentals.Size = New System.Drawing.Size(226, 20)
        Me.Label_Parentals.Text = "Parentals"
        Me.Label_Parentals.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Button_PrintMales
        '
        Me.Button_PrintMales.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_PrintMales.ForeColor = System.Drawing.Color.White
        Me.Button_PrintMales.Location = New System.Drawing.Point(123, 225)
        Me.Button_PrintMales.Name = "Button_PrintMales"
        Me.Button_PrintMales.Size = New System.Drawing.Size(110, 20)
        Me.Button_PrintMales.TabIndex = 5
        Me.Button_PrintMales.Text = "Print Males"
        '
        'TabPage_View
        '
        Me.TabPage_View.Controls.Add(Me.ComboBox1)
        Me.TabPage_View.Controls.Add(Me.Label14)
        Me.TabPage_View.Controls.Add(Me.C1FlexGrid1)
        Me.TabPage_View.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_View.Name = "TabPage_View"
        Me.TabPage_View.Size = New System.Drawing.Size(232, 242)
        Me.TabPage_View.Text = "View"
        '
        'ComboBox1
        '
        Me.ComboBox1.Items.Add("")
        Me.ComboBox1.Items.Add("Cruzamiento")
        Me.ComboBox1.Items.Add("Cosecha de Cruzamiento")
        Me.ComboBox1.Items.Add("Cosecha de Frutos")
        Me.ComboBox1.Location = New System.Drawing.Point(69, 7)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(142, 22)
        Me.ComboBox1.TabIndex = 46
        '
        'Label14
        '
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(8, 7)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(83, 20)
        Me.Label14.Text = "View type"
        '
        'C1FlexGrid1
        '
        Me.C1FlexGrid1.AllowEditing = False
        Me.C1FlexGrid1.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.C1FlexGrid1.AutoResize = True
        Me.C1FlexGrid1.AutoSearchDelay = 1
        Me.C1FlexGrid1.ClipSeparators = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.C1FlexGrid1.Col = 0
        Me.C1FlexGrid1.ColSel = 0
        Me.C1FlexGrid1.ComboList = Nothing
        Me.C1FlexGrid1.EditMask = Nothing
        Me.C1FlexGrid1.ExtendLastCol = False
        Me.C1FlexGrid1.LeftCol = 1
        Me.C1FlexGrid1.Location = New System.Drawing.Point(7, 30)
        Me.C1FlexGrid1.Name = "C1FlexGrid1"
        Me.C1FlexGrid1.Redraw = True
        Me.C1FlexGrid1.Row = -2
        Me.C1FlexGrid1.RowSel = -2
        Me.C1FlexGrid1.ScrollPosition = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid1.ScrollTrack = True
        Me.C1FlexGrid1.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.C1FlexGrid1.ShowCursor = False
        Me.C1FlexGrid1.ShowSort = True
        Me.C1FlexGrid1.Size = New System.Drawing.Size(226, 200)
        Me.C1FlexGrid1.StyleInfo = resources.GetString("C1FlexGrid1.StyleInfo")
        Me.C1FlexGrid1.TabIndex = 44
        Me.C1FlexGrid1.TopRow = 1
        '
        'TabPage_Crossing
        '
        Me.TabPage_Crossing.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.TabPage_Crossing.Controls.Add(Me.ComboBox_GenMale)
        Me.TabPage_Crossing.Controls.Add(Me.ComboBox_GenFemale)
        Me.TabPage_Crossing.Controls.Add(Me.TextBox_SearchValueCross)
        Me.TabPage_Crossing.Controls.Add(Me.Button_Search_ValueCross)
        Me.TabPage_Crossing.Controls.Add(Me.lblPor)
        Me.TabPage_Crossing.Controls.Add(Me.lblSearchCrossing)
        Me.TabPage_Crossing.Controls.Add(Me.lblMale)
        Me.TabPage_Crossing.Controls.Add(Me.lblFemale)
        Me.TabPage_Crossing.Controls.Add(Me.C1FlexGrid_ValuesCross)
        Me.TabPage_Crossing.Controls.Add(Me.TextBox_NroRepetitions)
        Me.TabPage_Crossing.Controls.Add(Me.Label_CountMalesCross)
        Me.TabPage_Crossing.Controls.Add(Me.Label_CountFemalesCross)
        Me.TabPage_Crossing.Controls.Add(Me.Label_CrossingMales)
        Me.TabPage_Crossing.Controls.Add(Me.Label_CrossingFemales)
        Me.TabPage_Crossing.Controls.Add(Me.Button_SearchMaleCross)
        Me.TabPage_Crossing.Controls.Add(Me.Button_SearchFemaleCross)
        Me.TabPage_Crossing.Controls.Add(Me.TextBox_SearchMaleCross)
        Me.TabPage_Crossing.Controls.Add(Me.TextBox_SearchFemaleCross)
        Me.TabPage_Crossing.Controls.Add(Me.Label_CrossingTitle)
        Me.TabPage_Crossing.Controls.Add(Me.TextBox_Data3)
        Me.TabPage_Crossing.Controls.Add(Me.TextBox_Data2)
        Me.TabPage_Crossing.Controls.Add(Me.Label_Order)
        Me.TabPage_Crossing.Controls.Add(Me.C1FlexGrid_MaleCross)
        Me.TabPage_Crossing.Controls.Add(Me.C1FlexGrid_FemaleCross)
        Me.TabPage_Crossing.Controls.Add(Me.DateTimePicker_Cross)
        Me.TabPage_Crossing.Controls.Add(Me.ComboBox_TypeCross)
        Me.TabPage_Crossing.Controls.Add(Me.Label_TypeCross)
        Me.TabPage_Crossing.Controls.Add(Me.ComboBox_Flowers)
        Me.TabPage_Crossing.Controls.Add(Me.TextBox_Order)
        Me.TabPage_Crossing.Controls.Add(Me.TextBox_Cross)
        Me.TabPage_Crossing.Controls.Add(Me.Button_PrintCross)
        Me.TabPage_Crossing.Controls.Add(Me.Label_CrossingDate)
        Me.TabPage_Crossing.Controls.Add(Me.Label_Flowers)
        Me.TabPage_Crossing.Controls.Add(Me.Label_Crossing)
        Me.TabPage_Crossing.Controls.Add(Me.Button_SelectCross)
        Me.TabPage_Crossing.Controls.Add(Me.Label6)
        Me.TabPage_Crossing.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_Crossing.Name = "TabPage_Crossing"
        Me.TabPage_Crossing.Size = New System.Drawing.Size(232, 242)
        Me.TabPage_Crossing.Text = "Crossing"
        '
        'ComboBox_GenMale
        '
        Me.ComboBox_GenMale.Items.Add("1")
        Me.ComboBox_GenMale.Items.Add("2")
        Me.ComboBox_GenMale.Items.Add("3")
        Me.ComboBox_GenMale.Items.Add("4")
        Me.ComboBox_GenMale.Items.Add("5")
        Me.ComboBox_GenMale.Items.Add("6")
        Me.ComboBox_GenMale.Items.Add("7")
        Me.ComboBox_GenMale.Items.Add("8")
        Me.ComboBox_GenMale.Items.Add("9")
        Me.ComboBox_GenMale.Items.Add("10")
        Me.ComboBox_GenMale.Items.Add("11")
        Me.ComboBox_GenMale.Items.Add("12")
        Me.ComboBox_GenMale.Items.Add("13")
        Me.ComboBox_GenMale.Items.Add("14")
        Me.ComboBox_GenMale.Items.Add("15")
        Me.ComboBox_GenMale.Items.Add("16")
        Me.ComboBox_GenMale.Items.Add("17")
        Me.ComboBox_GenMale.Items.Add("18")
        Me.ComboBox_GenMale.Items.Add("19")
        Me.ComboBox_GenMale.Items.Add("20")
        Me.ComboBox_GenMale.Items.Add("21")
        Me.ComboBox_GenMale.Items.Add("22")
        Me.ComboBox_GenMale.Items.Add("23")
        Me.ComboBox_GenMale.Items.Add("24")
        Me.ComboBox_GenMale.Items.Add("25")
        Me.ComboBox_GenMale.Items.Add("26")
        Me.ComboBox_GenMale.Items.Add("27")
        Me.ComboBox_GenMale.Items.Add("28")
        Me.ComboBox_GenMale.Items.Add("29")
        Me.ComboBox_GenMale.Items.Add("30")
        Me.ComboBox_GenMale.Items.Add("31")
        Me.ComboBox_GenMale.Items.Add("32")
        Me.ComboBox_GenMale.Items.Add("33")
        Me.ComboBox_GenMale.Items.Add("34")
        Me.ComboBox_GenMale.Items.Add("35")
        Me.ComboBox_GenMale.Items.Add("36")
        Me.ComboBox_GenMale.Items.Add("37")
        Me.ComboBox_GenMale.Items.Add("38")
        Me.ComboBox_GenMale.Items.Add("39")
        Me.ComboBox_GenMale.Items.Add("40")
        Me.ComboBox_GenMale.Items.Add("41")
        Me.ComboBox_GenMale.Items.Add("42")
        Me.ComboBox_GenMale.Items.Add("43")
        Me.ComboBox_GenMale.Items.Add("44")
        Me.ComboBox_GenMale.Items.Add("45")
        Me.ComboBox_GenMale.Items.Add("46")
        Me.ComboBox_GenMale.Items.Add("47")
        Me.ComboBox_GenMale.Items.Add("48")
        Me.ComboBox_GenMale.Items.Add("49")
        Me.ComboBox_GenMale.Items.Add("50")
        Me.ComboBox_GenMale.Items.Add("Bulk")
        Me.ComboBox_GenMale.Items.Add("AP")
        Me.ComboBox_GenMale.Items.Add("LP")
        Me.ComboBox_GenMale.Location = New System.Drawing.Point(199, 20)
        Me.ComboBox_GenMale.Name = "ComboBox_GenMale"
        Me.ComboBox_GenMale.Size = New System.Drawing.Size(38, 22)
        Me.ComboBox_GenMale.TabIndex = 93
        '
        'ComboBox_GenFemale
        '
        Me.ComboBox_GenFemale.Items.Add("1")
        Me.ComboBox_GenFemale.Items.Add("2")
        Me.ComboBox_GenFemale.Items.Add("3")
        Me.ComboBox_GenFemale.Items.Add("4")
        Me.ComboBox_GenFemale.Items.Add("5")
        Me.ComboBox_GenFemale.Items.Add("6")
        Me.ComboBox_GenFemale.Items.Add("7")
        Me.ComboBox_GenFemale.Items.Add("8")
        Me.ComboBox_GenFemale.Items.Add("9")
        Me.ComboBox_GenFemale.Items.Add("10")
        Me.ComboBox_GenFemale.Items.Add("11")
        Me.ComboBox_GenFemale.Items.Add("12")
        Me.ComboBox_GenFemale.Items.Add("13")
        Me.ComboBox_GenFemale.Items.Add("14")
        Me.ComboBox_GenFemale.Items.Add("15")
        Me.ComboBox_GenFemale.Items.Add("16")
        Me.ComboBox_GenFemale.Items.Add("17")
        Me.ComboBox_GenFemale.Items.Add("18")
        Me.ComboBox_GenFemale.Items.Add("19")
        Me.ComboBox_GenFemale.Items.Add("20")
        Me.ComboBox_GenFemale.Items.Add("21")
        Me.ComboBox_GenFemale.Items.Add("22")
        Me.ComboBox_GenFemale.Items.Add("23")
        Me.ComboBox_GenFemale.Items.Add("24")
        Me.ComboBox_GenFemale.Items.Add("25")
        Me.ComboBox_GenFemale.Items.Add("26")
        Me.ComboBox_GenFemale.Items.Add("27")
        Me.ComboBox_GenFemale.Items.Add("28")
        Me.ComboBox_GenFemale.Items.Add("29")
        Me.ComboBox_GenFemale.Items.Add("30")
        Me.ComboBox_GenFemale.Items.Add("31")
        Me.ComboBox_GenFemale.Items.Add("32")
        Me.ComboBox_GenFemale.Items.Add("33")
        Me.ComboBox_GenFemale.Items.Add("34")
        Me.ComboBox_GenFemale.Items.Add("35")
        Me.ComboBox_GenFemale.Items.Add("36")
        Me.ComboBox_GenFemale.Items.Add("37")
        Me.ComboBox_GenFemale.Items.Add("38")
        Me.ComboBox_GenFemale.Items.Add("39")
        Me.ComboBox_GenFemale.Items.Add("40")
        Me.ComboBox_GenFemale.Items.Add("41")
        Me.ComboBox_GenFemale.Items.Add("42")
        Me.ComboBox_GenFemale.Items.Add("43")
        Me.ComboBox_GenFemale.Items.Add("44")
        Me.ComboBox_GenFemale.Items.Add("45")
        Me.ComboBox_GenFemale.Items.Add("46")
        Me.ComboBox_GenFemale.Items.Add("47")
        Me.ComboBox_GenFemale.Items.Add("48")
        Me.ComboBox_GenFemale.Items.Add("49")
        Me.ComboBox_GenFemale.Items.Add("50")
        Me.ComboBox_GenFemale.Location = New System.Drawing.Point(129, 21)
        Me.ComboBox_GenFemale.Name = "ComboBox_GenFemale"
        Me.ComboBox_GenFemale.Size = New System.Drawing.Size(38, 22)
        Me.ComboBox_GenFemale.TabIndex = 92
        '
        'TextBox_SearchValueCross
        '
        Me.TextBox_SearchValueCross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_SearchValueCross.Location = New System.Drawing.Point(7, 23)
        Me.TextBox_SearchValueCross.Name = "TextBox_SearchValueCross"
        Me.TextBox_SearchValueCross.Size = New System.Drawing.Size(84, 19)
        Me.TextBox_SearchValueCross.TabIndex = 91
        '
        'Button_Search_ValueCross
        '
        Me.Button_Search_ValueCross.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_Search_ValueCross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_Search_ValueCross.ForeColor = System.Drawing.Color.White
        Me.Button_Search_ValueCross.Location = New System.Drawing.Point(95, 22)
        Me.Button_Search_ValueCross.Name = "Button_Search_ValueCross"
        Me.Button_Search_ValueCross.Size = New System.Drawing.Size(22, 20)
        Me.Button_Search_ValueCross.TabIndex = 90
        Me.Button_Search_ValueCross.Text = "S"
        '
        'lblPor
        '
        Me.lblPor.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblPor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblPor.Location = New System.Drawing.Point(171, 20)
        Me.lblPor.Name = "lblPor"
        Me.lblPor.Size = New System.Drawing.Size(10, 20)
        Me.lblPor.Text = "x"
        '
        'lblSearchCrossing
        '
        Me.lblSearchCrossing.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblSearchCrossing.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblSearchCrossing.Location = New System.Drawing.Point(7, 3)
        Me.lblSearchCrossing.Name = "lblSearchCrossing"
        Me.lblSearchCrossing.Size = New System.Drawing.Size(60, 17)
        Me.lblSearchCrossing.Text = "Value"
        '
        'lblMale
        '
        Me.lblMale.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblMale.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblMale.Location = New System.Drawing.Point(182, 21)
        Me.lblMale.Name = "lblMale"
        Me.lblMale.Size = New System.Drawing.Size(14, 17)
        Me.lblMale.Text = "M"
        '
        'lblFemale
        '
        Me.lblFemale.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblFemale.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblFemale.Location = New System.Drawing.Point(118, 21)
        Me.lblFemale.Name = "lblFemale"
        Me.lblFemale.Size = New System.Drawing.Size(10, 21)
        Me.lblFemale.Text = "F"
        '
        'C1FlexGrid_ValuesCross
        '
        Me.C1FlexGrid_ValuesCross.AllowEditing = False
        Me.C1FlexGrid_ValuesCross.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.C1FlexGrid_ValuesCross.AutoResize = True
        Me.C1FlexGrid_ValuesCross.AutoSearchDelay = 1
        Me.C1FlexGrid_ValuesCross.ClipSeparators = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.C1FlexGrid_ValuesCross.Col = 0
        Me.C1FlexGrid_ValuesCross.ColSel = 0
        Me.C1FlexGrid_ValuesCross.ComboList = Nothing
        Me.C1FlexGrid_ValuesCross.EditMask = Nothing
        Me.C1FlexGrid_ValuesCross.ExtendLastCol = False
        Me.C1FlexGrid_ValuesCross.LeftCol = 1
        Me.C1FlexGrid_ValuesCross.Location = New System.Drawing.Point(7, 46)
        Me.C1FlexGrid_ValuesCross.Name = "C1FlexGrid_ValuesCross"
        Me.C1FlexGrid_ValuesCross.Redraw = True
        Me.C1FlexGrid_ValuesCross.Row = -2
        Me.C1FlexGrid_ValuesCross.RowSel = -2
        Me.C1FlexGrid_ValuesCross.ScrollPosition = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid_ValuesCross.ScrollTrack = True
        Me.C1FlexGrid_ValuesCross.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
        Me.C1FlexGrid_ValuesCross.ShowCursor = False
        Me.C1FlexGrid_ValuesCross.ShowSort = True
        Me.C1FlexGrid_ValuesCross.Size = New System.Drawing.Size(226, 83)
        Me.C1FlexGrid_ValuesCross.StyleInfo = resources.GetString("C1FlexGrid_ValuesCross.StyleInfo")
        Me.C1FlexGrid_ValuesCross.TabIndex = 81
        Me.C1FlexGrid_ValuesCross.TopRow = 1
        '
        'TextBox_NroRepetitions
        '
        Me.TextBox_NroRepetitions.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_NroRepetitions.Location = New System.Drawing.Point(153, 202)
        Me.TextBox_NroRepetitions.Name = "TextBox_NroRepetitions"
        Me.TextBox_NroRepetitions.ReadOnly = True
        Me.TextBox_NroRepetitions.Size = New System.Drawing.Size(11, 19)
        Me.TextBox_NroRepetitions.TabIndex = 69
        Me.TextBox_NroRepetitions.Visible = False
        '
        'Label_CountMalesCross
        '
        Me.Label_CountMalesCross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_CountMalesCross.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CountMalesCross.Location = New System.Drawing.Point(189, 132)
        Me.Label_CountMalesCross.Name = "Label_CountMalesCross"
        Me.Label_CountMalesCross.Size = New System.Drawing.Size(44, 20)
        Me.Label_CountMalesCross.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label_CountFemalesCross
        '
        Me.Label_CountFemalesCross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_CountFemalesCross.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CountFemalesCross.Location = New System.Drawing.Point(7, 132)
        Me.Label_CountFemalesCross.Name = "Label_CountFemalesCross"
        Me.Label_CountFemalesCross.Size = New System.Drawing.Size(44, 20)
        '
        'Label_CrossingMales
        '
        Me.Label_CrossingMales.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_CrossingMales.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CrossingMales.Location = New System.Drawing.Point(173, 5)
        Me.Label_CrossingMales.Name = "Label_CrossingMales"
        Me.Label_CrossingMales.Size = New System.Drawing.Size(60, 17)
        Me.Label_CrossingMales.Text = "Males"
        Me.Label_CrossingMales.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label_CrossingMales.Visible = False
        '
        'Label_CrossingFemales
        '
        Me.Label_CrossingFemales.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_CrossingFemales.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CrossingFemales.Location = New System.Drawing.Point(7, 5)
        Me.Label_CrossingFemales.Name = "Label_CrossingFemales"
        Me.Label_CrossingFemales.Size = New System.Drawing.Size(60, 17)
        Me.Label_CrossingFemales.Text = "Females"
        Me.Label_CrossingFemales.Visible = False
        '
        'Button_SearchMaleCross
        '
        Me.Button_SearchMaleCross.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SearchMaleCross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SearchMaleCross.ForeColor = System.Drawing.Color.White
        Me.Button_SearchMaleCross.Location = New System.Drawing.Point(211, 22)
        Me.Button_SearchMaleCross.Name = "Button_SearchMaleCross"
        Me.Button_SearchMaleCross.Size = New System.Drawing.Size(22, 20)
        Me.Button_SearchMaleCross.TabIndex = 53
        Me.Button_SearchMaleCross.Text = "S"
        Me.Button_SearchMaleCross.Visible = False
        '
        'Button_SearchFemaleCross
        '
        Me.Button_SearchFemaleCross.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SearchFemaleCross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SearchFemaleCross.ForeColor = System.Drawing.Color.White
        Me.Button_SearchFemaleCross.Location = New System.Drawing.Point(95, 22)
        Me.Button_SearchFemaleCross.Name = "Button_SearchFemaleCross"
        Me.Button_SearchFemaleCross.Size = New System.Drawing.Size(22, 20)
        Me.Button_SearchFemaleCross.TabIndex = 52
        Me.Button_SearchFemaleCross.Text = "S"
        Me.Button_SearchFemaleCross.Visible = False
        '
        'TextBox_SearchMaleCross
        '
        Me.TextBox_SearchMaleCross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_SearchMaleCross.Location = New System.Drawing.Point(123, 22)
        Me.TextBox_SearchMaleCross.Name = "TextBox_SearchMaleCross"
        Me.TextBox_SearchMaleCross.Size = New System.Drawing.Size(84, 19)
        Me.TextBox_SearchMaleCross.TabIndex = 51
        Me.TextBox_SearchMaleCross.Visible = False
        '
        'TextBox_SearchFemaleCross
        '
        Me.TextBox_SearchFemaleCross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_SearchFemaleCross.Location = New System.Drawing.Point(7, 22)
        Me.TextBox_SearchFemaleCross.Name = "TextBox_SearchFemaleCross"
        Me.TextBox_SearchFemaleCross.Size = New System.Drawing.Size(84, 19)
        Me.TextBox_SearchFemaleCross.TabIndex = 50
        Me.TextBox_SearchFemaleCross.Visible = False
        '
        'Label_CrossingTitle
        '
        Me.Label_CrossingTitle.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_CrossingTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CrossingTitle.Location = New System.Drawing.Point(7, 1)
        Me.Label_CrossingTitle.Name = "Label_CrossingTitle"
        Me.Label_CrossingTitle.Size = New System.Drawing.Size(226, 20)
        Me.Label_CrossingTitle.Text = "Crossing"
        Me.Label_CrossingTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TextBox_Data3
        '
        Me.TextBox_Data3.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_Data3.Location = New System.Drawing.Point(222, 226)
        Me.TextBox_Data3.Name = "TextBox_Data3"
        Me.TextBox_Data3.ReadOnly = True
        Me.TextBox_Data3.Size = New System.Drawing.Size(11, 19)
        Me.TextBox_Data3.TabIndex = 42
        Me.TextBox_Data3.Visible = False
        '
        'TextBox_Data2
        '
        Me.TextBox_Data2.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_Data2.Location = New System.Drawing.Point(222, 201)
        Me.TextBox_Data2.Name = "TextBox_Data2"
        Me.TextBox_Data2.ReadOnly = True
        Me.TextBox_Data2.Size = New System.Drawing.Size(11, 19)
        Me.TextBox_Data2.TabIndex = 41
        Me.TextBox_Data2.Visible = False
        '
        'Label_Order
        '
        Me.Label_Order.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Order.Location = New System.Drawing.Point(7, 179)
        Me.Label_Order.Name = "Label_Order"
        Me.Label_Order.Size = New System.Drawing.Size(65, 20)
        Me.Label_Order.Text = "Order"
        '
        'C1FlexGrid_MaleCross
        '
        Me.C1FlexGrid_MaleCross.AllowEditing = False
        Me.C1FlexGrid_MaleCross.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.C1FlexGrid_MaleCross.AutoResize = True
        Me.C1FlexGrid_MaleCross.AutoSearchDelay = 1
        Me.C1FlexGrid_MaleCross.ClipSeparators = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.C1FlexGrid_MaleCross.Col = 0
        Me.C1FlexGrid_MaleCross.ColSel = 0
        Me.C1FlexGrid_MaleCross.ComboList = Nothing
        Me.C1FlexGrid_MaleCross.EditMask = Nothing
        Me.C1FlexGrid_MaleCross.ExtendLastCol = False
        Me.C1FlexGrid_MaleCross.LeftCol = 1
        Me.C1FlexGrid_MaleCross.Location = New System.Drawing.Point(123, 47)
        Me.C1FlexGrid_MaleCross.Name = "C1FlexGrid_MaleCross"
        Me.C1FlexGrid_MaleCross.Redraw = True
        Me.C1FlexGrid_MaleCross.Row = -2
        Me.C1FlexGrid_MaleCross.RowSel = -2
        Me.C1FlexGrid_MaleCross.ScrollPosition = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid_MaleCross.ScrollTrack = True
        Me.C1FlexGrid_MaleCross.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.C1FlexGrid_MaleCross.ShowCursor = False
        Me.C1FlexGrid_MaleCross.ShowSort = True
        Me.C1FlexGrid_MaleCross.Size = New System.Drawing.Size(110, 83)
        Me.C1FlexGrid_MaleCross.StyleInfo = resources.GetString("C1FlexGrid_MaleCross.StyleInfo")
        Me.C1FlexGrid_MaleCross.TabIndex = 34
        Me.C1FlexGrid_MaleCross.TopRow = 1
        Me.C1FlexGrid_MaleCross.Visible = False
        '
        'C1FlexGrid_FemaleCross
        '
        Me.C1FlexGrid_FemaleCross.AllowEditing = False
        Me.C1FlexGrid_FemaleCross.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.C1FlexGrid_FemaleCross.AutoResize = True
        Me.C1FlexGrid_FemaleCross.AutoSearchDelay = 1
        Me.C1FlexGrid_FemaleCross.ClipSeparators = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.C1FlexGrid_FemaleCross.Col = 0
        Me.C1FlexGrid_FemaleCross.ColSel = 0
        Me.C1FlexGrid_FemaleCross.ComboList = Nothing
        Me.C1FlexGrid_FemaleCross.EditMask = Nothing
        Me.C1FlexGrid_FemaleCross.ExtendLastCol = False
        Me.C1FlexGrid_FemaleCross.LeftCol = 1
        Me.C1FlexGrid_FemaleCross.Location = New System.Drawing.Point(7, 47)
        Me.C1FlexGrid_FemaleCross.Name = "C1FlexGrid_FemaleCross"
        Me.C1FlexGrid_FemaleCross.Redraw = True
        Me.C1FlexGrid_FemaleCross.Row = -2
        Me.C1FlexGrid_FemaleCross.RowSel = -2
        Me.C1FlexGrid_FemaleCross.ScrollPosition = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid_FemaleCross.ScrollTrack = True
        Me.C1FlexGrid_FemaleCross.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.C1FlexGrid_FemaleCross.ShowCursor = False
        Me.C1FlexGrid_FemaleCross.ShowSort = True
        Me.C1FlexGrid_FemaleCross.Size = New System.Drawing.Size(110, 83)
        Me.C1FlexGrid_FemaleCross.StyleInfo = resources.GetString("C1FlexGrid_FemaleCross.StyleInfo")
        Me.C1FlexGrid_FemaleCross.TabIndex = 33
        Me.C1FlexGrid_FemaleCross.TopRow = 1
        Me.C1FlexGrid_FemaleCross.Visible = False
        '
        'DateTimePicker_Cross
        '
        Me.DateTimePicker_Cross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.DateTimePicker_Cross.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_Cross.Location = New System.Drawing.Point(76, 225)
        Me.DateTimePicker_Cross.Name = "DateTimePicker_Cross"
        Me.DateTimePicker_Cross.Size = New System.Drawing.Size(60, 20)
        Me.DateTimePicker_Cross.TabIndex = 26
        '
        'ComboBox_TypeCross
        '
        Me.ComboBox_TypeCross.Location = New System.Drawing.Point(76, 202)
        Me.ComboBox_TypeCross.Name = "ComboBox_TypeCross"
        Me.ComboBox_TypeCross.Size = New System.Drawing.Size(71, 22)
        Me.ComboBox_TypeCross.TabIndex = 25
        '
        'Label_TypeCross
        '
        Me.Label_TypeCross.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_TypeCross.Location = New System.Drawing.Point(7, 202)
        Me.Label_TypeCross.Name = "Label_TypeCross"
        Me.Label_TypeCross.Size = New System.Drawing.Size(72, 19)
        Me.Label_TypeCross.Text = "Type cross"
        '
        'ComboBox_Flowers
        '
        Me.ComboBox_Flowers.Items.Add("1")
        Me.ComboBox_Flowers.Items.Add("2")
        Me.ComboBox_Flowers.Items.Add("3")
        Me.ComboBox_Flowers.Items.Add("4")
        Me.ComboBox_Flowers.Items.Add("5")
        Me.ComboBox_Flowers.Items.Add("6")
        Me.ComboBox_Flowers.Items.Add("7")
        Me.ComboBox_Flowers.Items.Add("8")
        Me.ComboBox_Flowers.Items.Add("9")
        Me.ComboBox_Flowers.Items.Add("10")
        Me.ComboBox_Flowers.Items.Add("11")
        Me.ComboBox_Flowers.Items.Add("12")
        Me.ComboBox_Flowers.Items.Add("13")
        Me.ComboBox_Flowers.Items.Add("14")
        Me.ComboBox_Flowers.Items.Add("15")
        Me.ComboBox_Flowers.Items.Add("16")
        Me.ComboBox_Flowers.Items.Add("17")
        Me.ComboBox_Flowers.Items.Add("18")
        Me.ComboBox_Flowers.Items.Add("19")
        Me.ComboBox_Flowers.Items.Add("20")
        Me.ComboBox_Flowers.Items.Add("21")
        Me.ComboBox_Flowers.Items.Add("22")
        Me.ComboBox_Flowers.Items.Add("23")
        Me.ComboBox_Flowers.Items.Add("24")
        Me.ComboBox_Flowers.Items.Add("25")
        Me.ComboBox_Flowers.Items.Add("26")
        Me.ComboBox_Flowers.Items.Add("27")
        Me.ComboBox_Flowers.Items.Add("28")
        Me.ComboBox_Flowers.Items.Add("29")
        Me.ComboBox_Flowers.Items.Add("30")
        Me.ComboBox_Flowers.Items.Add("31")
        Me.ComboBox_Flowers.Items.Add("32")
        Me.ComboBox_Flowers.Items.Add("33")
        Me.ComboBox_Flowers.Items.Add("34")
        Me.ComboBox_Flowers.Items.Add("35")
        Me.ComboBox_Flowers.Items.Add("36")
        Me.ComboBox_Flowers.Items.Add("37")
        Me.ComboBox_Flowers.Items.Add("38")
        Me.ComboBox_Flowers.Items.Add("39")
        Me.ComboBox_Flowers.Items.Add("40")
        Me.ComboBox_Flowers.Items.Add("41")
        Me.ComboBox_Flowers.Items.Add("42")
        Me.ComboBox_Flowers.Items.Add("43")
        Me.ComboBox_Flowers.Items.Add("44")
        Me.ComboBox_Flowers.Items.Add("45")
        Me.ComboBox_Flowers.Items.Add("46")
        Me.ComboBox_Flowers.Items.Add("47")
        Me.ComboBox_Flowers.Items.Add("48")
        Me.ComboBox_Flowers.Items.Add("49")
        Me.ComboBox_Flowers.Items.Add("50")
        Me.ComboBox_Flowers.Location = New System.Drawing.Point(195, 179)
        Me.ComboBox_Flowers.Name = "ComboBox_Flowers"
        Me.ComboBox_Flowers.Size = New System.Drawing.Size(38, 22)
        Me.ComboBox_Flowers.TabIndex = 17
        '
        'TextBox_Order
        '
        Me.TextBox_Order.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_Order.Location = New System.Drawing.Point(76, 179)
        Me.TextBox_Order.Name = "TextBox_Order"
        Me.TextBox_Order.ReadOnly = True
        Me.TextBox_Order.Size = New System.Drawing.Size(71, 19)
        Me.TextBox_Order.TabIndex = 14
        '
        'TextBox_Cross
        '
        Me.TextBox_Cross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_Cross.Location = New System.Drawing.Point(76, 156)
        Me.TextBox_Cross.Name = "TextBox_Cross"
        Me.TextBox_Cross.ReadOnly = True
        Me.TextBox_Cross.Size = New System.Drawing.Size(157, 19)
        Me.TextBox_Cross.TabIndex = 13
        '
        'Button_PrintCross
        '
        Me.Button_PrintCross.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_PrintCross.ForeColor = System.Drawing.Color.White
        Me.Button_PrintCross.Location = New System.Drawing.Point(153, 224)
        Me.Button_PrintCross.Name = "Button_PrintCross"
        Me.Button_PrintCross.Size = New System.Drawing.Size(80, 21)
        Me.Button_PrintCross.TabIndex = 12
        Me.Button_PrintCross.Text = "Print"
        '
        'Label_CrossingDate
        '
        Me.Label_CrossingDate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CrossingDate.Location = New System.Drawing.Point(7, 225)
        Me.Label_CrossingDate.Name = "Label_CrossingDate"
        Me.Label_CrossingDate.Size = New System.Drawing.Size(63, 20)
        Me.Label_CrossingDate.Text = "Date"
        '
        'Label_Flowers
        '
        Me.Label_Flowers.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Flowers.Location = New System.Drawing.Point(149, 179)
        Me.Label_Flowers.Name = "Label_Flowers"
        Me.Label_Flowers.Size = New System.Drawing.Size(50, 20)
        Me.Label_Flowers.Text = "Flowers"
        '
        'Label_Crossing
        '
        Me.Label_Crossing.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Crossing.Location = New System.Drawing.Point(7, 156)
        Me.Label_Crossing.Name = "Label_Crossing"
        Me.Label_Crossing.Size = New System.Drawing.Size(65, 20)
        Me.Label_Crossing.Text = "Crossing"
        '
        'Button_SelectCross
        '
        Me.Button_SelectCross.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SelectCross.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SelectCross.ForeColor = System.Drawing.Color.White
        Me.Button_SelectCross.Location = New System.Drawing.Point(76, 132)
        Me.Button_SelectCross.Name = "Button_SelectCross"
        Me.Button_SelectCross.Size = New System.Drawing.Size(86, 20)
        Me.Button_SelectCross.TabIndex = 6
        Me.Button_SelectCross.Text = "Select Cross"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(116, 62)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(10, 20)
        Me.Label6.Text = "x"
        '
        'TabPage_Harvest
        '
        Me.TabPage_Harvest.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.TabPage_Harvest.Controls.Add(Me.txtHarvest_Potato)
        Me.TabPage_Harvest.Controls.Add(Me.ComboBox_TotalFlowers)
        Me.TabPage_Harvest.Controls.Add(Me.lblNroFloresG)
        Me.TabPage_Harvest.Controls.Add(Me.TextBox_HideNroRepetitions)
        Me.TabPage_Harvest.Controls.Add(Me.TextBox_IndexHarvest)
        Me.TabPage_Harvest.Controls.Add(Me.Label_CountHarvest)
        Me.TabPage_Harvest.Controls.Add(Me.Button_LoadHarvest)
        Me.TabPage_Harvest.Controls.Add(Me.TextBox_Harvest)
        Me.TabPage_Harvest.Controls.Add(Me.Button_PrintHarvest)
        Me.TabPage_Harvest.Controls.Add(Me.Label_HarvestDate)
        Me.TabPage_Harvest.Controls.Add(Me.Label_FruitSize)
        Me.TabPage_Harvest.Controls.Add(Me.DateTimePicker_Harvest)
        Me.TabPage_Harvest.Controls.Add(Me.ComboBox_FruitSize)
        Me.TabPage_Harvest.Controls.Add(Me.ComboBox_NroFruits)
        Me.TabPage_Harvest.Controls.Add(Me.Button_SelectHarvest)
        Me.TabPage_Harvest.Controls.Add(Me.Button_SearchHarvest)
        Me.TabPage_Harvest.Controls.Add(Me.TextBox_SearchHarvest)
        Me.TabPage_Harvest.Controls.Add(Me.C1FlexGrid_Harvest)
        Me.TabPage_Harvest.Controls.Add(Me.Label_HarvestTitle)
        Me.TabPage_Harvest.Controls.Add(Me.Label_Fruits)
        Me.TabPage_Harvest.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_Harvest.Name = "TabPage_Harvest"
        Me.TabPage_Harvest.Size = New System.Drawing.Size(240, 245)
        Me.TabPage_Harvest.Text = "Harvest"
        '
        'txtHarvest_Potato
        '
        Me.txtHarvest_Potato.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.txtHarvest_Potato.Location = New System.Drawing.Point(8, 149)
        Me.txtHarvest_Potato.Name = "txtHarvest_Potato"
        Me.txtHarvest_Potato.ReadOnly = True
        Me.txtHarvest_Potato.Size = New System.Drawing.Size(149, 19)
        Me.txtHarvest_Potato.TabIndex = 96
        Me.txtHarvest_Potato.Visible = False
        '
        'ComboBox_TotalFlowers
        '
        Me.ComboBox_TotalFlowers.Items.Add("1")
        Me.ComboBox_TotalFlowers.Items.Add("2")
        Me.ComboBox_TotalFlowers.Items.Add("3")
        Me.ComboBox_TotalFlowers.Items.Add("4")
        Me.ComboBox_TotalFlowers.Items.Add("5")
        Me.ComboBox_TotalFlowers.Items.Add("6")
        Me.ComboBox_TotalFlowers.Items.Add("7")
        Me.ComboBox_TotalFlowers.Items.Add("8")
        Me.ComboBox_TotalFlowers.Items.Add("9")
        Me.ComboBox_TotalFlowers.Items.Add("10")
        Me.ComboBox_TotalFlowers.Items.Add("11")
        Me.ComboBox_TotalFlowers.Items.Add("12")
        Me.ComboBox_TotalFlowers.Items.Add("13")
        Me.ComboBox_TotalFlowers.Items.Add("14")
        Me.ComboBox_TotalFlowers.Items.Add("15")
        Me.ComboBox_TotalFlowers.Items.Add("16")
        Me.ComboBox_TotalFlowers.Items.Add("17")
        Me.ComboBox_TotalFlowers.Items.Add("18")
        Me.ComboBox_TotalFlowers.Items.Add("19")
        Me.ComboBox_TotalFlowers.Items.Add("20")
        Me.ComboBox_TotalFlowers.Items.Add("21")
        Me.ComboBox_TotalFlowers.Items.Add("22")
        Me.ComboBox_TotalFlowers.Items.Add("23")
        Me.ComboBox_TotalFlowers.Items.Add("24")
        Me.ComboBox_TotalFlowers.Items.Add("25")
        Me.ComboBox_TotalFlowers.Items.Add("26")
        Me.ComboBox_TotalFlowers.Items.Add("27")
        Me.ComboBox_TotalFlowers.Items.Add("28")
        Me.ComboBox_TotalFlowers.Items.Add("29")
        Me.ComboBox_TotalFlowers.Items.Add("30")
        Me.ComboBox_TotalFlowers.Items.Add("31")
        Me.ComboBox_TotalFlowers.Items.Add("32")
        Me.ComboBox_TotalFlowers.Items.Add("33")
        Me.ComboBox_TotalFlowers.Items.Add("34")
        Me.ComboBox_TotalFlowers.Items.Add("35")
        Me.ComboBox_TotalFlowers.Items.Add("36")
        Me.ComboBox_TotalFlowers.Items.Add("37")
        Me.ComboBox_TotalFlowers.Items.Add("38")
        Me.ComboBox_TotalFlowers.Items.Add("39")
        Me.ComboBox_TotalFlowers.Items.Add("40")
        Me.ComboBox_TotalFlowers.Items.Add("41")
        Me.ComboBox_TotalFlowers.Items.Add("42")
        Me.ComboBox_TotalFlowers.Items.Add("43")
        Me.ComboBox_TotalFlowers.Items.Add("44")
        Me.ComboBox_TotalFlowers.Items.Add("45")
        Me.ComboBox_TotalFlowers.Items.Add("46")
        Me.ComboBox_TotalFlowers.Items.Add("47")
        Me.ComboBox_TotalFlowers.Items.Add("48")
        Me.ComboBox_TotalFlowers.Items.Add("49")
        Me.ComboBox_TotalFlowers.Items.Add("50")
        Me.ComboBox_TotalFlowers.Items.Add("51")
        Me.ComboBox_TotalFlowers.Items.Add("52")
        Me.ComboBox_TotalFlowers.Items.Add("53")
        Me.ComboBox_TotalFlowers.Items.Add("54")
        Me.ComboBox_TotalFlowers.Items.Add("55")
        Me.ComboBox_TotalFlowers.Items.Add("56")
        Me.ComboBox_TotalFlowers.Items.Add("57")
        Me.ComboBox_TotalFlowers.Items.Add("58")
        Me.ComboBox_TotalFlowers.Items.Add("59")
        Me.ComboBox_TotalFlowers.Items.Add("60")
        Me.ComboBox_TotalFlowers.Items.Add("61")
        Me.ComboBox_TotalFlowers.Items.Add("62")
        Me.ComboBox_TotalFlowers.Items.Add("63")
        Me.ComboBox_TotalFlowers.Items.Add("64")
        Me.ComboBox_TotalFlowers.Items.Add("65")
        Me.ComboBox_TotalFlowers.Items.Add("66")
        Me.ComboBox_TotalFlowers.Items.Add("67")
        Me.ComboBox_TotalFlowers.Items.Add("68")
        Me.ComboBox_TotalFlowers.Items.Add("69")
        Me.ComboBox_TotalFlowers.Items.Add("70")
        Me.ComboBox_TotalFlowers.Items.Add("71")
        Me.ComboBox_TotalFlowers.Items.Add("72")
        Me.ComboBox_TotalFlowers.Items.Add("73")
        Me.ComboBox_TotalFlowers.Items.Add("74")
        Me.ComboBox_TotalFlowers.Items.Add("75")
        Me.ComboBox_TotalFlowers.Items.Add("76")
        Me.ComboBox_TotalFlowers.Items.Add("77")
        Me.ComboBox_TotalFlowers.Items.Add("78")
        Me.ComboBox_TotalFlowers.Items.Add("79")
        Me.ComboBox_TotalFlowers.Items.Add("80")
        Me.ComboBox_TotalFlowers.Items.Add("81")
        Me.ComboBox_TotalFlowers.Items.Add("82")
        Me.ComboBox_TotalFlowers.Items.Add("83")
        Me.ComboBox_TotalFlowers.Items.Add("84")
        Me.ComboBox_TotalFlowers.Items.Add("85")
        Me.ComboBox_TotalFlowers.Items.Add("86")
        Me.ComboBox_TotalFlowers.Items.Add("87")
        Me.ComboBox_TotalFlowers.Items.Add("88")
        Me.ComboBox_TotalFlowers.Items.Add("89")
        Me.ComboBox_TotalFlowers.Items.Add("90")
        Me.ComboBox_TotalFlowers.Items.Add("91")
        Me.ComboBox_TotalFlowers.Items.Add("92")
        Me.ComboBox_TotalFlowers.Items.Add("93")
        Me.ComboBox_TotalFlowers.Items.Add("94")
        Me.ComboBox_TotalFlowers.Items.Add("95")
        Me.ComboBox_TotalFlowers.Items.Add("96")
        Me.ComboBox_TotalFlowers.Items.Add("97")
        Me.ComboBox_TotalFlowers.Items.Add("98")
        Me.ComboBox_TotalFlowers.Items.Add("99")
        Me.ComboBox_TotalFlowers.Items.Add("100")
        Me.ComboBox_TotalFlowers.Items.Add("101")
        Me.ComboBox_TotalFlowers.Items.Add("102")
        Me.ComboBox_TotalFlowers.Items.Add("103")
        Me.ComboBox_TotalFlowers.Items.Add("104")
        Me.ComboBox_TotalFlowers.Items.Add("105")
        Me.ComboBox_TotalFlowers.Items.Add("106")
        Me.ComboBox_TotalFlowers.Items.Add("107")
        Me.ComboBox_TotalFlowers.Items.Add("108")
        Me.ComboBox_TotalFlowers.Items.Add("109")
        Me.ComboBox_TotalFlowers.Items.Add("110")
        Me.ComboBox_TotalFlowers.Items.Add("111")
        Me.ComboBox_TotalFlowers.Items.Add("112")
        Me.ComboBox_TotalFlowers.Items.Add("113")
        Me.ComboBox_TotalFlowers.Items.Add("114")
        Me.ComboBox_TotalFlowers.Items.Add("115")
        Me.ComboBox_TotalFlowers.Items.Add("116")
        Me.ComboBox_TotalFlowers.Items.Add("117")
        Me.ComboBox_TotalFlowers.Items.Add("118")
        Me.ComboBox_TotalFlowers.Items.Add("119")
        Me.ComboBox_TotalFlowers.Items.Add("120")
        Me.ComboBox_TotalFlowers.Items.Add("121")
        Me.ComboBox_TotalFlowers.Items.Add("122")
        Me.ComboBox_TotalFlowers.Items.Add("123")
        Me.ComboBox_TotalFlowers.Items.Add("124")
        Me.ComboBox_TotalFlowers.Items.Add("125")
        Me.ComboBox_TotalFlowers.Items.Add("126")
        Me.ComboBox_TotalFlowers.Items.Add("127")
        Me.ComboBox_TotalFlowers.Items.Add("128")
        Me.ComboBox_TotalFlowers.Items.Add("129")
        Me.ComboBox_TotalFlowers.Items.Add("130")
        Me.ComboBox_TotalFlowers.Items.Add("131")
        Me.ComboBox_TotalFlowers.Items.Add("132")
        Me.ComboBox_TotalFlowers.Items.Add("133")
        Me.ComboBox_TotalFlowers.Items.Add("134")
        Me.ComboBox_TotalFlowers.Items.Add("135")
        Me.ComboBox_TotalFlowers.Items.Add("136")
        Me.ComboBox_TotalFlowers.Items.Add("137")
        Me.ComboBox_TotalFlowers.Items.Add("138")
        Me.ComboBox_TotalFlowers.Items.Add("139")
        Me.ComboBox_TotalFlowers.Items.Add("140")
        Me.ComboBox_TotalFlowers.Items.Add("141")
        Me.ComboBox_TotalFlowers.Items.Add("142")
        Me.ComboBox_TotalFlowers.Items.Add("143")
        Me.ComboBox_TotalFlowers.Items.Add("144")
        Me.ComboBox_TotalFlowers.Items.Add("145")
        Me.ComboBox_TotalFlowers.Items.Add("146")
        Me.ComboBox_TotalFlowers.Items.Add("147")
        Me.ComboBox_TotalFlowers.Items.Add("148")
        Me.ComboBox_TotalFlowers.Items.Add("149")
        Me.ComboBox_TotalFlowers.Items.Add("150")
        Me.ComboBox_TotalFlowers.Location = New System.Drawing.Point(75, 174)
        Me.ComboBox_TotalFlowers.Name = "ComboBox_TotalFlowers"
        Me.ComboBox_TotalFlowers.Size = New System.Drawing.Size(36, 22)
        Me.ComboBox_TotalFlowers.TabIndex = 73
        '
        'lblNroFloresG
        '
        Me.lblNroFloresG.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblNroFloresG.Location = New System.Drawing.Point(9, 176)
        Me.lblNroFloresG.Name = "lblNroFloresG"
        Me.lblNroFloresG.Size = New System.Drawing.Size(75, 20)
        Me.lblNroFloresG.Text = "T. Flowers"
        '
        'TextBox_HideNroRepetitions
        '
        Me.TextBox_HideNroRepetitions.Location = New System.Drawing.Point(156, 223)
        Me.TextBox_HideNroRepetitions.Name = "TextBox_HideNroRepetitions"
        Me.TextBox_HideNroRepetitions.Size = New System.Drawing.Size(10, 21)
        Me.TextBox_HideNroRepetitions.TabIndex = 52
        Me.TextBox_HideNroRepetitions.Visible = False
        '
        'TextBox_IndexHarvest
        '
        Me.TextBox_IndexHarvest.Location = New System.Drawing.Point(227, 222)
        Me.TextBox_IndexHarvest.Name = "TextBox_IndexHarvest"
        Me.TextBox_IndexHarvest.Size = New System.Drawing.Size(10, 21)
        Me.TextBox_IndexHarvest.TabIndex = 46
        Me.TextBox_IndexHarvest.Visible = False
        '
        'Label_CountHarvest
        '
        Me.Label_CountHarvest.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_CountHarvest.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CountHarvest.Location = New System.Drawing.Point(219, 152)
        Me.Label_CountHarvest.Name = "Label_CountHarvest"
        Me.Label_CountHarvest.Size = New System.Drawing.Size(18, 17)
        '
        'Button_LoadHarvest
        '
        Me.Button_LoadHarvest.BackColor = System.Drawing.Color.White
        Me.Button_LoadHarvest.Font = New System.Drawing.Font("Tahoma", 6.0!, System.Drawing.FontStyle.Bold)
        Me.Button_LoadHarvest.ForeColor = System.Drawing.Color.Red
        Me.Button_LoadHarvest.Location = New System.Drawing.Point(211, 22)
        Me.Button_LoadHarvest.Name = "Button_LoadHarvest"
        Me.Button_LoadHarvest.Size = New System.Drawing.Size(22, 20)
        Me.Button_LoadHarvest.TabIndex = 40
        Me.Button_LoadHarvest.Text = "X"
        '
        'TextBox_Harvest
        '
        Me.TextBox_Harvest.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_Harvest.Location = New System.Drawing.Point(7, 149)
        Me.TextBox_Harvest.Name = "TextBox_Harvest"
        Me.TextBox_Harvest.ReadOnly = True
        Me.TextBox_Harvest.Size = New System.Drawing.Size(149, 19)
        Me.TextBox_Harvest.TabIndex = 35
        '
        'Button_PrintHarvest
        '
        Me.Button_PrintHarvest.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_PrintHarvest.ForeColor = System.Drawing.Color.White
        Me.Button_PrintHarvest.Location = New System.Drawing.Point(156, 220)
        Me.Button_PrintHarvest.Name = "Button_PrintHarvest"
        Me.Button_PrintHarvest.Size = New System.Drawing.Size(75, 22)
        Me.Button_PrintHarvest.TabIndex = 34
        Me.Button_PrintHarvest.Text = "Print"
        '
        'Label_HarvestDate
        '
        Me.Label_HarvestDate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_HarvestDate.Location = New System.Drawing.Point(132, 171)
        Me.Label_HarvestDate.Name = "Label_HarvestDate"
        Me.Label_HarvestDate.Size = New System.Drawing.Size(39, 20)
        Me.Label_HarvestDate.Text = "Date"
        '
        'Label_FruitSize
        '
        Me.Label_FruitSize.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_FruitSize.Location = New System.Drawing.Point(9, 199)
        Me.Label_FruitSize.Name = "Label_FruitSize"
        Me.Label_FruitSize.Size = New System.Drawing.Size(66, 20)
        Me.Label_FruitSize.Text = "Fruit size"
        Me.Label_FruitSize.Visible = False
        '
        'DateTimePicker_Harvest
        '
        Me.DateTimePicker_Harvest.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.DateTimePicker_Harvest.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_Harvest.Location = New System.Drawing.Point(173, 171)
        Me.DateTimePicker_Harvest.Name = "DateTimePicker_Harvest"
        Me.DateTimePicker_Harvest.Size = New System.Drawing.Size(60, 20)
        Me.DateTimePicker_Harvest.TabIndex = 27
        '
        'ComboBox_FruitSize
        '
        Me.ComboBox_FruitSize.Items.Add("1")
        Me.ComboBox_FruitSize.Items.Add("2")
        Me.ComboBox_FruitSize.Items.Add("3")
        Me.ComboBox_FruitSize.Items.Add("4")
        Me.ComboBox_FruitSize.Items.Add("5")
        Me.ComboBox_FruitSize.Items.Add("6")
        Me.ComboBox_FruitSize.Items.Add("7")
        Me.ComboBox_FruitSize.Items.Add("8")
        Me.ComboBox_FruitSize.Items.Add("9")
        Me.ComboBox_FruitSize.Items.Add("10")
        Me.ComboBox_FruitSize.Items.Add("11")
        Me.ComboBox_FruitSize.Items.Add("12")
        Me.ComboBox_FruitSize.Items.Add("13")
        Me.ComboBox_FruitSize.Items.Add("14")
        Me.ComboBox_FruitSize.Items.Add("15")
        Me.ComboBox_FruitSize.Items.Add("16")
        Me.ComboBox_FruitSize.Items.Add("17")
        Me.ComboBox_FruitSize.Items.Add("18")
        Me.ComboBox_FruitSize.Items.Add("19")
        Me.ComboBox_FruitSize.Items.Add("20")
        Me.ComboBox_FruitSize.Items.Add("21")
        Me.ComboBox_FruitSize.Items.Add("22")
        Me.ComboBox_FruitSize.Items.Add("23")
        Me.ComboBox_FruitSize.Items.Add("24")
        Me.ComboBox_FruitSize.Items.Add("25")
        Me.ComboBox_FruitSize.Items.Add("26")
        Me.ComboBox_FruitSize.Items.Add("27")
        Me.ComboBox_FruitSize.Items.Add("28")
        Me.ComboBox_FruitSize.Items.Add("29")
        Me.ComboBox_FruitSize.Items.Add("30")
        Me.ComboBox_FruitSize.Items.Add("31")
        Me.ComboBox_FruitSize.Items.Add("32")
        Me.ComboBox_FruitSize.Items.Add("33")
        Me.ComboBox_FruitSize.Items.Add("34")
        Me.ComboBox_FruitSize.Items.Add("35")
        Me.ComboBox_FruitSize.Items.Add("36")
        Me.ComboBox_FruitSize.Items.Add("37")
        Me.ComboBox_FruitSize.Items.Add("38")
        Me.ComboBox_FruitSize.Items.Add("39")
        Me.ComboBox_FruitSize.Items.Add("40")
        Me.ComboBox_FruitSize.Items.Add("41")
        Me.ComboBox_FruitSize.Items.Add("42")
        Me.ComboBox_FruitSize.Items.Add("43")
        Me.ComboBox_FruitSize.Items.Add("44")
        Me.ComboBox_FruitSize.Items.Add("45")
        Me.ComboBox_FruitSize.Items.Add("46")
        Me.ComboBox_FruitSize.Items.Add("47")
        Me.ComboBox_FruitSize.Items.Add("48")
        Me.ComboBox_FruitSize.Items.Add("49")
        Me.ComboBox_FruitSize.Items.Add("50")
        Me.ComboBox_FruitSize.Items.Add("51")
        Me.ComboBox_FruitSize.Items.Add("52")
        Me.ComboBox_FruitSize.Items.Add("53")
        Me.ComboBox_FruitSize.Items.Add("54")
        Me.ComboBox_FruitSize.Items.Add("55")
        Me.ComboBox_FruitSize.Items.Add("56")
        Me.ComboBox_FruitSize.Items.Add("57")
        Me.ComboBox_FruitSize.Items.Add("58")
        Me.ComboBox_FruitSize.Items.Add("59")
        Me.ComboBox_FruitSize.Items.Add("60")
        Me.ComboBox_FruitSize.Items.Add("61")
        Me.ComboBox_FruitSize.Items.Add("62")
        Me.ComboBox_FruitSize.Items.Add("63")
        Me.ComboBox_FruitSize.Items.Add("64")
        Me.ComboBox_FruitSize.Items.Add("65")
        Me.ComboBox_FruitSize.Items.Add("66")
        Me.ComboBox_FruitSize.Items.Add("67")
        Me.ComboBox_FruitSize.Items.Add("68")
        Me.ComboBox_FruitSize.Items.Add("69")
        Me.ComboBox_FruitSize.Items.Add("70")
        Me.ComboBox_FruitSize.Items.Add("71")
        Me.ComboBox_FruitSize.Items.Add("72")
        Me.ComboBox_FruitSize.Items.Add("73")
        Me.ComboBox_FruitSize.Items.Add("74")
        Me.ComboBox_FruitSize.Items.Add("75")
        Me.ComboBox_FruitSize.Items.Add("76")
        Me.ComboBox_FruitSize.Items.Add("77")
        Me.ComboBox_FruitSize.Items.Add("78")
        Me.ComboBox_FruitSize.Items.Add("79")
        Me.ComboBox_FruitSize.Items.Add("80")
        Me.ComboBox_FruitSize.Location = New System.Drawing.Point(75, 199)
        Me.ComboBox_FruitSize.Name = "ComboBox_FruitSize"
        Me.ComboBox_FruitSize.Size = New System.Drawing.Size(79, 22)
        Me.ComboBox_FruitSize.TabIndex = 10
        '
        'ComboBox_NroFruits
        '
        Me.ComboBox_NroFruits.Items.Add("0")
        Me.ComboBox_NroFruits.Items.Add("1")
        Me.ComboBox_NroFruits.Items.Add("2")
        Me.ComboBox_NroFruits.Items.Add("3")
        Me.ComboBox_NroFruits.Items.Add("4")
        Me.ComboBox_NroFruits.Items.Add("5")
        Me.ComboBox_NroFruits.Items.Add("6")
        Me.ComboBox_NroFruits.Items.Add("7")
        Me.ComboBox_NroFruits.Items.Add("8")
        Me.ComboBox_NroFruits.Items.Add("9")
        Me.ComboBox_NroFruits.Items.Add("10")
        Me.ComboBox_NroFruits.Items.Add("11")
        Me.ComboBox_NroFruits.Items.Add("12")
        Me.ComboBox_NroFruits.Items.Add("13")
        Me.ComboBox_NroFruits.Items.Add("14")
        Me.ComboBox_NroFruits.Items.Add("15")
        Me.ComboBox_NroFruits.Items.Add("16")
        Me.ComboBox_NroFruits.Items.Add("17")
        Me.ComboBox_NroFruits.Items.Add("18")
        Me.ComboBox_NroFruits.Items.Add("19")
        Me.ComboBox_NroFruits.Items.Add("20")
        Me.ComboBox_NroFruits.Items.Add("21")
        Me.ComboBox_NroFruits.Items.Add("22")
        Me.ComboBox_NroFruits.Items.Add("23")
        Me.ComboBox_NroFruits.Items.Add("24")
        Me.ComboBox_NroFruits.Items.Add("25")
        Me.ComboBox_NroFruits.Items.Add("26")
        Me.ComboBox_NroFruits.Items.Add("27")
        Me.ComboBox_NroFruits.Items.Add("28")
        Me.ComboBox_NroFruits.Items.Add("29")
        Me.ComboBox_NroFruits.Items.Add("30")
        Me.ComboBox_NroFruits.Items.Add("31")
        Me.ComboBox_NroFruits.Items.Add("32")
        Me.ComboBox_NroFruits.Items.Add("33")
        Me.ComboBox_NroFruits.Items.Add("34")
        Me.ComboBox_NroFruits.Items.Add("35")
        Me.ComboBox_NroFruits.Items.Add("36")
        Me.ComboBox_NroFruits.Items.Add("37")
        Me.ComboBox_NroFruits.Items.Add("38")
        Me.ComboBox_NroFruits.Items.Add("39")
        Me.ComboBox_NroFruits.Items.Add("40")
        Me.ComboBox_NroFruits.Items.Add("41")
        Me.ComboBox_NroFruits.Items.Add("42")
        Me.ComboBox_NroFruits.Items.Add("43")
        Me.ComboBox_NroFruits.Items.Add("44")
        Me.ComboBox_NroFruits.Items.Add("45")
        Me.ComboBox_NroFruits.Items.Add("46")
        Me.ComboBox_NroFruits.Items.Add("47")
        Me.ComboBox_NroFruits.Items.Add("48")
        Me.ComboBox_NroFruits.Items.Add("49")
        Me.ComboBox_NroFruits.Items.Add("50")
        Me.ComboBox_NroFruits.Items.Add("51")
        Me.ComboBox_NroFruits.Items.Add("52")
        Me.ComboBox_NroFruits.Items.Add("53")
        Me.ComboBox_NroFruits.Items.Add("54")
        Me.ComboBox_NroFruits.Items.Add("55")
        Me.ComboBox_NroFruits.Items.Add("56")
        Me.ComboBox_NroFruits.Items.Add("57")
        Me.ComboBox_NroFruits.Items.Add("58")
        Me.ComboBox_NroFruits.Items.Add("59")
        Me.ComboBox_NroFruits.Items.Add("60")
        Me.ComboBox_NroFruits.Items.Add("61")
        Me.ComboBox_NroFruits.Items.Add("62")
        Me.ComboBox_NroFruits.Items.Add("63")
        Me.ComboBox_NroFruits.Items.Add("64")
        Me.ComboBox_NroFruits.Items.Add("65")
        Me.ComboBox_NroFruits.Items.Add("66")
        Me.ComboBox_NroFruits.Items.Add("67")
        Me.ComboBox_NroFruits.Items.Add("68")
        Me.ComboBox_NroFruits.Items.Add("69")
        Me.ComboBox_NroFruits.Items.Add("70")
        Me.ComboBox_NroFruits.Items.Add("71")
        Me.ComboBox_NroFruits.Items.Add("72")
        Me.ComboBox_NroFruits.Items.Add("73")
        Me.ComboBox_NroFruits.Items.Add("74")
        Me.ComboBox_NroFruits.Items.Add("75")
        Me.ComboBox_NroFruits.Items.Add("76")
        Me.ComboBox_NroFruits.Items.Add("77")
        Me.ComboBox_NroFruits.Items.Add("78")
        Me.ComboBox_NroFruits.Items.Add("79")
        Me.ComboBox_NroFruits.Items.Add("80")
        Me.ComboBox_NroFruits.Location = New System.Drawing.Point(75, 199)
        Me.ComboBox_NroFruits.Name = "ComboBox_NroFruits"
        Me.ComboBox_NroFruits.Size = New System.Drawing.Size(36, 22)
        Me.ComboBox_NroFruits.TabIndex = 9
        '
        'Button_SelectHarvest
        '
        Me.Button_SelectHarvest.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SelectHarvest.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SelectHarvest.ForeColor = System.Drawing.Color.White
        Me.Button_SelectHarvest.Location = New System.Drawing.Point(161, 149)
        Me.Button_SelectHarvest.Name = "Button_SelectHarvest"
        Me.Button_SelectHarvest.Size = New System.Drawing.Size(52, 20)
        Me.Button_SelectHarvest.TabIndex = 7
        Me.Button_SelectHarvest.Text = "Select"
        '
        'Button_SearchHarvest
        '
        Me.Button_SearchHarvest.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SearchHarvest.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SearchHarvest.ForeColor = System.Drawing.Color.White
        Me.Button_SearchHarvest.Location = New System.Drawing.Point(154, 22)
        Me.Button_SearchHarvest.Name = "Button_SearchHarvest"
        Me.Button_SearchHarvest.Size = New System.Drawing.Size(53, 20)
        Me.Button_SearchHarvest.TabIndex = 6
        Me.Button_SearchHarvest.Text = "Search"
        '
        'TextBox_SearchHarvest
        '
        Me.TextBox_SearchHarvest.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_SearchHarvest.Location = New System.Drawing.Point(7, 22)
        Me.TextBox_SearchHarvest.Name = "TextBox_SearchHarvest"
        Me.TextBox_SearchHarvest.Size = New System.Drawing.Size(141, 19)
        Me.TextBox_SearchHarvest.TabIndex = 5
        '
        'C1FlexGrid_Harvest
        '
        Me.C1FlexGrid_Harvest.AllowEditing = False
        Me.C1FlexGrid_Harvest.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        'Me.C1FlexGrid_Harvest.Cols.Count = 25
        Me.C1FlexGrid_Harvest.Cols.Fixed = 0
        Me.C1FlexGrid_Harvest.DataSource = Me.BindingSource1
        Me.C1FlexGrid_Harvest.LicensingInformation.LicenseToken = "nkmxg4Z0BbinPbpYa4E6n8nZaRDDXDexXOjVe9x0gX/ea83GfB6pRZHRqbTpLQ6/"
        Me.C1FlexGrid_Harvest.Location = New System.Drawing.Point(7, 47)
        Me.C1FlexGrid_Harvest.Name = "C1FlexGrid_Harvest"
        Me.C1FlexGrid_Harvest.Rows.Count = 1
        Me.C1FlexGrid_Harvest.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.C1FlexGrid_Harvest.Size = New System.Drawing.Size(226, 96)
        Me.C1FlexGrid_Harvest.StyleInfo = resources.GetString("C1FlexGrid_Harvest.StyleInfo")
        Me.C1FlexGrid_Harvest.TabIndex = 3
        '
        'BindingSource1
        '
        Me.BindingSource1.DataMember = "results"
        Me.BindingSource1.DataSource = Me.Results_DataSet1
        '
        'Results_DataSet1
        '
        Me.Results_DataSet1.DataSetName = "Results_DataSet"
        Me.Results_DataSet1.Prefix = ""
        Me.Results_DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label_HarvestTitle
        '
        Me.Label_HarvestTitle.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_HarvestTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_HarvestTitle.Location = New System.Drawing.Point(7, 1)
        Me.Label_HarvestTitle.Name = "Label_HarvestTitle"
        Me.Label_HarvestTitle.Size = New System.Drawing.Size(226, 20)
        Me.Label_HarvestTitle.Text = "Harvest"
        Me.Label_HarvestTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label_Fruits
        '
        Me.Label_Fruits.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Fruits.Location = New System.Drawing.Point(9, 199)
        Me.Label_Fruits.Name = "Label_Fruits"
        Me.Label_Fruits.Size = New System.Drawing.Size(66, 20)
        Me.Label_Fruits.Text = "Nro Fruits"
        '
        'TabPage_Harvest_Tuber
        '
        Me.TabPage_Harvest_Tuber.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.Label12)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.TextBox2)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.DateTimePicker1)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.Label17)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.txtCosechaTuber)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.lblColector)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.lblNroTuber)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.ComboBox_NroTubers)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.cboProcedence)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.lblProcedence)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.cboFemaleTuber)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.TextBox_SearchValue)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.btnSearchTuberculusHarvest)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.lblValueTuberculusHarvest)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.Label13)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.C1FlexGrid_ValuesTubHarv)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.lblTuberHarvestTitle)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.btnPrintTuberHarvest)
        Me.TabPage_Harvest_Tuber.Controls.Add(Me.btnSelectTuberHarv)
        Me.TabPage_Harvest_Tuber.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_Harvest_Tuber.Name = "TabPage_Harvest_Tuber"
        Me.TabPage_Harvest_Tuber.Size = New System.Drawing.Size(232, 242)
        Me.TabPage_Harvest_Tuber.Text = "Cosecha de Tuber / Frutos"
        '
        'Label12
        '
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(137, 190)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(39, 20)
        Me.Label12.Text = "Peso"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(197, 189)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(36, 21)
        Me.TextBox2.TabIndex = 125
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(173, 163)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(60, 20)
        Me.DateTimePicker1.TabIndex = 118
        '
        'Label17
        '
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(137, 163)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(30, 20)
        Me.Label17.Text = "Date"
        '
        'txtCosechaTuber
        '
        Me.txtCosechaTuber.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.txtCosechaTuber.Location = New System.Drawing.Point(84, 135)
        Me.txtCosechaTuber.Name = "txtCosechaTuber"
        Me.txtCosechaTuber.ReadOnly = True
        Me.txtCosechaTuber.Size = New System.Drawing.Size(93, 19)
        Me.txtCosechaTuber.TabIndex = 113
        '
        'lblColector
        '
        Me.lblColector.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblColector.Location = New System.Drawing.Point(7, 135)
        Me.lblColector.Name = "lblColector"
        Me.lblColector.Size = New System.Drawing.Size(65, 20)
        Me.lblColector.Text = "Colector"
        '
        'lblNroTuber
        '
        Me.lblNroTuber.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblNroTuber.Location = New System.Drawing.Point(7, 191)
        Me.lblNroTuber.Name = "lblNroTuber"
        Me.lblNroTuber.Size = New System.Drawing.Size(66, 30)
        Me.lblNroTuber.Text = "Nro Fru / Tub"
        '
        'ComboBox_NroTubers
        '
        Me.ComboBox_NroTubers.Items.Add("0")
        Me.ComboBox_NroTubers.Items.Add("1")
        Me.ComboBox_NroTubers.Items.Add("2")
        Me.ComboBox_NroTubers.Items.Add("3")
        Me.ComboBox_NroTubers.Items.Add("4")
        Me.ComboBox_NroTubers.Items.Add("5")
        Me.ComboBox_NroTubers.Items.Add("6")
        Me.ComboBox_NroTubers.Items.Add("7")
        Me.ComboBox_NroTubers.Items.Add("8")
        Me.ComboBox_NroTubers.Items.Add("9")
        Me.ComboBox_NroTubers.Items.Add("10")
        Me.ComboBox_NroTubers.Items.Add("11")
        Me.ComboBox_NroTubers.Items.Add("12")
        Me.ComboBox_NroTubers.Items.Add("13")
        Me.ComboBox_NroTubers.Items.Add("14")
        Me.ComboBox_NroTubers.Items.Add("15")
        Me.ComboBox_NroTubers.Items.Add("16")
        Me.ComboBox_NroTubers.Items.Add("17")
        Me.ComboBox_NroTubers.Items.Add("18")
        Me.ComboBox_NroTubers.Items.Add("19")
        Me.ComboBox_NroTubers.Items.Add("20")
        Me.ComboBox_NroTubers.Items.Add("21")
        Me.ComboBox_NroTubers.Items.Add("22")
        Me.ComboBox_NroTubers.Items.Add("23")
        Me.ComboBox_NroTubers.Items.Add("24")
        Me.ComboBox_NroTubers.Items.Add("25")
        Me.ComboBox_NroTubers.Items.Add("26")
        Me.ComboBox_NroTubers.Items.Add("27")
        Me.ComboBox_NroTubers.Items.Add("28")
        Me.ComboBox_NroTubers.Items.Add("29")
        Me.ComboBox_NroTubers.Items.Add("30")
        Me.ComboBox_NroTubers.Items.Add("31")
        Me.ComboBox_NroTubers.Items.Add("32")
        Me.ComboBox_NroTubers.Items.Add("33")
        Me.ComboBox_NroTubers.Items.Add("34")
        Me.ComboBox_NroTubers.Items.Add("35")
        Me.ComboBox_NroTubers.Items.Add("36")
        Me.ComboBox_NroTubers.Items.Add("37")
        Me.ComboBox_NroTubers.Items.Add("38")
        Me.ComboBox_NroTubers.Items.Add("39")
        Me.ComboBox_NroTubers.Items.Add("40")
        Me.ComboBox_NroTubers.Items.Add("41")
        Me.ComboBox_NroTubers.Items.Add("42")
        Me.ComboBox_NroTubers.Items.Add("43")
        Me.ComboBox_NroTubers.Items.Add("44")
        Me.ComboBox_NroTubers.Items.Add("45")
        Me.ComboBox_NroTubers.Items.Add("46")
        Me.ComboBox_NroTubers.Items.Add("47")
        Me.ComboBox_NroTubers.Items.Add("48")
        Me.ComboBox_NroTubers.Items.Add("49")
        Me.ComboBox_NroTubers.Items.Add("50")
        Me.ComboBox_NroTubers.Items.Add("51")
        Me.ComboBox_NroTubers.Items.Add("52")
        Me.ComboBox_NroTubers.Items.Add("53")
        Me.ComboBox_NroTubers.Items.Add("54")
        Me.ComboBox_NroTubers.Items.Add("55")
        Me.ComboBox_NroTubers.Items.Add("56")
        Me.ComboBox_NroTubers.Items.Add("57")
        Me.ComboBox_NroTubers.Items.Add("58")
        Me.ComboBox_NroTubers.Items.Add("59")
        Me.ComboBox_NroTubers.Items.Add("60")
        Me.ComboBox_NroTubers.Items.Add("61")
        Me.ComboBox_NroTubers.Items.Add("62")
        Me.ComboBox_NroTubers.Items.Add("63")
        Me.ComboBox_NroTubers.Items.Add("64")
        Me.ComboBox_NroTubers.Items.Add("65")
        Me.ComboBox_NroTubers.Items.Add("66")
        Me.ComboBox_NroTubers.Items.Add("67")
        Me.ComboBox_NroTubers.Items.Add("68")
        Me.ComboBox_NroTubers.Items.Add("69")
        Me.ComboBox_NroTubers.Items.Add("70")
        Me.ComboBox_NroTubers.Items.Add("71")
        Me.ComboBox_NroTubers.Items.Add("72")
        Me.ComboBox_NroTubers.Items.Add("73")
        Me.ComboBox_NroTubers.Items.Add("74")
        Me.ComboBox_NroTubers.Items.Add("75")
        Me.ComboBox_NroTubers.Items.Add("76")
        Me.ComboBox_NroTubers.Items.Add("77")
        Me.ComboBox_NroTubers.Items.Add("78")
        Me.ComboBox_NroTubers.Items.Add("79")
        Me.ComboBox_NroTubers.Items.Add("80")
        Me.ComboBox_NroTubers.Items.Add("80")
        Me.ComboBox_NroTubers.Items.Add("81")
        Me.ComboBox_NroTubers.Items.Add("82")
        Me.ComboBox_NroTubers.Items.Add("83")
        Me.ComboBox_NroTubers.Items.Add("84")
        Me.ComboBox_NroTubers.Items.Add("85")
        Me.ComboBox_NroTubers.Items.Add("86")
        Me.ComboBox_NroTubers.Items.Add("87")
        Me.ComboBox_NroTubers.Items.Add("88")
        Me.ComboBox_NroTubers.Items.Add("89")
        Me.ComboBox_NroTubers.Items.Add("90")
        Me.ComboBox_NroTubers.Items.Add("91")
        Me.ComboBox_NroTubers.Items.Add("92")
        Me.ComboBox_NroTubers.Items.Add("93")
        Me.ComboBox_NroTubers.Items.Add("94")
        Me.ComboBox_NroTubers.Items.Add("95")
        Me.ComboBox_NroTubers.Items.Add("96")
        Me.ComboBox_NroTubers.Items.Add("97")
        Me.ComboBox_NroTubers.Items.Add("98")
        Me.ComboBox_NroTubers.Items.Add("99")
        Me.ComboBox_NroTubers.Items.Add("100")
        Me.ComboBox_NroTubers.Items.Add("101")
        Me.ComboBox_NroTubers.Items.Add("102")
        Me.ComboBox_NroTubers.Items.Add("103")
        Me.ComboBox_NroTubers.Items.Add("104")
        Me.ComboBox_NroTubers.Items.Add("105")
        Me.ComboBox_NroTubers.Items.Add("106")
        Me.ComboBox_NroTubers.Items.Add("107")
        Me.ComboBox_NroTubers.Items.Add("108")
        Me.ComboBox_NroTubers.Items.Add("109")
        Me.ComboBox_NroTubers.Items.Add("110")
        Me.ComboBox_NroTubers.Items.Add("111")
        Me.ComboBox_NroTubers.Items.Add("112")
        Me.ComboBox_NroTubers.Items.Add("113")
        Me.ComboBox_NroTubers.Items.Add("114")
        Me.ComboBox_NroTubers.Items.Add("115")
        Me.ComboBox_NroTubers.Items.Add("116")
        Me.ComboBox_NroTubers.Items.Add("117")
        Me.ComboBox_NroTubers.Items.Add("118")
        Me.ComboBox_NroTubers.Items.Add("119")
        Me.ComboBox_NroTubers.Items.Add("120")
        Me.ComboBox_NroTubers.Items.Add("121")
        Me.ComboBox_NroTubers.Items.Add("122")
        Me.ComboBox_NroTubers.Items.Add("123")
        Me.ComboBox_NroTubers.Items.Add("124")
        Me.ComboBox_NroTubers.Items.Add("125")
        Me.ComboBox_NroTubers.Items.Add("126")
        Me.ComboBox_NroTubers.Items.Add("127")
        Me.ComboBox_NroTubers.Items.Add("128")
        Me.ComboBox_NroTubers.Items.Add("129")
        Me.ComboBox_NroTubers.Items.Add("130")
        Me.ComboBox_NroTubers.Items.Add("131")
        Me.ComboBox_NroTubers.Items.Add("132")
        Me.ComboBox_NroTubers.Items.Add("133")
        Me.ComboBox_NroTubers.Items.Add("134")
        Me.ComboBox_NroTubers.Items.Add("135")
        Me.ComboBox_NroTubers.Items.Add("136")
        Me.ComboBox_NroTubers.Items.Add("137")
        Me.ComboBox_NroTubers.Items.Add("138")
        Me.ComboBox_NroTubers.Items.Add("139")
        Me.ComboBox_NroTubers.Items.Add("140")
        Me.ComboBox_NroTubers.Items.Add("140")
        Me.ComboBox_NroTubers.Items.Add("141")
        Me.ComboBox_NroTubers.Items.Add("142")
        Me.ComboBox_NroTubers.Items.Add("143")
        Me.ComboBox_NroTubers.Items.Add("144")
        Me.ComboBox_NroTubers.Items.Add("145")
        Me.ComboBox_NroTubers.Items.Add("146")
        Me.ComboBox_NroTubers.Items.Add("147")
        Me.ComboBox_NroTubers.Items.Add("148")
        Me.ComboBox_NroTubers.Items.Add("149")
        Me.ComboBox_NroTubers.Items.Add("150")
        Me.ComboBox_NroTubers.Location = New System.Drawing.Point(85, 191)
        Me.ComboBox_NroTubers.Name = "ComboBox_NroTubers"
        Me.ComboBox_NroTubers.Size = New System.Drawing.Size(36, 22)
        Me.ComboBox_NroTubers.TabIndex = 109
        '
        'cboProcedence
        '
        Me.cboProcedence.Items.Add("S")
        Me.cboProcedence.Items.Add("T")
        Me.cboProcedence.Items.Add("I")
        Me.cboProcedence.Items.Add("E")
        Me.cboProcedence.Location = New System.Drawing.Point(84, 160)
        Me.cboProcedence.Name = "cboProcedence"
        Me.cboProcedence.Size = New System.Drawing.Size(36, 22)
        Me.cboProcedence.TabIndex = 106
        '
        'lblProcedence
        '
        Me.lblProcedence.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblProcedence.Location = New System.Drawing.Point(7, 163)
        Me.lblProcedence.Name = "lblProcedence"
        Me.lblProcedence.Size = New System.Drawing.Size(72, 20)
        Me.lblProcedence.Text = "Procedencia"
        '
        'cboFemaleTuber
        '
        Me.cboFemaleTuber.Items.Add("1")
        Me.cboFemaleTuber.Items.Add("2")
        Me.cboFemaleTuber.Items.Add("3")
        Me.cboFemaleTuber.Items.Add("4")
        Me.cboFemaleTuber.Items.Add("5")
        Me.cboFemaleTuber.Items.Add("6")
        Me.cboFemaleTuber.Items.Add("7")
        Me.cboFemaleTuber.Items.Add("8")
        Me.cboFemaleTuber.Items.Add("9")
        Me.cboFemaleTuber.Items.Add("10")
        Me.cboFemaleTuber.Items.Add("11")
        Me.cboFemaleTuber.Items.Add("12")
        Me.cboFemaleTuber.Items.Add("13")
        Me.cboFemaleTuber.Items.Add("14")
        Me.cboFemaleTuber.Items.Add("15")
        Me.cboFemaleTuber.Items.Add("16")
        Me.cboFemaleTuber.Items.Add("17")
        Me.cboFemaleTuber.Items.Add("18")
        Me.cboFemaleTuber.Items.Add("19")
        Me.cboFemaleTuber.Items.Add("20")
        Me.cboFemaleTuber.Items.Add("21")
        Me.cboFemaleTuber.Items.Add("22")
        Me.cboFemaleTuber.Items.Add("23")
        Me.cboFemaleTuber.Items.Add("24")
        Me.cboFemaleTuber.Items.Add("25")
        Me.cboFemaleTuber.Items.Add("26")
        Me.cboFemaleTuber.Items.Add("27")
        Me.cboFemaleTuber.Items.Add("28")
        Me.cboFemaleTuber.Items.Add("29")
        Me.cboFemaleTuber.Items.Add("30")
        Me.cboFemaleTuber.Items.Add("31")
        Me.cboFemaleTuber.Items.Add("32")
        Me.cboFemaleTuber.Items.Add("33")
        Me.cboFemaleTuber.Items.Add("34")
        Me.cboFemaleTuber.Items.Add("35")
        Me.cboFemaleTuber.Items.Add("36")
        Me.cboFemaleTuber.Items.Add("37")
        Me.cboFemaleTuber.Items.Add("38")
        Me.cboFemaleTuber.Items.Add("39")
        Me.cboFemaleTuber.Items.Add("40")
        Me.cboFemaleTuber.Items.Add("41")
        Me.cboFemaleTuber.Items.Add("42")
        Me.cboFemaleTuber.Items.Add("43")
        Me.cboFemaleTuber.Items.Add("44")
        Me.cboFemaleTuber.Items.Add("45")
        Me.cboFemaleTuber.Items.Add("46")
        Me.cboFemaleTuber.Items.Add("47")
        Me.cboFemaleTuber.Items.Add("48")
        Me.cboFemaleTuber.Items.Add("49")
        Me.cboFemaleTuber.Items.Add("50")
        Me.cboFemaleTuber.Location = New System.Drawing.Point(129, 20)
        Me.cboFemaleTuber.Name = "cboFemaleTuber"
        Me.cboFemaleTuber.Size = New System.Drawing.Size(38, 22)
        Me.cboFemaleTuber.TabIndex = 101
        '
        'TextBox_SearchValue
        '
        Me.TextBox_SearchValue.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_SearchValue.Location = New System.Drawing.Point(7, 22)
        Me.TextBox_SearchValue.Name = "TextBox_SearchValue"
        Me.TextBox_SearchValue.Size = New System.Drawing.Size(84, 19)
        Me.TextBox_SearchValue.TabIndex = 100
        '
        'btnSearchTuberculusHarvest
        '
        Me.btnSearchTuberculusHarvest.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnSearchTuberculusHarvest.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.btnSearchTuberculusHarvest.ForeColor = System.Drawing.Color.White
        Me.btnSearchTuberculusHarvest.Location = New System.Drawing.Point(95, 21)
        Me.btnSearchTuberculusHarvest.Name = "btnSearchTuberculusHarvest"
        Me.btnSearchTuberculusHarvest.Size = New System.Drawing.Size(22, 20)
        Me.btnSearchTuberculusHarvest.TabIndex = 99
        Me.btnSearchTuberculusHarvest.Text = "S"
        '
        'lblValueTuberculusHarvest
        '
        Me.lblValueTuberculusHarvest.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblValueTuberculusHarvest.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblValueTuberculusHarvest.Location = New System.Drawing.Point(7, 2)
        Me.lblValueTuberculusHarvest.Name = "lblValueTuberculusHarvest"
        Me.lblValueTuberculusHarvest.Size = New System.Drawing.Size(48, 17)
        Me.lblValueTuberculusHarvest.Text = "Value"
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(118, 20)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(10, 21)
        Me.Label13.Text = "F"
        '
        'C1FlexGrid_ValuesTubHarv
        '
        Me.C1FlexGrid_ValuesTubHarv.AllowEditing = False
        Me.C1FlexGrid_ValuesTubHarv.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.C1FlexGrid_ValuesTubHarv.AutoResize = True
        Me.C1FlexGrid_ValuesTubHarv.AutoSearchDelay = 1
        Me.C1FlexGrid_ValuesTubHarv.ClipSeparators = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.C1FlexGrid_ValuesTubHarv.Col = 0
        Me.C1FlexGrid_ValuesTubHarv.ColSel = 0
        Me.C1FlexGrid_ValuesTubHarv.ComboList = Nothing
        Me.C1FlexGrid_ValuesTubHarv.EditMask = Nothing
        Me.C1FlexGrid_ValuesTubHarv.ExtendLastCol = False
        Me.C1FlexGrid_ValuesTubHarv.LeftCol = 1
        Me.C1FlexGrid_ValuesTubHarv.Location = New System.Drawing.Point(7, 46)
        Me.C1FlexGrid_ValuesTubHarv.Name = "C1FlexGrid_ValuesTubHarv"
        Me.C1FlexGrid_ValuesTubHarv.Redraw = True
        Me.C1FlexGrid_ValuesTubHarv.Row = -2
        Me.C1FlexGrid_ValuesTubHarv.RowSel = -2
        Me.C1FlexGrid_ValuesTubHarv.ScrollPosition = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid_ValuesTubHarv.ScrollTrack = True
        Me.C1FlexGrid_ValuesTubHarv.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
        Me.C1FlexGrid_ValuesTubHarv.ShowCursor = False
        Me.C1FlexGrid_ValuesTubHarv.ShowSort = True
        Me.C1FlexGrid_ValuesTubHarv.Size = New System.Drawing.Size(226, 83)
        Me.C1FlexGrid_ValuesTubHarv.StyleInfo = resources.GetString("C1FlexGrid_ValuesTubHarv.StyleInfo")
        Me.C1FlexGrid_ValuesTubHarv.TabIndex = 81
        Me.C1FlexGrid_ValuesTubHarv.TopRow = 1
        '
        'lblTuberHarvestTitle
        '
        Me.lblTuberHarvestTitle.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblTuberHarvestTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblTuberHarvestTitle.Location = New System.Drawing.Point(49, 2)
        Me.lblTuberHarvestTitle.Name = "lblTuberHarvestTitle"
        Me.lblTuberHarvestTitle.Size = New System.Drawing.Size(160, 20)
        Me.lblTuberHarvestTitle.Text = "Cosecha de tuberculos"
        Me.lblTuberHarvestTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnPrintTuberHarvest
        '
        Me.btnPrintTuberHarvest.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnPrintTuberHarvest.ForeColor = System.Drawing.Color.White
        Me.btnPrintTuberHarvest.Location = New System.Drawing.Point(153, 223)
        Me.btnPrintTuberHarvest.Name = "btnPrintTuberHarvest"
        Me.btnPrintTuberHarvest.Size = New System.Drawing.Size(80, 21)
        Me.btnPrintTuberHarvest.TabIndex = 97
        Me.btnPrintTuberHarvest.Text = "Print"
        '
        'btnSelectTuberHarv
        '
        Me.btnSelectTuberHarv.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnSelectTuberHarv.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.btnSelectTuberHarv.ForeColor = System.Drawing.Color.White
        Me.btnSelectTuberHarv.Location = New System.Drawing.Point(183, 134)
        Me.btnSelectTuberHarv.Name = "btnSelectTuberHarv"
        Me.btnSelectTuberHarv.Size = New System.Drawing.Size(50, 20)
        Me.btnSelectTuberHarv.TabIndex = 96
        Me.btnSelectTuberHarv.Text = "Select"
        '
        'TabPage_Maceration
        '
        Me.TabPage_Maceration.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.TabPage_Maceration.Controls.Add(Me.Label9)
        Me.TabPage_Maceration.Controls.Add(Me.Label7)
        Me.TabPage_Maceration.Controls.Add(Me.Label5)
        Me.TabPage_Maceration.Controls.Add(Me.Label4)
        Me.TabPage_Maceration.Controls.Add(Me.TextBox_Weight1)
        Me.TabPage_Maceration.Controls.Add(Me.TextBox_Weight)
        Me.TabPage_Maceration.Controls.Add(Me.TextBox_Comments)
        Me.TabPage_Maceration.Controls.Add(Me.TextBox_Quantity)
        Me.TabPage_Maceration.Controls.Add(Me.TextBox_HideNroRepetitions2)
        Me.TabPage_Maceration.Controls.Add(Me.TextBox_IndexMaceration)
        Me.TabPage_Maceration.Controls.Add(Me.Button_PrintMaceration)
        Me.TabPage_Maceration.Controls.Add(Me.Label_MacerationDate)
        Me.TabPage_Maceration.Controls.Add(Me.DateTimePicker_Maceration)
        Me.TabPage_Maceration.Controls.Add(Me.Label_CountMaceration)
        Me.TabPage_Maceration.Controls.Add(Me.TextBox_Maceration)
        Me.TabPage_Maceration.Controls.Add(Me.Button_SelectMaceration)
        Me.TabPage_Maceration.Controls.Add(Me.C1FlexGrid_Maceration)
        Me.TabPage_Maceration.Controls.Add(Me.Button_LoadMaceration)
        Me.TabPage_Maceration.Controls.Add(Me.Button_SearchMaceration)
        Me.TabPage_Maceration.Controls.Add(Me.TextBox_SearchMaceration)
        Me.TabPage_Maceration.Controls.Add(Me.Label_MacerationTitle)
        Me.TabPage_Maceration.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_Maceration.Name = "TabPage_Maceration"
        Me.TabPage_Maceration.Size = New System.Drawing.Size(232, 242)
        Me.TabPage_Maceration.Text = "Maceration"
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(4, 177)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(63, 20)
        Me.Label9.Text = "Comments"
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(4, 148)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 20)
        Me.Label7.Text = "Quantity"
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(118, 149)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 20)
        Me.Label5.Text = "Weight 1"
        Me.Label5.Visible = False
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(4, 149)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 20)
        Me.Label4.Text = "Weight"
        Me.Label4.Visible = False
        '
        'TextBox_Weight1
        '
        Me.TextBox_Weight1.Location = New System.Drawing.Point(187, 148)
        Me.TextBox_Weight1.Name = "TextBox_Weight1"
        Me.TextBox_Weight1.Size = New System.Drawing.Size(36, 21)
        Me.TextBox_Weight1.TabIndex = 69
        Me.TextBox_Weight1.Visible = False
        '
        'TextBox_Weight
        '
        Me.TextBox_Weight.Location = New System.Drawing.Point(73, 148)
        Me.TextBox_Weight.Name = "TextBox_Weight"
        Me.TextBox_Weight.Size = New System.Drawing.Size(39, 21)
        Me.TextBox_Weight.TabIndex = 67
        Me.TextBox_Weight.Visible = False
        '
        'TextBox_Comments
        '
        Me.TextBox_Comments.Location = New System.Drawing.Point(73, 177)
        Me.TextBox_Comments.MaxLength = 100
        Me.TextBox_Comments.Multiline = True
        Me.TextBox_Comments.Name = "TextBox_Comments"
        Me.TextBox_Comments.Size = New System.Drawing.Size(150, 18)
        Me.TextBox_Comments.TabIndex = 66
        '
        'TextBox_Quantity
        '
        Me.TextBox_Quantity.Location = New System.Drawing.Point(73, 150)
        Me.TextBox_Quantity.Name = "TextBox_Quantity"
        Me.TextBox_Quantity.Size = New System.Drawing.Size(39, 21)
        Me.TextBox_Quantity.TabIndex = 64
        '
        'TextBox_HideNroRepetitions2
        '
        Me.TextBox_HideNroRepetitions2.Location = New System.Drawing.Point(158, 223)
        Me.TextBox_HideNroRepetitions2.Name = "TextBox_HideNroRepetitions2"
        Me.TextBox_HideNroRepetitions2.Size = New System.Drawing.Size(10, 21)
        Me.TextBox_HideNroRepetitions2.TabIndex = 60
        Me.TextBox_HideNroRepetitions2.Visible = False
        '
        'TextBox_IndexMaceration
        '
        Me.TextBox_IndexMaceration.Location = New System.Drawing.Point(223, 223)
        Me.TextBox_IndexMaceration.Name = "TextBox_IndexMaceration"
        Me.TextBox_IndexMaceration.Size = New System.Drawing.Size(10, 21)
        Me.TextBox_IndexMaceration.TabIndex = 55
        Me.TextBox_IndexMaceration.Visible = False
        '
        'Button_PrintMaceration
        '
        Me.Button_PrintMaceration.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_PrintMaceration.ForeColor = System.Drawing.Color.White
        Me.Button_PrintMaceration.Location = New System.Drawing.Point(158, 222)
        Me.Button_PrintMaceration.Name = "Button_PrintMaceration"
        Me.Button_PrintMaceration.Size = New System.Drawing.Size(75, 22)
        Me.Button_PrintMaceration.TabIndex = 51
        Me.Button_PrintMaceration.Text = "Print"
        '
        'Label_MacerationDate
        '
        Me.Label_MacerationDate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_MacerationDate.Location = New System.Drawing.Point(8, 202)
        Me.Label_MacerationDate.Name = "Label_MacerationDate"
        Me.Label_MacerationDate.Size = New System.Drawing.Size(38, 20)
        Me.Label_MacerationDate.Text = "Date"
        '
        'DateTimePicker_Maceration
        '
        Me.DateTimePicker_Maceration.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.DateTimePicker_Maceration.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_Maceration.Location = New System.Drawing.Point(73, 201)
        Me.DateTimePicker_Maceration.Name = "DateTimePicker_Maceration"
        Me.DateTimePicker_Maceration.Size = New System.Drawing.Size(60, 20)
        Me.DateTimePicker_Maceration.TabIndex = 50
        '
        'Label_CountMaceration
        '
        Me.Label_CountMaceration.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_CountMaceration.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CountMaceration.Location = New System.Drawing.Point(216, 200)
        Me.Label_CountMaceration.Name = "Label_CountMaceration"
        Me.Label_CountMaceration.Size = New System.Drawing.Size(18, 20)
        '
        'TextBox_Maceration
        '
        Me.TextBox_Maceration.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_Maceration.Location = New System.Drawing.Point(4, 126)
        Me.TextBox_Maceration.Name = "TextBox_Maceration"
        Me.TextBox_Maceration.ReadOnly = True
        Me.TextBox_Maceration.Size = New System.Drawing.Size(149, 19)
        Me.TextBox_Maceration.TabIndex = 47
        '
        'Button_SelectMaceration
        '
        Me.Button_SelectMaceration.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SelectMaceration.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SelectMaceration.ForeColor = System.Drawing.Color.White
        Me.Button_SelectMaceration.Location = New System.Drawing.Point(158, 126)
        Me.Button_SelectMaceration.Name = "Button_SelectMaceration"
        Me.Button_SelectMaceration.Size = New System.Drawing.Size(52, 20)
        Me.Button_SelectMaceration.TabIndex = 46
        Me.Button_SelectMaceration.Text = "Select"
        '
        'C1FlexGrid_Maceration
        '
        Me.C1FlexGrid_Maceration.AllowEditing = False
        Me.C1FlexGrid_Maceration.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        'Me.C1FlexGrid_Maceration.Cols.Count = 25
        Me.C1FlexGrid_Maceration.Cols.Fixed = 0
        Me.C1FlexGrid_Maceration.DataSource = Me.BindingSource1
        Me.C1FlexGrid_Maceration.LicensingInformation.LicenseToken = "nkmxg4Z0BbinPbpYa4E6n8nZaRDDXDexXOjVe9x0gX/ea83GfB6pRZHRqbTpLQ6/"
        Me.C1FlexGrid_Maceration.Location = New System.Drawing.Point(7, 47)
        Me.C1FlexGrid_Maceration.Name = "C1FlexGrid_Maceration"
        Me.C1FlexGrid_Maceration.Rows.Count = 1
        Me.C1FlexGrid_Maceration.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.C1FlexGrid_Maceration.Size = New System.Drawing.Size(226, 73)
        Me.C1FlexGrid_Maceration.StyleInfo = resources.GetString("C1FlexGrid_Maceration.StyleInfo")
        Me.C1FlexGrid_Maceration.TabIndex = 44
        '
        'Button_LoadMaceration
        '
        Me.Button_LoadMaceration.BackColor = System.Drawing.Color.White
        Me.Button_LoadMaceration.Font = New System.Drawing.Font("Tahoma", 6.0!, System.Drawing.FontStyle.Bold)
        Me.Button_LoadMaceration.ForeColor = System.Drawing.Color.Red
        Me.Button_LoadMaceration.Location = New System.Drawing.Point(211, 22)
        Me.Button_LoadMaceration.Name = "Button_LoadMaceration"
        Me.Button_LoadMaceration.Size = New System.Drawing.Size(22, 20)
        Me.Button_LoadMaceration.TabIndex = 43
        Me.Button_LoadMaceration.Text = "X"
        '
        'Button_SearchMaceration
        '
        Me.Button_SearchMaceration.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SearchMaceration.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_SearchMaceration.ForeColor = System.Drawing.Color.White
        Me.Button_SearchMaceration.Location = New System.Drawing.Point(154, 22)
        Me.Button_SearchMaceration.Name = "Button_SearchMaceration"
        Me.Button_SearchMaceration.Size = New System.Drawing.Size(53, 20)
        Me.Button_SearchMaceration.TabIndex = 42
        Me.Button_SearchMaceration.Text = "Search"
        '
        'TextBox_SearchMaceration
        '
        Me.TextBox_SearchMaceration.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_SearchMaceration.Location = New System.Drawing.Point(7, 22)
        Me.TextBox_SearchMaceration.Name = "TextBox_SearchMaceration"
        Me.TextBox_SearchMaceration.Size = New System.Drawing.Size(141, 19)
        Me.TextBox_SearchMaceration.TabIndex = 41
        '
        'Label_MacerationTitle
        '
        Me.Label_MacerationTitle.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_MacerationTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_MacerationTitle.Location = New System.Drawing.Point(7, 1)
        Me.Label_MacerationTitle.Name = "Label_MacerationTitle"
        Me.Label_MacerationTitle.Size = New System.Drawing.Size(226, 20)
        Me.Label_MacerationTitle.Text = "Maceration"
        Me.Label_MacerationTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TabPage_Options
        '
        Me.TabPage_Options.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.TabPage_Options.Controls.Add(Me.TextBox1)
        Me.TabPage_Options.Controls.Add(Me.Panel5)
        Me.TabPage_Options.Controls.Add(Me.Panel4)
        Me.TabPage_Options.Controls.Add(Me.Label_OptionsTitle)
        Me.TabPage_Options.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_Options.Name = "TabPage_Options"
        Me.TabPage_Options.Size = New System.Drawing.Size(232, 242)
        Me.TabPage_Options.Text = "Options"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Tahoma", 6.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox1.Location = New System.Drawing.Point(4, 224)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(33, 21)
        Me.TextBox1.TabIndex = 54
        Me.TextBox1.Text = "About CIPCROSS SYSTEM" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Version: 1.0.2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Integrated IT & Computational Research" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "� " & _
            "2013 International Potato Center. All rights reserved"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox1.Visible = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.Panel5.Controls.Add(Me.Label_CrossingOptions)
        Me.Panel5.Controls.Add(Me.cboBarcodeForCross)
        Me.Panel5.Controls.Add(Me.Label8)
        Me.Panel5.Controls.Add(Me.Label_TypeCrossOption)
        Me.Panel5.Controls.Add(Me.ComboBox_TypeCrossOption)
        Me.Panel5.Location = New System.Drawing.Point(4, 168)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(233, 72)
        '
        'Label_CrossingOptions
        '
        Me.Label_CrossingOptions.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_CrossingOptions.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CrossingOptions.Location = New System.Drawing.Point(3, 3)
        Me.Label_CrossingOptions.Name = "Label_CrossingOptions"
        Me.Label_CrossingOptions.Size = New System.Drawing.Size(199, 17)
        Me.Label_CrossingOptions.Text = "Crossing options"
        '
        'cboBarcodeForCross
        '
        Me.cboBarcodeForCross.Items.Add("Collecting number")
        Me.cboBarcodeForCross.Items.Add("Collecting Number + Female number")
        Me.cboBarcodeForCross.Location = New System.Drawing.Point(92, 51)
        Me.cboBarcodeForCross.Name = "cboBarcodeForCross"
        Me.cboBarcodeForCross.Size = New System.Drawing.Size(137, 22)
        Me.cboBarcodeForCross.TabIndex = 29
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(4, 51)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(82, 22)
        Me.Label8.Text = "Barcode Type"
        '
        'Label_TypeCrossOption
        '
        Me.Label_TypeCrossOption.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_TypeCrossOption.Location = New System.Drawing.Point(4, 26)
        Me.Label_TypeCrossOption.Name = "Label_TypeCrossOption"
        Me.Label_TypeCrossOption.Size = New System.Drawing.Size(82, 22)
        Me.Label_TypeCrossOption.Text = "Type cross"
        '
        'ComboBox_TypeCrossOption
        '
        Me.ComboBox_TypeCrossOption.Location = New System.Drawing.Point(92, 26)
        Me.ComboBox_TypeCrossOption.Name = "ComboBox_TypeCrossOption"
        Me.ComboBox_TypeCrossOption.Size = New System.Drawing.Size(137, 22)
        Me.ComboBox_TypeCrossOption.TabIndex = 26
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.Panel4.Controls.Add(Me.ComboBox_Printer)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Controls.Add(Me.ComboBox_PrintType)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Controls.Add(Me.ListBox_BarcodeType)
        Me.Panel4.Controls.Add(Me.ListBox_BarcodeTypeID)
        Me.Panel4.Controls.Add(Me.Label_PrintOptions)
        Me.Panel4.Controls.Add(Me.Label_BarcodeType)
        Me.Panel4.Controls.Add(Me.ComboBox_BarcodeType)
        Me.Panel4.Controls.Add(Me.ListBox_Hide)
        Me.Panel4.Controls.Add(Me.Label_Port)
        Me.Panel4.Controls.Add(Me.ComboBox_Port)
        Me.Panel4.Location = New System.Drawing.Point(4, 24)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(233, 143)
        '
        'ComboBox_Printer
        '
        Me.ComboBox_Printer.Items.Add("RW 220")
        Me.ComboBox_Printer.Items.Add("ZQ 510")
        Me.ComboBox_Printer.Location = New System.Drawing.Point(111, 113)
        Me.ComboBox_Printer.Name = "ComboBox_Printer"
        Me.ComboBox_Printer.Size = New System.Drawing.Size(90, 22)
        Me.ComboBox_Printer.TabIndex = 62
        '
        'Label16
        '
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(24, 113)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(83, 20)
        Me.Label16.Text = "Printer"
        '
        'ComboBox_PrintType
        '
        Me.ComboBox_PrintType.Items.Add("Default label")
        Me.ComboBox_PrintType.Items.Add("Potato wild label")
        Me.ComboBox_PrintType.Items.Add("SweetPotato wild label")
        Me.ComboBox_PrintType.Items.Add("Collecting label")
        Me.ComboBox_PrintType.Location = New System.Drawing.Point(111, 85)
        Me.ComboBox_PrintType.Name = "ComboBox_PrintType"
        Me.ComboBox_PrintType.Size = New System.Drawing.Size(90, 22)
        Me.ComboBox_PrintType.TabIndex = 27
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(22, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 20)
        Me.Label1.Text = "Label type"
        '
        'ListBox_BarcodeType
        '
        Me.ListBox_BarcodeType.Location = New System.Drawing.Point(211, 24)
        Me.ListBox_BarcodeType.Name = "ListBox_BarcodeType"
        Me.ListBox_BarcodeType.Size = New System.Drawing.Size(19, 16)
        Me.ListBox_BarcodeType.TabIndex = 57
        Me.ListBox_BarcodeType.Visible = False
        '
        'ListBox_BarcodeTypeID
        '
        Me.ListBox_BarcodeTypeID.Location = New System.Drawing.Point(211, 46)
        Me.ListBox_BarcodeTypeID.Name = "ListBox_BarcodeTypeID"
        Me.ListBox_BarcodeTypeID.Size = New System.Drawing.Size(19, 16)
        Me.ListBox_BarcodeTypeID.TabIndex = 56
        Me.ListBox_BarcodeTypeID.Visible = False
        '
        'Label_PrintOptions
        '
        Me.Label_PrintOptions.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_PrintOptions.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_PrintOptions.Location = New System.Drawing.Point(3, 3)
        Me.Label_PrintOptions.Name = "Label_PrintOptions"
        Me.Label_PrintOptions.Size = New System.Drawing.Size(199, 19)
        Me.Label_PrintOptions.Text = "Print options"
        '
        'Label_BarcodeType
        '
        Me.Label_BarcodeType.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_BarcodeType.Location = New System.Drawing.Point(24, 30)
        Me.Label_BarcodeType.Name = "Label_BarcodeType"
        Me.Label_BarcodeType.Size = New System.Drawing.Size(83, 20)
        Me.Label_BarcodeType.Text = "Barcode type"
        '
        'ComboBox_BarcodeType
        '
        Me.ComboBox_BarcodeType.Location = New System.Drawing.Point(112, 30)
        Me.ComboBox_BarcodeType.Name = "ComboBox_BarcodeType"
        Me.ComboBox_BarcodeType.Size = New System.Drawing.Size(33, 22)
        Me.ComboBox_BarcodeType.TabIndex = 1
        '
        'ListBox_Hide
        '
        Me.ListBox_Hide.Location = New System.Drawing.Point(211, 3)
        Me.ListBox_Hide.Name = "ListBox_Hide"
        Me.ListBox_Hide.Size = New System.Drawing.Size(19, 16)
        Me.ListBox_Hide.TabIndex = 39
        Me.ListBox_Hide.Visible = False
        '
        'Label_Port
        '
        Me.Label_Port.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Port.Location = New System.Drawing.Point(24, 58)
        Me.Label_Port.Name = "Label_Port"
        Me.Label_Port.Size = New System.Drawing.Size(81, 20)
        Me.Label_Port.Text = "Port"
        '
        'ComboBox_Port
        '
        Me.ComboBox_Port.Items.Add("COM1:")
        Me.ComboBox_Port.Items.Add("COM2:")
        Me.ComboBox_Port.Items.Add("COM3:")
        Me.ComboBox_Port.Items.Add("COM4:")
        Me.ComboBox_Port.Items.Add("COM5:")
        Me.ComboBox_Port.Items.Add("COM6:")
        Me.ComboBox_Port.Items.Add("COM7:")
        Me.ComboBox_Port.Items.Add("COM8:")
        Me.ComboBox_Port.Items.Add("COM9:")
        Me.ComboBox_Port.Items.Add("COM10:")
        Me.ComboBox_Port.Location = New System.Drawing.Point(112, 58)
        Me.ComboBox_Port.Name = "ComboBox_Port"
        Me.ComboBox_Port.Size = New System.Drawing.Size(60, 22)
        Me.ComboBox_Port.TabIndex = 6
        '
        'Label_OptionsTitle
        '
        Me.Label_OptionsTitle.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_OptionsTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_OptionsTitle.Location = New System.Drawing.Point(7, 1)
        Me.Label_OptionsTitle.Name = "Label_OptionsTitle"
        Me.Label_OptionsTitle.Size = New System.Drawing.Size(226, 20)
        Me.Label_OptionsTitle.Text = "Options"
        Me.Label_OptionsTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TabPage_Options2
        '
        Me.TabPage_Options2.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.TabPage_Options2.Controls.Add(Me.Panel6)
        Me.TabPage_Options2.Controls.Add(Me.Label10)
        Me.TabPage_Options2.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_Options2.Name = "TabPage_Options2"
        Me.TabPage_Options2.Size = New System.Drawing.Size(232, 242)
        Me.TabPage_Options2.Text = "Options 2"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.Panel6.Controls.Add(Me.Label3)
        Me.Panel6.Controls.Add(Me.TextBox_PasswordDB)
        Me.Panel6.Controls.Add(Me.Label_ExportDB)
        Me.Panel6.Controls.Add(Me.Button_DeleteDB)
        Me.Panel6.Controls.Add(Me.Label_DBOptions)
        Me.Panel6.Controls.Add(Me.Button_ExportDB)
        Me.Panel6.Controls.Add(Me.chkPrintHarvestGeneral)
        Me.Panel6.Controls.Add(Me.ChkDateHarvest)
        Me.Panel6.Controls.Add(Me.dtpHarvestFin)
        Me.Panel6.Controls.Add(Me.dtpHarvestIni)
        Me.Panel6.Controls.Add(Me.Label11)
        Me.Panel6.Location = New System.Drawing.Point(3, 28)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(234, 214)
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(8, 164)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(226, 20)
        Me.Label3.Text = "Enter the password to remove the database"
        '
        'TextBox_PasswordDB
        '
        Me.TextBox_PasswordDB.Location = New System.Drawing.Point(4, 186)
        Me.TextBox_PasswordDB.Name = "TextBox_PasswordDB"
        Me.TextBox_PasswordDB.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBox_PasswordDB.Size = New System.Drawing.Size(138, 21)
        Me.TextBox_PasswordDB.TabIndex = 73
        '
        'Label_ExportDB
        '
        Me.Label_ExportDB.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label_ExportDB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_ExportDB.Location = New System.Drawing.Point(8, 122)
        Me.Label_ExportDB.Name = "Label_ExportDB"
        Me.Label_ExportDB.Size = New System.Drawing.Size(110, 49)
        Me.Label_ExportDB.Text = "Export the database to the path:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "\My Documents\"
        '
        'Button_DeleteDB
        '
        Me.Button_DeleteDB.BackColor = System.Drawing.Color.Red
        Me.Button_DeleteDB.Enabled = False
        Me.Button_DeleteDB.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_DeleteDB.ForeColor = System.Drawing.Color.White
        Me.Button_DeleteDB.Location = New System.Drawing.Point(147, 184)
        Me.Button_DeleteDB.Name = "Button_DeleteDB"
        Me.Button_DeleteDB.Size = New System.Drawing.Size(82, 23)
        Me.Button_DeleteDB.TabIndex = 72
        Me.Button_DeleteDB.Text = "Delete DB"
        '
        'Label_DBOptions
        '
        Me.Label_DBOptions.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_DBOptions.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_DBOptions.Location = New System.Drawing.Point(8, 100)
        Me.Label_DBOptions.Name = "Label_DBOptions"
        Me.Label_DBOptions.Size = New System.Drawing.Size(199, 22)
        Me.Label_DBOptions.Text = "Data Base options"
        '
        'Button_ExportDB
        '
        Me.Button_ExportDB.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_ExportDB.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Button_ExportDB.ForeColor = System.Drawing.Color.White
        Me.Button_ExportDB.Location = New System.Drawing.Point(130, 122)
        Me.Button_ExportDB.Name = "Button_ExportDB"
        Me.Button_ExportDB.Size = New System.Drawing.Size(98, 30)
        Me.Button_ExportDB.TabIndex = 70
        Me.Button_ExportDB.Text = "Export DB"
        '
        'chkPrintHarvestGeneral
        '
        Me.chkPrintHarvestGeneral.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.chkPrintHarvestGeneral.Location = New System.Drawing.Point(5, 83)
        Me.chkPrintHarvestGeneral.Name = "chkPrintHarvestGeneral"
        Me.chkPrintHarvestGeneral.Size = New System.Drawing.Size(197, 20)
        Me.chkPrintHarvestGeneral.TabIndex = 38
        Me.chkPrintHarvestGeneral.Text = "Print Harvest General"
        '
        'ChkDateHarvest
        '
        Me.ChkDateHarvest.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ChkDateHarvest.Location = New System.Drawing.Point(3, 23)
        Me.ChkDateHarvest.Name = "ChkDateHarvest"
        Me.ChkDateHarvest.Size = New System.Drawing.Size(106, 20)
        Me.ChkDateHarvest.TabIndex = 36
        Me.ChkDateHarvest.Text = "Date Harvest"
        '
        'dtpHarvestFin
        '
        Me.dtpHarvestFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHarvestFin.Location = New System.Drawing.Point(117, 49)
        Me.dtpHarvestFin.Name = "dtpHarvestFin"
        Me.dtpHarvestFin.Size = New System.Drawing.Size(85, 22)
        Me.dtpHarvestFin.TabIndex = 33
        '
        'dtpHarvestIni
        '
        Me.dtpHarvestIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHarvestIni.Location = New System.Drawing.Point(117, 21)
        Me.dtpHarvestIni.Name = "dtpHarvestIni"
        Me.dtpHarvestIni.Size = New System.Drawing.Size(86, 22)
        Me.dtpHarvestIni.TabIndex = 32
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(3, 1)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(199, 17)
        Me.Label11.Text = "Harvest options"
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(8, 5)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(226, 20)
        Me.Label10.Text = "Options 2"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel_Message
        '
        Me.Panel_Message.BackColor = System.Drawing.SystemColors.Info
        Me.Panel_Message.Controls.Add(Me.Label_Message)
        Me.Panel_Message.Controls.Add(Me.Button_Close)
        Me.Panel_Message.Location = New System.Drawing.Point(0, 1)
        Me.Panel_Message.Name = "Panel_Message"
        Me.Panel_Message.Size = New System.Drawing.Size(1, 1)
        Me.Panel_Message.Visible = False
        '
        'Label_Message
        '
        Me.Label_Message.BackColor = System.Drawing.SystemColors.Info
        Me.Label_Message.ForeColor = System.Drawing.Color.Red
        Me.Label_Message.Location = New System.Drawing.Point(8, 4)
        Me.Label_Message.Name = "Label_Message"
        Me.Label_Message.Size = New System.Drawing.Size(225, 206)
        '
        'Button_Close
        '
        Me.Button_Close.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_Close.ForeColor = System.Drawing.Color.White
        Me.Button_Close.Location = New System.Drawing.Point(84, 223)
        Me.Button_Close.Name = "Button_Close"
        Me.Button_Close.Size = New System.Drawing.Size(72, 20)
        Me.Button_Close.TabIndex = 1
        Me.Button_Close.Text = "Close"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Tuber
        '
        Me.Tuber.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.Tuber.Location = New System.Drawing.Point(0, 0)
        Me.Tuber.Name = "Tuber"
        Me.Tuber.Size = New System.Drawing.Size(240, 245)
        Me.Tuber.Text = "TabPage_Harvest"
        '
        'TextBox_SearchValueTuberHarvest
        '
        Me.TextBox_SearchValueTuberHarvest.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox_SearchValueTuberHarvest.Location = New System.Drawing.Point(7, 22)
        Me.TextBox_SearchValueTuberHarvest.Name = "TextBox_SearchValueTuberHarvest"
        Me.TextBox_SearchValueTuberHarvest.Size = New System.Drawing.Size(84, 19)
        Me.TextBox_SearchValueTuberHarvest.TabIndex = 100
        '
        'btnSearchTubHarv
        '
        Me.btnSearchTubHarv.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnSearchTubHarv.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.btnSearchTubHarv.ForeColor = System.Drawing.Color.White
        Me.btnSearchTubHarv.Location = New System.Drawing.Point(95, 21)
        Me.btnSearchTubHarv.Name = "btnSearchTubHarv"
        Me.btnSearchTubHarv.Size = New System.Drawing.Size(22, 20)
        Me.btnSearchTubHarv.TabIndex = 99
        Me.btnSearchTubHarv.Text = "S"
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.TextBox3.Location = New System.Drawing.Point(84, 135)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(93, 19)
        Me.TextBox3.TabIndex = 113
        '
        'TabPage1
        '
        Me.TabPage1.Location = New System.Drawing.Point(0, 0)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(240, 245)
        Me.TabPage1.Text = "TabPage_View"
        '
        'ResultsTableAdapter1
        '
        Me.ResultsTableAdapter1.ClearBeforeFill = True
        '
        'Form_Cross
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 268)
        Me.Controls.Add(Me.Panel_Message)
        Me.Controls.Add(Me.TabControl)
        Me.Menu = Me.mainMenu1
        Me.Name = "Form_Cross"
        Me.Text = "CIPCROSS SYSTEM"
        Me.TabControl.ResumeLayout(False)
        Me.TabPage_User.ResumeLayout(False)
        Me.Panel_SelectCrossPlan.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TabPage_Parentals.ResumeLayout(False)
        Me.TabPage_View.ResumeLayout(False)
        Me.TabPage_Crossing.ResumeLayout(False)
        Me.TabPage_Harvest.ResumeLayout(False)
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Results_DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage_Harvest_Tuber.ResumeLayout(False)
        Me.TabPage_Maceration.ResumeLayout(False)
        Me.TabPage_Options.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.TabPage_Options2.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel_Message.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl As System.Windows.Forms.TabControl
    Friend WithEvents TabPage_User As System.Windows.Forms.TabPage
    Friend WithEvents ComboBox_Language As System.Windows.Forms.ComboBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents TabPage_Parentals As System.Windows.Forms.TabPage
    Friend WithEvents Button_PrintMales As System.Windows.Forms.Button
    Friend WithEvents TabPage_Options As System.Windows.Forms.TabPage
    Friend WithEvents Label_BarcodeType As System.Windows.Forms.Label
    Friend WithEvents ComboBox_BarcodeType As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox_Port As System.Windows.Forms.ComboBox
    Friend WithEvents Label_Port As System.Windows.Forms.Label
    Friend WithEvents TabPage_Crossing As System.Windows.Forms.TabPage
    Friend WithEvents Label_Parentals As System.Windows.Forms.Label
    Friend WithEvents Label_Crossing As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_Flowers As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox_Order As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Cross As System.Windows.Forms.TextBox
    Friend WithEvents Button_PrintCross As System.Windows.Forms.Button
    Friend WithEvents Label_CrossingDate As System.Windows.Forms.Label
    Friend WithEvents Label_Flowers As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker_Male As System.Windows.Forms.DateTimePicker
    Friend WithEvents ComboBox_TypeCross As System.Windows.Forms.ComboBox
    Friend WithEvents Label_TypeCross As System.Windows.Forms.Label
    Friend WithEvents Label_TypeCrossOption As System.Windows.Forms.Label
    Friend WithEvents ComboBox_TypeCrossOption As System.Windows.Forms.ComboBox
    Friend WithEvents DateTimePicker_Cross As System.Windows.Forms.DateTimePicker
    Friend WithEvents ListBox_Hide As System.Windows.Forms.ListBox
    Friend WithEvents TabPage_Harvest As System.Windows.Forms.TabPage
    Friend WithEvents Label_HarvestTitle As System.Windows.Forms.Label
    Friend WithEvents C1FlexGrid_Harvest As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents Button_SearchHarvest As System.Windows.Forms.Button
    Friend WithEvents TextBox_SearchHarvest As System.Windows.Forms.TextBox
    Friend WithEvents Button_SelectHarvest As System.Windows.Forms.Button
    Friend WithEvents ComboBox_FruitSize As System.Windows.Forms.ComboBox
    Friend WithEvents Button_PrintHarvest As System.Windows.Forms.Button
    Friend WithEvents Label_HarvestDate As System.Windows.Forms.Label
    Friend WithEvents Label_FruitSize As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker_Harvest As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox_Harvest As System.Windows.Forms.TextBox
    Friend WithEvents Button_LoadHarvest As System.Windows.Forms.Button
    Friend WithEvents Label_CountHarvest As System.Windows.Forms.Label
    Friend WithEvents TabPage_Maceration As System.Windows.Forms.TabPage
    Friend WithEvents Label_MacerationTitle As System.Windows.Forms.Label
    Friend WithEvents Button_PrintMaceration As System.Windows.Forms.Button
    Friend WithEvents Label_MacerationDate As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker_Maceration As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label_CountMaceration As System.Windows.Forms.Label
    Friend WithEvents TextBox_Maceration As System.Windows.Forms.TextBox
    Friend WithEvents Button_SelectMaceration As System.Windows.Forms.Button
    Friend WithEvents C1FlexGrid_Maceration As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents Button_LoadMaceration As System.Windows.Forms.Button
    Friend WithEvents Button_SearchMaceration As System.Windows.Forms.Button
    Friend WithEvents TextBox_SearchMaceration As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button_CrossingPlan As System.Windows.Forms.Button
    Friend WithEvents TextBox_CrossingPlan As System.Windows.Forms.TextBox
    Friend WithEvents C1FlexGrid_Male As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents Button_SearchMale As System.Windows.Forms.Button
    Friend WithEvents C1FlexGrid_MaleCross As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents C1FlexGrid_FemaleCross As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents Label_Order As System.Windows.Forms.Label
    Friend WithEvents TextBox_Data3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Data2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_IndexHarvest As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_IndexMaceration As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label_Languaje As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label_CrossingPlan As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox_Location As System.Windows.Forms.ComboBox
    Friend WithEvents Label_Location As System.Windows.Forms.Label
    Friend WithEvents TextBox_Password As System.Windows.Forms.TextBox
    Friend WithEvents Label_Password As System.Windows.Forms.Label
    Friend WithEvents Label_Username As System.Windows.Forms.Label
    Friend WithEvents Label_SignIn As System.Windows.Forms.Label
    Friend WithEvents Button_SignIn As System.Windows.Forms.Button
    Friend WithEvents ComboBox_Username As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents ListBox_Password As System.Windows.Forms.ListBox
    Friend WithEvents ListBox_Username As System.Windows.Forms.ListBox
    Friend WithEvents ListBox_FullName As System.Windows.Forms.ListBox
    Friend WithEvents Label_OptionsTitle As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label_CrossingOptions As System.Windows.Forms.Label
    Friend WithEvents Label_PrintOptions As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ListBox_BarcodeTypeID As System.Windows.Forms.ListBox
    Friend WithEvents ListBox_BarcodeType As System.Windows.Forms.ListBox
    Friend WithEvents ListBox_MaleLabels As System.Windows.Forms.ListBox
    Friend WithEvents Button_SearchMaleCross As System.Windows.Forms.Button
    Friend WithEvents Button_SearchFemaleCross As System.Windows.Forms.Button
    Friend WithEvents TextBox_SearchMaleCross As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_SearchFemaleCross As System.Windows.Forms.TextBox
    Friend WithEvents Label_CrossingTitle As System.Windows.Forms.Label
    Friend WithEvents TextBox_SearchMale As System.Windows.Forms.TextBox
    Friend WithEvents ListBox_FemaleLabels As System.Windows.Forms.ListBox
    Friend WithEvents Button_SearchFemale As System.Windows.Forms.Button
    Friend WithEvents TextBox_SearchFemale As System.Windows.Forms.TextBox
    Friend WithEvents Label_CountFemales As System.Windows.Forms.Label
    Friend WithEvents C1FlexGrid_Female As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents DateTimePicker_Female As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button_PrintFemales As System.Windows.Forms.Button
    Friend WithEvents Label_CountMales As System.Windows.Forms.Label
    Friend WithEvents Panel_Message As System.Windows.Forms.Panel
    Friend WithEvents Label_Message As System.Windows.Forms.Label
    Friend WithEvents Button_Close As System.Windows.Forms.Button
    Friend WithEvents TextBox_HideNroRepetitions As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_HideNroRepetitions2 As System.Windows.Forms.TextBox
    Friend WithEvents Label_ParentalsMales As System.Windows.Forms.Label
    Friend WithEvents Label_ParentalsFemales As System.Windows.Forms.Label
    Friend WithEvents Label_CountMalesCross As System.Windows.Forms.Label
    Friend WithEvents Label_CountFemalesCross As System.Windows.Forms.Label
    Friend WithEvents Label_CrossingMales As System.Windows.Forms.Label
    Friend WithEvents Label_CrossingFemales As System.Windows.Forms.Label
    Friend WithEvents TextBox_NroRepetitions As System.Windows.Forms.TextBox
    Friend WithEvents Panel_SelectCrossPlan As System.Windows.Forms.Panel
    Friend WithEvents ComboBox_SelectCrossingPlan As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button_Cancel As System.Windows.Forms.Button
    Friend WithEvents Button_SelectPlan As System.Windows.Forms.Button
    Friend WithEvents TextBox_CrossingPlanSelected As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_CrossingPlanSelectedHide As System.Windows.Forms.TextBox
    Friend WithEvents ListBox_SelectPlan As System.Windows.Forms.ListBox
    Friend WithEvents ComboBox_PrintType As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_NroFruits As System.Windows.Forms.ComboBox
    Friend WithEvents Label_Fruits As System.Windows.Forms.Label
    Friend WithEvents C1FlexGrid_ValuesCross As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents ComboBox_GenMale As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox_GenFemale As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox_SearchValueCross As System.Windows.Forms.TextBox
    Friend WithEvents Button_Search_ValueCross As System.Windows.Forms.Button
    Friend WithEvents lblPor As System.Windows.Forms.Label
    Friend WithEvents lblSearchCrossing As System.Windows.Forms.Label
    Friend WithEvents lblMale As System.Windows.Forms.Label
    Friend WithEvents lblFemale As System.Windows.Forms.Label
    Friend WithEvents Button_SelectCross As System.Windows.Forms.Button
    Friend WithEvents ComboBox_TotalFlowers As System.Windows.Forms.ComboBox
    Friend WithEvents lblNroFloresG As System.Windows.Forms.Label
    Friend WithEvents TextBox_Comments As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Quantity As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Weight1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Weight As System.Windows.Forms.TextBox
    Friend WithEvents cboBarcodeForCross As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TabPage_Options2 As System.Windows.Forms.TabPage
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtpHarvestFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHarvestIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents ChkDateHarvest As System.Windows.Forms.CheckBox
    Friend WithEvents txtHarvest_Potato As System.Windows.Forms.TextBox
    Friend WithEvents chkPrintHarvestGeneral As System.Windows.Forms.CheckBox
    Friend WithEvents TabPage_Harvest_Tuber As System.Windows.Forms.TabPage
    Friend WithEvents Tuber As System.Windows.Forms.TabPage
    Friend WithEvents cboFemaleTuber As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox_SearchValue As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchTuberculusHarvest As System.Windows.Forms.Button
    Friend WithEvents lblValueTuberculusHarvest As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents C1FlexGrid_ValuesTubHarv As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents lblTuberHarvestTitle As System.Windows.Forms.Label
    Friend WithEvents btnPrintTuberHarvest As System.Windows.Forms.Button
    Friend WithEvents btnSelectTuberHarv As System.Windows.Forms.Button
    Friend WithEvents lblNroTuber As System.Windows.Forms.Label
    Friend WithEvents ComboBox_NroTubers As System.Windows.Forms.ComboBox
    Friend WithEvents cboProcedence As System.Windows.Forms.ComboBox
    Friend WithEvents lblProcedence As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCosechaTuber As System.Windows.Forms.TextBox
    Friend WithEvents lblColector As System.Windows.Forms.Label
    Friend WithEvents TextBox_SearchValueTuberHarvest As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchTubHarv As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents ResultsTableAdapter1 As DeviceApplication1.Results_DataSetTableAdapters.resultsTableAdapter
    Friend WithEvents Results_DataSet1 As DeviceApplication1.Results_DataSet
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox_PasswordDB As System.Windows.Forms.TextBox
    Friend WithEvents Label_ExportDB As System.Windows.Forms.Label
    Friend WithEvents Button_DeleteDB As System.Windows.Forms.Button
    Friend WithEvents Label_DBOptions As System.Windows.Forms.Label
    Friend WithEvents Button_ExportDB As System.Windows.Forms.Button
    Friend WithEvents TabPage_View As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents C1FlexGrid1 As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_Printer As System.Windows.Forms.ComboBox
End Class