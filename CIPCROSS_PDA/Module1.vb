﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Module Module1
    Public intRowStart = 10
    Public intColumnStart = 5
    Public intNroMaxColumnsResults = 41

    'Positions of results
    Public intPositionF = 11
    Public intPositionM = 12
    Public intPosF1 = 0
    Public intPosF2 = 1
    Public intPosF3 = 2
    Public intPosM1 = 5
    Public intPosM2 = 6
    Public intPosM3 = 7
    Public intPosTypeCross = 10
    Public intPosRepetitions = 16
    Public intPosFlower As Integer = 13

    Public intPosHarvestResume = 19
    Public intPosTotalFlowers = 20
    Public intPosFruits As Integer = 21

    Public intPosDatePrintCross = 17
    Public intPosCrossingDate = 14
    Public intPosHarvestDate = 22
    Public intPosMacerationDate = 34
    Public intPosUser1 = 37
    Public intPosUser2 = 38
    Public intPosUser3 = 39
    Public intPosLocation1 = 15
    Public intPosLocation2 = 23
    Public intPosNroPlan = 40
    Public intPosPlanName = 41



    Public strMessage1 = ""
    Public strMessage2 = ""
    Public strMessage3 = ""
    Public strMessage4 = ""
    Public strMessage5 = ""
    Public strMessage6 = ""
    Public strMessage7 = ""
    Public strMessage8 = ""
    Public strMessage9 = ""
    Public strMessage10 = ""
    Public strMessage11 = ""
    Public strMessage12 = ""
    Public strMessage13 = ""
    Public strMessage14 = ""
    Public strMessage15 = ""
    Public strMessage16 = ""
    Public strMessage17 = ""
    Public strMessage18 = ""
    Public strMessage19 = ""
    Public strMessage20 = ""
    Public strMessage21 = ""
    Public strMessage22 = ""
    Public strMessage23 = ""
    Public strMessage24 = ""
    Public strMessage25 = ""

    Sub First()
    End Sub

    Public Sub ReadIniSetup(ByVal strPath As String, ByVal strFile As String)
        Dim oRead As System.IO.StreamReader
        Dim strLine, arrLine() As String

        oRead = File.OpenText(strPath + strFile)
        While oRead.Peek <> -1
            strLine = Trim(oRead.ReadLine())
            If strLine <> "" Then
                arrLine = Split(strLine, "=")
                If UBound(arrLine) = 1 Then
                    If "[SERVER]" = UCase(Trim(arrLine(0))) Then
                        strServer = Trim(arrLine(1))
                    End If
                    If "[DB]" = UCase(Trim(arrLine(0))) Then
                        strDB = Trim(arrLine(1))
                    End If
                    If "[USER]" = UCase(Trim(arrLine(0))) Then
                        strUser = Trim(arrLine(1))
                    End If
                    If "[PASSWORD]" = UCase(Trim(arrLine(0))) Then
                        strPassword = Trim(arrLine(1))
                    End If
                    If "[OTHERLANGUAGE]" = UCase(Trim(arrLine(0))) Then
                        strOtherLanguage = Trim(arrLine(1))
                    End If
                    If "[DESCRIPTORS]" = UCase(Trim(arrLine(0))) Then
                        strDescriptors = Trim(arrLine(1))
                    End If
                End If
            End If
        End While

        strConnection = "data source=" + strServer + "; initial catalog=" + strDB + ";password=" + strPassword + ";user id=" + strUser

        oRead.Close()
    End Sub

    Public Function FormUpdateSQL(ByVal strTypeValue As String, ByVal strValue As String, ByVal strField As String, ByVal strSQL As String) As String
        Dim strSQL2 As String

        strSQL = Trim(strSQL)
        If strSQL <> "" Then
            strSQL = strSQL + ", "
        End If

        If LCase(strTypeValue) = "n" Then '' para campo numerico
            If strValue = "" Then
                strSQL = strSQL + " " + strField + "= null "
            Else
                strSQL = strSQL + " " + strField + " =" + strValue + ""
            End If
        End If
        If LCase(strTypeValue) = "s" Then '' para campo cadena
            If strValue = "" Then
                strSQL = strSQL + " " + strField + "= null "
            Else
                strSQL = strSQL + " " + strField + " ='" + strValue + "'"
            End If
        End If
        If LCase(strTypeValue) = "d" Then '' para campo fecha
            If strValue = "" Then
                strSQL = strSQL + " " + strField + "= null "
            Else
                strSQL = strSQL + " " + strField + " =convert(datetime,'" + strValue + "',101)"
            End If
        End If

        FormUpdateSQL = strSQL
    End Function

    Public Function FormInsertSQL2(ByVal strTypeValue As String, ByVal strValue As String, ByVal strField As String, ByVal strSQL As String) As String
        Dim strTag As String = ") values ("
        Dim intPos, intPos2 As Integer

        strSQL = Trim(strSQL)

        If Trim(strSQL) = "" Then
            If strTypeValue = "n" Then
                If strValue = "" Then
                    strSQL = "(" + strField + strTag + " null )"
                Else
                    strSQL = "(" + strField + strTag + strValue + " )"
                End If
            End If
            If strTypeValue = "s" Then
                If strValue = "" Then
                    strSQL = "(" + strField + strTag + " null )"
                Else
                    strSQL = "(" + strField + strTag + "'" + strValue + "' )"
                End If
            End If
            If strTypeValue = "d" Then
                If strValue = "" Then
                    strSQL = "(" + strField + strTag + " null )"
                Else
                    strSQL = "(" + strField + strTag + " convert(datetime,'" + strValue + "',101)" + " )"
                End If
            End If
        Else
            intPos = strSQL.IndexOf(strTag, 0)
            If strTypeValue = "n" Then
                If strValue = "" Then
                    strSQL = strSQL.Insert(intPos, ", " + strField)
                    intPos2 = strSQL.Length - 1
                    strSQL = strSQL.Insert(intPos2, ", " + "null")
                Else
                    strSQL = strSQL.Insert(intPos, ", " + strField)
                    intPos2 = strSQL.Length - 1
                    strSQL = strSQL.Insert(intPos2, ", " + strValue)
                End If
            End If
            If strTypeValue = "s" Then
                If strValue = "" Then
                    strSQL = strSQL.Insert(intPos, ", " + strField)
                    intPos2 = strSQL.Length - 1
                    strSQL = strSQL.Insert(intPos2, ", " + "null")
                Else
                    strSQL = strSQL.Insert(intPos, ", " + strField)
                    intPos2 = strSQL.Length - 1
                    strSQL = strSQL.Insert(intPos2, ", '" + strValue + "'")
                End If
            End If
            If strTypeValue = "d" Then
                If strValue = "" Then
                    strSQL = strSQL.Insert(intPos, ", " + strField)
                    intPos2 = strSQL.Length - 1
                    strSQL = strSQL.Insert(intPos2, ", " + "null")
                Else
                    strSQL = strSQL.Insert(intPos, ", " + strField)
                    intPos2 = strSQL.Length - 1
                    strSQL = strSQL.Insert(intPos2, ", " + " convert(datetime,'" + strValue + "',101)" + " )")
                End If
            End If
        End If

        FormInsertSQL2 = strSQL
    End Function

    Public Function FormInsertSQL(ByVal strTypeValue As String, ByVal strValue As String, ByVal strSQL As String) As String
        If strTypeValue = "n" Then
            If strValue = "" Then
                strSQL = strSQL + " " + "null, "
            Else
                strSQL = strSQL + " " + strValue + ","
            End If
        End If
        If strTypeValue = "s" Then
            If strValue = "" Then
                strSQL = strSQL + " " + "null, "
            Else
                strSQL = strSQL + " '" + strValue + "',"
            End If
        End If
        If strTypeValue = "d" Then
            If strValue = "" Then
                strSQL = strSQL + " " + "null, "
            Else
                strSQL = strSQL + " convert(datetime,'" + strValue + "',101),"
            End If
        End If
        FormInsertSQL = strSQL
    End Function

    Public Function AssignValueToText(ByVal objTxt As TextBox, ByVal objDr As SqlDataReader, ByVal strField As String)
        If IsNull(objDr, strField, True) Then
            objTxt.Text = ""
        Else
            objTxt.Text = Trim(objDr(strField))
        End If
    End Function

    Public Function IsNull(ByVal objDr As SqlDataReader, ByVal strCampo As String, ByVal bolUseDtr As Boolean) As Boolean
        If bolUseDtr = True Then
            If objDr(strCampo) Is System.DBNull.Value Then
                IsNull = True
            Else
                If Trim(objDr(strCampo)) = "" Then
                    IsNull = True
                Else
                    IsNull = False
                End If
            End If
        Else
            If Trim(strCampo) = "" Then
                IsNull = True
            Else
                IsNull = False
            End If
        End If
    End Function

    Public Sub Llenar_text(ByVal strSQL As String, ByVal objtxt As TextBox, ByVal strCampo1 As String)
        Dim objCon As New SqlConnection(strConnection)
        Dim objCmdselect As New SqlCommand(strSQL, objCon)
        Dim dtrDpassport As SqlDataReader
        objCon.Open()
        dtrDpassport = objCmdselect.ExecuteReader()
        While dtrDpassport.Read()
            objtxt.Text = ""
            objtxt.Text = Trim(dtrDpassport(strCampo1))
        End While
        dtrDpassport.Close()
        objCon.Close()
    End Sub

    Public Function ReturnDataReader(ByVal strSQL As String, ByRef objCon As SqlConnection) As SqlClient.SqlDataReader
        Dim strDataSource As String

        Try
            strDataSource = objCon.DataSource()
            If (strDataSource) = "" Then
                objCon = New SqlConnection(strConnection)
            End If
        Catch
            objCon = New SqlConnection(strConnection)
        End Try

        Dim objSqlCommand As New SqlCommand(strSQL, objCon)
        Try
            If Not objCon.State = ConnectionState.Open Then
                objCon.Open()
            End If

            objSqlCommand.CommandTimeout = intTimeOut
            ReturnDataReader = objSqlCommand.ExecuteReader()
        Catch o As SqlException
            MsgBox(o.Message)
        End Try
    End Function

    Public Function DateTodayServer() As String
        Dim strSQL As String
        Dim objCon As New SqlConnection(strConnection)
        Dim objCmdselect As New SqlCommand(strSQL, objCon)
        Dim dtrReader As SqlDataReader

        strSQL = "select convert(char(10),getdate(),101)"

        objCon.Open()
        objCmdselect.CommandText = strSQL
        dtrReader = objCmdselect.ExecuteReader
        If dtrReader.Read() Then
            DateTodayServer = Trim(dtrReader(0))
        End If
        'DateTodayServer = Trim(objCmdselect.ExecuteScalar)

        objCmdselect = Nothing
        dtrReader.Close()
        dtrReader = Nothing
        objCon.Close()
        objCon = Nothing
    End Function

    Public Function DateTimeTodayServer() As String
        Dim strSQL As String
        Dim objCon As New SqlConnection(strConnection)
        Dim objCmdselect As New SqlCommand(strSQL, objCon)
        Dim dtrReader As SqlDataReader

        strSQL = "select convert(char(10),getdate(),101)+' '+convert(char(8),getdate(),108)"

        objCon.Open()
        objCmdselect.CommandText = strSQL
        dtrReader = objCmdselect.ExecuteReader
        If dtrReader.Read() Then
            DateTimeTodayServer = Trim(dtrReader(0))
        End If
        'DateTodayServer = Trim(objCmdselect.ExecuteScalar)

        objCmdselect = Nothing
        dtrReader.Close()
        dtrReader = Nothing
        objCon.Close()
        objCon = Nothing
    End Function

    Public Sub LoadCombo(ByVal ShowCode As Boolean, ByVal strSQL As String, ByVal objCombo As ComboBox, ByVal strCampo1 As String, ByVal strCampo2 As String, Optional ByVal intShowOnlyAll As Int16 = 0, Optional ByVal intAddAll As Int16 = 0, Optional ByVal bolAddBlanks As Boolean = False, Optional ByVal bolAddToTheEnd As Boolean = False, Optional ByVal strSQL2 As String = "")
        'intShowOnlyAll = 0 visualiza (empty)
        '                 1 visualiza (All)
        Dim objCon As New SqlConnection(strConnection)
        Dim objCmdselect As New SqlCommand(strSQL, objCon)
        Dim dtrReader As SqlDataReader
        Dim bolFoundRecords As Boolean

        If bolAddToTheEnd = False Then
            objCombo.Items.Clear()

            If intShowOnlyAll = 0 Then
                objCombo.Items.Add("")
                objCombo.SelectedIndex = objCombo.Items.Count - 1
                objCombo.SelectedValue = "(empty)"
            Else
                objCombo.Items.Add("")
                objCombo.SelectedIndex = objCombo.Items.Count - 1
                objCombo.SelectedValue = "(All)"
            End If

            ''intAddAll =1 visualiza All 
            If intAddAll = 1 Then
                objCombo.Items.Add("All" + Space(200) + "all")
                objCombo.SelectedIndex = objCombo.Items.Count - 1
            End If
        End If

        Try
            objCon.Open()
            objCmdselect.CommandTimeout = intTimeOut
            dtrReader = objCmdselect.ExecuteReader()
        Catch o As SqlException
            MsgBox("Error: " + o.Message)
        End Try

        bolFoundRecords = False
        While dtrReader.Read()
            bolFoundRecords = True
            If Not ShowCode Then
                If bolAddBlanks = True Then
                    objCombo.Items.Add(Space(300))
                End If
                objCombo.Items.Add(Trim(dtrReader(strCampo2)) + Space(200) + Trim(dtrReader(strCampo1)))
            Else
                objCombo.Items.Add(Trim(dtrReader(strCampo1)) + " -> " + Trim(dtrReader(strCampo2)))
            End If
            objCombo.SelectedIndex = objCombo.Items.Count - 1
        End While

        If bolFoundRecords = False And strSQL2 <> "" Then
            dtrReader.Close()
            objCmdselect.CommandText = strSQL2
            dtrReader = objCmdselect.ExecuteReader()

            While dtrReader.Read()
                If Not ShowCode Then
                    If bolAddBlanks = True Then
                        objCombo.Items.Add(Space(300))
                    End If
                    objCombo.Items.Add(Trim(dtrReader(strCampo2)) + Space(200) + Trim(dtrReader(strCampo1)))
                Else
                    objCombo.Items.Add(Trim(dtrReader(strCampo1)) + " -> " + Trim(dtrReader(strCampo2)))
                End If
                objCombo.SelectedIndex = objCombo.Items.Count - 1
            End While
        End If



        objCombo.SelectedIndex = 0
        dtrReader.Close()
        objCon.Close()
    End Sub

    Public Function Fill_Left(ByVal Valor As String, ByVal Tamano As Integer, ByVal Car As String)
        Dim miTamano, i As Integer
        Dim cadena As String
        cadena = Valor
        miTamano = Len(Trim(cadena))
        For i = 1 To (Tamano - miTamano)
            cadena = Car & cadena
        Next i
        Fill_Left = cadena
    End Function

    Public Function SearchCombo(ByVal strValue As String, ByVal bolForUpdate As Boolean, ByVal bolFirstValue As Boolean) As String

        If bolForUpdate = False Then
            If bolFirstValue = True Then
                strValue = Trim(Mid(strValue, 1, 50))
            Else
                strValue = Trim(Mid(strValue, 200))
            End If

            SearchCombo = strValue
        Else
            strValue = Trim(Mid(strValue, 200))
            If strValue = "" Then
                SearchCombo = "Null"
            Else
                SearchCombo = "'" + strValue + "'"
            End If
        End If
    End Function

    Public Function SearchIndex(ByRef objDr As SqlDataReader, ByVal strCampo1 As String, ByVal strCampo2 As String, ByVal objCombo As ComboBox, ByVal bolSearchValue As Boolean, ByVal bolUseDtr As Boolean, Optional ByVal bolIniText As Boolean = False) As Int32

        Dim intElement As Integer
        Dim strValue As String
        Dim intPosition As Integer
        Dim strIniText As String
        Dim intPos As Integer
        Dim objObject

        SearchIndex = objCombo.Items.Count - 1
        If bolSearchValue = True Then

        End If
        ' SearchIndex(objDR, strCrop, "", cboCrop, False, False) 
        If bolSearchValue = False Then
            If bolUseDtr = False Then
                If IsNull(objDr, strCampo1, bolUseDtr) Then
                    SearchIndex = objCombo.Items.Count - 1
                Else
                    If Trim(strCampo2) <> "" Then
                        For intElement = 0 To objCombo.Items.Count - 1
                            objObject = objCombo.Items.Item(intElement)
                            If LCase(Trim(strCampo1) + " -> " + Trim(strCampo2)) = LCase(Trim(objObject)) Then

                                SearchIndex = intElement
                                Exit Function
                            End If
                        Next
                    Else
                        For intElement = 0 To objCombo.Items.Count - 1
                            objObject = objCombo.Items.Item(intElement)
                            strValue = LCase(Trim(objObject))
                            strValue = Trim(Mid(strValue, 200))

                            'intPosition = InStr(strValue, "->", CompareMethod.Text)
                            'If intPosition > 0 Then
                            'strValue = LCase(Trim(Mid(strValue, 1, intPosition - 1)))
                            'Else
                            '    strValue = ""
                            'End If
                            If LCase(Trim(strCampo1)) = strValue Then
                                SearchIndex = intElement
                                Exit Function
                            End If
                        Next
                    End If
                End If
            Else
                If IsNull(objDr, strCampo1, True) Then
                    If bolIniText = True Then
                        SearchIndex = objCombo.Items.Count - 1
                    Else
                        If IsNull(objDr, strCampo1, True) Or IsNull(objDr, strCampo2, True) Then
                            SearchIndex = objCombo.Items.Count - 1
                        End If
                    End If

                Else
                    If bolIniText = False Then
                        For intElement = 0 To objCombo.Items.Count - 1
                            objObject = objCombo.Items.Item(intElement)
                            If LCase(Trim(objDr(strCampo1)) + " -> " + LCase(Trim(objDr(strCampo2)))) = LCase(Trim(objObject)) Then

                                SearchIndex = intElement
                                Exit Function
                            End If
                        Next
                    Else
                        For intElement = 0 To objCombo.Items.Count - 1
                            objObject = objCombo.Items.Item(intElement)
                            intPos = InStr(1, Trim(objObject), "->", CompareMethod.Binary)
                            If intPos > 0 Then
                                objObject = objCombo.Items.Item(intElement)
                                strIniText = LCase(Trim(Mid(Trim(objObject), 1, intPos - 1)))

                                If LCase(Trim(objDr(strCampo1))) = strIniText Then
                                    SearchIndex = intElement
                                    Exit Function
                                End If
                            End If
                        Next
                    End If
                End If
            End If
        End If
    End Function

    Public Function SearchColumn(ByVal objVsf As C1.Win.C1FlexGrid.C1FlexGrid, ByVal strColumnName As String, Optional ByVal intRow As Integer = 0, Optional ByVal intColBegin As Integer = 0) As Integer
        Dim intCol As Integer

        SearchColumn = -1
        strColumnName = LCase(Trim(strColumnName))
        If objVsf.Cols.Count > 0 Then
            For intCol = intColBegin To objVsf.Cols.Count - 1
                If LCase(Trim(objVsf(intRow, intCol))) = strColumnName Then
                    SearchColumn = intCol
                    Exit For
                End If
            Next
        End If
    End Function

    Public Function QN(ByVal varValue) As String
        If IsDBNull(varValue) = True Then
            QN = ""
        Else
            QN = Trim(varValue)
        End If
    End Function

    Public Function ExecuteUpdateInsert(ByRef objCon As SqlConnection, ByVal strSQL As String, Optional ByRef strError As String = "") As Integer
        strError = ""
        objCon = New SqlConnection(strConnection)
        Dim objSqlCommand As New SqlCommand(strSQL, objCon)

        If Not objCon.State = ConnectionState.Open Then
            objCon.Open()
        End If
        Try
            objSqlCommand.CommandText = strSQL
            ExecuteUpdateInsert = objSqlCommand.ExecuteNonQuery()
        Catch ex As Exception
            strError = ex.Message
        End Try
        objSqlCommand = Nothing
        objCon.Close()
        objCon = Nothing
    End Function

    Public Function ExecuteSQL(ByVal strSQL As String, ByVal strField As String, ByRef objCon As SqlConnection) As String
        Dim objDataReader As SqlDataReader

        objCon.Close()
        Dim objSQLCommand As New SqlCommand(strSQL, objCon)

        If Not objCon.State = ConnectionState.Open Then
            objCon.Open()
        End If

        Try
            If strField <> "" Then
                objDataReader = objSQLCommand.ExecuteReader()
            Else
                objSQLCommand.ExecuteNonQuery()
            End If


            ExecuteSQL = ""
            If strField <> "" Then
                If objDataReader.Read() Then
                    If Not IsDBNull(objDataReader(strField)) Then
                        ExecuteSQL = Trim(objDataReader(strField))
                    End If
                End If
                objDataReader.Close()
            End If


        Catch o As SqlException
            MsgBox("Error: " + o.Message)
        End Try

        objCon.Close()


    End Function

    Public Sub RemoveRows(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal intBeginRow As Integer)
        Dim intRowsSelected As Integer
        Dim intRow As Integer
        Dim intLoop As Integer

        'remove row
        If objGrid.Rows.Count > intBeginRow Then
            If objGrid.Row > intBeginRow - 1 Then

                intRow = objGrid.Row
                intRowsSelected = objGrid.RowSel - intRow + 1

                For intLoop = 1 To intRowsSelected
                    objGrid.RemoveItem(intRow)
                Next


            End If
        End If
    End Sub

    Public Function OpenConnection(ByRef objCon As SqlConnection) As String

        Try
            OpenConnection = ""
            If Not objCon.State = ConnectionState.Open Then
                objCon.ConnectionString = strConnection
                objCon.Open()
                OpenConnection = ""
            End If
        Catch err As SqlException
            OpenConnection = err.Message.ToString
        Catch err As Exception
            OpenConnection = err.Message.ToString
        End Try

    End Function

    Function verifyPermission(ByVal strUser As String, ByVal intPermissionId As Integer, ByVal objCon As SqlConnection) As Boolean
        Dim strSQL As String
        Dim resultado As Integer
        Dim objDR As SqlDataReader
        Call OpenConnection(objCon)

        verifyPermission = False
        strSQL = "select count(*) from v_IVU_Permissions where " + _
        " permissionid=" + Trim(intPermissionId) + _
        " and [user]='" + strUser + "'"

        objDR = ReturnDataReader(strSQL, objCon)


        Do While objDR.Read()
            resultado = objDR.GetInt32(0)
            Exit Do
        Loop



        If resultado = 0 Then
            verifyPermission = False
        Else
            verifyPermission = True
        End If
        objDR.Close()
        objCon.Close()
    End Function


    Public Function ExecuteProcedure(ByVal strProcedureName As String, ByRef objCon As SqlConnection, ByVal strParameters As String, ByVal strValues As String, ByVal strParameterOutput As String) As String

        Dim cmd As SqlCommand = objCon.CreateCommand
        objCon.Open()
        cmd.CommandType = CommandType.StoredProcedure

        Dim arrParameters As String() = strParameters.Split("-")
        Dim arrValues As String() = strValues.Split("-")
        Dim LotID As String
        ' Use For Each loop over words and display them
        Dim parameter As String
        Dim value As String
        Dim i As String = 0

        For Each parameter In arrParameters
            value = arrValues(i)
            i += 1
            cmd.Parameters.Add(New SqlParameter(parameter, value))
        Next

        Dim LotIdParameter As IDbDataParameter = cmd.CreateParameter()
        If strParameterOutput <> "" Then
            LotIdParameter.ParameterName = strParameterOutput
            LotIdParameter.Direction = System.Data.ParameterDirection.Output
            LotIdParameter.DbType = System.Data.DbType.String
            LotIdParameter.Size = 50
            cmd.Parameters.Add(LotIdParameter)
        End If
        cmd.CommandText = strProcedureName
        cmd.ExecuteNonQuery()

        If strParameterOutput <> "" Then
            LotID = LotIdParameter.Value
        End If

        objCon.Close()
        Return LotID
    End Function
End Module
