﻿
Public Module Config

    Public Const strINIFile = "CIPCROSSINI.txt"

    Public strServer = ""
    Public strDB = ""
    Public strUser = ""
    Public strPassword = ""
    Public strConnection = ""
    Public strOtherLanguage = ""
    Public strDescriptors = ""

    Public Const intTimeOut = 60 * 10

    Public intRowMetaDatas As Integer = 0
    Public intBeginFieldNameRow As Integer = 1
    Public intBeginDataRow As Integer = 3

    Public intSystemID As Integer = 6
    Public strMainTableName As String = "v_cipser_search_for_list"

    Public strDateToday As String 'mm /dd / yyyy
    Public strDateTimeToday As String 'hh:mm:ss
    Public strDomain As String = "cip_lima"

    Public Const strKeyColDup = "cipser090507062411"
    Public strFieldsDups As String = ""
    Public strFieldsUser As String = ""
    Public Const strColQty As String = "seeds"

    Public bolDebugMode As Boolean = False

    Enum WhenAddLotShowBy
        LOTID = 0
        AcceNumb = 1
        CollNumb = 2
        FemaleParentStart = 3
        FemaleParentContain = 4
    End Enum

    Enum TABId
        Tab_User = 0
        Tab_Chamber = 1
        Tab_Stock = 2
        Tab_NewLot = 3
        Tab_Justification = 4
        Tab_DistAcq = 5
        Tab_Options = 8
        Tab_MoveList = 7
        Tab_Movements = 6
    End Enum

    Enum LanguageForSystem
        OtherLanguage = 0
        English = 1
    End Enum

    Enum TypeSearch
        search_by_collnumb_equal = 1
        search_by_collnumb_start_with = 2
        search_by_accenumb = 3
        search_by_LotId = 4
        search_by_Lot_Container = 5
        search_by_accename_equal = 6
        search_by_accename_start_with = 7
        search_by_notes_equal = 8
        search_by_notes_start_with = 9
        search_by_notes_contain_with = 10
        search_by_female_start_with = 11
        search_by_collnumb_contain_with = 12
        search_by_labcode_start_with = 13
        search_by_labcode_contain = 14

        search_by_collnumb_equal_when_create_lot = 20
        search_by_collnumb_start_with_when_create_lot = 21
        search_by_collnumb_contain_when_create_lot = 22
        search_by_accenumb_when_create_lot = 23
        search_by_female_start_with_when_create_lot = 30
        search_by_female_contain_with_when_create_lot = 31
        search_by_accename_equal_when_create_lot = 25
        search_by_accename_start_with_when_create_lot = 26
        search_by_accename_contain_when_create_lot = 27
        search_by_GID_when_create_lot = 28
        search_by_labcode_start_with_when_create_lot = 29
        search_by_labcode_contain_when_create_lot = 30
        search_by_LotId_number_when_create_lot = 100
    End Enum

    Enum BarCode
        search_by_accenumb = 4
        search_by_LotId = 6
    End Enum

    Enum PermissionsID
        deleteTransaction = 198
    End Enum
End Module
