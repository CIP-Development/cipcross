Imports OpenNETCF.IO.Serial.Port
Imports OpenNETCF.IO.Serial
Imports Excel
Imports ICSharpCode
Imports System.IO
Imports System.Data
Imports XLSExportDemo
Imports C1.Win.C1FlexGrid
Imports Microsoft.Office.Interop
Imports System.Data.SqlClient
Imports System.Data.SqlServerCe

Public Class Form_Cross
    Private WithEvents SerialPort As OpenNETCF.IO.Serial.Port
    Private CurrentFolder As String
    'Dim curFile As String = "\My Documents\CrossingResults.xls"
    Dim boolImprimio As Boolean = True
    Dim leftMarginOfLabel As String

    '========Load Form=======
    Private Sub Form_Print_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Parametros iniciales
        Call HideParameters()

        ''Formato de calendario
        Call CalendarFormat()

        'Ancho de columnas para C1FlexGrid
        Call ColumnsWidth()

        'Inicializacion de ComboBox's
        Call InitializationComboBoxAll()
        CurrentFolder = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase
        CurrentFolder = CurrentFolder.Substring(0, CurrentFolder.LastIndexOf("\") + 1)
        Me.ComboBox_Language.SelectedIndex = 1
        'Call RefrescarVista()
        'Read Configuration
        'CurrentFolder = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase
        'CurrentFolder = CurrentFolder.Substring(0, CurrentFolder.LastIndexOf("\") + 1)
        'Try
        '    Call ReadIniSetup(CurrentFolder, strINIFile)
        '    strDateToday = DateTodayServer()
        'Catch ex As Exception
        '    MessageBox.Show("No existe conexi�n al CIP-LAN", "Mensaje de Alerta")
        'End Try
    End Sub

    Private Sub CalendarFormat()
        DateTimePicker_Harvest.Value = Date.Now.Date
        DateTimePicker_Cross.Value = Date.Now.Date
    End Sub

    Private Sub SelectPrinter()

        If ComboBox_Printer.SelectedIndex = 0 Then
            leftMarginOfLabel = "! 0 200 200 500 1" ' Default printer RW 220
        ElseIf ComboBox_Printer.SelectedIndex = 1 Then
            leftMarginOfLabel = "! 55 200 200 500 1" ' printer ZQ 510        
        End If

    End Sub

    Private Sub GetView() 'ByVal selectCommand As String

        Dim conn As New SqlCeConnection("Data Source=" & CurrentFolder & "\CIPCROSS_DB.sdf; ")
        Dim query As String = ""
        'Dim query As String = "SELECT ID, FDato1, FDato2, FDato3, TipoCruzamiento, FOrder, MOrder, FloresPolinizadas, LocalidadCruce, UsuarioCruce,FechaCruce,TotalFrutos, UsuarioCosecha, LocalidadCosecha, FechaCosecha, TotalTuberculos, LocalidadCosechaTub, UsuarioCosechaTub,FechaCosechaTub, [Plan], NroPlan  FROM results ORDER BY TotalFrutos, FDato1, FOrder, MOrder"

        If ComboBox_PrintType.SelectedIndex = 1 Then
            If ComboBox1.SelectedIndex = 1 Then
                query = "SELECT LocalidadCruce AS Loc, FDato2, FDato3, FDato1, CONVERT(INT, FOrder) AS Fem, MOrder AS Masc, SUM(FloresPolinizadas) AS TF FROM results WHERE TotalTuberculos = 0 GROUP BY FDato1, FDato2, FDato3, FOrder, MOrder, LocalidadCruce ORDER BY Loc, FDato1, Fem, Masc"
            ElseIf ComboBox1.SelectedIndex = 2 Then
                query = "SELECT LocalidadCruce AS Loc, FDato2, FDato3, FDato1, CONVERT(INT, FOrder) AS Fem, MOrder AS Masc, SUM(FloresPolinizadas) AS TF, SUM(CONVERT(INT, TotalFrutos)) AS Fru FROM results WHERE TotalTuberculos = 0 GROUP BY FDato1, FDato2, FDato3, FOrder, MOrder, LocalidadCruce ORDER BY Loc, FDato1, Fem, Masc"
            ElseIf ComboBox1.SelectedIndex = 3 Then
                query = "SELECT LocalidadCosechaTub, FDato2, FDato3, FDato1, CONVERT(INT, FOrder) AS Fem, SUM(CONVERT(INT, TotalTuberculos)) AS Fru FROM results WHERE(TotalTuberculos > 0) GROUP BY FDato1, FDato2, FDato3, FOrder, LocalidadCosechaTub ORDER BY LocalidadCosechaTub, FDato1, Fem"
            End If
        ElseIf ComboBox_PrintType.SelectedIndex = 2 Then
            If ComboBox1.SelectedIndex = 1 Then
                query = "SELECT LocalidadCruce AS Loc, FDato2, FDato3, FDato1, CONVERT(INT, FOrder) AS Fem,  MOrder AS Masc, SUM(FloresPolinizadas) AS TF FROM results WHERE TotalTuberculos = 0 GROUP BY FDato1, FDato2, FDato3, FOrder, MOrder, LocalidadCruce ORDER BY Loc, FDato1, Fem, Masc"
            ElseIf ComboBox1.SelectedIndex = 2 Then
                query = "SELECT LocalidadCruce AS Loc, FDato2, FDato3, FDato1, CONVERT(INT, FOrder) AS Fem,  CONVERT(INT, REPLACE(MOrder,'Bulk',-1)) AS Masc, SUM(FloresPolinizadas) AS TF, SUM(CONVERT(INT, TotalFrutos)) AS Fru FROM results WHERE TotalTuberculos = 0 GROUP BY FDato1, FDato2, FDato3, FOrder, MOrder, LocalidadCruce ORDER BY Loc, FDato1, Fem, Masc"
            ElseIf ComboBox1.SelectedIndex = 3 Then
                query = "SELECT LocalidadCosechaTub, FDato2, FDato3, FDato1, CONVERT(INT, FOrder) AS Fem, SUM(CONVERT(INT, TotalTuberculos)) AS Fru FROM results WHERE(TotalTuberculos > 0) GROUP BY FDato1, FDato2, FDato3, FOrder, LocalidadCosechaTub ORDER BY LocalidadCosechaTub, FDato1, Fem"
            End If
        End If

        If query <> "" Then
            Dim cmd As New SqlCeCommand(query, conn)

            conn.Open()
            Dim rdr As SqlCeDataReader = cmd.ExecuteReader()
            Dim dt As New DataTable

            Try
                dt.Load(rdr)
                C1FlexGrid1.DataSource = dt

                'Me.C1FlexGrid1.Cols.Fixed = 2
            Finally
                ' Always call Close when done reading
                '
                rdr.Close()

                ' Always call Close when done reading
                '
                conn.Close()
            End Try
        End If


        'Try
        '    Dim dataAdapter As SqlCeDataAdapter
        '    ' Specify a connection string. Replace the given value with a 
        '    ' valid connection string for a Northwind SQL Server sample
        '    ' database accessible to your system.
        '    Dim connectionString As String = _
        '        "Integrated Security=SSPI;Persist Security Info=False;" + _
        '        "Initial Catalog=CIPCROSS_DB;Data Source=.\CIPCROSS_DB.sdf"
        '    Dim connStr As New SqlCeConnection("Data Source=" & CurrentFolder & "\CIPCROSS_DB.sdf; ")
        '    ' Create a new data adapter based on the specified query.
        '    dataAdapter = New SqlCeDataAdapter(selectCommand, connStr)

        '    ' Create a command builder to generate SQL update, insert, and
        '    ' delete commands based on selectCommand. These are used to
        '    ' update the database.
        '    Dim commandBuilder As New SqlCeCommandBuilder(dataAdapter)

        '    ' Populate a new data table and bind it to the BindingSource.
        '    Dim table As New DataTable()
        '    table.Locale = System.Globalization.CultureInfo.InvariantCulture
        '    dataAdapter.Fill(table)
        '    'Me.BindingSource1.DataSource = table
        '    C1FlexGrid1.DataSource = table
        '    ' Resize the DataGridView columns to fit the newly loaded content.
        '    'Me.dataGridView1.AutoResizeColumns( _
        '    '    DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader)
        'Catch ex As SqlException
        '    MessageBox.Show("To run this example, replace the value of the " + _
        '        "connectionString variable with a connection string that is " + _
        '        "valid for your system.")
        'End Try

    End Sub

    'Private Sub GetData(ByVal selectCommand As String)

    '    Try
    '        ' Specify a connection string. Replace the given value with a 
    '        ' valid connection string for a Northwind SQL Server sample
    '        ' database accessible to your system.
    '        Dim connectionString As String = _
    '            "Integrated Security=SSPI;Persist Security Info=False;" + _
    '            "Initial Catalog=Northwind;Data Source=localhost"

    '        ' Create a new data adapter based on the specified query.
    '        Me.dataAdapter = New SqlDataAdapter(selectCommand, connectionString)
    '        ResultsTableAdapter1
    '        ' Create a command builder to generate SQL update, insert, and
    '        ' delete commands based on selectCommand. These are used to
    '        ' update the database.
    '        Dim commandBuilder As New SqlCommandBuilder(Me.dataAdapter)

    '        ' Populate a new data table and bind it to the BindingSource.
    '        Dim table As New DataTable()
    '        table.Locale = System.Globalization.CultureInfo.InvariantCulture
    '        Me.dataAdapter.Fill(table)
    '        Me.bindingSource1.DataSource = table

    '        ' Resize the DataGridView columns to fit the newly loaded content.
    '        'Me.dataGridView1.AutoResizeColumns( _
    '        '    DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader)
    '    Catch ex As SqlException
    '        MessageBox.Show("To run this example, replace the value of the " + _
    '            "connectionString variable with a connection string that is " + _
    '            "valid for your system.")
    '    End Try

    'End Sub

#Region "TAB User Login"
    '=====Tab User Login=====

    'Choose Crossing Plan
    Private Sub Button_CrossingPlan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_CrossingPlan.Click
        Me.ComboBox_SelectCrossingPlan.Items.Clear()

        'Limpia parametros por defecto
        Call Clean()

        'Abre el archivo de manera correcta
        Call OpenFile()
    End Sub

    'Button cancel
    Private Sub Button_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Cancel.Click
        Me.Button_SignIn.Enabled = False
        Me.Panel_SelectCrossPlan.Visible = False
    End Sub

    'Button select Plan
    Private Sub Button_SelectPlan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SelectPlan.Click
        Call Clean()
        Panel_SelectCrossPlan.Visible = True
        Panel_SelectCrossPlan.Height = Me.Height - 3
        Panel_SelectCrossPlan.Width = Me.Width - 2
    End Sub

    'Clean Password
    Private Sub ComboBox_Username_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_Username.SelectedIndexChanged
        If Me.ComboBox_Username.SelectedIndex <> -1 Then
            Me.TextBox_Password.Text = ""
        End If
    End Sub

    'Select Crossing Plan
    Private Sub ComboBox_SelectCrossingPlan_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_SelectCrossingPlan.SelectedIndexChanged
        'Cargamos el Excel
        Dim result As DataSet = Nothing
        Call ReadExcelFile(result)

        Dim objTable As DataTable
        Dim intRow As Integer, intCol As Integer

        '=====Crossing Plans=====
        Try
            objTable = result.Tables(0)
        Catch ex As Exception
            Call ShowSystemMessage(strMessage6)
            Exit Sub
        End Try

        objTable = result.Tables(Me.ComboBox_SelectCrossingPlan.SelectedIndex)

        'If objTable.Rows(2).ItemArray(0) = "TemplateCrossingPlan" Then
        'Sign in
        Dim nro As String = Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1

        Me.TextBox_CrossingPlanSelected.Text = "Plan " + nro + ": " + objTable.Rows(0).ItemArray(0)
        Me.TextBox_CrossingPlanSelectedHide.Text = objTable.Rows(0).ItemArray(0)

        'CrossingPlan
        'Female
        Dim intFC As Integer = 2
        Dim countRowsFC As Integer = 0

        'For intRow = 8 To objTable.Rows.Count - 1
        For intRow = intRowStart To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                Exit For
            End If
            countRowsFC += 1
        Next

        'Me.C1FlexGrid_CrossingPlan.Rows.Count = countRowsFC + 2

        'For intRow = 8 To objTable.Rows.Count - 1
        For intRow = intRowStart To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                Exit For
            End If

            'Me.C1FlexGrid_CrossingPlan(intFC, 0) = objTable.Rows(intRow).ItemArray(0)
            'Me.C1FlexGrid_CrossingPlan(intFC, 1) = objTable.Rows(intRow).ItemArray(1)
            intFC += 1
        Next

        'Male
        'Valida que No existen machos en columnas
        Dim intMCValida As Integer = 0
        Dim intMC As Integer = 2
        Dim countRowsMC As Integer = 0

        'Valida para ver si hay columnas en blanco
        'For intCol = 4 To objTable.Columns.Count - 1
        For intCol = intColumnStart To objTable.Columns.Count - 1
            'If objTable.Rows(5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(4).ItemArray(intCol) Is DBNull.Value And objTable.Rows(3).ItemArray(intCol) Is DBNull.Value Then
            If objTable.Rows(intRowStart - 5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 6).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 7).ItemArray(intCol) Is DBNull.Value Then
                Exit For
            End If
            intMCValida += 1
            Exit For
        Next

        If intMCValida = 0 Then
            'For intRow = 8 To objTable.Rows.Count - 1
            For intRow = intRowStart To objTable.Rows.Count - 1
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                    Exit For
                End If
                countRowsMC += 1
            Next
        Else
            'For intCol = 4 To objTable.Columns.Count - 1
            For intCol = intColumnStart To objTable.Columns.Count - 1
                'If objTable.Rows(5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(4).ItemArray(intCol) Is DBNull.Value And objTable.Rows(3).ItemArray(intCol) Is DBNull.Value Then
                If objTable.Rows(intRowStart - 5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 6).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 7).ItemArray(intCol) Is DBNull.Value Then
                    Exit For
                End If
                countRowsMC += 1
            Next
        End If

        'Me.C1FlexGrid_CrossingPlan.Cols.Count = countRowsMC + 2

        If intMCValida = 0 Then
            'For intRow = 8 To objTable.Rows.Count - 1
            For intRow = intRowStart To objTable.Rows.Count - 1
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value And objTable.Rows(intRow).ItemArray(4) Is DBNull.Value Then
                    Exit For
                End If

                'Me.C1FlexGrid_CrossingPlan(0, intMC) = objTable.Rows(intRow).ItemArray(0)
                'Me.C1FlexGrid_CrossingPlan(1, intMC) = objTable.Rows(intRow).ItemArray(1)
                intMC += 1
            Next
        Else
            ''For intCol = 4 To objTable.Columns.Count - 1
            For intCol = intColumnStart To objTable.Columns.Count - 1
                'If objTable.Rows(5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(4).ItemArray(intCol) Is DBNull.Value And objTable.Rows(3).ItemArray(intCol) Is DBNull.Value Then
                If objTable.Rows(intRowStart - 5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 6).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 7).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 8).ItemArray(intCol) Is DBNull.Value Then
                    Exit For
                End If

                'Me.C1FlexGrid_CrossingPlan(0, intMC) = objTable.Rows(6).ItemArray(intCol)
                'Me.C1FlexGrid_CrossingPlan(1, intMC) = objTable.Rows(5).ItemArray(intCol)
                intMC += 1
            Next
        End If

        'Me.C1FlexGrid_CrossingPlan.Rows(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_CrossingPlan.Rows(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter

        'Parentals
        'Female
        Dim intF As Integer = 1
        Dim countRowsF As Integer = 0

        'For intRow = 8 To objTable.Rows.Count - 1
        For intRow = intRowStart To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                Exit For
            End If
            countRowsF += 1
        Next

        Me.C1FlexGrid_Female.Rows.Count = countRowsF + 1

        'For intRow = 8 To objTable.Rows.Count - 1
        For intRow = intRowStart To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                Exit For
            End If

            Me.C1FlexGrid_Female(intF, 0) = objTable.Rows(intRow).ItemArray(0)
            Me.C1FlexGrid_Female(intF, 1) = objTable.Rows(intRow).ItemArray(1)
            Me.C1FlexGrid_Female(intF, 2) = objTable.Rows(intRow).ItemArray(2)
            Me.C1FlexGrid_Female(intF, 3) = objTable.Rows(intRow).ItemArray(3)
            Me.C1FlexGrid_Female(intF, 4) = objTable.Rows(intRow).ItemArray(4)
            intF += 1
        Next

        'Male
        Dim intM As Integer = 1
        Dim countRowsM As Integer = 0

        If intMCValida = 0 Then
            'For intRow = 8 To objTable.Rows.Count - 1
            For intRow = intRowStart To objTable.Rows.Count - 1
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                    Exit For
                End If
                countRowsM += 1
            Next
        Else
            'For intCol = 4 To objTable.Columns.Count - 1
            For intCol = intColumnStart To objTable.Columns.Count - 1
                'If objTable.Rows(5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(4).ItemArray(intCol) Is DBNull.Value And objTable.Rows(3).ItemArray(intCol) Is DBNull.Value Then
                If objTable.Rows(intRowStart - 5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 6).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 7).ItemArray(intCol) Is DBNull.Value Then
                    Exit For
                End If
                countRowsM += 1
            Next
        End If

        Me.C1FlexGrid_Male.Rows.Count = countRowsM + 1

        If intMCValida = 0 Then
            'For intRow = 8 To objTable.Rows.Count - 1
            For intRow = intRowStart To objTable.Rows.Count - 1
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                    Exit For
                End If

                Me.C1FlexGrid_Male(intM, 0) = objTable.Rows(intRow).ItemArray(0)
                Me.C1FlexGrid_Male(intM, 1) = objTable.Rows(intRow).ItemArray(1)
                Me.C1FlexGrid_Male(intM, 2) = objTable.Rows(intRow).ItemArray(2)
                Me.C1FlexGrid_Male(intM, 3) = objTable.Rows(intRow).ItemArray(3)
                Me.C1FlexGrid_Male(intM, 4) = objTable.Rows(intRow).ItemArray(4)
                intM += 1
            Next
        Else
            'For intCol = 4 To objTable.Columns.Count - 1
            For intCol = intColumnStart To objTable.Columns.Count - 1
                'If objTable.Rows(5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(4).ItemArray(intCol) Is DBNull.Value And objTable.Rows(3).ItemArray(intCol) Is DBNull.Value Then
                If objTable.Rows(intRowStart - 5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 6).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 7).ItemArray(intCol) Is DBNull.Value Then
                    Exit For
                End If

                Me.C1FlexGrid_Male(intM, 0) = objTable.Rows(6).ItemArray(intCol)
                Me.C1FlexGrid_Male(intM, 1) = objTable.Rows(5).ItemArray(intCol)
                Me.C1FlexGrid_Male(intM, 2) = objTable.Rows(4).ItemArray(intCol)
                Me.C1FlexGrid_Male(intM, 3) = objTable.Rows(3).ItemArray(intCol)
                Me.C1FlexGrid_Male(intM, 4) = objTable.Rows(2).ItemArray(intCol)
                intM += 1
            Next
        End If

        'Crossing
        'Values
        If intMCValida = 0 Then

            Dim intVCross As Integer = 1
            Dim countRowsVCross As Integer = 0

            'For intRow = 8 To objTable.Rows.Count - 1
            For intRow = intRowStart To objTable.Rows.Count - 1
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value And objTable.Rows(intRow).ItemArray(4) Is DBNull.Value Then
                    Exit For
                End If
                countRowsVCross += 1
            Next

            Me.C1FlexGrid_ValuesCross.Rows.Count = countRowsVCross + 1

            'Para Cosecha de tuberculos:
            Me.C1FlexGrid_ValuesTubHarv.Rows.Count = countRowsVCross + 1

            'For intRow = 8 To objTable.Rows.Count - 1
            For intRow = 9 To 9
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value And objTable.Rows(intRow).ItemArray(4) Is DBNull.Value Then
                    Exit For
                End If

                Me.C1FlexGrid_ValuesCross(intVCross, 0) = objTable.Rows(intRow).ItemArray(0)
                Me.C1FlexGrid_ValuesCross(intVCross, 1) = objTable.Rows(intRow).ItemArray(1)
                Me.C1FlexGrid_ValuesCross(intVCross, 2) = objTable.Rows(intRow).ItemArray(2)
                Me.C1FlexGrid_ValuesCross(intVCross, 3) = objTable.Rows(intRow).ItemArray(3)
                Me.C1FlexGrid_ValuesCross(intVCross, 4) = objTable.Rows(intRow).ItemArray(4)
            Next

            For intRow = intRowStart To objTable.Rows.Count - 1
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value And objTable.Rows(intRow).ItemArray(4) Is DBNull.Value Then
                    Exit For
                End If

                Me.C1FlexGrid_ValuesCross(intVCross, 0) = objTable.Rows(intRow).ItemArray(0)
                Me.C1FlexGrid_ValuesCross(intVCross, 1) = objTable.Rows(intRow).ItemArray(1)
                Me.C1FlexGrid_ValuesCross(intVCross, 2) = objTable.Rows(intRow).ItemArray(2)
                Me.C1FlexGrid_ValuesCross(intVCross, 3) = objTable.Rows(intRow).ItemArray(3)
                Me.C1FlexGrid_ValuesCross(intVCross, 4) = objTable.Rows(intRow).ItemArray(4)

                'Para Cosecha de tuberculos:
                Me.C1FlexGrid_ValuesTubHarv(intVCross, 0) = objTable.Rows(intRow).ItemArray(0)
                Me.C1FlexGrid_ValuesTubHarv(intVCross, 1) = objTable.Rows(intRow).ItemArray(1)
                Me.C1FlexGrid_ValuesTubHarv(intVCross, 2) = objTable.Rows(intRow).ItemArray(2)
                Me.C1FlexGrid_ValuesTubHarv(intVCross, 3) = objTable.Rows(intRow).ItemArray(3)
                Me.C1FlexGrid_ValuesTubHarv(intVCross, 4) = objTable.Rows(intRow).ItemArray(4)

                intVCross += 1
            Next

        Else

            C1FlexGrid_ValuesCross.Visible = False
            'Para Cosecha de tuberculos:
            C1FlexGrid_ValuesTubHarv.Visible = False

            ComboBox_GenFemale.Visible = False
            ComboBox_GenMale.Visible = False
            lblFemale.Visible = False
            lblMale.Visible = False
            lblPor.Visible = False
            lblSearchCrossing.Visible = False
            TextBox_SearchValueCross.Visible = False
            Button_Search_ValueCross.Visible = False

            C1FlexGrid_FemaleCross.Visible = True
            C1FlexGrid_MaleCross.Visible = True
            TextBox_SearchMaleCross.Visible = True
            TextBox_SearchFemaleCross.Visible = True
            Button_SearchMaleCross.Visible = True
            Button_SearchFemaleCross.Visible = True
            Label_CrossingMales.Visible = True
            Label_CrossingFemales.Visible = True

        End If

        'Female Cross
        Dim intFCross As Integer = 1
        Dim countRowsFCross As Integer = 0

        'For intRow = 8 To objTable.Rows.Count - 1
        For intRow = intRowStart To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                Exit For
            End If
            countRowsFCross += 1
        Next

        Me.C1FlexGrid_FemaleCross.Rows.Count = countRowsFCross + 1

        'For intRow = 8 To objTable.Rows.Count - 1
        For intRow = intRowStart To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                Exit For
            End If

            Me.C1FlexGrid_FemaleCross(intFCross, 0) = objTable.Rows(intRow).ItemArray(0)
            Me.C1FlexGrid_FemaleCross(intFCross, 1) = objTable.Rows(intRow).ItemArray(1)
            Me.C1FlexGrid_FemaleCross(intFCross, 2) = objTable.Rows(intRow).ItemArray(2)
            Me.C1FlexGrid_FemaleCross(intFCross, 3) = objTable.Rows(intRow).ItemArray(3)
            Me.C1FlexGrid_FemaleCross(intFCross, 4) = objTable.Rows(intRow).ItemArray(4)
            intFCross += 1
        Next

        'Male Cross
        Dim intMCross As Integer = 1
        Dim countRowsMCross As Integer = 0

        'For intCol = 4 To objTable.Columns.Count - 1
        For intCol = intColumnStart To objTable.Columns.Count - 1
            'If objTable.Rows(5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(4).ItemArray(intCol) Is DBNull.Value And objTable.Rows(3).ItemArray(intCol) Is DBNull.Value Then
            If objTable.Rows(intRowStart - 5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 6).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 7).ItemArray(intCol) Is DBNull.Value Then
                Exit For
            End If
            countRowsMCross += 1
        Next

        Me.C1FlexGrid_MaleCross.Rows.Count = countRowsMCross + 1

        'For intCol = 4 To objTable.Columns.Count - 1
        For intCol = intColumnStart To objTable.Columns.Count - 1
            'If objTable.Rows(5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(4).ItemArray(intCol) Is DBNull.Value And objTable.Rows(3).ItemArray(intCol) Is DBNull.Value Then
            If objTable.Rows(intRowStart - 5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 6).ItemArray(intCol) Is DBNull.Value And objTable.Rows(intRowStart - 7).ItemArray(intCol) Is DBNull.Value Then
                Exit For
            End If

            Me.C1FlexGrid_MaleCross(intMCross, 0) = objTable.Rows(6).ItemArray(intCol)
            Me.C1FlexGrid_MaleCross(intMCross, 1) = objTable.Rows(5).ItemArray(intCol)
            Me.C1FlexGrid_MaleCross(intMCross, 2) = objTable.Rows(4).ItemArray(intCol)
            Me.C1FlexGrid_MaleCross(intMCross, 3) = objTable.Rows(3).ItemArray(intCol)
            Me.C1FlexGrid_MaleCross(intMCross, 4) = objTable.Rows(2).ItemArray(intCol)
            intMCross += 1
        Next

        '=====Options=====
        objTable = result.Tables(result.Tables.Count - 2)

        'Username
        For intRow = 6 To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(0) Is DBNull.Value Then
                Exit For
            End If
            Me.ComboBox_Username.Items.Add(objTable.Rows(intRow).ItemArray(0))
            Me.ListBox_Username.Items.Add(objTable.Rows(intRow).ItemArray(0))
            Me.ListBox_FullName.Items.Add(objTable.Rows(intRow).ItemArray(1))
        Next

        If Me.ComboBox_Username.Items.Contains(objTable.Rows(intRow - 1).ItemArray(0)) Then
            Me.ComboBox_Username.SelectedIndex = 0
        Else
            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage3)
            Else
                Call ShowSystemMessage(strMessage3)
            End If
            Exit Sub
        End If

        'Password
        For intRow = 6 To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(2) Is DBNull.Value Then
                Exit For
            End If
            Me.ListBox_Password.Items.Add(objTable.Rows(intRow).ItemArray(2))
        Next

        'Location
        For intRow = 6 To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(4) Is DBNull.Value Then
                Exit For
            End If
            Me.ComboBox_Location.Items.Add(objTable.Rows(intRow).ItemArray(4))
        Next

        If Me.ComboBox_Location.Items.Contains(objTable.Rows(intRow - 1).ItemArray(4)) Then
            Me.ComboBox_Location.SelectedIndex = 0
        Else
            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage4)
            Else
                Call ShowSystemMessage(strMessage4)
            End If
            Exit Sub
        End If

        'Barcode Type
        For intRow = 6 To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(7) Is DBNull.Value Then
                Exit For
            End If
            Me.ComboBox_BarcodeType.Items.Add(objTable.Rows(intRow).ItemArray(7))
            Me.ListBox_BarcodeType.Items.Add(objTable.Rows(intRow).ItemArray(7))
        Next

        If Me.ComboBox_BarcodeType.Items.Contains(objTable.Rows(intRow - 1).ItemArray(7)) Then
            Me.ComboBox_BarcodeType.SelectedIndex = 0
        Else
            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage5)
            Else
                Call ShowSystemMessage(strMessage5)
            End If
            Exit Sub
        End If

        'Barcode Type ID
        For intRow = 6 To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(9) Is DBNull.Value Then
                Exit For
            End If
            Me.ListBox_BarcodeTypeID.Items.Add(objTable.Rows(intRow).ItemArray(9))
        Next

        'Type crossing
        For intRow = 6 To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(11) Is DBNull.Value Then
                Exit For
            End If
            Me.ComboBox_TypeCross.Items.Add(objTable.Rows(intRow).ItemArray(11))
            Me.ComboBox_TypeCrossOption.Items.Add(objTable.Rows(intRow).ItemArray(11))
        Next

        If Me.ComboBox_TypeCross.Items.Contains(objTable.Rows(intRow - 1).ItemArray(11)) Then
            Me.ComboBox_TypeCross.SelectedIndex = 0
            Me.ComboBox_TypeCrossOption.SelectedIndex = 0
        End If

        'Fruit size
        For intRow = 6 To objTable.Rows.Count - 1
            If objTable.Rows(intRow).ItemArray(14) Is DBNull.Value Then
                Exit For
            End If
            Me.ComboBox_FruitSize.Items.Add(objTable.Rows(intRow).ItemArray(14))
        Next

        If Me.ComboBox_FruitSize.Items.Contains(objTable.Rows(intRow - 1).ItemArray(14)) Then
            Me.ComboBox_FruitSize.SelectedIndex = 0
        End If

        '=====Fields=====
        objTable = result.Tables(result.Tables.Count - 1)

        'Female labels
        For intRow = 5 To 8
            Me.ListBox_FemaleLabels.Items.Add(objTable.Rows(intRow).ItemArray(2))
        Next

        'Male labels
        For intRow = 15 To 18
            Me.ListBox_MaleLabels.Items.Add(objTable.Rows(intRow).ItemArray(2))
        Next

        'excelReader.Close()

        'Fields
        If Me.ListBox_FemaleLabels.Items.Item(0) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(1) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(2) Is DBNull.Value Then
            'Value Cross
            Me.C1FlexGrid_ValuesCross(0, 0) = "Order"
            Me.C1FlexGrid_ValuesCross(0, 1) = "F.Data1"
            Me.C1FlexGrid_ValuesCross(0, 2) = "F.Data2"
            Me.C1FlexGrid_ValuesCross(0, 3) = "F.Data3"
            Me.C1FlexGrid_ValuesCross(0, 4) = "F.Data4"
            'Para cosecha de tuberculos.
            Me.C1FlexGrid_ValuesTubHarv(0, 0) = "Order"
            Me.C1FlexGrid_ValuesTubHarv(0, 1) = "F.Data1"
            Me.C1FlexGrid_ValuesTubHarv(0, 2) = "F.Data2"
            Me.C1FlexGrid_ValuesTubHarv(0, 3) = "F.Data3"
            Me.C1FlexGrid_ValuesTubHarv(0, 4) = "F.Data4"
        Else
            'Value Cross
            Me.C1FlexGrid_ValuesCross(0, 0) = "Order"
            'Para cosecha de tuberculos.
            Me.C1FlexGrid_ValuesTubHarv(0, 0) = "Order"

            For intRow = 0 To Me.ListBox_FemaleLabels.Items.Count - 1
                Me.C1FlexGrid_ValuesCross(0, intRow + 1) = Me.ListBox_FemaleLabels.Items(intRow)
                Me.C1FlexGrid_ValuesTubHarv(0, intRow + 1) = Me.ListBox_FemaleLabels.Items(intRow)
            Next
        End If

        If Me.ListBox_FemaleLabels.Items.Item(0) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(1) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(2) Is DBNull.Value Then
            'Female
            Me.C1FlexGrid_Female(0, 0) = "Order"
            Me.C1FlexGrid_Female(0, 1) = "F.Data1"
            Me.C1FlexGrid_Female(0, 2) = "F.Data2"
            Me.C1FlexGrid_Female(0, 3) = "F.Data3"
            Me.C1FlexGrid_Female(0, 4) = "F.Data4"

            'Female Cross
            Me.C1FlexGrid_FemaleCross(0, 0) = "Order"
            Me.C1FlexGrid_FemaleCross(0, 1) = "F.Data1"
            Me.C1FlexGrid_FemaleCross(0, 2) = "F.Data2"
            Me.C1FlexGrid_FemaleCross(0, 3) = "F.Data3"
            Me.C1FlexGrid_FemaleCross(0, 4) = "F.Data4"
        Else
            'Female
            Me.C1FlexGrid_Female(0, 0) = "Order"
            For intRow = 0 To Me.ListBox_FemaleLabels.Items.Count - 1
                Me.C1FlexGrid_Female(0, intRow + 1) = Me.ListBox_FemaleLabels.Items(intRow)
            Next

            'Female Cross
            Me.C1FlexGrid_FemaleCross(0, 0) = "Order"
            For intRow = 0 To Me.ListBox_FemaleLabels.Items.Count - 1
                Me.C1FlexGrid_FemaleCross(0, intRow + 1) = Me.ListBox_FemaleLabels.Items(intRow)
            Next
        End If

        If Me.ListBox_MaleLabels.Items.Item(0) Is DBNull.Value And Me.ListBox_MaleLabels.Items.Item(1) Is DBNull.Value And Me.ListBox_MaleLabels.Items.Item(2) Is DBNull.Value Then
            'Male
            Me.C1FlexGrid_Male(0, 0) = "Order"
            Me.C1FlexGrid_Male(0, 1) = "M.Data1"
            Me.C1FlexGrid_Male(0, 2) = "M.Data2"
            Me.C1FlexGrid_Male(0, 3) = "M.Data3"
            Me.C1FlexGrid_Male(0, 4) = "M.Data4"

            'Male Cross
            Me.C1FlexGrid_MaleCross(0, 0) = "Order"
            Me.C1FlexGrid_MaleCross(0, 1) = "M.Data1"
            Me.C1FlexGrid_MaleCross(0, 2) = "M.Data2"
            Me.C1FlexGrid_MaleCross(0, 3) = "M.Data3"
            Me.C1FlexGrid_MaleCross(0, 4) = "M.Data4"
        Else
            'Female
            Me.C1FlexGrid_Male(0, 0) = "Order"
            For intRow = 0 To Me.ListBox_MaleLabels.Items.Count - 1
                Me.C1FlexGrid_Male(0, intRow + 1) = Me.ListBox_MaleLabels.Items(intRow)
            Next

            'Female Cross
            Me.C1FlexGrid_MaleCross(0, 0) = "Order"
            For intRow = 0 To Me.ListBox_MaleLabels.Items.Count - 1
                Me.C1FlexGrid_MaleCross(0, intRow + 1) = Me.ListBox_MaleLabels.Items(intRow)
            Next
        End If

        Me.Label_CountFemales.Text = Me.C1FlexGrid_Female.Rows.Count - 1
        Me.Label_CountMales.Text = Me.C1FlexGrid_Male.Rows.Count - 1
        Me.Label_CountFemalesCross.Text = Me.C1FlexGrid_FemaleCross.Rows.Count - 1
        Me.Label_CountMalesCross.Text = Me.C1FlexGrid_MaleCross.Rows.Count - 1

        'Call WriteC1FlexGrid() DESCOMENTAR ESTO
        Me.TextBox_Password.Focus()
        Me.Button_SignIn.Enabled = True
        Me.Button_SelectCross.Enabled = True
        'Else
        'If Me.ComboBox_Language.SelectedIndex = 0 Then
        '    Call ShowSystemMessage(strMessage6)
        '    Me.TextBox_CrossingPlan.Text = ""
        'Else
        '    Call ShowSystemMessage(strMessage6)
        '    Me.TextBox_CrossingPlan.Text = ""
        'End If
        'End If

        Panel_SelectCrossPlan.Visible = False
    End Sub

    'Sing in/out
    Private Sub Button_SignIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SignIn.Click
        If Me.TextBox_Password.Text <> "" Then
            Dim username, password, selection As String
            Dim intRow As Integer

            username = Me.ComboBox_Username.SelectedItem
            password = Me.TextBox_Password.Text

            selection = Me.Button_SignIn.Text

            Select Case selection

                Case "Sign in"
                    For intRow = 0 To Me.ListBox_Username.Items.Count - 1
                        If username = Me.ListBox_Username.Items(intRow) Then
                            Try
                                If password = Me.ListBox_Password.Items(intRow) Then
                                    Me.TabControl.TabPages(1).Enabled = True
                                    Me.TabControl.TabPages(2).Enabled = True
                                    Me.TabControl.TabPages(3).Enabled = True
                                    Me.TabControl.TabPages(4).Enabled = True
                                    Me.TabControl.TabPages(5).Enabled = True
                                    Me.TabControl.TabPages(6).Enabled = True
                                    Me.TabControl.TabPages(7).Enabled = True
                                    Me.TabControl.TabPages(8).Enabled = True

                                    Me.TextBox_CrossingPlan.Enabled = False
                                    Me.Button_CrossingPlan.Enabled = False
                                    Me.ComboBox_Username.Enabled = False
                                    Me.TextBox_Password.Enabled = False
                                    Me.ComboBox_Location.Enabled = False
                                    Me.ComboBox_Language.Enabled = False

                                    Me.Button_SignIn.Text = "Sign out"
                                    Me.Button_SignIn.BackColor = Drawing.Color.DodgerBlue
                                    Me.TabControl.SelectedIndex = 7
                                    Me.Button_SelectPlan.Enabled = False

                                    'Carga el C1FlexGrid para cosecha y maceracion
                                    Me.ResultsTableAdapter1.FillByNPlan(Me.Results_DataSet1.results, Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1)
                                    GridHeaderName()

                                    'Select printer
                                    SelectPrinter()

                                    'Listar Vista
                                    GetView()
                                Else
                                    Call ShowSystemMessage(strMessage7)
                                End If
                            Catch ex As Exception
                                Call ShowSystemMessage(ex.Message.ToString())
                                Me.TextBox_Password.Text = ""
                                Exit Sub
                            End Try
                        End If
                    Next

                Case "Iniciar sesi�n"
                    For intRow = 0 To Me.ListBox_Username.Items.Count - 1
                        If username = Me.ListBox_Username.Items(intRow) Then
                            Try
                                If password = Me.ListBox_Password.Items(intRow) Then
                                    Me.TabControl.TabPages(1).Enabled = True
                                    Me.TabControl.TabPages(2).Enabled = True
                                    Me.TabControl.TabPages(3).Enabled = True
                                    Me.TabControl.TabPages(4).Enabled = True
                                    Me.TabControl.TabPages(5).Enabled = True
                                    Me.TabControl.TabPages(6).Enabled = True
                                    Me.TabControl.TabPages(7).Enabled = True
                                    Me.TabControl.TabPages(8).Enabled = True

                                    Me.TextBox_CrossingPlan.Enabled = False
                                    Me.Button_CrossingPlan.Enabled = False
                                    Me.ComboBox_Username.Enabled = False
                                    Me.TextBox_Password.Enabled = False
                                    Me.ComboBox_Location.Enabled = False
                                    Me.ComboBox_Language.Enabled = False

                                    Me.Button_SignIn.Text = "Cerrar sesi�n"
                                    Me.Button_SignIn.BackColor = Drawing.Color.DodgerBlue
                                    Me.TabControl.SelectedIndex = 7
                                    Me.Button_SelectPlan.Enabled = False

                                    'Carga el C1FlexGrid para cosecha y maceracion
                                    Me.ResultsTableAdapter1.FillByNPlan(Me.Results_DataSet1.results, Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1)
                                    GridHeaderName()
                                    'Listar Vista
                                    GetView()
                                Else
                                    Call ShowSystemMessage(strMessage7)
                                End If
                            Catch ex As Exception
                                Call ShowSystemMessage(strMessage8)
                                Me.TextBox_Password.Text = ""
                                Exit Sub
                            End Try
                        End If
                    Next

                Case "Sign out"

                    Me.TabControl.TabPages(1).Enabled = False
                    Me.TabControl.TabPages(2).Enabled = False
                    Me.TabControl.TabPages(3).Enabled = False
                    Me.TabControl.TabPages(4).Enabled = False
                    Me.TabControl.TabPages(5).Enabled = False
                    Me.TabControl.TabPages(6).Enabled = False
                    Me.TabControl.TabPages(7).Enabled = False
                    Me.TabControl.TabPages(8).Enabled = False

                    Me.TextBox_CrossingPlan.Enabled = True
                    Me.Button_CrossingPlan.Enabled = True
                    Me.ComboBox_Username.Enabled = True
                    Me.TextBox_Password.Enabled = True
                    Me.ComboBox_Location.Enabled = True
                    Me.ComboBox_Language.Enabled = True

                    Me.Button_SignIn.Text = "Sign in"

                    Me.TextBox_SearchFemale.Text = ""
                    Me.TextBox_SearchMale.Text = ""
                    Me.TextBox_Cross.Text = ""
                    Me.TextBox_Order.Text = ""
                    Me.TextBox_SearchFemaleCross.Text = ""
                    Me.TextBox_SearchMaleCross.Text = ""
                    Me.ComboBox_Flowers.SelectedIndex = 0

                    If Me.ComboBox_TypeCross.SelectedIndex <> -1 Then
                        Me.ComboBox_TypeCross.SelectedIndex = 0
                    End If

                    Me.TextBox_SearchHarvest.Text = ""
                    Me.TextBox_Harvest.Text = ""
                    Me.ComboBox_NroFruits.SelectedIndex = 0

                    If Me.ComboBox_FruitSize.SelectedIndex <> -1 Then
                        Me.ComboBox_FruitSize.SelectedIndex = 0
                    End If

                    Me.TextBox_SearchMaceration.Text = ""
                    Me.TextBox_Maceration.Text = ""
                    Me.ComboBox_BarcodeType.SelectedIndex = 0
                    Me.ComboBox_Port.SelectedIndex = 5
                    Me.ComboBox_Printer.SelectedIndex = 0 ' select default RW 220 printer

                    If Me.ComboBox_TypeCrossOption.SelectedIndex <> -1 Then
                        Me.ComboBox_TypeCrossOption.SelectedIndex = 0
                    End If

                    If Me.ComboBox_BarcodeType.SelectedIndex <> -1 Then
                        Me.ComboBox_BarcodeType.SelectedIndex = 0
                    End If

                    Dim signInColor As Color = Drawing.Color.FromArgb(255, 89, 0)

                    Me.Button_SignIn.BackColor = signInColor
                    Me.TextBox_Password.Text = ""
                    Me.TextBox_Password.Focus()
                    Me.Button_SelectPlan.Enabled = True

                Case "Cerrar sesi�n"

                    Me.TabControl.TabPages(1).Enabled = False
                    Me.TabControl.TabPages(2).Enabled = False
                    Me.TabControl.TabPages(3).Enabled = False
                    Me.TabControl.TabPages(4).Enabled = False
                    Me.TabControl.TabPages(5).Enabled = False
                    Me.TabControl.TabPages(6).Enabled = False
                    Me.TabControl.TabPages(7).Enabled = False
                    Me.TabControl.TabPages(8).Enabled = False

                    Me.TextBox_CrossingPlan.Enabled = True
                    Me.Button_CrossingPlan.Enabled = True
                    Me.ComboBox_Username.Enabled = True
                    Me.TextBox_Password.Enabled = True
                    Me.ComboBox_Location.Enabled = True
                    Me.ComboBox_Language.Enabled = True

                    Me.Button_SignIn.Text = "Iniciar sesi�n"

                    Me.TextBox_SearchFemale.Text = ""
                    Me.TextBox_SearchMale.Text = ""
                    Me.TextBox_Cross.Text = ""
                    Me.TextBox_Order.Text = ""
                    Me.TextBox_SearchFemaleCross.Text = ""
                    Me.TextBox_SearchMaleCross.Text = ""
                    Me.ComboBox_Flowers.SelectedIndex = 0

                    If Me.ComboBox_TypeCross.SelectedIndex <> -1 Then
                        Me.ComboBox_TypeCross.SelectedIndex = 0
                    End If

                    Me.TextBox_SearchHarvest.Text = ""
                    Me.TextBox_Harvest.Text = ""
                    Me.ComboBox_NroFruits.SelectedIndex = 0

                    If Me.ComboBox_FruitSize.SelectedIndex <> -1 Then
                        Me.ComboBox_FruitSize.SelectedIndex = 0
                    End If

                    Me.TextBox_SearchMaceration.Text = ""
                    Me.TextBox_Maceration.Text = ""
                    Me.ComboBox_BarcodeType.SelectedIndex = 0
                    Me.ComboBox_Port.SelectedIndex = 5

                    If Me.ComboBox_TypeCrossOption.SelectedIndex <> -1 Then
                        Me.ComboBox_TypeCrossOption.SelectedIndex = 0
                    End If

                    If Me.ComboBox_BarcodeType.SelectedIndex <> -1 Then
                        Me.ComboBox_BarcodeType.SelectedIndex = 0
                    End If

                    Dim signInColor As Color = Drawing.Color.FromArgb(255, 89, 0)

                    Me.Button_SignIn.BackColor = signInColor
                    Me.TextBox_Password.Text = ""
                    Me.TextBox_Password.Focus()
                    Me.Button_SelectPlan.Enabled = True

            End Select
        Else
            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage9)
            Else
                Call ShowSystemMessage(strMessage9)
            End If
        End If
    End Sub


#End Region
    'Languaje
    Private Sub ComboBox_Language_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_Language.SelectedIndexChanged
        If Trim(LCase(Me.ComboBox_Language.SelectedItem.ToString)) = "english (ingles)" Then
            'Messages
            strMessage1 = "Please, upload a Excel File"
            strMessage2 = "Incorrect template"
            strMessage3 = "Please, add username"
            strMessage4 = "Please, add location"
            strMessage5 = "Please, add barcode type"
            strMessage6 = "Incorrect template"
            strMessage7 = "Incorrect password"
            strMessage8 = "Please, add password in the template"
            strMessage9 = "Please, enter a password"
            strMessage10 = "Select parents to crossing"
            strMessage11 = "Select harvest row"
            strMessage12 = "Select maceration row"
            strMessage13 = "Please, enable bluetooth or turn on the printer..."
            strMessage14 = "Please, total berries must be less or equal to the number of pollinated flowers."
            strMessage15 = "Error: Write a valid number for Weight."
            strMessage16 = "Error: Write a valid number Weight1."
            strMessage17 = "Error: Write a valid integer for Quantity"
            strMessage18 = "Error: The CIPNumber does not exist."
            strMessage19 = "Error: The user does not exist."
            strMessage20 = "Error: The location does not exist."
            strMessage21 = "Error: Write a valid integer for Total Flowers."
            strMessage22 = "Error: The Type Cross does not exist."
            strMessage23 = "Error: Write a valid integer for Total Bayas."
            strMessage24 = "Are you sure want to print?"
            strMessage25 = "Are you sure delete to row?"

            'TabControl
            Me.Text = "CIPCROSS SYSTEM"

            'Tab1: User Log in
            Me.TabPage_User.Text = "User Log in"
            Me.Label_Languaje.Text = "Language"
            Me.Label_CrossingPlan.Text = "Crossing Plan"
            Me.Label_SignIn.Text = "Sign in"
            Me.Label_Username.Text = "Username"
            Me.Label_Password.Text = "Password"
            Me.Label_Location.Text = "Location"
            Me.Button_SignIn.Text = "Sign in"

            'Tab2: Crossing Plan
            'Me.TabPage_CrossingPlan.Text = "CrossingPlan"
            'Me.Label_CrossingPlanTitle.Text = "Crossing Plan"

            'Tab3: Parentals
            Me.TabPage_Parentals.Text = "Parentals"
            Me.Label_Parentals.Text = "Parentals"
            Me.Button_PrintFemales.Text = "Print Females"
            Me.Button_PrintMales.Text = "Print Males"
            Me.Label_ParentalsFemales.Text = "Females"
            Me.Label_ParentalsMales.Text = "Males"

            'Tab4: Crossing
            Me.TabPage_Crossing.Text = "Crossing"
            Me.Label_CrossingTitle.Text = "Crossing"
            Me.Button_SelectCross.Text = "Select Cross"
            Me.Label_Crossing.Text = "Crossing"
            Me.Label_Order.Text = "Order"
            Me.Label_TypeCross.Text = "Type cross"
            Me.Label_CrossingDate.Text = "Date"
            Me.Label_Flowers.Text = "Flowers"
            Me.Button_PrintCross.Text = "Print"
            Me.Label_CrossingFemales.Text = "Females"
            Me.Label_CrossingMales.Text = "Males"

            'Tab5: Harvest
            Me.TabPage_Harvest.Text = "Harvest"
            Me.Label_HarvestTitle.Text = "Harvest"
            Me.Button_SearchHarvest.Text = "Search"
            Me.Button_SelectHarvest.Text = "Select"
            Me.Label_Fruits.Text = "Nro Fruits"
            Me.Label_FruitSize.Text = "Fruit size"
            Me.Label_HarvestDate.Text = "Date"
            Me.Button_PrintHarvest.Text = "Print"

            'Tab6: Maceration
            Me.TabPage_Maceration.Text = "Maceration"
            Me.Label_MacerationTitle.Text = "Maceration"
            Me.Button_SearchMaceration.Text = "Search"
            Me.Button_SelectMaceration.Text = "Select"
            Me.Label_MacerationDate.Text = "Date"
            Me.Button_PrintMaceration.Text = "Print"

            'Tab7: Options
            Me.TabPage_Options.Text = "Options"
            Me.Label_OptionsTitle.Text = "Options"
            Me.Label_PrintOptions.Text = "Print options"
            Me.Label_BarcodeType.Text = "Barcode type"
            Me.Label_Port.Text = "Port"
            Me.Label_CrossingOptions.Text = "Crossing options"
            Me.Label_TypeCrossOption.Text = "Type cross"
            Me.Button_Close.Text = "Close"
        End If

        If Trim(LCase(Me.ComboBox_Language.SelectedItem.ToString)) = "spanish (espa�ol)" Then
            'Messages
            strMessage1 = "Por favor, subir un archivo de Excel"
            strMessage2 = "Plantilla incorrecta"
            strMessage3 = "Por favor, agregar usuario"
            strMessage4 = "Por favor, agregar ubicaci�n"
            strMessage5 = "Por favor, a�adir tipo de c�digo de barras"
            strMessage6 = "Plantilla incorrecta"
            strMessage7 = "Contrase�a incorrecta"
            strMessage8 = "Por favor, agregue una contrase�a en la plantilla"
            strMessage9 = "Por favor, ingrese una contrase�a"
            strMessage10 = "Seleccione los parentales para la cruza"
            strMessage11 = "Seleccione una cosecha"
            strMessage12 = "Seleccione una maceraci�n"
            strMessage13 = "Por favor, active el bluetooth o encienda la impresora..."
            strMessage14 = "Por favor, el total de bayas debe ser menor o igual al n�mero de flores polinizadas."
            strMessage15 = "Error: Digite un n�mero v�lido para el Peso."
            strMessage16 = "Error: Digite un n�mero v�lido para el Peso1."
            strMessage17 = "Error: Digite un entero v�lido para la Cantidad."
            strMessage18 = "Error: El CIPNumber no existe."
            strMessage19 = "Error: El Usuario no existe."
            strMessage20 = "Error: La Locaci�n no existe."
            strMessage21 = "Error: Digite un entero v�lido para el Total de Flores."
            strMessage22 = "Error: El Tipo de Cruzamiento no existe."
            strMessage23 = "Error: Digite un entero v�lido para el Total de Bayas."
            strMessage24 = "Est� seguro que desea imprimir?"
            strMessage25 = "Est� seguro que desea eliminar la fila seleccionada?"

            'TabControl
            Me.Text = "SISTEMA CIPCROSS"

            'Tab1: Usuario L-+og in
            Me.TabPage_User.Text = "Usuario Log in"
            Me.Label_Languaje.Text = "Idioma"
            Me.Label_CrossingPlan.Text = "Cruzamiento"
            Me.Label_SignIn.Text = "Iniciar sesi�n"
            Me.Label_Username.Text = "Usuario"
            Me.Label_Password.Text = "Contrase�a"
            Me.Label_Location.Text = "Ubicaci�n"
            Me.Button_SignIn.Text = "Iniciar sesi�n"

            'Tab2: Crossing Plan
            'Me.TabPage_CrossingPlan.Text = "PlanCruzamientos"
            'Me.Label_CrossingPlanTitle.Text = "Plan de Cruzamientos"

            'Tab3: Parentales
            Me.TabPage_Parentals.Text = "Parentales"
            Me.Label_Parentals.Text = "Parentales"
            Me.Button_PrintFemales.Text = "Imp. Hembras"
            Me.Button_PrintMales.Text = "Imp. Machos"
            Me.Label_ParentalsFemales.Text = "Hembras"
            Me.Label_ParentalsMales.Text = "Machos"

            'Tab4: Cruzamientos
            Me.TabPage_Crossing.Text = "Cruzamientos"
            Me.Label_CrossingTitle.Text = "Cruzamientos"
            Me.Button_SelectCross.Text = "Seleccionar"
            Me.Label_Crossing.Text = "Cruza"
            Me.Label_Order.Text = "Orden"
            Me.Label_TypeCross.Text = "Tipo cruza"
            Me.Label_CrossingDate.Text = "Fecha"
            Me.Label_Flowers.Text = "Flores"
            Me.Button_PrintCross.Text = "Imprimir"
            Me.Label_CrossingFemales.Text = "Hembras"
            Me.Label_CrossingMales.Text = "Machos"

            'Tab5: Cosecha
            Me.TabPage_Harvest.Text = "Cosecha bayas"
            Me.TabPage_Harvest_Tuber.Text = "Cosecha tuberculos/frutos"
            Me.Label_HarvestTitle.Text = "Cosecha de bayas"
            Me.Button_SearchHarvest.Text = "Buscar"
            Me.Button_SelectHarvest.Text = "Selec."
            Me.Label_Fruits.Text = "Nro Frutos"
            Me.Label_FruitSize.Text = "Fruit size"
            Me.Label_FruitSize.Text = "Tama�o Fr."
            Me.Label_HarvestDate.Text = "Fecha"
            Me.Button_PrintHarvest.Text = "Imprimir"

            'Tab6: Maceraci�n
            Me.TabPage_Maceration.Text = "Maceraci�n"
            Me.Label_MacerationTitle.Text = "Maceraci�n"
            Me.Button_SearchMaceration.Text = "Buscar"
            Me.Button_SelectMaceration.Text = "Selec."
            Me.Label_MacerationDate.Text = "Fecha"
            Me.Button_PrintMaceration.Text = "Imprimir"

            'Tab7: Opciones
            Me.TabPage_Options.Text = "Opciones"
            Me.Label_OptionsTitle.Text = "Opciones"
            Me.Label_PrintOptions.Text = "Opciones de impresi�n"
            Me.Label_BarcodeType.Text = "C�d. barras"
            Me.Label_Port.Text = "Puerto"
            Me.Label_CrossingOptions.Text = "Opciones de Cruzamiento"
            Me.Label_TypeCrossOption.Text = "Tipo cruza"
            Me.Button_Close.Text = "Cerrar"
        End If
    End Sub







    '=====Tab Parentals======
    'Print Females
    Private Sub Button_PrintFemales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_PrintFemales.Click
        Dim intRow As Integer, arrayValues() As String
        ReDim arrayValues(4)
        Dim strError As String

        strError = ""

        If Me.TextBox_CrossingPlan.Text <> "" Then
            For intRow = Me.C1FlexGrid_Female.Selection.TopRow To Me.C1FlexGrid_Female.Selection.BottomRow
                Call PopulateArray(intRow, arrayValues, "female")
                Call PrintLabel(arrayValues, Me.ComboBox_BarcodeType.SelectedItem.ToString, Me.ComboBox_Port.SelectedItem.ToString, "Female", strError)
                If strError <> "" Then
                    Exit For
                End If
            Next
        Else
            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage1)
            Else
                Call ShowSystemMessage(strMessage1)
            End If
            Exit Sub
        End If
    End Sub

    'Print Males
    Private Sub Button_PrintMales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_PrintMales.Click
        Dim intRow As Integer, arrayValues() As String
        ReDim arrayValues(4)
        Dim strError As String

        strError = ""

        If Me.TextBox_CrossingPlan.Text <> "" Then
            For intRow = Me.C1FlexGrid_Male.Selection.TopRow To Me.C1FlexGrid_Male.Selection.BottomRow
                Call PopulateArray(intRow, arrayValues, "male")
                Call PrintLabel(arrayValues, Me.ComboBox_BarcodeType.SelectedItem.ToString, Me.ComboBox_Port.SelectedItem.ToString, "Male", strError)
            Next
        Else
            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage1)
            Else
                Call ShowSystemMessage(strMessage1)
            End If
            Exit Sub
        End If
    End Sub

    'PopulateArray
    Private Sub PopulateArray(ByVal strValue As String, ByRef arrayValues() As String, ByVal strParentType As String)
        ReDim arrayValues(5)

        Dim stream As FileStream = File.Open(Me.OpenFileDialog1.FileName, FileMode.Open, FileAccess.Read)
        Dim excelReader As IExcelDataReader

        If Path.GetExtension(Me.TextBox_CrossingPlan.Text).ToLower().Equals(".xlsx") Then
            excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
        Else
            excelReader = ExcelReaderFactory.CreateBinaryReader(stream)
        End If

        Dim result As DataSet = excelReader.AsDataSet()
        Dim objTable As DataTable
        Dim intRow As Integer, intCol As Integer

        objTable = result.Tables(Me.ComboBox_SelectCrossingPlan.SelectedIndex)

        'Female
        If strParentType = "female" Then
            For intRow = 8 To objTable.Rows.Count - 1
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                    Exit For
                End If

                If objTable.Rows(intRow).ItemArray(0) = strValue Then
                    arrayValues(0) = objTable.Rows(intRow).ItemArray(0).ToString
                    arrayValues(1) = objTable.Rows(intRow).ItemArray(1).ToString
                    arrayValues(2) = objTable.Rows(intRow).ItemArray(2).ToString
                    arrayValues(3) = objTable.Rows(intRow).ItemArray(3).ToString
                    arrayValues(4) = objTable.Rows(intRow).ItemArray(4).ToString
                    Exit For
                End If
            Next

            excelReader.Close()
            arrayValues(5) = Me.DateTimePicker_Female.Text
        End If

        'Male
        If strParentType = "male" Then
            'Valida que No existen machos en columnas
            Dim intMCValida As Integer = 0
            For intCol = 4 To objTable.Columns.Count - 1
                If objTable.Rows(5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(4).ItemArray(intCol) Is DBNull.Value And objTable.Rows(3).ItemArray(intCol) Is DBNull.Value Then
                    Exit For
                End If
                intMCValida += 1
                Exit For
            Next

            If intMCValida = 0 Then
                For intRow = 8 To objTable.Rows.Count - 1
                    If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                        Exit For
                    End If

                    If objTable.Rows(intRow).ItemArray(0) = strValue Then
                        arrayValues(0) = objTable.Rows(intRow).ItemArray(0).ToString
                        arrayValues(1) = objTable.Rows(intRow).ItemArray(1).ToString
                        arrayValues(2) = objTable.Rows(intRow).ItemArray(2).ToString
                        arrayValues(3) = objTable.Rows(intRow).ItemArray(3).ToString
                        arrayValues(4) = objTable.Rows(intRow).ItemArray(4).ToString
                        Exit For
                    End If
                Next
            Else
                For intCol = 4 To objTable.Columns.Count - 1
                    If objTable.Rows(5).ItemArray(intCol) Is DBNull.Value And objTable.Rows(4).ItemArray(intCol) Is DBNull.Value And objTable.Rows(3).ItemArray(intCol) Is DBNull.Value Then
                        Exit For
                    End If

                    If objTable.Rows(intRow + 6).ItemArray(intCol) = strValue Then
                        arrayValues(0) = objTable.Rows(6).ItemArray(intCol).ToString
                        arrayValues(1) = objTable.Rows(5).ItemArray(intCol).ToString
                        arrayValues(2) = objTable.Rows(4).ItemArray(intCol).ToString
                        arrayValues(3) = objTable.Rows(3).ItemArray(intCol).ToString
                        arrayValues(4) = objTable.Rows(4).ItemArray(intCol).ToString
                        Exit For
                    End If
                Next
            End If

            excelReader.Close()
            arrayValues(5) = Me.DateTimePicker_Male.Text
        End If
    End Sub

    Private Sub PopulateArrayPotato(ByVal strValue As String, ByRef arrayValues() As String, ByVal strParentType As String)
        ReDim arrayValues(4)

        Dim stream As FileStream = File.Open(Me.OpenFileDialog1.FileName, FileMode.Open, FileAccess.Read)
        Dim excelReader As IExcelDataReader

        If Path.GetExtension(Me.TextBox_CrossingPlan.Text).ToLower().Equals(".xlsx") Then
            excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
        Else
            excelReader = ExcelReaderFactory.CreateBinaryReader(stream)
        End If

        Dim result As DataSet = excelReader.AsDataSet()
        Dim objTable As DataTable
        Dim intRow As Integer, intCol As Integer

        objTable = result.Tables(Me.ComboBox_SelectCrossingPlan.SelectedIndex)

        'Female
        If strParentType = "female" Then
            For intRow = intRowStart To objTable.Rows.Count - 1
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                    Exit For
                End If

                If objTable.Rows(intRow).ItemArray(1) = strValue Then
                    arrayValues(0) = objTable.Rows(intRow).ItemArray(0).ToString
                    arrayValues(1) = objTable.Rows(intRow).ItemArray(1).ToString
                    arrayValues(2) = objTable.Rows(intRow).ItemArray(2).ToString
                    arrayValues(3) = objTable.Rows(intRow).ItemArray(3).ToString
                    Exit For
                End If
            Next

            excelReader.Close()
            arrayValues(4) = Me.DateTimePicker_Female.Text
        End If

        'Male
        If strParentType = "male" Then
            For intRow = intRowStart To objTable.Rows.Count - 1
                If objTable.Rows(intRow).ItemArray(1) Is DBNull.Value And objTable.Rows(intRow).ItemArray(2) Is DBNull.Value And objTable.Rows(intRow).ItemArray(3) Is DBNull.Value Then
                    Exit For
                End If

                If objTable.Rows(intRow).ItemArray(1) = strValue Then
                    arrayValues(0) = objTable.Rows(intRow).ItemArray(0).ToString
                    arrayValues(1) = objTable.Rows(intRow).ItemArray(1).ToString
                    arrayValues(2) = objTable.Rows(intRow).ItemArray(2).ToString
                    arrayValues(3) = objTable.Rows(intRow).ItemArray(3).ToString
                    Exit For
                End If
            Next

            excelReader.Close()
            arrayValues(4) = Me.DateTimePicker_Male.Text
        End If

    End Sub

    'Search Female
    Private Sub Button_SearchFemale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SearchFemale.Click
        If Me.TextBox_SearchFemale.Text <> "" Then
            Call SearchParent(Me.C1FlexGrid_Female, Me.TextBox_SearchFemale.Text.ToUpper())
        End If
    End Sub

    'Search Male
    Private Sub Button_SearchMale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SearchMale.Click
        If Me.TextBox_SearchMale.Text <> "" Then
            Call SearchParent(Me.C1FlexGrid_Male, Me.TextBox_SearchMale.Text.ToUpper())
        End If
    End Sub

    'Search Parent
    Sub SearchParent(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal searchvalue As String)
        Dim intRow As Integer
        searchvalue = searchvalue.Replace(" ", "")

        For intRow = 0 To objGrid.Rows.Count - 1
            If searchvalue = objGrid(intRow, 0).ToString.ToUpper() Then
                objGrid.Select(intRow, 0)
            End If
            If searchvalue = objGrid(intRow, 1).ToString.ToUpper() Then
                objGrid.Select(intRow, 0)
            End If
            If searchvalue = objGrid(intRow, 2).ToString.ToUpper() Then
                objGrid.Select(intRow, 0)
            End If
            If searchvalue = objGrid(intRow, 3).ToString.ToUpper() Then
                objGrid.Select(intRow, 0)
            End If
        Next
    End Sub

#Region "Cruzamiento"

    '======Tab Crossing======
    'Select Cross
    Private Sub Button_SelectCross_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SelectCross.Click
        Dim intRowSelectedFemale, intRowSelectedMale As Integer, FData1, MData1, OrderF, OrderM, FData2, MData2, FData3, MData3 As String
        Dim count As Integer = 0
        Dim intRow As Integer


        'Dim arrayOutput As Byte() = New Byte(0) {}
        'Dim prueba As String
        'prueba = "..."
        'Dim encoding As New System.Text.ASCIIEncoding
        'arrayOutput(0) = encoding.GetBytes(prueba)(0)
        'arrayOutput(0) = Convert.ToByte(prueba)

        Try
            If Me.TextBox_CrossingPlan.Text <> "" Then
                If ComboBox_GenFemale.Visible = False Then

                    intRowSelectedFemale = Me.C1FlexGrid_FemaleCross.Selection.TopRow
                    intRowSelectedMale = Me.C1FlexGrid_MaleCross.Selection.TopRow

                    'Crossing
                    FData1 = Me.C1FlexGrid_FemaleCross.Item(intRowSelectedFemale, 1).ToString
                    MData1 = Me.C1FlexGrid_MaleCross.Item(intRowSelectedMale, 1).ToString

                    'Order
                    OrderF = Me.C1FlexGrid_FemaleCross.Item(intRowSelectedFemale, 0)
                    OrderM = Me.C1FlexGrid_MaleCross.Item(intRowSelectedMale, 0)

                    'Data2
                    FData2 = Me.C1FlexGrid_FemaleCross.Item(intRowSelectedFemale, 2).ToString
                    MData2 = Me.C1FlexGrid_MaleCross.Item(intRowSelectedMale, 2).ToString

                    'Data3
                    FData3 = Me.C1FlexGrid_FemaleCross.Item(intRowSelectedFemale, 3).ToString
                    MData3 = Me.C1FlexGrid_MaleCross.Item(intRowSelectedMale, 3).ToString

                    Me.TextBox_Cross.Text = FData1 + "x" + MData1
                    Me.TextBox_Order.Text = OrderF + " - " + OrderM
                    Me.TextBox_Data2.Text = FData2 + " - " + MData2
                    Me.TextBox_Data3.Text = FData3 + " - " + MData3

  

                Else

                    Dim intRowSelectedValue As Integer, VData1, VData2, VData3 As String
                    intRowSelectedValue = Me.C1FlexGrid_ValuesCross.Selection.TopRow

                    'Crossing
                    VData1 = Me.C1FlexGrid_ValuesCross.Item(intRowSelectedValue, 1).ToString

                    'Order
                    OrderF = Me.ComboBox_GenFemale.Text
                    OrderM = Me.ComboBox_GenMale.Text

                    'Data2
                    VData2 = Me.C1FlexGrid_ValuesCross.Item(intRowSelectedValue, 2).ToString

                    'Data3
                    VData3 = Me.C1FlexGrid_ValuesCross.Item(intRowSelectedValue, 3).ToString

                    Me.TextBox_Cross.Text = VData1 + "x" + VData1
                    Me.TextBox_Order.Text = OrderF + " - " + OrderM
                    Me.TextBox_Data2.Text = VData2 + " - " + VData2
                    Me.TextBox_Data3.Text = VData3 + " - " + VData3

                    If OrderF = OrderM Or OrderM = "AP" Then
                        Me.ComboBox_TypeCross.SelectedIndex = 2
                    ElseIf OrderM = "LP" Then
                        Me.ComboBox_TypeCross.SelectedIndex = 3
                    ElseIf OrderM = "Bulk" Then
                        Me.ComboBox_TypeCross.SelectedIndex = 1
                    Else
                        Me.ComboBox_TypeCross.SelectedIndex = 0
                    End If

                End If

                    ''Arreglar NroRepeticiones
                    'If File.Exists(curFile) Then
                    '    Call ReadCrossingResults()

                    '    For intRow = 0 To Me.ListBox_Hide.Items.Count - 1
                    '        Dim cadena As String = Me.ListBox_Hide.Items(intRow)
                    '        Dim words As String() = cadena.Split(New Char() {Chr(9)})
                    '        Dim word As String

                    '        Dim i As String = 0
                    '        Dim array(intNroMaxColumnsResults) As String

                    '        For Each word In words
                    '            array(i) = word
                    '            i += 1
                    '        Next

                    '        If array(intPositionF).Replace(" ", "") = OrderF And array(intPositionM).Replace(" ", "") = OrderM And array(intNroMaxColumnsResults - 1) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1).ToString() Then
                    '            count = array(intPosRepetitions)
                    '        End If
                    '    Next

                    '    Me.TextBox_NroRepetitions.Text = count + 1
                    'Else
                    '    Me.TextBox_NroRepetitions.Text = count + 1
                    'End If

                Else
                    If Me.ComboBox_Language.SelectedIndex = 0 Then
                        Call ShowSystemMessage(strMessage1)
                    Else
                        Call ShowSystemMessage(strMessage1)
                    End If
                    Exit Sub
                End If

            Me.ListBox_Hide.Items.Clear()
        Catch ex As Exception

        End Try
    End Sub

    'Print Cross
    Private Sub Button_PrintCross_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_PrintCross.Click
        If MsgBox(strMessage24, MsgBoxStyle.OkCancel, "Mensaje") = MsgBoxResult.Ok Then   ' User chose Yes.
            ' Perform some action.
            Dim arrayValues() As String
            ReDim arrayValues(8)
            Dim strError, ID As String

            boolImprimio = True

            strError = ""

            If Me.TextBox_Cross.Text <> "" Then
                'Genera ID
                Dim ss As String
                Dim rn As New Random
                Dim n1, n2, n3 As Integer
                ss = Date.Now.ToString("ss")
                n1 = rn.Next(10, 99)
                n2 = rn.Next(10, 99)
                n3 = rn.Next(10, 99)
                ID = n1.ToString + n2.ToString + n3.ToString + ss

                'Asigna valores al array.
                arrayValues(0) = Me.TextBox_Order.Text.Replace(" - ", "x")
                arrayValues(1) = ReplaceSpecialCharacters(Me.TextBox_Cross.Text)
                arrayValues(2) = Me.TextBox_Data2.Text
                arrayValues(3) = Me.TextBox_Data3.Text
                arrayValues(4) = Me.ComboBox_Flowers.SelectedItem
                arrayValues(5) = Me.ComboBox_TypeCross.SelectedItem
                arrayValues(6) = Me.DateTimePicker_Cross.Text
                arrayValues(7) = Me.TextBox_NroRepetitions.Text
                arrayValues(8) = ID

                Me.ListBox_Hide.Items.Clear()

                Call PrintLabel(arrayValues, Me.ComboBox_BarcodeType.SelectedItem.ToString, Me.ComboBox_Port.SelectedItem.ToString, "Crossing", strError)
                'Call WriteCrossingResults()
                'Call WriteC1FlexGrid()
                If boolImprimio = True Then
                    WriteCrossing(ID)
                    Me.TextBox_Cross.Text = ""
                    Me.TextBox_Order.Text = ""
                    Call GetView()
                End If
            Else
                If Me.ComboBox_Language.SelectedIndex = 0 Then
                    Call ShowSystemMessage(strMessage10)
                Else
                    Call ShowSystemMessage(strMessage10)
                End If
            End If
        Else
            ' Perform some other action.
        End If
    End Sub

    'Escribe los resultados en la BD
    Private Sub WriteCrossing(ByVal ID As String)
        Dim arrayValues() As String
        ReDim arrayValues(3)
        Dim intRow As Integer
        Dim valuesOrd(), valuesColN() As String

        Dim Plan, NroPlan, FDato1, FDato2, FDato3, FDato4, FDato5, MDato1, MDato2, MDato3, MDato4, MDato5, TipoCruzamiento, FOrder, MOrder, FloresPolinizadas, LocalidadCruce, NroRepeticiones, FechaImpresionCruce, UsuarioCruce, FechaCruce As String
        'Dim ResumenCosecha, TotalFloresPolinizadas, TotalFrutos, UsuarioCosecha, FechaCosecha, FechaImpresionCosecha As String
        'Dim TotalTuberculos, ProcedenciaTub, FechaCosechaTub, LocalidadCosecha, LocalidadCosechaTub, UsuarioCosechaTub As String
        'Dim LocalidadMaceracion, FechaMaceracion, UsuarioMaceracion, LOTID, EstadoResultado As String

        'Asignacion de valor a las variables de cruzamiento.
        NroPlan = Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1
        Plan = Me.TextBox_CrossingPlanSelectedHide.Text
        UsuarioCruce = Me.ComboBox_Username.SelectedItem
        LocalidadCruce = Me.ComboBox_Location.SelectedItem
        TipoCruzamiento = Me.ComboBox_TypeCross.SelectedItem
        NroRepeticiones = 0
        FloresPolinizadas = Me.ComboBox_Flowers.SelectedItem
        FechaImpresionCruce = Date.Now.ToString
        FechaCruce = Me.DateTimePicker_Cross.Text

        If ComboBox_GenMale.Visible = False Then
            valuesOrd = Me.TextBox_Order.Text.Replace(" ", "").Split("-")
            FOrder = valuesOrd(0)
            MOrder = valuesOrd(1)

            Call PopulateArray(FOrder, arrayValues, "female")
            FDato1 = arrayValues(1)
            FDato2 = arrayValues(2)
            FDato3 = arrayValues(3)
            FDato4 = arrayValues(4)

            Call PopulateArray(MOrder, arrayValues, "male")
            MDato1 = arrayValues(1)
            MDato2 = arrayValues(2)
            MDato3 = arrayValues(3)
            MDato4 = arrayValues(4)
        Else
            valuesOrd = Me.TextBox_Order.Text.Split("-")
            FOrder = valuesOrd(0)
            MOrder = valuesOrd(1)

            If Me.TextBox_Cross.Text.Split("x").Length > 2 Then
                valuesColN = Me.TextBox_Cross.Text.Split("x")
                FDato2 = valuesColN(0) + "x" + valuesColN(1)
                MDato2 = valuesColN(0) + "x" + valuesColN(1)
            Else
                valuesColN = Me.TextBox_Cross.Text.Split("x")
                FDato2 = valuesColN(0)
                MDato2 = valuesColN(1)
            End If

            Call PopulateArrayPotato(FDato2, arrayValues, "female")
            FDato1 = arrayValues(1)
            FDato2 = arrayValues(2)
            FDato3 = arrayValues(3)
            FDato4 = arrayValues(4)

            Call PopulateArrayPotato(MDato2, arrayValues, "male")
            MDato1 = arrayValues(1)
            MDato2 = arrayValues(2)
            MDato3 = arrayValues(3)
            MDato4 = arrayValues(4)
        End If

        'Escribe un record de cruza en la BD
        Dim drv As DataRowView = DirectCast(Me.BindingSource1.AddNew(), DataRowView)
        Dim dr As Results_Dataset.resultsRow = DirectCast(drv.Row, Results_Dataset.resultsRow)

        dr.ID = ID
        dr.FDato1 = FDato1
        dr.FDato2 = FDato2
        dr.FDato3 = FDato3
        dr.FDato4 = FDato4
        dr.FDato5 = FDato5
        dr.MDato1 = MDato1
        dr.MDato2 = MDato2
        dr.MDato3 = MDato3
        dr.MDato4 = MDato4
        dr.MDato5 = MDato5
        dr.TipoCruzamiento = TipoCruzamiento
        dr.FOrder = FOrder
        dr.MOrder = MOrder
        dr.FloresPolinizadas = FloresPolinizadas
        dr.LocalidadCruce = LocalidadCruce
        dr.NroRepeticiones = NroRepeticiones
        dr.FechaImpresionCruce = FechaImpresionCruce
        dr.UsuarioCruce = UsuarioCruce
        dr.FechaCruce = FechaCruce
        dr.ResumenCosecha = ""
        dr.TotalFloresPolinizadas = ""
        dr.TotalFrutos = ""
        dr.UsuarioCosecha = ""
        dr.FechaCosecha = ""
        dr.FechaImpresionCosecha = ""
        dr.TotalTuberculos = ""
        dr.ProcedenciaTub = ""
        dr.FechaCosechaTub = ""
        dr.LocalidadCosecha = ""
        dr.LocalidadCosechaTub = ""
        dr.UsuarioCosechaTub = ""
        dr.FechaImpresionCosechaTub = ""
        dr.LocalidadMaceracion = ""
        dr.FechaMaceracion = ""
        dr.UsuarioMaceracion = ""
        dr.LOTID = ""
        dr.EstadoResultado = 0
        dr.Plan = Plan
        dr.NroPlan = NroPlan

        drv.EndEdit()
        Me.ResultsTableAdapter1.Update(Me.Results_DataSet1.results)

        'Me.Label_CountHM.Text = Me.C1FlexGrid_HM.Rows.Count - 1
        Me.ListBox_Hide.Items.Clear()
    End Sub

    ''Write Crossing Results
    'Private Sub WriteCrossingResults()
    '    Dim arrayValues() As String
    '    ReDim arrayValues(3)
    '    Dim intRow As Integer

    '    Dim OrderF, FData1, FData2, FData3, OrderM, MData1, MData2, MData3, TypeCross, NroRepetitions, NroFlowers, PrintingCrossDate, CrossingDate, user1, location1, NroPlan, PlanName As String
    '    Dim valuesOrd(), valuesColN() As String

    '    user1 = Me.ComboBox_Username.SelectedItem
    '    location1 = Me.ComboBox_Location.SelectedItem
    '    NroPlan = Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1
    '    PlanName = Me.TextBox_CrossingPlanSelectedHide.Text

    '    If ComboBox_GenMale.Visible = False Then
    '        valuesOrd = Me.TextBox_Order.Text.Replace(" ", "").Split("-")
    '        OrderF = valuesOrd(0)
    '        OrderM = valuesOrd(1)

    '        Call PopulateArray(OrderF, arrayValues, "female")
    '        FData1 = arrayValues(1)
    '        FData2 = arrayValues(2)
    '        FData3 = arrayValues(3)

    '        Call PopulateArray(OrderM, arrayValues, "male")
    '        MData1 = arrayValues(1)
    '        MData2 = arrayValues(2)
    '        MData3 = arrayValues(3)
    '    Else
    '        valuesOrd = Me.TextBox_Order.Text.Split("-")
    '        OrderF = valuesOrd(0)
    '        OrderM = valuesOrd(1)

    '        If Me.TextBox_Cross.Text.Split("x").Length > 2 Then
    '            valuesColN = Me.TextBox_Cross.Text.Split("x")
    '            FData2 = valuesColN(0) + "x" + valuesColN(1)
    '            MData2 = valuesColN(0) + "x" + valuesColN(1)
    '        Else
    '            valuesColN = Me.TextBox_Cross.Text.Split("x")
    '            FData2 = valuesColN(0)
    '            MData2 = valuesColN(1)
    '        End If

    '        Call PopulateArrayPotato(FData2, arrayValues, "female")
    '        FData1 = arrayValues(1)
    '        FData2 = arrayValues(2)
    '        FData3 = arrayValues(3)

    '        Call PopulateArrayPotato(MData2, arrayValues, "male")
    '        MData1 = arrayValues(1)
    '        MData2 = arrayValues(2)
    '        MData3 = arrayValues(3)
    '    End If

    '    TypeCross = Me.ComboBox_TypeCross.SelectedItem
    '    NroRepetitions = Me.TextBox_NroRepetitions.Text
    '    NroFlowers = Me.ComboBox_Flowers.SelectedItem
    '    PrintingCrossDate = Date.Now
    '    CrossingDate = Me.DateTimePicker_Cross.Text

    '    If File.Exists(curFile) Then
    '        'Read
    '        Call ReadCrossingResults()

    '        'Write
    '        Dim detailsNameWrite As String = curFile
    '        Dim objWriter As New System.IO.StreamWriter(detailsNameWrite)

    '        For intRow = 0 To Me.ListBox_Hide.Items.Count - 1
    '            objWriter.WriteLine(Me.ListBox_Hide.Items(intRow))
    '        Next
    '        objWriter.WriteLine( _
    '            FData1 + Chr(9) + _
    '            FData2 + Chr(9) + _
    '            FData3 + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            MData1 + Chr(9) + _
    '            MData2 + Chr(9) + _
    '            MData3 + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            TypeCross + Chr(9) + _
    '            OrderF + Chr(9) + _
    '            OrderM + Chr(9) + _
    '            NroFlowers + Chr(9) + _
    '            CrossingDate + Chr(9) + _
    '            location1 + Chr(9) + _
    '            NroRepetitions + Chr(9) + _
    '            PrintingCrossDate + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            user1 + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            NroPlan + Chr(9) + _
    '            PlanName)
    '        objWriter.Close()

    '    Else
    '        Dim detailsNameWrite As String = curFile
    '        Dim objWriter As New System.IO.StreamWriter(detailsNameWrite)

    '        For intRow = 0 To Me.ListBox_Hide.Items.Count - 1
    '            objWriter.WriteLine(Me.ListBox_Hide.Items(intRow))
    '        Next
    '        objWriter.WriteLine( _
    '            FData1 + Chr(9) + _
    '            FData2 + Chr(9) + _
    '            FData3 + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            MData1 + Chr(9) + _
    '            MData2 + Chr(9) + _
    '            MData3 + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            TypeCross + Chr(9) + _
    '            OrderF + Chr(9) + _
    '            OrderM + Chr(9) + _
    '            NroFlowers + Chr(9) + _
    '            CrossingDate + Chr(9) + _
    '            location1 + Chr(9) + _
    '            NroRepetitions + Chr(9) + _
    '            PrintingCrossDate + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            user1 + Chr(9) + _
    '            "" + Chr(9) + _
    '            "" + Chr(9) + _
    '            NroPlan + Chr(9) + _
    '            PlanName)
    '        objWriter.Close()
    '    End If

    '    Me.ListBox_Hide.Items.Clear()
    'End Sub

    'Search FemaleCross

    Private Sub Button_SearchFemaleCross_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SearchFemaleCross.Click
        If Me.TextBox_SearchFemaleCross.Text <> "" Then
            Call SearchParent(Me.C1FlexGrid_FemaleCross, Me.TextBox_SearchFemaleCross.Text.ToUpper())
        End If
    End Sub

    'Search MaleCross
    Private Sub Button_SearchMaleCross_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SearchMaleCross.Click
        If Me.TextBox_SearchMaleCross.Text <> "" Then
            Call SearchParent(Me.C1FlexGrid_MaleCross, Me.TextBox_SearchMaleCross.Text.ToUpper())
        End If
    End Sub

#End Region

#Region "Cosecha"
    '======Tab Harvest=======
    'Select Harvest
    Private Sub Button_SelectHarvest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SelectHarvest.Click
        Dim intRowSelected, intRowM As Integer, strColectorSel, FData1, MData1, NroFData1, NroMData1, NroFlores, strColector, strTipoCruza As String, indexHarvest As Integer
        Dim intNroFloresHarvest, intTotalFloresHarvest, intNroPlan, intSuperTotalBayas, intSuperTotalFlores As Integer, FData1Harvest, FechaInicial, FechaFinal, FechaCruzamiento As String
        Dim FechaInicio, FechaFin, FechaPrueba As Date

        If Me.C1FlexGrid_Harvest.Rows.Count > 1 Then
            intRowSelected = Me.C1FlexGrid_Harvest.Selection.BottomRow
            lblID.Text = Me.C1FlexGrid_Harvest.Item(intRowSelected, 0)
            FData1 = Me.C1FlexGrid_Harvest.Item(intRowSelected, 1)
            MData1 = Me.C1FlexGrid_Harvest.Item(intRowSelected, 6)
            NroFData1 = Me.C1FlexGrid_Harvest.Item(intRowSelected, 12)
            NroFlores = Me.C1FlexGrid_Harvest.Item(intRowSelected, 14)
            strColectorSel = Me.C1FlexGrid_Harvest.Item(intRowSelected, 1)
            FechaInicial = dtpHarvestIni.Text
            FechaFinal = dtpHarvestFin.Text

            indexHarvest = Me.C1FlexGrid_Harvest.Row
            Me.TextBox_IndexHarvest.Text = indexHarvest
            Me.TextBox_Harvest.Text = FData1 + "x" + MData1
            Me.TextBox_HideNroRepetitions.Text = Me.C1FlexGrid_Harvest.Item(indexHarvest, 16)

            strColector = Me.C1FlexGrid_Harvest.Item(CInt(Me.TextBox_IndexHarvest.Text), 1)
            strTipoCruza = Me.C1FlexGrid_Harvest.Item(CInt(Me.TextBox_IndexHarvest.Text), 11)
            'strTipoCruza = ComboBox_TypeCrossOption.Text

            'CDate(Me.C1FlexGrid_Harvest(intRow1, 19)).ToShortDateString()

            'FechaInicial = CDate(dtpHarvestIni.Text).ToShortDateString()
            'FechaFinal = CDate(dtpHarvestFin.Text).ToShortDateString()
            'FechaCruzamiento = CDate(Me.C1FlexGrid_Harvest(CInt(Me.TextBox_IndexHarvest.Text), 19)).ToShortDateString()

            'If FechaCruzamiento >= FechaInicial And FechaCruzamiento <= FechaFinal Then
            'End If

            If ChkDateHarvest.Checked = True Then
                TextBox_Harvest.Visible = False
                txtHarvest_Potato.Visible = True
                FechaInicio = CDate(dtpHarvestIni.Text)
                FechaFin = CDate(dtpHarvestFin.Text)

                'Obtener el total de flores acumuladas por colector, nro de progenitor hembra y que est� dentro del rango de fechas.
                'For intRowM = 1 To Me.C1FlexGrid_Harvest.Rows.Count - 1
                'intNroFloresHarvest = Me.C1FlexGrid_Harvest.Item(intRowM, 10)
                'FData1Harvest = Me.C1FlexGrid_Harvest.Item(intRowM, 1)
                'FechaCruzamiento = Me.C1FlexGrid_Harvest(intRowM, 15)

                'If FData1 = FData1Harvest And NroFData1 = Me.C1FlexGrid_Harvest.Item(intRowM, 0) And (FechaCruzamiento >= FechaInicial And FechaCruzamiento <= FechaFinal) Then
                '    If NroMData1 = "" Then
                '        NroMData1 = Me.C1FlexGrid_Harvest.Item(intRowM, 4)
                '    Else
                '        NroMData1 = NroMData1 + "," + Me.C1FlexGrid_Harvest.Item(intRowM, 4)
                '    End If
                '    intTotalFloresHarvest = intTotalFloresHarvest + intNroFloresHarvest
                'End If
                'Next

                For intRow1 As Integer = 1 To Me.C1FlexGrid_Harvest.Rows.Count - 1
                    'intNroPlan = Me.C1FlexGrid_Harvest.Item(CInt(Me.TextBox_IndexHarvest.Text), 23)

                    'Dim PRUEBA As String = Me.C1FlexGrid_Harvest(intRow1, 14)
                    'Dim PRUEBA1 As String = Me.C1FlexGrid_Harvest(intRow1, 15)
                    'Dim PRUEBA2 As String = Me.C1FlexGrid_Harvest(intRow1, 16)
                    'Dim PRUEBA3 As String = Me.C1FlexGrid_Harvest(intRow1, 17)
                    'Dim prueba4 As String = Me.C1FlexGrid_Harvest.Item(intRow1, 8)
                    'Dim prueba5 As String = Me.C1FlexGrid_Harvest.Item(intRow1, 1)
                    'Dim prueba6 As String = Me.C1FlexGrid_Harvest.Item(intRow1, 23)
                    'Dim prueba7 As String = Me.C1FlexGrid_Harvest.Item(intRow1, 16)
                    'Dim prueba8 As String = Me.C1FlexGrid_Harvest.Item(intRow1, 13)
                    'Dim prueba8 As String = CDate(Me.C1FlexGrid_Harvest(intRow1, 19)).ToShortDateString()
                    'Dim prueba7 As String = CDate(dtpHarvestIni.Text).ToShortDateString()
                    'Dim prueba9 As String = CDate(dtpHarvestFin.Text).ToShortDateString()
                    'If CDate(Me.C1FlexGrid_Harvest(intRow1, 19)).ToShortDateString() >= CDate(dtpHarvestIni.Text).ToShortDateString() And CDate(Me.C1FlexGrid_Harvest(intRow1, 19)).ToShortDateString() <= CDate(dtpHarvestFin.Text).ToShortDateString() Then
                    '    Dim prueba As String = Me.C1FlexGrid_Harvest.Item(intRow1, 21)
                    '    Dim prueba2 As String = Me.C1FlexGrid_Harvest.Item(intRow1, 22)
                    '    If Me.C1FlexGrid_Harvest.Item(intRow1, 22) <> "" Then 'Que exista cosecha.
                    '        intSuperTotalFlores = intSuperTotalFlores + Me.C1FlexGrid_Harvest.Item(intRow1, 21)
                    '        intSuperTotalBayas = intSuperTotalBayas + Me.C1FlexGrid_Harvest.Item(intRow1, 22)
                    '    End If
                    'End If
                    'If Me.C1FlexGrid_Harvest(intRow1, 16) >= dtpHarvestIni.Text Then
                    '    Dim prueba222 As String = "esto es una prueba"
                    'End If
                    'Dim prueba As String = Me.C1FlexGrid_Harvest.Item(intRow1,)
                    'If strTipoCruza = Me.C1FlexGrid_Harvest.Item(intRow1, 11) And strColector = Me.C1FlexGrid_Harvest.Item(intRow1, 1) And intNroPlan = Me.C1FlexGrid_Harvest.Item(intRow1, 39) And (Me.C1FlexGrid_Harvest(intRow1, 16) >= dtpHarvestIni.Text And Me.C1FlexGrid_Harvest(intRow1, 16) <= dtpHarvestFin.Text) Then
                    'Validar si Nro Plan va 'strTipoCruza = Me.C1FlexGrid_Harvest.Item(intRow1, 11) And '
                    FechaCruzamiento = (Me.C1FlexGrid_Harvest(intRow1, 19))
                    If FechaCruzamiento <> "" Then
                        If strColector = Me.C1FlexGrid_Harvest.Item(intRow1, 1) And strTipoCruza = Me.C1FlexGrid_Harvest.Item(intRow1, 11) And (FechaCruzamiento >= FechaInicio And FechaCruzamiento <= FechaFin) Then
                            'Dim prueba As String = Me.C1FlexGrid_Harvest.Item(intRow1, 21)
                            'Dim prueba2 As String = Me.C1FlexGrid_Harvest.Item(intRow1, 22)
                            If Me.C1FlexGrid_Harvest.Item(intRow1, 22) <> "" Then 'Que exista cosecha.
                                intSuperTotalFlores = intSuperTotalFlores + Me.C1FlexGrid_Harvest.Item(intRow1, 21)
                                intSuperTotalBayas = intSuperTotalBayas + Me.C1FlexGrid_Harvest.Item(intRow1, 22)
                            End If
                        End If
                    End If
                Next

                If intSuperTotalFlores <> 0 And intSuperTotalBayas <> 0 Then
                    'Me.txtHarvest_Potato.Text = FData1 + " \ F: " + NroFData1.ToString() + " M: " + NroMData1.ToString()
                    Me.txtHarvest_Potato.Text = strColectorSel + " \ T. Flores: " + intSuperTotalFlores.ToString() + " T. Frutos: " + intSuperTotalBayas.ToString()
                Else
                    Me.txtHarvest_Potato.Text = ""
                End If

                'Me.ComboBox_TotalFlowers.Text = intTotalFloresHarvest
                'Me.ComboBox_NroPlants.Text = intTotalFloresHarvest ::::::::::::::Resumen de Cosecha = 20, Total de Flores = 13, Tipo de Cruza:11, Total de Flores: 21, Total de Frutos: 22
                'Nro Plan:39, Fecha de Cruce: 19
            Else
                TextBox_Harvest.Visible = True
                txtHarvest_Potato.Visible = False
                Me.ComboBox_TotalFlowers.Text = NroFlores
            End If
        End If
    End Sub

    'Print Harvest
    Private Sub Button_PrintHarvest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_PrintHarvest.Click
        If MsgBox(strMessage24, MsgBoxStyle.OkCancel, "Mensaje") = MsgBoxResult.Ok Then
            Dim arrayValues() As String
            Dim intTotalFlores As Integer
            Dim intSuperTotalFlores As Integer
            Dim intSuperTotalBayas As Integer
            ReDim arrayValues(14)
            Dim strError As String
            Dim FechaInicio, FechaFin, FechaCruzamiento As Date
            Dim strColector, strTipoCruza As String

            strError = ""

            If Me.TextBox_Harvest.Text <> "" Then
                'If boolImprimio = True Then

                'Validar que el Total de Flores no sea Menor al total de Frutos
                intTotalFlores = ComboBox_TotalFlowers.Text
                If validatePrintHarvest(intTotalFlores) = True Then

                    'Obtener SuperTotalFlores and SuperTotalBayas solo si el Checkbox est� activado
                    If ChkDateHarvest.Checked = True Then
                        strColector = Me.C1FlexGrid_Harvest.Item(CInt(Me.TextBox_IndexHarvest.Text), 1)
                        strTipoCruza = Me.C1FlexGrid_Harvest.Item(CInt(Me.TextBox_IndexHarvest.Text), 11)
                        FechaInicio = CDate(dtpHarvestIni.Text)
                        FechaFin = CDate(dtpHarvestFin.Text)
                        For intRow1 As Integer = 1 To Me.C1FlexGrid_Harvest.Rows.Count - 1
                            'strColector = Me.C1FlexGrid_Harvest.Item(CInt(Me.TextBox_IndexHarvest.Text), 1)
                            'strTipoCruza = ComboBox_TypeCrossOption.Text
                            'Dim intNroFemale = Me.C1FlexGrid_Harvest.Item(CInt(Me.TextBox_IndexHarvest.Text), 0)
                            'Dim intNroPlan = Me.C1FlexGrid_Harvest.Item(CInt(Me.TextBox_IndexHarvest.Text), 39)
                            'If strTipoCruza = Me.C1FlexGrid_Harvest.Item(intRow1, 11) And strColector = Me.C1FlexGrid_Harvest.Item(intRow1, 1) And (Me.C1FlexGrid_Harvest(intRow1, 19) >= dtpHarvestIni.Text And Me.C1FlexGrid_Harvest(intRow1, 19) <= dtpHarvestFin.Text) Then

                            If (Me.C1FlexGrid_Harvest(intRow1, 19)) <> "" Then
                                FechaCruzamiento = (Me.C1FlexGrid_Harvest(intRow1, 19))
                                If strColector = Me.C1FlexGrid_Harvest.Item(intRow1, 1) And strTipoCruza = Me.C1FlexGrid_Harvest.Item(intRow1, 11) And (FechaCruzamiento >= FechaInicio And FechaCruzamiento <= FechaFin) Then
                                    'If strColector = Me.C1FlexGrid_Harvest.Item(intRow1, 1) And strTipoCruza = Me.C1FlexGrid_Harvest.Item(intRow1, 11) And (CDate(Me.C1FlexGrid_Harvest(intRow1, 19)).ToShortDateString() >= CDate(dtpHarvestIni.Text).ToShortDateString() And CDate(Me.C1FlexGrid_Harvest(intRow1, 19)).ToShortDateString() <= CDate(dtpHarvestFin.Text).ToShortDateString()) Then
                                    If Me.C1FlexGrid_Harvest.Item(intRow1, 22) <> "" Then
                                        intSuperTotalFlores = intSuperTotalFlores + Me.C1FlexGrid_Harvest.Item(intRow1, 21)
                                        intSuperTotalBayas = intSuperTotalBayas + Me.C1FlexGrid_Harvest.Item(intRow1, 22)
                                    End If
                                End If
                            End If
                        Next
                    End If

                    'Llena el ArraValues para la impresi�n
                    Dim intRow As Integer
                    Dim strUser2 As String
                    strUser2 = Me.ComboBox_Username.SelectedItem
                    arrayValues(0) = Me.TextBox_Harvest.Text
                    For intRow = 1 To Me.C1FlexGrid_Harvest.Rows.Count - 1
                        If Me.TextBox_IndexHarvest.Text = intRow Then
                            arrayValues(1) = Me.C1FlexGrid_Harvest.Item(intRow, 12) + "x" + Me.C1FlexGrid_Harvest.Item(intRow, 13) 'Progenitores
                            arrayValues(2) = Me.C1FlexGrid_Harvest.Item(intRow, 2) + " - " + Me.C1FlexGrid_Harvest.Item(intRow, 7) 'CipNumber
                            arrayValues(3) = Me.C1FlexGrid_Harvest.Item(intRow, 3) + " - " + Me.C1FlexGrid_Harvest.Item(intRow, 8) 'Especie
                            arrayValues(4) = CStr(Me.C1FlexGrid_Harvest.Item(intRow, 14)) + "F  " '+ Me.C1FlexGrid_Harvest.Item(intRow, 8) No se sabe su uso.
                            arrayValues(5) = Me.C1FlexGrid_Harvest.Item(intRow, 14) 'No se sabe su uso.
                            'Set value - Potato wild print
                            arrayValues(9) = Me.C1FlexGrid_Harvest.Item(intRow, 8) 'No se sabe su uso.
                            arrayValues(10) = Me.C1FlexGrid_Harvest.Item(intRow, 11) 'Tipo de Cruce
                        End If
                    Next

                    arrayValues(6) = Me.ComboBox_NroFruits.SelectedItem + "Fr  " + Me.ComboBox_FruitSize.SelectedItem 'Total de Bayas + Tipo de Fruto
                    arrayValues(7) = Me.DateTimePicker_Harvest.Text 'Fecha de Cosecha
                    arrayValues(8) = Me.ComboBox_TotalFlowers.Text 'Flores
                    'Set value - Potato wild print
                    arrayValues(11) = Me.ComboBox_NroFruits.SelectedItem 'Bayas
                    arrayValues(12) = intSuperTotalFlores
                    arrayValues(13) = intSuperTotalBayas

                    'Add Barcode
                    arrayValues(14) = lblID.Text

                    'Call ReadCrossingResults()
                    'Dim strCrossValue, numbers(), progenitores() As String
                    'strCrossValue = Me.TextBox_Harvest.Text
                    'numbers = strCrossValue.Split("x")
                    'progenitores = arrayValues(1).Split("x")
                    If ComboBox_PrintType.SelectedIndex = 1 Then
                        If ChkDateHarvest.Checked = True Then 'Solo imprime la etiqueta general.
                            Call PrintLabel(arrayValues, Me.ComboBox_BarcodeType.SelectedItem.ToString, Me.ComboBox_Port.SelectedItem.ToString, "Harvest", strError)
                        Else
                            boolImprimio = True
                        End If
                    ElseIf ComboBox_PrintType.SelectedIndex = 2 Then
                        Call PrintLabel(arrayValues, Me.ComboBox_BarcodeType.SelectedItem.ToString, Me.ComboBox_Port.SelectedItem.ToString, "Harvest", strError)
                    End If

                    If boolImprimio = True Then
                        Dim resultRow As Results_Dataset.resultsRow
                        resultRow = Results_DataSet1.results.FindByID(CInt(lblID.Text))

                        If ChkDateHarvest.Checked = True Then
                            resultRow.ResumenCosecha = txtHarvest_Potato.Text
                        Else
                            'resultRow.FloresPolinizadas = CInt(ComboBox_TotalFlowers.Text)
                            resultRow.TotalFloresPolinizadas = CInt(ComboBox_TotalFlowers.Text)
                            resultRow.TotalFrutos = ComboBox_NroFruits.Text
                            resultRow.UsuarioCosecha = strUser2
                            resultRow.FechaCosecha = Me.DateTimePicker_Harvest.Text
                            resultRow.FechaImpresionCosecha = Date.Now.ToString()
                            resultRow.LocalidadCosecha = ComboBox_Location.Text
                            resultRow.EstadoResultado = 1
                        End If
                        'Save the updated row to the database 
                        Me.ResultsTableAdapter1.Update(Me.Results_DataSet1.results)

                        Me.ListBox_Hide.Items.Clear()
                        lblID.Text = ""
                        TextBox_Harvest.Visible = True
                        txtHarvest_Potato.Visible = False
                        TextBox_Harvest.Text = ""
                        txtHarvest_Potato.Text = ""
                        'Me.ComboBox_TotalFlowers.SelectedIndex = -1
                        'Me.ComboBox_FruitSize.SelectedIndex = -1
                        'Me.TextBox_SearchHarvest.Text = ""
                        Call GetView()
                    End If

                End If
                'If boolImprimio = True Then
                '    WriteCrossing(ID)
                '    Me.TextBox_Cross.Text = ""
                '    Me.TextBox_Order.Text = ""
                'End If

                'If strColector = Me.C1FlexGrid_Harvest.Item(intRow1, 1) And strTipoCruza = Me.C1FlexGrid_Harvest.Item(intRow1, 11) And (CDate(Me.C1FlexGrid_Harvest(intRow1, 19)).ToShortDateString() >= CDate(dtpHarvestIni.Text).ToShortDateString() And CDate(Me.C1FlexGrid_Harvest(intRow1, 19)).ToShortDateString() <= CDate(dtpHarvestFin.Text).ToShortDateString()) Then
                '    'Dim prueba As String = Me.C1FlexGrid_Harvest.Item(intRow1, 21)
                '    'Dim prueba2 As String = Me.C1FlexGrid_Harvest.Item(intRow1, 22)
                '    If Me.C1FlexGrid_Harvest.Item(intRow1, 22) <> "" Then 'Que exista cosecha.
                '        intSuperTotalFlores = intSuperTotalFlores + Me.C1FlexGrid_Harvest.Item(intRow1, 21)
                '        intSuperTotalBayas = intSuperTotalBayas + Me.C1FlexGrid_Harvest.Item(intRow1, 22)
                '    End If
                'End If


            Else
                If Me.ComboBox_Language.SelectedIndex = 0 Then
                    Call ShowSystemMessage(strMessage11)
                Else
                    Call ShowSystemMessage(strMessage11)
                End If
            End If
        End If
    End Sub

    'Validate print Harvest
    Private Function validatePrintHarvest(ByVal tflores As Integer) As Boolean
        Dim retorna As Boolean = True
        'If ComboBox_TypeHarvest.SelectedIndex = 0 Then
        If CInt(ComboBox_NroFruits.Text) > tflores Then
            retorna = False
            Call ShowSystemMessage(strMessage14)
        End If
        'End If
        Return retorna
    End Function

    'Validate print Harvest Tuber
    Private Function validatePrintHarvestTuber(ByVal tflores As Integer) As Boolean
        Dim retorna As Boolean = True
        'If ComboBox_TypeHarvest.SelectedIndex = 0 Then
        If CInt(ComboBox_NroTubers.Text) > tflores Then
            retorna = False
            Call ShowSystemMessage("Por favor, el total de tub�rculos debe ser menor o igual al n�mero de plantas.")
        End If
        'End If
        Return retorna
    End Function

    'Search Harvest
    Private Sub Button_SearchHarvest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SearchHarvest.Click
        If Me.TextBox_SearchHarvest.Text <> "" Then
            Call SearchCross(Me.C1FlexGrid_Harvest, Me.TextBox_SearchHarvest.Text.ToUpper(), Me.Label_CountHarvest, Me.Button_LoadHarvest, "Nuevo")
            Me.TextBox_SearchHarvest.Focus()
            Me.TextBox_SearchHarvest.SelectAll()
        End If
    End Sub

    'Load Harvest
    Private Sub Button_LoadHarvest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_LoadHarvest.Click

        If MsgBox(strMessage25, MsgBoxStyle.OkCancel, "Mensaje") = MsgBoxResult.Ok Then
            lblID.Text = Me.C1FlexGrid_Harvest.Item(Me.C1FlexGrid_Harvest.Selection.BottomRow, 0)

            Dim resultRow As Results_Dataset.resultsRow
            resultRow = Results_DataSet1.results.FindByID(CInt(lblID.Text))
            ' Delete the row from the dataset
            resultRow.Delete()
            ' Save the updated row to the database 
            lblID.Text = ""
            Me.ResultsTableAdapter1.Update(Me.Results_DataSet1.results)
            'Listar Vista
            GetView()
        End If
        'If File.Exists(curFile) Then
        '    Me.TextBox_SearchHarvest.Text = ""
        '    Call WriteC1FlexGrid()
        '    Me.Button_LoadHarvest.Visible = False
        '    Me.TextBox_SearchHarvest.Focus()
        'End If
    End Sub

    'Search Cross
    Sub SearchCross(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal searchValue As String, ByRef objLabel As Label, ByRef objButton As Button, ByVal strCondicional As String)
        Dim arrayValues() As String
        Dim intRow As Integer

        If strCondicional = "Antiguo" Then

            '' ''Call WriteC1FlexGrid()

            Dim i As Integer = 1

            'searchValue = searchValue.Replace(" ", "")
            arrayValues = searchValue.Split("X")

            'Fields
            Dim posF1, posF2, posF3, posM1, posM2, posM3 As String

            'Females
            If Me.ListBox_FemaleLabels.Items.Item(0) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(1) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(2) Is DBNull.Value Then
                'Female
                posF1 = "F.Data1"
                posF2 = "F.Data2"
                posF3 = "F.Data3"
            Else
                'Female
                posF1 = Me.ListBox_FemaleLabels.Items.Item(0).ToString
                posF2 = Me.ListBox_FemaleLabels.Items.Item(1).ToString
                posF3 = Me.ListBox_FemaleLabels.Items.Item(2).ToString
            End If

            'Males
            If Me.ListBox_MaleLabels.Items.Item(0) Is DBNull.Value And Me.ListBox_MaleLabels.Items.Item(1) Is DBNull.Value And Me.ListBox_MaleLabels.Items.Item(2) Is DBNull.Value Then
                'Female
                posM1 = "M.Data1"
                posM2 = "M.Data2"
                posM3 = "M.Data3"
            Else
                'Female
                posM1 = Me.ListBox_MaleLabels.Items.Item(0).ToString
                posM2 = Me.ListBox_MaleLabels.Items.Item(1).ToString
                posM3 = Me.ListBox_MaleLabels.Items.Item(2).ToString
            End If

            'Columns Name
            'Haverts
            objGrid(0, 0) = "OrderF."
            objGrid(0, 1) = posF1
            objGrid(0, 2) = posF2
            objGrid(0, 3) = posF3
            objGrid(0, 4) = "OrderM."
            objGrid(0, 5) = posM1
            objGrid(0, 6) = posM2
            objGrid(0, 7) = posM3
            objGrid(0, 8) = "TypeCross"
            objGrid(0, 9) = "Repetitions"
            objGrid(0, 10) = "Flower(s)"
            objGrid(0, 11) = "Fruit(s)"
            objGrid(0, 12) = "FruitSize"
            objGrid(0, 13) = "PrintingCrossDate"
            objGrid(0, 14) = "CrossingDate"
            objGrid(0, 15) = "HarvestDate"
            objGrid(0, 16) = "MacerationDate"
            objGrid(0, 17) = "User1"
            objGrid(0, 18) = "User2"
            objGrid(0, 19) = "User3"
            objGrid(0, 20) = "Location1"
            objGrid(0, 21) = "Location2"
            objGrid(0, 22) = "NroPlan"
            objGrid(0, 23) = "PlanName"

            For intRow = 0 To objGrid.Rows.Count - 1
                If objGrid.Item(intRow, 0).ToString.ToUpper() = searchValue Then
                    objGrid(i, 0) = objGrid.Item(intRow, 0)
                    objGrid(i, 1) = objGrid.Item(intRow, 1)
                    objGrid(i, 2) = objGrid.Item(intRow, 2)
                    objGrid(i, 3) = objGrid.Item(intRow, 3)
                    objGrid(i, 4) = objGrid.Item(intRow, 4)
                    objGrid(i, 5) = objGrid.Item(intRow, 5)
                    objGrid(i, 6) = objGrid.Item(intRow, 6)
                    objGrid(i, 7) = objGrid.Item(intRow, 7)
                    objGrid(i, 8) = objGrid.Item(intRow, 8)
                    objGrid(i, 9) = objGrid.Item(intRow, 9)
                    objGrid(i, 10) = objGrid.Item(intRow, 10)
                    objGrid(i, 11) = objGrid.Item(intRow, 11)
                    objGrid(i, 12) = objGrid.Item(intRow, 12)
                    objGrid(i, 13) = objGrid.Item(intRow, 13)
                    objGrid(i, 14) = objGrid.Item(intRow, 14)
                    objGrid(i, 15) = objGrid.Item(intRow, 15)
                    objGrid(i, 16) = objGrid.Item(intRow, 16)
                    objGrid(i, 17) = objGrid.Item(intRow, 17)
                    objGrid(i, 18) = objGrid.Item(intRow, 18)
                    objGrid(i, 19) = objGrid.Item(intRow, 19)
                    objGrid(i, 20) = objGrid.Item(intRow, 20)
                    objGrid(i, 21) = objGrid.Item(intRow, 21)
                    objGrid(i, 22) = objGrid.Item(intRow, 22)
                    objGrid(i, 23) = objGrid.Item(intRow, 23)

                    i += 1
                End If

                If objGrid.Item(intRow, 1).ToString.ToUpper() = arrayValues(0) Then
                    objGrid(i, 0) = objGrid.Item(intRow, 0)
                    objGrid(i, 1) = objGrid.Item(intRow, 1)
                    objGrid(i, 2) = objGrid.Item(intRow, 2)
                    objGrid(i, 3) = objGrid.Item(intRow, 3)
                    objGrid(i, 4) = objGrid.Item(intRow, 4)
                    objGrid(i, 5) = objGrid.Item(intRow, 5)
                    objGrid(i, 6) = objGrid.Item(intRow, 6)
                    objGrid(i, 7) = objGrid.Item(intRow, 7)
                    objGrid(i, 8) = objGrid.Item(intRow, 8)
                    objGrid(i, 9) = objGrid.Item(intRow, 9)
                    objGrid(i, 10) = objGrid.Item(intRow, 10)
                    objGrid(i, 11) = objGrid.Item(intRow, 11)
                    objGrid(i, 12) = objGrid.Item(intRow, 12)
                    objGrid(i, 13) = objGrid.Item(intRow, 13)
                    objGrid(i, 14) = objGrid.Item(intRow, 14)
                    objGrid(i, 15) = objGrid.Item(intRow, 15)
                    objGrid(i, 16) = objGrid.Item(intRow, 16)
                    objGrid(i, 17) = objGrid.Item(intRow, 17)
                    objGrid(i, 18) = objGrid.Item(intRow, 18)
                    objGrid(i, 19) = objGrid.Item(intRow, 19)
                    objGrid(i, 20) = objGrid.Item(intRow, 20)
                    objGrid(i, 21) = objGrid.Item(intRow, 21)
                    objGrid(i, 22) = objGrid.Item(intRow, 22)
                    objGrid(i, 23) = objGrid.Item(intRow, 23)

                    i += 1
                End If

                If objGrid.Item(intRow, 2).ToString.ToUpper() = arrayValues(0) Then
                    objGrid(i, 0) = objGrid.Item(intRow, 0)
                    objGrid(i, 1) = objGrid.Item(intRow, 1)
                    objGrid(i, 2) = objGrid.Item(intRow, 2)
                    objGrid(i, 3) = objGrid.Item(intRow, 3)
                    objGrid(i, 4) = objGrid.Item(intRow, 4)
                    objGrid(i, 5) = objGrid.Item(intRow, 5)
                    objGrid(i, 6) = objGrid.Item(intRow, 6)
                    objGrid(i, 7) = objGrid.Item(intRow, 7)
                    objGrid(i, 8) = objGrid.Item(intRow, 8)
                    objGrid(i, 9) = objGrid.Item(intRow, 9)
                    objGrid(i, 10) = objGrid.Item(intRow, 10)
                    objGrid(i, 11) = objGrid.Item(intRow, 11)
                    objGrid(i, 12) = objGrid.Item(intRow, 12)
                    objGrid(i, 13) = objGrid.Item(intRow, 13)
                    objGrid(i, 14) = objGrid.Item(intRow, 14)
                    objGrid(i, 15) = objGrid.Item(intRow, 15)
                    objGrid(i, 16) = objGrid.Item(intRow, 16)
                    objGrid(i, 17) = objGrid.Item(intRow, 17)
                    objGrid(i, 18) = objGrid.Item(intRow, 18)
                    objGrid(i, 19) = objGrid.Item(intRow, 19)
                    objGrid(i, 20) = objGrid.Item(intRow, 20)
                    objGrid(i, 21) = objGrid.Item(intRow, 21)
                    objGrid(i, 22) = objGrid.Item(intRow, 22)
                    objGrid(i, 23) = objGrid.Item(intRow, 23)

                    i += 1
                End If
            Next

            objGrid.Rows.Count = i
            objLabel.Text = i - 1

            If objLabel.Text > 0 Then
                objButton.Visible = True
            Else
                ' '' '' ''Call WriteC1FlexGrid()
            End If

            Try
                For intRow = 0 To objGrid.Rows.Count - 1
                    'If arrayValues(0) = objGrid(intRow, 1).ToString.ToUpper() And arrayValues(1) = objGrid(intRow, 5).ToString.ToUpper() And arrayValues(2) = objGrid(intRow, 9).ToString.ToUpper() Then
                    If arrayValues(0) = objGrid(intRow, 1).ToString.ToUpper() And arrayValues(1) = objGrid(intRow, 5).ToString.ToUpper() And arrayValues(2) = objGrid(intRow, 0).ToString.ToUpper().Replace(" ", "") And arrayValues(3) = objGrid(intRow, 4).ToString.ToUpper().Replace(" ", "") And arrayValues(4) = objGrid(intRow, 9).ToString.ToUpper() Then
                        objGrid.Select(intRow, 0)
                    End If
                Next
            Catch ex As Exception
                Exit Sub
            End Try

            Call colorScheme()
            'ElseIf strCondicional = "Nuevo" Then
            '    arrayValues = searchValue.Split("X")

            '    Try
            '        For intRow = 0 To objGrid.Rows.Count - 1
            '            'If arrayValues(0) = objGrid(intRow, 1).ToString.ToUpper() And arrayValues(1) = objGrid(intRow, 5).ToString.ToUpper() And arrayValues(2) = objGrid(intRow, 9).ToString.ToUpper() Then
            '            If arrayValues(0) = objGrid(intRow, 1).ToString.ToUpper() And arrayValues(1) = objGrid(intRow, 5).ToString.ToUpper() And arrayValues(2) = objGrid(intRow, 0).ToString.ToUpper().Replace(" ", "") And arrayValues(3) = objGrid(intRow, 4).ToString.ToUpper().Replace(" ", "") And arrayValues(4) = objGrid(intRow, 9).ToString.ToUpper() Then
            '                objGrid.Select(intRow, 0)
            '            End If
            '        Next
            '    Catch ex As Exception
            '        Exit Sub
            '    End Try

            '    Call colorScheme()
        ElseIf strCondicional = "Nuevo" Then
            Try
                For intRow = 0 To objGrid.Rows.Count - 1
                    'If arrayValues(0) = objGrid(intRow, 1).ToString.ToUpper() And arrayValues(1) = objGrid(intRow, 5).ToString.ToUpper() And arrayValues(2) = objGrid(intRow, 9).ToString.ToUpper() Then
                    If arrayValues(0) = objGrid(intRow, 1).ToString.ToUpper() Then
                        objGrid.Select(intRow, 0)
                    End If
                Next
            Catch ex As Exception
                Exit Sub
            End Try

            Call colorScheme()
        End If
    End Sub

    Function SearchCrossNew(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal searchValue As String) As Boolean
        Dim intRow As Integer
        Try
            SearchCrossNew = False
            For intRow = 0 To objGrid.Rows.Count - 1
                'If arrayValues(0) = objGrid(intRow, 1).ToString.ToUpper() And arrayValues(1) = objGrid(intRow, 5).ToString.ToUpper() And arrayValues(2) = objGrid(intRow, 9).ToString.ToUpper() Then
                If LCase(Trim(searchValue)) = LCase(Trim(objGrid(intRow, 0).ToString)) Then
                    objGrid.Select(intRow, 0)
                    SearchCrossNew = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            Exit Function
        End Try
        Call colorScheme()
    End Function

#End Region

#Region "Maceraci�n"

    '=====Tab Maceration=====
    'Select Maceration
    Private Sub Button_SelectMaceration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SelectMaceration.Click
        Dim intRowSelected As Integer, FData1, MData1 As String, indexHarvest As Integer

        If Me.C1FlexGrid_Harvest.Rows.Count > 1 Then
            intRowSelected = Me.C1FlexGrid_Maceration.Selection.BottomRow
            'FData1 = Me.C1FlexGrid_Maceration.Item(intRowSelected, 1)
            'MData1 = Me.C1FlexGrid_Maceration.Item(intRowSelected, 5)
            indexHarvest = Me.C1FlexGrid_Maceration.Row

            Me.TextBox_IndexMaceration.Text = CStr(Me.C1FlexGrid_Maceration.Item(intRowSelected, 0)) + "|" + CStr(indexHarvest) 'indexHarvest Carga el ID
            'Me.TextBox_Maceration.Text = FData1 + "x" + MData1 
            'Me.TextBox_HideNroRepetitions2.Text = Me.C1FlexGrid_Maceration.Item(indexHarvest, 9)
            'Asignar el Resumen de Cosecha
            Me.TextBox_Maceration.Text = Me.C1FlexGrid_Maceration.Item(intRowSelected, 20)
        End If
    End Sub

    'Print Maceration
    Private Sub Button_PrintMaceration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_PrintMaceration.Click
        Dim arrayValues() As String
        Dim arrayValuesID() As String
        Dim arrayResumen() As String
        Dim arrayResumenMaceration() As String
        Dim arrayResumen2() As String
        Dim arrayResumen3() As String
        ReDim arrayValues(14)
        Dim strError As String

        strError = ""

        If Me.TextBox_Maceration.Text <> "" Then
            If chkPrintHarvestGeneral.Checked = True Then
                arrayResumenMaceration = TextBox_Maceration.Text.Split("|")
                'strColector + "|" + strTipoCruzamiento + "|" + strTotalFloresPolinizadas + "|" + strTotalFrutos + "|" + ID + "|" + strEspecie
                arrayValues(0) = arrayResumenMaceration(0).ToString() 'Colector
                arrayValues(3) = arrayResumenMaceration(5).ToString() 'Acronimo
                arrayValues(5) = Me.DateTimePicker_Maceration.Text
                arrayValues(8) = Me.ComboBox_Username.SelectedItem
                arrayValues(11) = arrayResumenMaceration(1).ToString() 'Tipo de Cruzamiento
                arrayValues(12) = "TF: " + arrayResumenMaceration(2).ToString() + " TB: " + arrayResumenMaceration(3).ToString() 'strResumen
                arrayValues(13) = ComboBox_Location.Text
                arrayValues(14) = arrayResumenMaceration(4).ToString() 'ID

                Call PrintLabel(arrayValues, Me.ComboBox_BarcodeType.SelectedItem.ToString, Me.ComboBox_Port.SelectedItem.ToString, "Maceration", strError)

            Else
                'Validation
                'Dim objCon As SqlConnection
                'Dim strSQL As String
                Dim totalFlores, totalBayas, ColNumber, CIPNumber, CROSSID, Location, User As String
                'Dim Use_Id, GID, LOCID As String
                Dim intRowM As Integer
                arrayValuesID = Me.TextBox_IndexMaceration.Text.Split("|")

                For intRowM = 1 To Me.C1FlexGrid_Maceration.Rows.Count - 1
                    'If Me.TextBox_IndexMaceration.Text = intRowM Then
                    If arrayValuesID(1) = intRowM Then
                        totalFlores = Me.C1FlexGrid_Maceration.Item(intRowM, 21)
                        totalBayas = Me.C1FlexGrid_Maceration.Item(intRowM, 22)
                        ColNumber = Me.C1FlexGrid_Maceration.Item(intRowM, 2)
                        CIPNumber = Me.C1FlexGrid_Maceration.Item(intRowM, 3)

                        arrayValues(1) = Me.C1FlexGrid_Maceration.Item(intRowM, 12) + "x" + Me.C1FlexGrid_Maceration.Item(intRowM, 13) 'OK
                        arrayValues(0) = Me.C1FlexGrid_Maceration.Item(intRowM, 1) + " - " + Me.C1FlexGrid_Maceration.Item(intRowM, 6)
                        arrayValues(2) = Me.C1FlexGrid_Maceration.Item(intRowM, 3) + " - " + Me.C1FlexGrid_Maceration.Item(intRowM, 8)
                        arrayValues(3) = Me.C1FlexGrid_Maceration.Item(intRowM, 2) + " - " + Me.C1FlexGrid_Maceration.Item(intRowM, 7)
                        arrayValues(7) = Me.C1FlexGrid_Maceration.Item(intRowM, 23) 'Usuario de Cosecha
                        arrayValues(11) = Me.C1FlexGrid_Maceration.Item(intRowM, 11) 'OK Tipo de Cruzamiento

                        'arrayValues(12) = "F  " + totalFlores + " ;  " + "B  " + totalBayas 'OK
                        arrayResumen = Me.C1FlexGrid_Maceration.Item(intRowM, 20).ToString().Split("\")
                        If arrayResumen.Length > 1 Then
                            Dim strResumen As String
                            strResumen = arrayResumen(1).Replace("T. Flores: ", "TF:")
                            strResumen = strResumen.Replace("T. Frutos: ", "TB:")
                            arrayValues(12) = strResumen
                        End If
                    End If
                Next
                User = Me.ComboBox_Username.SelectedItem
                Location = ComboBox_Location.Text
                arrayValues(5) = Me.DateTimePicker_Maceration.Text
                arrayValues(8) = User
                arrayValues(13) = Location
                'Add Barcode
                arrayValues(14) = arrayValuesID(0)

                Call PrintLabel(arrayValues, Me.ComboBox_BarcodeType.SelectedItem.ToString, Me.ComboBox_Port.SelectedItem.ToString, "Maceration", strError)


                '    arrayValues(0) = Me.TextBox_Maceration.Text

                '    For intRow = 1 To Me.C1FlexGrid_Maceration.Rows.Count - 1
                '        If Me.TextBox_IndexMaceration.Text = intRow Then
                '            arrayValues(1) = Me.C1FlexGrid_Maceration.Item(intRow, 0) + "x" + Me.C1FlexGrid_Maceration.Item(intRow, 4)
                '            arrayValues(2) = Me.C1FlexGrid_Maceration.Item(intRow, 2) + " - " + Me.C1FlexGrid_Maceration.Item(intRow, 6)
                '            arrayValues(3) = Me.C1FlexGrid_Maceration.Item(intRow, 3) + " - " + Me.C1FlexGrid_Maceration.Item(intRow, 7)
                '            arrayValues(4) = Me.C1FlexGrid_Maceration.Item(intRow, 10) + "F  " + Me.C1FlexGrid_Maceration.Item(intRow, 8)
                '            arrayValues(5) = Me.C1FlexGrid_Maceration.Item(intRow, 14)
                '            arrayValues(6) = Me.C1FlexGrid_Maceration.Item(intRow, 11) + "Fr  " + Me.C1FlexGrid_Maceration.Item(intRow, 12)
                '            arrayValues(7) = Me.C1FlexGrid_Maceration.Item(intRow, 15)
                '            arrayValues(9) = Me.C1FlexGrid_Maceration.Item(intRow, 9)
                '            arrayValues(11) = Me.C1FlexGrid_Maceration.Item(intRow, 8)
                '            arrayValues(12) = "F  " + Me.C1FlexGrid_Maceration.Item(intRow, 10) + " ;  " + "B  " + Me.C1FlexGrid_Maceration.Item(intRow, 11)
                '        End If
                '    Next

                '    arrayValues(8) = Me.DateTimePicker_Maceration.Text
                '    arrayValues(10) = LOTID
                '    arrayValues(13) = Me.ComboBox_Location.Text






                'validate that the weight, wieght1 and Quantity are numerica and > 0
                'If Validate_Qty(Me.TextBox_Weight.Text) = False Then
                '    Call ShowSystemMessage(strMessage15)
                '    Exit Sub
                'End If

                'If Validate_Qty(Me.TextBox_Weight1.Text) = False Then
                '    Call ShowSystemMessage(strMessage16)
                '    Exit Sub
                'End If

                'If Validate_Qty(Me.TextBox_Quantity.Text) = False Then
                '    Call ShowSystemMessage(strMessage17)
                '    Exit Sub
                'End If

                ''Total de Flores.
                'If Validate_Qty(totalFlores) = False Then
                '    Call ShowSystemMessage(strMessage21)
                '    Exit Sub
                'End If

                ''Total de Bayas
                'If Validate_Qty(totalBayas) = False Then
                '    Call ShowSystemMessage(strMessage23)
                '    Exit Sub
                'End If

                ''Validate that CROSSID exists
                'objCon = New SqlConnection(strConnection)
                'strSQL = "Select TCROSSID from TypeCross where TCROSSSNAME =  '" + CROSSID + "'"
                'CROSSID = ExecuteSQL(strSQL, "TCROSSID", objCon)

                'If CROSSID = "" Then
                '    objCon.Close()
                '    Call ShowSystemMessage(strMessage22)
                '    Exit Sub
                'End If

                ''validate that GID exists
                'objCon = New SqlConnection(strConnection)
                'strSQL = "Select GID from GERMPLSM where CIPNUMBER = '" + CIPNumber + "'"
                'GID = ExecuteSQL(strSQL, "GID", objCon)

                'If GID = "" Then
                '    objCon.Close()
                '    Call ShowSystemMessage(strMessage18)
                '    Exit Sub
                'End If

                ''validate that user exists
                'objCon = New SqlConnection(strConnection)
                'strSQL = "Select use_id_n from users where use_id =  '" + User + "' AND use_active = 1"
                'Use_Id = ExecuteSQL(strSQL, "use_id_n", objCon)
                'If Use_Id = "" Then
                '    objCon.Close()
                '    Call ShowSystemMessage(strMessage19)
                '    Exit Sub
                'End If

                ''Validate that location exists
                'objCon = New SqlConnection(strConnection)
                'strSQL = "Select LOCID from m_enviroment_locality where locality =  '" + Location + "'"
                'LOCID = ExecuteSQL(strSQL, "LOCID", objCon)

                'If LOCID = "" Then
                '    objCon.Close()
                '    Call ShowSystemMessage(strMessage20)
                '    Exit Sub
                'End If
                'End validation

                '            'Create LOTID
                '            Dim TRNDATE = (Trim(Year(strDateToday)) + Fill_Left(Trim(Month(strDateToday)), 2, "0") + Fill_Left(Trim(Microsoft.VisualBasic.DateAndTime.Day(CDate(strDateToday).Date)), 2, "0")).ToString()
                '            Dim CROSSDATE = (Trim(Year(DateTimePicker_Maceration.Text)) + Fill_Left(Trim(Month(DateTimePicker_Maceration.Text)), 2, "0") + Fill_Left(Trim(Microsoft.VisualBasic.DateAndTime.Day(CDate(DateTimePicker_Maceration.Text).Date)), 2, "0")).ToString()
                '            Dim strProcedureName, strParameters, strValues, LOTID As String
                '            strProcedureName = "sp_cipcross_insert_lot"
                '            'strParameters = "@GID" + "-" + "@USER_ID" + "-" + "@TCROSSID" + "-" + "@TCROSSDATE" + "-" + _
                '            '"@LOCIDGC" + "-" + "@COMMENTS" + "-" + "@TRNQTY1" + "-" + "@TOTALFLORES" + "-" + "@TOTALBAYAS" + "-" + "@PS1" + "-" + "@PESO" + "-" + "@TRNDATE"
                '            strParameters = "@GID" + "-" + "@USER_ID" + "-" + "@TCROSSID" + "-" + "@TCROSSDATE" + "-" + _
                '"@LOCIDGC" + "-" + "@COMMENTS" + "-" + "@TRNQTY1" + "-" + "@TOTALFLORES" + "-" + "@TOTALBAYAS" + "-" + "@TRNDATE"
                '            strValues = GID + "-" + Use_Id + "-" + CROSSID + "-" + CROSSDATE + "-" + LOCID + "-" + TextBox_Comments.Text.Trim + _
                '            "-" + Me.TextBox_Quantity.Text + "-" + totalFlores + "-" + totalBayas + "-" + TRNDATE
                '            '            strParameters = "@GID" + "-" + "@USER_ID" + "-" + "@TCROSSID" + "-" + "@TCROSSDATE" + "-" + _
                '            '"@LOCIDGC" + "-" + "@COMMENTS" + "-" + "@TRNQTY1" + "-" + "@TOTALFLORES" + "-" + "@TOTALBAYAS" + "-" + "@PS1" + "-" + "@PESO" + "-" + "@TRNDATE"
                '            '            strValues = GID + "-" + Use_Id + "-" + CROSSID + "-" + CROSSDATE + "-" + LOCID + "-" + TextBox_Comments.Text.Trim + _
                '            '            "-" + Me.TextBox_Quantity.Text + "-" + totalFlores + "-" + ComboBox_Bayas.Text + "-" + TextBox_Weight1.Text + "-" + TextBox_Weight.Text + "-" + TRNDATE

                '            LOTID = ExecuteProcedure(strProcedureName, objCon, strParameters, strValues, "@LOTID_OUT")
                '            objCon.Close()

                'If LOTID <> "" Then
                '    Dim intRow As Integer
                '    Dim user3, location2 As String

                '    user3 = Me.ComboBox_Username.SelectedItem
                '    location2 = Me.ComboBox_Location.SelectedItem

                '    arrayValues(0) = Me.TextBox_Maceration.Text

                '    For intRow = 1 To Me.C1FlexGrid_Maceration.Rows.Count - 1
                '        If Me.TextBox_IndexMaceration.Text = intRow Then
                '            arrayValues(1) = Me.C1FlexGrid_Maceration.Item(intRow, 0) + "x" + Me.C1FlexGrid_Maceration.Item(intRow, 4)
                '            arrayValues(2) = Me.C1FlexGrid_Maceration.Item(intRow, 2) + " - " + Me.C1FlexGrid_Maceration.Item(intRow, 6)
                '            arrayValues(3) = Me.C1FlexGrid_Maceration.Item(intRow, 3) + " - " + Me.C1FlexGrid_Maceration.Item(intRow, 7)
                '            arrayValues(4) = Me.C1FlexGrid_Maceration.Item(intRow, 10) + "F  " + Me.C1FlexGrid_Maceration.Item(intRow, 8)
                '            arrayValues(5) = Me.C1FlexGrid_Maceration.Item(intRow, 14)
                '            arrayValues(6) = Me.C1FlexGrid_Maceration.Item(intRow, 11) + "Fr  " + Me.C1FlexGrid_Maceration.Item(intRow, 12)
                '            arrayValues(7) = Me.C1FlexGrid_Maceration.Item(intRow, 15)
                '            arrayValues(9) = Me.C1FlexGrid_Maceration.Item(intRow, 9)
                '            arrayValues(11) = Me.C1FlexGrid_Maceration.Item(intRow, 8)
                '            arrayValues(12) = "F  " + Me.C1FlexGrid_Maceration.Item(intRow, 10) + " ;  " + "B  " + Me.C1FlexGrid_Maceration.Item(intRow, 11)
                '        End If
                '    Next

                '    arrayValues(8) = Me.DateTimePicker_Maceration.Text
                '    arrayValues(10) = LOTID
                '    arrayValues(13) = Me.ComboBox_Location.Text

                '    'Call ReadCrossingResults()

                '    Dim crossValue, numbers(), progenitores() As String

                '    crossValue = Me.TextBox_Maceration.Text
                '    numbers = crossValue.Split("x")
                '    progenitores = arrayValues(1).Split("x")

                '    'Dim detailsNameWrite As String = curFile
                '    'Dim objWriter As New System.IO.StreamWriter(detailsNameWrite)

                '    'For intRow = 0 To Me.ListBox_Hide.Items.Count - 1
                '    '    Dim cadena As String = Me.ListBox_Hide.Items(intRow)
                '    '    Dim words As String() = cadena.Split(New Char() {Chr(9)})
                '    '    Dim word As String

                '    '    Dim i As String = 0
                '    '    Dim array(intNroMaxColumnsResults) As String

                '    '    For Each word In words
                '    '        array(i) = word
                '    '        i += 1
                '    '    Next
                'End If

                If boolImprimio = True Then
                    If chkPrintHarvestGeneral.Checked = False Then
                        Dim resultRow As Results_Dataset.resultsRow
                        resultRow = Results_DataSet1.results.FindByID(CInt(arrayValuesID(0).ToString()))

                        resultRow.FechaMaceracion = Me.DateTimePicker_Maceration.Text
                        resultRow.UsuarioMaceracion = Me.ComboBox_Username.SelectedItem
                        resultRow.LocalidadMaceracion = ComboBox_Location.Text
                        resultRow.EstadoResultado = 2

                        'Save the updated row to the database 
                        Me.ResultsTableAdapter1.Update(Me.Results_DataSet1.results)

                        'Me.ListBox_Hide.Items.Clear()
                        Me.TextBox_IndexMaceration.Text = ""
                        TextBox_Maceration.Text = ""
                        Call GetView()
                    End If
                End If
            End If
        Else
            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage12)
            Else
                Call ShowSystemMessage(strMessage12)
            End If
        End If
    End Sub

#End Region

    Function Validate_Qty(ByVal strQty As String) As Boolean
        Validate_Qty = True
        If strQty = "" Then
            Validate_Qty = False
            Exit Function
        End If
        If IsNumeric(strQty) = False Then
            Validate_Qty = False
            Exit Function
        End If
        If Val(strQty) < 0 Then
            Validate_Qty = False
            Exit Function
        End If
    End Function

    'Search Maceration
    Private Sub Button_SearchMaceration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SearchMaceration.Click
        If Me.TextBox_SearchMaceration.Text <> "" Then
            Call SearchCross(Me.C1FlexGrid_Maceration, Me.TextBox_SearchMaceration.Text.ToUpper(), Me.Label_CountMaceration, Me.Button_LoadMaceration, "Antiguo")
            Me.TextBox_SearchMaceration.Focus()
            Me.TextBox_SearchMaceration.SelectAll()
        End If
    End Sub

    'Load Maceration
    Private Sub Button_LoadMaceration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_LoadMaceration.Click
        'If File.Exists(curFile) Then
        '    Me.TextBox_SearchMaceration.Text = ""
        '    Call WriteC1FlexGrid()
        '    Me.Button_LoadMaceration.Visible = False
        '    Me.TextBox_SearchMaceration.Focus()
        'End If
    End Sub
    '========================
    '=====Others Funtions====
    'Read Crossing Results
    'Private Sub ReadCrossingResults()
    '    Dim detailsNameRead As String = curFile
    '    Dim objReader As New System.IO.StreamReader(detailsNameRead)

    '    Do While objReader.Peek() >= 0
    '        Me.ListBox_Hide.Items.Add(objReader.ReadLine())
    '    Loop
    '    objReader.Close()
    'End Sub

    ''Write C1FlexGrid Harvest and Maceration
    'Private Sub WriteC1FlexGrid()
    '    If File.Exists(curFile) Then
    '        Dim intRow As Integer

    '        Me.TextBox_Harvest.Text = ""
    '        Me.TextBox_Maceration.Text = ""

    '        Call ReadCrossingResults()

    '        Dim cont As Integer
    '        cont = 0

    '        For intRow = 0 To Me.ListBox_Hide.Items.Count - 1
    '            Dim cadena As String = Me.ListBox_Hide.Items(intRow)
    '            Dim words As String() = cadena.Split(New Char() {Chr(9)})
    '            Dim word As String

    '            Dim i As String = 0
    '            'Dim array(35) As String
    '            Dim array(intNroMaxColumnsResults) As String

    '            For Each word In words
    '                array(i) = word
    '                i += 1
    '            Next

    '            'If array(34) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
    '            If array(intPosNroPlan) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
    '                cont += 1
    '            End If

    '        Next

    '        Me.C1FlexGrid_Harvest.Rows.Count = cont + 1
    '        Me.C1FlexGrid_Maceration.Rows.Count = cont + 1 ''ver porque esta comentado

    '        ''Fields
    '        'Dim posF1, posF2, posF3, posM1, posM2, posM3 As String

    '        ''Females
    '        'If Me.ListBox_FemaleLabels.Items.Item(0) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(1) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(2) Is DBNull.Value Then
    '        '    'Female
    '        '    posF1 = "F.Data1"
    '        '    posF2 = "F.Data2"
    '        '    posF3 = "F.Data3"
    '        'Else
    '        '    'Female
    '        '    posF1 = Me.ListBox_FemaleLabels.Items.Item(0).ToString
    '        '    posF2 = Me.ListBox_FemaleLabels.Items.Item(1).ToString
    '        '    posF3 = Me.ListBox_FemaleLabels.Items.Item(2).ToString
    '        'End If

    '        ''Males
    '        'If Me.ListBox_MaleLabels.Items.Item(0) Is DBNull.Value And Me.ListBox_MaleLabels.Items.Item(1) Is DBNull.Value And Me.ListBox_MaleLabels.Items.Item(2) Is DBNull.Value Then
    '        '    'Female
    '        '    posM1 = "M.Data1"
    '        '    posM2 = "M.Data2"
    '        '    posM3 = "M.Data3"
    '        'Else
    '        '    'Female
    '        '    posM1 = Me.ListBox_MaleLabels.Items.Item(0).ToString
    '        '    posM2 = Me.ListBox_MaleLabels.Items.Item(1).ToString
    '        '    posM3 = Me.ListBox_MaleLabels.Items.Item(2).ToString
    '        'End If

    '        ''Columns Name
    '        ''Haverts
    '        'Me.C1FlexGrid_Harvest(0, 0) = "OrderF."
    '        'Me.C1FlexGrid_Harvest(0, 1) = posF1
    '        'Me.C1FlexGrid_Harvest(0, 2) = posF2
    '        'Me.C1FlexGrid_Harvest(0, 3) = posF3
    '        'Me.C1FlexGrid_Harvest(0, 4) = "OrderM."
    '        'Me.C1FlexGrid_Harvest(0, 5) = posM1
    '        'Me.C1FlexGrid_Harvest(0, 6) = posM2
    '        'Me.C1FlexGrid_Harvest(0, 7) = posM3
    '        'Me.C1FlexGrid_Harvest(0, 8) = "TypeCross"
    '        'Me.C1FlexGrid_Harvest(0, 9) = "Repetitions"
    '        'Me.C1FlexGrid_Harvest(0, 10) = "Flower(s)"

    '        'Me.C1FlexGrid_Harvest(0, 11) = "Harvest Resume"
    '        'Me.C1FlexGrid_Harvest(0, 12) = "Total Flowers"
    '        'Me.C1FlexGrid_Harvest(0, 13) = "Fruit(s)"
    '        'Me.C1FlexGrid_Harvest(0, 14) = "PrintingCrossDate"
    '        'Me.C1FlexGrid_Harvest(0, 15) = "CrossingDate"
    '        'Me.C1FlexGrid_Harvest(0, 16) = "HarvestDate"
    '        'Me.C1FlexGrid_Harvest(0, 17) = "MacerationDate"
    '        'Me.C1FlexGrid_Harvest(0, 18) = "User1"
    '        'Me.C1FlexGrid_Harvest(0, 19) = "User2"
    '        'Me.C1FlexGrid_Harvest(0, 20) = "User3"
    '        'Me.C1FlexGrid_Harvest(0, 21) = "Location1"
    '        'Me.C1FlexGrid_Harvest(0, 22) = "Location2"
    '        'Me.C1FlexGrid_Harvest(0, 23) = "NroPlan"
    '        'Me.C1FlexGrid_Harvest(0, 24) = "PlanName"

    '        ''Maceration
    '        'Me.C1FlexGrid_Maceration(0, 0) = "OrderF."
    '        'Me.C1FlexGrid_Maceration(0, 1) = posF1
    '        'Me.C1FlexGrid_Maceration(0, 2) = posF2
    '        'Me.C1FlexGrid_Maceration(0, 3) = posF3
    '        'Me.C1FlexGrid_Maceration(0, 4) = "OrderM."
    '        'Me.C1FlexGrid_Maceration(0, 5) = posM1
    '        'Me.C1FlexGrid_Maceration(0, 6) = posM2
    '        'Me.C1FlexGrid_Maceration(0, 7) = posM3
    '        'Me.C1FlexGrid_Maceration(0, 8) = "TypeCross"
    '        'Me.C1FlexGrid_Maceration(0, 9) = "Repetitions"
    '        'Me.C1FlexGrid_Maceration(0, 10) = "Flower(s)"

    '        'Me.C1FlexGrid_Maceration(0, 11) = "Harvest Resume"
    '        'Me.C1FlexGrid_Maceration(0, 12) = "Total Flowers"
    '        'Me.C1FlexGrid_Maceration(0, 13) = "Fruit(s)"
    '        'Me.C1FlexGrid_Maceration(0, 14) = "PrintingCrossDate"
    '        'Me.C1FlexGrid_Maceration(0, 15) = "CrossingDate"
    '        'Me.C1FlexGrid_Maceration(0, 16) = "HarvestDate"
    '        'Me.C1FlexGrid_Maceration(0, 17) = "MacerationDate"
    '        'Me.C1FlexGrid_Maceration(0, 18) = "User1"
    '        'Me.C1FlexGrid_Maceration(0, 19) = "User2"
    '        'Me.C1FlexGrid_Maceration(0, 20) = "User3"
    '        'Me.C1FlexGrid_Maceration(0, 21) = "Location1"
    '        'Me.C1FlexGrid_Maceration(0, 22) = "Location2"
    '        'Me.C1FlexGrid_Maceration(0, 23) = "NroPlan"
    '        'Me.C1FlexGrid_Maceration(0, 24) = "PlanName"

    '        Dim cont2, int As Integer
    '        cont2 = 0

    '        For intRow = 0 To Me.ListBox_Hide.Items.Count - 1
    '            Dim cadena As String = Me.ListBox_Hide.Items(intRow)
    '            Dim words As String() = cadena.Split(New Char() {Chr(9)})
    '            Dim word As String


    '            Dim i As String = 0
    '            'Dim array(35) As String
    '            Dim array(intNroMaxColumnsResults) As String

    '            For Each word In words
    '                array(i) = word
    '                i += 1
    '            Next

    '            For int = 0 To Me.ListBox_Hide.Items.Count - 1
    '                'If array(34) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
    '                If array(intNroMaxColumnsResults - 1) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
    '                    cont += 1
    '                End If
    '            Next

    '            If array(intNroMaxColumnsResults - 1) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 0) = array(intPositionF) '"OrderF."
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 1) = array(intPosF1) 'posF1
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 2) = array(intPosF2) 'posF2
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 3) = array(intPosF3) 'posF3
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 4) = array(intPositionM) '"OrderM."
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 5) = array(intPosM1) 'posM1
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 6) = array(intPosM2) 'posM2
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 7) = array(intPosM3) 'posM3
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 8) = array(intPosTypeCross) 'TypeCross
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 9) = array(intPosRepetitions) 'Repetitions
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 10) = array(intPosFlower) 'Flower(s)

    '                Me.C1FlexGrid_Harvest(cont2 + 1, 11) = array(intPosHarvestResume) 'Resumen
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 12) = array(intPosTotalFlowers) 'Total Flowers
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 13) = array(intPosFruits) 'Fruit(s)
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 14) = array(intPosDatePrintCross) 'PrintingCrossDate
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 15) = array(intPosCrossingDate) 'CrossingDate
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 16) = array(intPosHarvestDate) 'HarvestDate
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 17) = array(intPosMacerationDate) 'MacerationDate
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 18) = array(intPosUser1) 'User1
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 19) = array(intPosUser2) 'User2
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 20) = array(intPosUser3) 'User3
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 21) = array(intPosLocation1) 'Location1
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 22) = array(intPosLocation2) 'Location2
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 23) = array(intPosNroPlan) 'NroPlan
    '                Me.C1FlexGrid_Harvest(cont2 + 1, 24) = array(intPosPlanName) 'PlanName

    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 10) = array(14)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 11) = array(15)

    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 0) = array(0)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 1) = array(1)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 2) = array(2)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 3) = array(3)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 4) = array(4)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 5) = array(5)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 6) = array(6)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 7) = array(7)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 8) = array(8)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 9) = array(9)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 10) = array(10)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 11) = array(11)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 12) = array(12)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 13) = array(13)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 14) = array(14)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 15) = array(15)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 16) = array(16)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 17) = array(17)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 18) = array(18)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 19) = array(19)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 20) = array(20)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 21) = array(21)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 22) = array(22)
    '                'Me.C1FlexGrid_Harvest(cont2 + 1, 23) = array(23)

    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 0) = array(0)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 1) = array(1)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 2) = array(2)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 3) = array(3)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 4) = array(4)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 5) = array(5)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 6) = array(6)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 7) = array(7)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 8) = array(8)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 9) = array(9)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 10) = array(10)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 11) = array(11)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 12) = array(12)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 13) = array(13)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 14) = array(14)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 15) = array(15)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 16) = array(16)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 17) = array(17)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 18) = array(18)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 19) = array(19)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 20) = array(20)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 21) = array(21)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 22) = array(22)
    '                'Me.C1FlexGrid_Maceration(cont2 + 1, 23) = array(23)

    '                Me.C1FlexGrid_Maceration(cont2 + 1, 0) = array(intPositionF)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 1) = array(intPosF1)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 2) = array(intPosF2)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 3) = array(intPosF3)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 4) = array(intPositionM)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 5) = array(intPosM1)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 6) = array(intPosM2)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 7) = array(intPosM3)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 8) = array(intPosTypeCross)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 9) = array(intPosRepetitions)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 10) = array(intPosFlower)

    '                Me.C1FlexGrid_Maceration(cont2 + 1, 11) = array(intPosHarvestResume) 'Resumen
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 12) = array(intPosTotalFlowers) 'Total Flowers
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 13) = array(intPosFruits) 'Fruit(s)
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 14) = array(intPosDatePrintCross) 'PrintingCrossDate
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 15) = array(intPosCrossingDate) 'CrossingDate
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 16) = array(intPosHarvestDate) 'HarvestDate
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 17) = array(intPosMacerationDate) 'MacerationDate
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 18) = array(intPosUser1) 'User1
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 19) = array(intPosUser2) 'User2
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 20) = array(intPosUser3) 'User3
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 21) = array(intPosLocation1) 'Location1
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 22) = array(intPosLocation2) 'Location2
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 23) = array(intPosNroPlan) 'NroPlan
    '                Me.C1FlexGrid_Maceration(cont2 + 1, 24) = array(intPosPlanName) 'PlanName
    '                cont2 += 1
    '            End If
    '        Next

    '        Me.Label_CountHarvest.Text = Me.C1FlexGrid_Harvest.Rows.Count - 1
    '        Me.Label_CountMaceration.Text = Me.C1FlexGrid_Maceration.Rows.Count - 1
    '        Me.ListBox_Hide.Items.Clear()

    '        Call colorScheme()
    '        'Call CrossingPlanShow()
    '    End If
    'End Sub

    'Color Scheme
    Private Sub colorScheme()
        Dim intRow As Integer

        '' ''Call ReadCrossingResults()

        For intRow = 0 To Me.ListBox_Hide.Items.Count - 1
            Dim cadena As String = Me.ListBox_Hide.Items(intRow)
            Dim words As String() = cadena.Split(New Char() {Chr(9)})
            Dim word As String

            Dim i As String = 0
            Dim array(intNroMaxColumnsResults) As String

            For Each word In words
                array(i) = word
                i += 1
            Next

            Dim row As Integer

            'Parentals
            'Female
            Dim femaleYellowColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Female.Styles.Add("void")
            femaleYellowColor.BackColor = Drawing.Color.FromArgb(243, 247, 129)

            For row = 0 To Me.C1FlexGrid_Female.Rows.Count - 1
                If Me.C1FlexGrid_Female(row, 0) = array(intPositionF) And array(intNroMaxColumnsResults - 1) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
                    Me.C1FlexGrid_Female.Rows(row).StyleNew.BackColor = femaleYellowColor.BackColor
                End If
            Next

            'Male
            Dim maleYellowColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Female.Styles.Add("void")
            maleYellowColor.BackColor = Drawing.Color.FromArgb(243, 247, 129)

            For row = 0 To Me.C1FlexGrid_Male.Rows.Count - 1
                If Me.C1FlexGrid_Male(row, 0) = array(intPositionM) And array(intNroMaxColumnsResults - 1) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
                    Me.C1FlexGrid_Male.Rows(row).StyleNew.BackColor = maleYellowColor.BackColor
                End If
            Next

            'Crossing
            'FemaleCross
            Dim femaleCrossYellowColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Female.Styles.Add("void")
            femaleCrossYellowColor.BackColor = Drawing.Color.FromArgb(243, 247, 129)

            For row = 0 To Me.C1FlexGrid_FemaleCross.Rows.Count - 1
                If Me.C1FlexGrid_FemaleCross(row, 0) = array(intPositionF) And array(intNroMaxColumnsResults - 1) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
                    Me.C1FlexGrid_FemaleCross.Rows(row).StyleNew.BackColor = femaleCrossYellowColor.BackColor
                End If
            Next

            'MaleCross
            Dim maleCrossYellowColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Female.Styles.Add("void")
            maleCrossYellowColor.BackColor = Drawing.Color.FromArgb(243, 247, 129)

            For row = 0 To Me.C1FlexGrid_MaleCross.Rows.Count - 1
                If Me.C1FlexGrid_MaleCross(row, 0) = array(intPositionM) And array(intNroMaxColumnsResults - 1) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
                    Me.C1FlexGrid_MaleCross.Rows(row).StyleNew.BackColor = maleCrossYellowColor.BackColor
                End If
            Next
        Next

        Dim yellowColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Female.Styles.Add("void")
        yellowColor.BackColor = Drawing.Color.FromArgb(243, 247, 129)

        Dim greenColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Male.Styles.Add("void")
        greenColor.BackColor = Drawing.Color.FromArgb(129, 247, 129)

        'Harvest
        For intRow = 0 To Me.C1FlexGrid_Harvest.Rows.Count - 1
            'If Me.C1FlexGrid_Harvest(intRow, 9) <> "Repetitions" Then
            '    Me.C1FlexGrid_Harvest.Rows(intRow).StyleNew.BackColor = greenColor.BackColor
            'End If
            If Me.C1FlexGrid_Harvest(intRow, 9) = "1" And Me.C1FlexGrid_Harvest(intRow, 22) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1).ToString Then
                Me.C1FlexGrid_Harvest.Rows(intRow).StyleNew.BackColor = yellowColor.BackColor
            End If
        Next

        'Tuberculus Harvest
        'For intRow = 0 To Me.C1FlexGrid_ValuesTubHarv.Rows.Count - 1
        '    If Me.C1FlexGrid_ValuesTubHarv(intRow, 9) <> "Repetitions" Then
        '        Me.C1FlexGrid_ValuesTubHarv.Rows(intRow).StyleNew.BackColor = greenColor.BackColor
        '    End If
        '    If Me.C1FlexGrid_ValuesTubHarv(intRow, 9) = "1" And Me.C1FlexGrid_ValuesTubHarv(intRow, 22) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1).ToString Then
        '        Me.C1FlexGrid_ValuesTubHarv.Rows(intRow).StyleNew.BackColor = yellowColor.BackColor
        '    End If
        'Next

        'Maceration
        For intRow = 0 To Me.C1FlexGrid_Maceration.Rows.Count - 1
            'If Me.C1FlexGrid_Maceration(intRow, 9) <> "Repetitions" Then
            '    Me.C1FlexGrid_Maceration.Rows(intRow).StyleNew.BackColor = greenColor.BackColor
            'End If
            If Me.C1FlexGrid_Maceration(intRow, 9) = "1" And Me.C1FlexGrid_Maceration(intRow, 22) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1).ToString Then
                Me.C1FlexGrid_Maceration.Rows(intRow).StyleNew.BackColor = yellowColor.BackColor
            End If
        Next

        Me.ListBox_Hide.Items.Clear()
    End Sub
    'CrossingPlan Show
    'Private Sub CrossingPlanShow()
    '    Dim intRow, x, y, indexX, indexY As Integer

    '    Call ReadCrossingResults()

    '    For intRow = 0 To Me.ListBox_Hide.Items.Count - 1
    '        Dim cadena As String = Me.ListBox_Hide.Items(intRow)
    '        Dim words As String() = cadena.Split(New Char() {Chr(9)})
    '        Dim word As String

    '        Dim i As String = 0
    '        Dim array(23) As String

    '        For Each word In words
    '            array(i) = word
    '            i += 1
    '        Next

    '        If array(22) = (Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1) Then
    '            For x = 0 To Me.C1FlexGrid_CrossingPlan.Rows.Count - 1
    '                If x = array(0) Then
    '                    indexX = Me.C1FlexGrid_CrossingPlan(x, 0) + 2
    '                End If
    '            Next

    '            For y = 0 To Me.C1FlexGrid_CrossingPlan.Cols.Count - 1
    '                If y = array(4) Then
    '                    indexY = Me.C1FlexGrid_CrossingPlan(y, 0) + 2
    '                End If
    '            Next

    '            If indexX <> 0 And indexY <> 0 Then
    '                Me.C1FlexGrid_CrossingPlan(indexX, indexY) = array(9)
    '            End If

    '            Dim yellowColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Female.Styles.Add("void")
    '            yellowColor.BackColor = Drawing.Color.FromArgb(243, 247, 129)

    '            Dim greenColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Male.Styles.Add("void")
    '            greenColor.BackColor = Drawing.Color.FromArgb(129, 247, 129)


    '            If array(9) = 1 Then
    '                Dim objStyle As C1.Win.C1FlexGrid.CellStyle
    '                objStyle = Me.C1FlexGrid_Hide.GetCellStyleDisplay(0, 2)

    '                objStyle.BackColor = yellowColor.BackColor
    '                objStyle.ForeColor = Color.Black
    '                Me.C1FlexGrid_CrossingPlan.SetCellStyle(indexX, indexY, objStyle)
    '            Else
    '                Dim objStyle2 As C1.Win.C1FlexGrid.CellStyle
    '                objStyle2 = Me.C1FlexGrid_Hide.GetCellStyleDisplay(0, 1)

    '                objStyle2.BackColor = greenColor.BackColor
    '                objStyle2.ForeColor = Color.Black
    '                Me.C1FlexGrid_CrossingPlan.SetCellStyle(indexX, indexY, objStyle2)
    '            End If

    '        End If
    '    Next

    '    Me.ListBox_Hide.Items.Clear()
    'End Sub
    'ComboBox Type Cross
    Private Sub ComboBox_TypeCrossOption_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_TypeCrossOption.SelectedIndexChanged
        Dim intRow As Integer

        For intRow = 0 To Me.ComboBox_TypeCrossOption.Items.Count - 1
            If Me.ComboBox_TypeCrossOption.SelectedIndex = intRow Then
                Me.ComboBox_TypeCross.SelectedIndex = intRow
            End If
        Next
    End Sub
    '========================
    '======Print Options=====
    'Printing template
    Private Sub PrintLabel(ByVal strValues() As String, ByVal strBarcodeType As String, ByVal strPort As String, ByVal strPrintType As String, ByRef strError As String)
        Dim strStartOfLabel, strLabelText, strEndPrint As String
        Dim GUID As String = getGUID()


        If strPrintType = "Female" Or strPrintType = "Male" Then
            strStartOfLabel = leftMarginOfLabel + vbNewLine
            strLabelText = "T 0 6 1222 17 " + strValues(0) + vbNewLine
            strLabelText = strLabelText + "T 7 1 40 20 " + strValues(1) + vbNewLine
            strLabelText = strLabelText + "T 7 0 40 80 " + strValues(2) + vbNewLine
            strLabelText = strLabelText + "T 7 0 40 120 " + strValues(3) + vbNewLine
            strLabelText = strLabelText + "T 7 0 40 160 " + strValues(4) + vbNewLine
            If Trim(LCase(strBarcodeType)) = "1d" Then
                strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
            Else
                strLabelText = strLabelText + "B DATAMATRIX 15540 47 H 6 S 200 " + vbNewLine
                strLabelText = strLabelText + strValues(1) + vbNewLine
                strLabelText = strLabelText + "ENDDATAMATRIX " + vbNewLine
            End If
            strEndPrint = "PRINT" + vbNewLine

            Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
        End If

        If ComboBox_PrintType.SelectedIndex = 0 Then

            If strPrintType = "Crossing" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine
                strLabelText = "T 0 5 1202 12 " + strValues(0) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 20 " + strValues(1) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 45 " + strValues(2) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 70 " + strValues(3) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 95 " + strValues(4) + "F  " + strValues(5) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 120 " + strValues(6) + "(C)" + vbNewLine
                strLabelText = strLabelText + "T 0 2 385 180 " + "(" + strValues(7) + ")" + vbNewLine
                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN," 'B DATAMATRIX 15520 27 H 4 S 200
                    If GUID = "" Then
                        strLabelText = strLabelText + strValues(1) + "x" + strValues(7) + vbNewLine
                    Else
                        strLabelText = strLabelText + strValues(1) + Chr(9) + GUID + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If
                strEndPrint = "PRINT" + vbNewLine

                '? strStartOfLabel + strLabelText + strEndPrint
                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If

            If strPrintType = "Harvest" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine
                strLabelText = "T 0 5 1202 12 " + strValues(1) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 20 " + strValues(0) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 45 " + strValues(2) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 70 " + strValues(3) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 95 " + strValues(4) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 120 " + strValues(5) + "(C)" + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 145 " + strValues(6) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 170 " + strValues(7) + "(H)" + vbNewLine
                strLabelText = strLabelText + "T 0 2 385 180 " + "(" + strValues(8) + ")" + vbNewLine
                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    If GUID = "" Then
                        strLabelText = strLabelText + strValues(0) + "x" + strValues(8) + vbNewLine
                    Else
                        strLabelText = strLabelText + strValues(0) + Chr(9) + GUID + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If
                strEndPrint = "PRINT" + vbNewLine

                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If


            If strPrintType = "Maceration" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine
                strLabelText = "T 0 5 1202 12 " + strValues(1) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 20 " + strValues(0) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 45 " + strValues(2) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 70 " + strValues(3) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 95 " + strValues(4) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 120 " + strValues(5) + "(C)" + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 145 " + strValues(6) + vbNewLine
                strLabelText = strLabelText + "T 0 2 40 170 " + strValues(7) + "(H) " + strValues(8) + "(M)" + vbNewLine
                strLabelText = strLabelText + "T 0 2 385 180 " + "(" + strValues(9) + ")" + vbNewLine
                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    If GUID = "" Then
                        strLabelText = strLabelText + strValues(0) + "x" + strValues(9) + vbNewLine
                    Else
                        strLabelText = strLabelText + strValues(0) + Chr(9) + GUID + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If
                strEndPrint = "PRINT" + vbNewLine

                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If

        ElseIf ComboBox_PrintType.SelectedIndex = 1 Then ' Wild potato

            If strPrintType = "Crossing" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine

                If strValues(5) = "Sib-cross" Then
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                ElseIf strValues(5) = "Bulk" Then
                    strLabelText = "T 0 3 40 95 " + "Bk" + vbNewLine
                ElseIf strValues(5) = "AP" Then
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                ElseIf strValues(5) = "LP" Then
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                Else
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                End If

                Dim intRow As Integer
                Dim Responsables As String = ""
                For intRow = 0 To ComboBox_Username.Items.Count - 1
                    If intRow = 0 Then
                        Responsables = Mid(ComboBox_Username.Items(intRow).ToString(), 1, 2)
                    Else
                        Responsables = Responsables + ", " + Mid(ComboBox_Username.Items(intRow).ToString(), 1, 2)
                    End If
                Next

                'Get Acr�nimo de especie
                ' We want to split this input string
                Dim s As String = strValues(3)
                ' Split string based on spaces
                Dim words As String() = s.Split("-")
                ' Use For Each loop over words and display them
                Dim word As String
                Dim Acronimo As String

                For Each word In words
                    Acronimo = word
                    Exit For
                Next

                strLabelText = strLabelText + "T 0 3 40 45 " + Acronimo + vbNewLine

                'Numero de colector
                ' We want to split this input string
                Dim colectorValue As String = strValues(1)

                ' Split string based on spaces
                Dim colectorArray As String() = colectorValue.Split("x")

                ' Use For Each loop over words and display them
                Dim colector As String
                Dim NombreColector As String

                If colectorArray.Length > 2 Then
                    NombreColector = colectorArray(0) + "x" + colectorArray(1)
                Else
                    For Each colector In colectorArray
                        NombreColector = colector
                        Exit For
                    Next
                End If

                strLabelText = strLabelText + "T 0 3 40 70 " + NombreColector + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 120 " + "T " + strValues(4) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 150 " + strValues(6) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 180 " + ComboBox_Location.Text + vbNewLine

                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    If strValues(8) = "" Then
                        strLabelText = strLabelText + strValues(1) + "x" + strValues(0) + "x" + strValues(7) + vbNewLine
                    Else
                        strLabelText = strLabelText + strValues(8) + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If

                strEndPrint = "PRINT" + vbNewLine

                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If

            If strPrintType = "Harvest" Then

                If ChkDateHarvest.Checked = True Then

                    strStartOfLabel = leftMarginOfLabel + vbNewLine

                    'If ComboBox_TypeHarvest.SelectedIndex = 0 Then

                    'Type of cross
                    If ChkDateHarvest.Checked = True Then
                        strLabelText = "T 0 3 40 95 " + strValues(10) + vbNewLine
                    Else
                        If strValues(9) = "Sib-cross" Then
                            strLabelText = "T 0 3 40 95 " + strValues(1) + vbNewLine
                        ElseIf strValues(9) = "Bulk" Then
                            strLabelText = "T 0 3 40 95 " + "Bk" + vbNewLine
                        ElseIf strValues(9) = "AP" Then
                            strLabelText = "T 0 3 40 95 " + strValues(1) + vbNewLine
                        ElseIf strValues(9) = "LP" Then
                            strLabelText = "T 0 3 40 95 " + "LP" + vbNewLine
                        Else
                            strLabelText = "T 0 3 40 95 " + strValues(1) + vbNewLine
                        End If
                    End If

                    'Acronimo
                    Dim s As String = strValues(3)
                    Dim words As String() = s.Split("-")
                    Dim word As String
                    Dim Acronimo As String
                    For Each word In words
                        Acronimo = word
                        Exit For
                    Next
                    strLabelText = strLabelText + "T 0 3 40 45 " + Acronimo + vbNewLine

                    'Nombre de colector
                    Dim colectorValue As String = strValues(0)
                    Dim colectorArray As String() = colectorValue.Split("x")
                    Dim colector As String
                    Dim NombreColector As String

                    If colectorArray.Length > 2 Then
                        NombreColector = ReplaceSpecialCharacters(colectorArray(0)) + "x" + ReplaceSpecialCharacters(colectorArray(1))
                    Else
                        For Each colector In colectorArray
                            NombreColector = ReplaceSpecialCharacters(colector)
                            Exit For
                        Next
                    End If
                    strLabelText = strLabelText + "T 0 3 40 70 " + NombreColector + vbNewLine

                    If ChkDateHarvest.Checked = True Then
                        strLabelText = strLabelText + "T 0 3 40 120 " + "T " + strValues(12) + " ; B " + strValues(13) + vbNewLine
                    Else
                        strLabelText = strLabelText + "T 0 3 40 120 " + "T " + strValues(8) + " ; B " + strValues(11) + vbNewLine
                    End If

                    strLabelText = strLabelText + "T 0 3 40 155 " + strValues(7) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 170 " + ComboBox_Location.Text + vbNewLine

                    'If Trim(LCase(strBarcodeType)) = "1d" Then
                    '    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                    'Else
                    '    'strLabelText = strLabelText + "B DATAMATRIX 15520 27 H 4 S 200" + vbNewLine
                    '    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    '    If GUID = "" Then
                    '        'strLabelText = strLabelText + strValues(0) + "x" + strValues(8) + vbNewLine
                    '        strLabelText = strLabelText + strValues(14)
                    '    Else
                    '        strLabelText = strLabelText + strValues(0) + Chr(9) + GUID + vbNewLine
                    '    End If
                    '    strLabelText = strLabelText + "ENDQR " + vbNewLine
                    'End If

                    If Trim(LCase(strBarcodeType)) = "1d" Then
                        strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                    Else
                        strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                        If strValues(14) = "" Then
                            strLabelText = strLabelText + strValues(1) + "x" + strValues(0) + "x" + strValues(7) + vbNewLine
                        Else
                            strLabelText = strLabelText + strValues(14) + vbNewLine
                        End If
                        strLabelText = strLabelText + "ENDQR " + vbNewLine
                    End If

                    strEndPrint = "PRINT" + vbNewLine

                    Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)

                Else
                    boolImprimio = True
                End If

            End If

            If strPrintType = "Harvest Tuberculus" Then

                strStartOfLabel = leftMarginOfLabel + vbNewLine

                'Imprimir segun el checkbox
                Dim Acronimo As String
                Dim colector As String
                Dim NombreColector As String
                If chkPrintHarvestGeneral.Checked = False Then
                    'Acronimo
                    Acronimo = strValues(0).Split("-")(1)

                    'Nombre de colector
                    Dim colectorValue As String = strValues(0)
                    Dim colectorArray As String() = colectorValue.Split("-")
                    For Each colector In colectorArray
                        NombreColector = ReplaceSpecialCharacters(colector)
                        Exit For
                    Next
                    strLabelText = strLabelText + "T 0 3 40 45 " + Acronimo + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 70 " + NombreColector + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 95 " + "P: " + strValues(1) + " T: " + strValues(11) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 290 25 " + "Proc: " + strValues(3) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 120 " + "Peso: " + strValues(5) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 150 " + strValues(7) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 175 " + ComboBox_Location.Text + vbNewLine

                    If Trim(LCase(strBarcodeType)) = "1d" Then
                        strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                    Else
                        'strLabelText = strLabelText + "B DATAMATRIX 15520 27 H 4 S 200" + vbNewLine
                        strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                        If GUID = "" Then
                            strLabelText = strLabelText + NombreColector + ";" + strValues(1) + vbNewLine
                        Else
                            strLabelText = strLabelText + strValues(0) + Chr(9) + GUID + vbNewLine
                            'strLabelText = strLabelText + strValues(8) + Chr(9) + GUID + vbNewLine
                        End If
                        strLabelText = strLabelText + "ENDQR " + vbNewLine
                    End If

                Else
                    'arrayValues(0) = arrayHarvest(0) + "-" + arrayHarvest(1)
                    'arrayValues(1) = ""
                    'arrayValues(2) = ""
                    'arrayValues(3) = ""
                    'arrayValues(4) = ""
                    'arrayValues(5) = ""
                    'arrayValues(6) = Me.cboProcedence.Text
                    'arrayValues(7) = Me.DateTimePicker1.Text
                    'arrayValues(8) = arrayHarvest(4)
                    'arrayValues(11) = arrayHarvest(2)
                    'arrayValues(12) = arrayHarvest(3)
                    'Acronimo
                    Acronimo = strValues(0).Split("-")(1)
                    strLabelText = strLabelText + "T 0 3 40 45 " + Acronimo + vbNewLine

                    'Especie
                    NombreColector = strValues(0).Split("-")(0)
                    strLabelText = strLabelText + "T 0 3 40 70 " + NombreColector + vbNewLine

                    'Total de Tuberculos
                    strLabelText = strLabelText + "T 0 3 40 95 " + "Total: " + strValues(11) + vbNewLine

                    'Total de Genotipos
                    strLabelText = strLabelText + "T 0 3 40 120 " + "TGen: " + strValues(12) + vbNewLine

                    'Barcode
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    'strLabelText = strLabelText + Acronimo + Chr(9) + GUID + vbNewLine
                    strLabelText = strLabelText + NombreColector + ";" + strValues(11) + ";" + strValues(12) + vbNewLine 'strLabelText = strLabelText + Acronimo + vbNewLine
                    strLabelText = strLabelText + "ENDQR " + vbNewLine

                    'Peso
                    strLabelText = strLabelText + "T 0 3 40 150 " + "Peso: " + strValues(5) + vbNewLine

                    ' Procedencia
                    strLabelText = strLabelText + "T 0 3 290 25 " + "Proc: " + strValues(6) + vbNewLine

                    'Localidad
                    strLabelText = strLabelText + "T 0 3 40 175 " + strValues(7) + "-" + ComboBox_Location.Text + vbNewLine

                End If

                strEndPrint = "PRINT" + vbNewLine

                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If

            If strPrintType = "Maceration" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine

                If chkPrintHarvestGeneral.Checked = True Then
                    'arrayValues(0) = arrayResumenMaceration(0).ToString() 'Colector
                    'arrayValues(3) = arrayResumenMaceration(5).ToString() 'Acronimo
                    'arrayValues(5) = Me.DateTimePicker_Maceration.Text
                    'arrayValues(8) = Me.ComboBox_Username.SelectedItem
                    'arrayValues(11) = arrayResumenMaceration(1).ToString() 'Tipo de Cruzamiento
                    'arrayValues(12) = "TF: " + arrayResumenMaceration(2).ToString() + " TB: " + arrayResumenMaceration(3).ToString() 'strResumen
                    'arrayValues(13) = ComboBox_Location.Text
                    'arrayValues(14) = arrayResumenMaceration(4).ToString() 'ID

                    strLabelText = "T 0 3 40 70 " + strValues(11) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 20 " + strValues(3) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 45 " + strValues(0) + vbNewLine

                    strLabelText = strLabelText + "T 0 3 40 95 " + strValues(12) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 120 " + strValues(5) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 145 " + strValues(13) + vbNewLine
                    'strLabelText = strLabelText + "T 0 3 40 180 " + strValues(13) + vbNewLine

                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    strLabelText = strLabelText + strValues(14) + vbNewLine
                    strLabelText = strLabelText + "ENDQR " + vbNewLine

                Else
                    strLabelText = "T 0 3 40 70 " + strValues(11) + vbNewLine
                    'Get Acr�nimo de especie
                    Dim s As String = strValues(2)
                    Dim words As String() = s.Split("-")
                    Dim word As String
                    Dim Acronimo As String

                    For Each word In words
                        Acronimo = word
                        Exit For
                    Next

                    strLabelText = strLabelText + "T 0 3 40 20 " + Acronimo + vbNewLine

                    'Numero de colector
                    Dim colectorValue As String = strValues(0)
                    Dim colectorArray As String() = colectorValue.Split("-")
                    Dim colector As String
                    Dim NombreColector As String

                    For Each colector In colectorArray
                        NombreColector = colector
                        Exit For
                    Next

                    strLabelText = strLabelText + "T 0 3 40 45 " + NombreColector + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 95 " + strValues(5) + vbNewLine
                    strLabelText = strLabelText + "T 0 3 40 120 " + strValues(13) + vbNewLine 'Antes era usuario
                    strLabelText = strLabelText + "T 0 3 25 145 " + strValues(12) + vbNewLine
                    'strLabelText = strLabelText + "T 0 3 40 180 " + strValues(13) + vbNewLine

                    If Trim(LCase(strBarcodeType)) = "1d" Then
                        strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(10) + vbNewLine
                    Else
                        strLabelText = strLabelText + "B QR 295 40 M 2 U 5" + vbNewLine + "MN,"
                        If GUID = "" Then
                            strLabelText = strLabelText + strValues(14) + vbNewLine
                        Else
                            strLabelText = strLabelText + strValues(14) + Chr(9) + GUID + vbNewLine
                        End If
                        strLabelText = strLabelText + "ENDQR " + vbNewLine
                        'strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                        'If GUID = "" Then
                        '    strLabelText = strLabelText + NombreColector + ";" + strValues(1) + vbNewLine
                        'Else
                        '    strLabelText = strLabelText + strValues(0) + Chr(9) + GUID + vbNewLine
                        'End If
                        'strLabelText = strLabelText + "ENDQR " + vbNewLine
                    End If
                End If

                strEndPrint = "PRINT" + vbNewLine

                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If

        ElseIf ComboBox_PrintType.SelectedIndex = 2 Then

            If strPrintType = "Crossing" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine

                If strValues(5) = "Sib-cross" Then
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                ElseIf strValues(5) = "Bulk" Then
                    strLabelText = "T 0 3 40 95 " + "Bk" + vbNewLine
                ElseIf strValues(5) = "AP" Then
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                ElseIf strValues(5) = "LP" Then
                    strLabelText = "T 0 3 40 95 " + "LP" + vbNewLine
                Else
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                End If

                Dim intRow As Integer
                Dim Responsables As String = ""
                For intRow = 0 To ComboBox_Username.Items.Count - 1
                    If intRow = 0 Then
                        Responsables = Mid(ComboBox_Username.Items(intRow).ToString(), 1, 2)
                    Else
                        Responsables = Responsables + ", " + Mid(ComboBox_Username.Items(intRow).ToString(), 1, 2)
                    End If
                Next

                'Get Acr�nimo de especie
                ' We want to split this input string
                Dim s As String = strValues(3)
                ' Split string based on spaces
                Dim words As String() = s.Split("-")
                ' Use For Each loop over words and display them
                Dim word As String
                Dim Acronimo As String

                For Each word In words
                    Acronimo = word
                    Exit For
                Next

                strLabelText = strLabelText + "T 0 4 40 45 " + Acronimo + vbNewLine

                'Numero de colector
                ' We want to split this input string
                Dim colectorValue As String = strValues(1)

                ' Split string based on spaces
                Dim colectorArray As String() = colectorValue.Split("x")

                ' Use For Each loop over words and display them
                Dim colector As String
                Dim NombreColector As String

                For Each colector In colectorArray
                    NombreColector = colector
                    Exit For
                Next

                strLabelText = strLabelText + "T 0 4 40 70 " + NombreColector + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 120 " + "T " + strValues(4) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 150 " + strValues(6) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 180 " + ComboBox_Location.Text + vbNewLine

                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    'If GUID = "" Then
                    '    strLabelText = strLabelText + strValues(1) + "x" + strValues(0) + "x" + strValues(7) + vbNewLine
                    'Else
                    '    strLabelText = strLabelText + strValues(1) + Chr(9) + GUID + vbNewLine
                    'End If
                    If strValues(8) = "" Then
                        strLabelText = strLabelText + strValues(1) + "x" + strValues(0) + "x" + strValues(7) + vbNewLine
                    Else
                        strLabelText = strLabelText + Integer.Parse(strValues(8).ToString()).ToString() + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If

                strEndPrint = "PRINT" + vbNewLine

                'Only
                For value As Integer = 1 To CInt(ComboBox_Flowers.Text)
                    ' Exit condition if the value is three.
                    Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
                Next

            End If

            If strPrintType = "Harvest" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine

                'If ComboBox_TypeHarvest.SelectedIndex = 0 Then

                'Type of cross
                If ChkDateHarvest.Checked = True Then
                    strLabelText = "T 0 3 40 95 " + strValues(10) + vbNewLine
                Else
                    If strValues(9) = "Sib-cross" Then
                        strLabelText = "T 0 3 40 95 " + strValues(1) + vbNewLine
                    ElseIf strValues(9) = "Bulk" Then
                        strLabelText = "T 0 3 40 95 " + "Bk" + vbNewLine
                    ElseIf strValues(9) = "AP" Then
                        strLabelText = "T 0 3 40 95 " + "AP" + vbNewLine
                    ElseIf strValues(9) = "LP" Then
                        strLabelText = "T 0 3 40 95 " + "LP" + vbNewLine
                    Else
                        strLabelText = "T 0 3 40 95 " + strValues(1) + vbNewLine
                    End If
                End If

                'Acronimo
                Dim s As String = strValues(3)
                Dim words As String() = s.Split("-")
                Dim word As String
                Dim Acronimo As String
                For Each word In words
                    Acronimo = word
                    Exit For
                Next
                strLabelText = strLabelText + "T 0 3 40 45 " + Acronimo + vbNewLine

                'Nombre de colector
                Dim colectorValue As String = strValues(0)
                Dim colectorArray As String() = colectorValue.Split("x")
                Dim colector As String
                Dim NombreColector As String
                For Each colector In colectorArray
                    NombreColector = colector
                    Exit For
                Next
                strLabelText = strLabelText + "T 0 3 40 70 " + NombreColector + vbNewLine

                If ChkDateHarvest.Checked = True Then
                    strLabelText = strLabelText + "T 0 3 40 120 " + "T " + strValues(12) + " ; B " + strValues(13) + vbNewLine
                Else
                    strLabelText = strLabelText + "T 0 3 40 120 " + "T " + strValues(8) + " ; B " + strValues(11) + vbNewLine
                End If

                strLabelText = strLabelText + "T 0 3 40 155 " + strValues(7) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 170 " + ComboBox_Location.Text + vbNewLine

                'If Trim(LCase(strBarcodeType)) = "1d" Then
                '    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                'Else
                '    'strLabelText = strLabelText + "B DATAMATRIX 15520 27 H 4 S 200" + vbNewLine
                '    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                '    If GUID = "" Then
                '        strLabelText = strLabelText + strValues(14) '+ "x" + strValues(8) + vbNewLine
                '    Else
                '        strLabelText = strLabelText + strValues(0) + Chr(9) + GUID + vbNewLine
                '    End If
                '    strLabelText = strLabelText + "ENDQR " + vbNewLine
                'End If

                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    If strValues(14) = "" Then
                        strLabelText = strLabelText + strValues(1) + "x" + strValues(0) + "x" + strValues(7) + vbNewLine
                    Else
                        strLabelText = strLabelText + strValues(14) + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If


                strEndPrint = "PRINT" + vbNewLine

                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If

            If strPrintType = "Harvest Tuberculus" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine
                'Acronimo
                Dim Acronimo As String
                Acronimo = strValues(0).Split("-")(1)

                'Nombre de colector
                Dim colectorValue As String = strValues(0)
                Dim colectorArray As String() = colectorValue.Split("-")
                Dim colector As String
                Dim NombreColector As String
                For Each colector In colectorArray
                    NombreColector = colector
                    Exit For
                Next
                strLabelText = strLabelText + "T 0 4 40 45 " + Acronimo + vbNewLine
                strLabelText = strLabelText + "T 0 4 40 70 " + NombreColector + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 95 " + "Hno. : " + strValues(1) + " Fr: " + strValues(11) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 120 " + "Sp: " + strValues(2) + vbNewLine 'strValues(3) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 150 " + strValues(7) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 175 " + ComboBox_Location.Text + vbNewLine
                strLabelText = strLabelText + "T 0 2 385 180 " + "(" + strValues(12) + ")" + vbNewLine

                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    'strLabelText = strLabelText + "B DATAMATRIX 15520 27 H 4 S 200" + vbNewLine
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    'If GUID = "" Then
                    '    strLabelText = strLabelText + NombreColector + ";" + strValues(1) + vbNewLine
                    'Else
                    '    strLabelText = strLabelText + strValues(0) + Chr(9) + GUID + vbNewLine
                    'End If
                    If strValues(8) = "" Then
                        strLabelText = strLabelText + NombreColector + ";" + strValues(1) + vbNewLine
                    Else
                        strLabelText = strLabelText + Integer.Parse(strValues(8).ToString()).ToString() + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If
                strEndPrint = "PRINT" + vbNewLine

                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If

        ElseIf ComboBox_PrintType.SelectedIndex = 3 Then

            If strPrintType = "Crossing" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine

                If strValues(5) = "Sib-cross" Then
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                ElseIf strValues(5) = "Bulk" Then
                    strLabelText = "T 0 3 40 95 " + "Bk" + vbNewLine
                ElseIf strValues(5) = "AP" Then
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                ElseIf strValues(5) = "LP" Then
                    strLabelText = "T 0 3 40 95 " + "LP" + vbNewLine
                Else
                    strLabelText = "T 0 3 40 95 " + strValues(0) + vbNewLine
                End If

                Dim intRow As Integer
                Dim Responsables As String = ""
                For intRow = 0 To ComboBox_Username.Items.Count - 1
                    If intRow = 0 Then
                        Responsables = Mid(ComboBox_Username.Items(intRow).ToString(), 1, 2)
                    Else
                        Responsables = Responsables + ", " + Mid(ComboBox_Username.Items(intRow).ToString(), 1, 2)
                    End If
                Next

                'Get Acr�nimo de especie
                ' We want to split this input string
                Dim s As String = strValues(3)
                ' Split string based on spaces
                Dim words As String() = s.Split("-")
                ' Use For Each loop over words and display them
                Dim word As String
                Dim Acronimo As String

                For Each word In words
                    Acronimo = word
                    Exit For
                Next

                strLabelText = strLabelText + "T 0 3 40 45 " + Acronimo + vbNewLine

                'Numero de colector
                ' We want to split this input string
                Dim colectorValue As String = strValues(1)

                ' Split string based on spaces
                Dim colectorArray As String() = colectorValue.Split("x")

                ' Use For Each loop over words and display them
                Dim colector As String
                Dim NombreColector As String

                For Each colector In colectorArray
                    NombreColector = colector
                    Exit For
                Next

                strLabelText = strLabelText + "T 0 3 40 70 " + NombreColector + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 120 " + "T " + strValues(4) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 150 " + strValues(6) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 180 " + ComboBox_Location.Text + vbNewLine

                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    'If GUID = "" Then
                    '    strLabelText = strLabelText + strValues(1) + "x" + strValues(0) + "x" + strValues(7) + vbNewLine
                    'Else
                    '    strLabelText = strLabelText + strValues(1) + Chr(9) + GUID + vbNewLine
                    'End If
                    If strValues(8) = "" Then
                        strLabelText = strLabelText + strValues(1) + "x" + strValues(0) + "x" + strValues(7) + vbNewLine
                    Else
                        strLabelText = strLabelText + Integer.Parse(strValues(8).ToString()).ToString() + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If

                strEndPrint = "PRINT" + vbNewLine

                'Only
                For value As Integer = 1 To CInt(ComboBox_Flowers.Text)
                    ' Exit condition if the value is three.
                    Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
                Next

            End If

            If strPrintType = "Harvest" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine

                'If ComboBox_TypeHarvest.SelectedIndex = 0 Then

                'Type of cross
                If ChkDateHarvest.Checked = True Then
                    strLabelText = "T 0 3 40 95 " + strValues(10) + vbNewLine
                Else
                    If strValues(9) = "Sib-cross" Then
                        strLabelText = "T 0 3 40 95 " + strValues(1) + vbNewLine
                    ElseIf strValues(9) = "Bulk" Then
                        strLabelText = "T 0 3 40 95 " + "Bk" + vbNewLine
                    ElseIf strValues(9) = "AP" Then
                        strLabelText = "T 0 3 40 95 " + "AP" + vbNewLine
                    ElseIf strValues(9) = "LP" Then
                        strLabelText = "T 0 3 40 95 " + "LP" + vbNewLine
                    Else
                        strLabelText = "T 0 3 40 95 " + strValues(1) + vbNewLine
                    End If
                End If

                'Acronimo
                Dim s As String = strValues(3)
                Dim words As String() = s.Split("-")
                Dim word As String
                Dim Acronimo As String
                For Each word In words
                    Acronimo = word
                    Exit For
                Next
                strLabelText = strLabelText + "T 0 3 40 45 " + Acronimo + vbNewLine

                'Nombre de colector
                Dim colectorValue As String = strValues(0)
                Dim colectorArray As String() = colectorValue.Split("x")
                Dim colector As String
                Dim NombreColector As String
                For Each colector In colectorArray
                    NombreColector = colector
                    Exit For
                Next
                strLabelText = strLabelText + "T 0 3 40 70 " + NombreColector + vbNewLine

                If ChkDateHarvest.Checked = True Then
                    strLabelText = strLabelText + "T 0 3 40 120 " + "T " + strValues(12) + " ; B " + strValues(13) + vbNewLine
                Else
                    strLabelText = strLabelText + "T 0 3 40 120 " + "T " + strValues(8) + " ; B " + strValues(11) + vbNewLine
                End If

                strLabelText = strLabelText + "T 0 3 40 155 " + strValues(7) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 170 " + ComboBox_Location.Text + vbNewLine

                'If Trim(LCase(strBarcodeType)) = "1d" Then
                '    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                'Else
                '    'strLabelText = strLabelText + "B DATAMATRIX 15520 27 H 4 S 200" + vbNewLine
                '    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                '    If GUID = "" Then
                '        strLabelText = strLabelText + strValues(14) '+ "x" + strValues(8) + vbNewLine
                '    Else
                '        strLabelText = strLabelText + strValues(0) + Chr(9) + GUID + vbNewLine
                '    End If
                '    strLabelText = strLabelText + "ENDQR " + vbNewLine
                'End If

                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    If strValues(14) = "" Then
                        strLabelText = strLabelText + strValues(1) + "x" + strValues(0) + "x" + strValues(7) + vbNewLine
                    Else
                        strLabelText = strLabelText + strValues(14) + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If


                strEndPrint = "PRINT" + vbNewLine

                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If

            If strPrintType = "Harvest Tuberculus" Then
                strStartOfLabel = leftMarginOfLabel + vbNewLine
                ''Get Acr�nimo de especie
                '' We want to split this input string
                'Dim s As String = strValues(3)
                '' Split string based on spaces
                'Dim words As String() = s.Split("-")
                '' Use For Each loop over words and display them
                'Dim word As String
                'Dim Acronimo As String

                'For Each word In words
                '    Acronimo = word
                '    Exit For
                'Next

                'strLabelText = strLabelText + "T 0 3 40 45 " + Acronimo + vbNewLine

                ''Numero de colector
                '' We want to split this input string
                'Dim colectorValue As String = strValues(1)

                '' Split string based on spaces
                'Dim colectorArray As String() = colectorValue.Split("x")

                '' Use For Each loop over words and display them
                'Dim colector As String
                'Dim NombreColector As String

                'If colectorArray.Length > 2 Then
                '    NombreColector = colectorArray(0) + "x" + colectorArray(1)
                'Else
                '    For Each colector In colectorArray
                '        NombreColector = colector
                '        Exit For
                '    Next
                'End If


                'Acronimo
                Dim Acronimo As String
                Acronimo = strValues(0).Split("-")(1)

                'Nombre de colector
                Dim colectorValue As String = strValues(0)
                Dim colectorArray As String() = colectorValue.Split("-")
                Dim colector As String
                Dim NombreColector As String
                For Each colector In colectorArray
                    NombreColector = colector
                    Exit For
                Next
                strLabelText = strLabelText + "T 0 3 40 45 " + Acronimo + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 70 " + NombreColector + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 95 " + strValues(6) + vbNewLine '" + strValues(1) + " Fr: " + strValues(11) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 120 " + "Hno : " + strValues(1) + vbNewLine
                'strLabelText = strLabelText + "T 0 3 40 120 " + "Procedencia: " + strValues(3) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 150 " + strValues(7) + vbNewLine
                strLabelText = strLabelText + "T 0 3 40 175 " + ComboBox_Location.Text + vbNewLine
                strLabelText = strLabelText + "T 0 2 385 180 " + "(" + strValues(12) + ")" + vbNewLine

                If Trim(LCase(strBarcodeType)) = "1d" Then
                    strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
                Else
                    'strLabelText = strLabelText + "B DATAMATRIX 15520 27 H 4 S 200" + vbNewLine
                    strLabelText = strLabelText + "B QR 290 50 M 2 U 5" + vbNewLine + "MN,"
                    'If GUID = "" Then
                    '    strLabelText = strLabelText + NombreColector + ";" + strValues(1) + vbNewLine
                    'Else
                    '    strLabelText = strLabelText + strValues(0) + Chr(9) + GUID + vbNewLine
                    'End If
                    If strValues(8) = "" Then
                        strLabelText = strLabelText + NombreColector + ";" + strValues(1) + vbNewLine
                    Else
                        strLabelText = strLabelText + Integer.Parse(strValues(8).ToString()).ToString() + vbNewLine
                    End If
                    strLabelText = strLabelText + "ENDQR " + vbNewLine
                End If
                strEndPrint = "PRINT" + vbNewLine

                Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
            End If

        End If

    End Sub

    'Printing characteristics
    Public Sub PrintRowLabels(ByVal strCommands As String, ByVal strPort As String, ByRef strError As String)
        Dim intIndex As Int16

        strError = ""
        SerialPort = New OpenNETCF.IO.Serial.Port(strPort)
        SerialPort.Close()
        SerialPort.Dispose()

        SerialPort = New OpenNETCF.IO.Serial.Port(strPort)
        SerialPort.Settings.BaudRate = 19200
        SerialPort.Settings.StopBits = OpenNETCF.IO.Serial.StopBits.one
        SerialPort.Settings.Parity = OpenNETCF.IO.Serial.Parity.none
        SerialPort.SThreshold = 1

        Try
            If Not SerialPort.IsOpen Then
                boolImprimio = True
                SerialPort.Open()
            Else
                strError = "1"
                Call ShowSystemMessage(strMessage13)
                Exit Sub
            End If

        Catch ex As Exception
            boolImprimio = False
            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage13)
            Else
                Call ShowSystemMessage(strMessage13)
            End If
            Exit Sub
        End Try

        Dim arrayOutput As Byte() = New Byte(0) {}
        Dim encoding As New System.Text.ASCIIEncoding

        For intIndex = 0 To strCommands.Length - 1
            Dim prueba As String
            prueba = strCommands(51)
            'arrayOutput(0) = Convert.ToByte(strCommands(intIndex))
            'arrayOutput(0) = BitConverter.ToInt32(strCommands, intIndex)
            arrayOutput(0) = encoding.GetBytes(strCommands(intIndex))(0)
            SerialPort.Output = arrayOutput
        Next

        SerialPort.Close()

        If SerialPort IsNot Nothing Then
            SerialPort.Dispose()
        End If
    End Sub

    'Globally unique identifier
    Private Function getGUID() As String
        Dim sGUID, barcodeType, valueListBox, value As String
        Dim intRow As Integer

        sGUID = System.Guid.NewGuid.ToString()
        barcodeType = Me.ComboBox_BarcodeType.SelectedItem

        For intRow = 0 To Me.ListBox_BarcodeType.Items.Count - 1
            If barcodeType = Me.ListBox_BarcodeType.Items(intRow) Then
                valueListBox = Me.ListBox_BarcodeTypeID.Items(intRow)
                value = valueListBox.ToUpper.Replace(" ", "")

                If value = "YES" Then
                ElseIf value = "NO" Then
                    sGUID = ""
                Else
                    sGUID = ""
                End If
            End If
        Next

        Return sGUID
    End Function
    '========================
    '==Search with KeyPress==

    'Parentals
    'Search Female KeyPress
    Private Sub TextBox_SearchFemale_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_SearchFemale.KeyPress
        Call SearchBarcodeParent(Me.C1FlexGrid_Female, Me.TextBox_SearchFemale, e.KeyChar)
    End Sub

    'Search Male KeyPress
    Private Sub TextBox_SearchMale_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_SearchMale.KeyPress
        Call SearchBarcodeParent(Me.C1FlexGrid_Male, Me.TextBox_SearchMale, e.KeyChar)
    End Sub

    'Search Barcode Parent
    Sub SearchBarcodeParent(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef objText As TextBox, ByVal strChar As Char)
        If strChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            Call SearchParent(objGrid, objText.Text.ToUpper())
            objText.SelectAll()
        End If
    End Sub

    'Crossing
    'Search FemaleCross KeyPress
    Private Sub TextBox_SearchFemaleCross_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_SearchFemaleCross.KeyPress
        Call SearchBarcodeParent(Me.C1FlexGrid_FemaleCross, Me.TextBox_SearchFemaleCross, e.KeyChar)
    End Sub

    'Search MaleCross KeyPress
    Private Sub TextBox_SearchMaleCross_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_SearchMaleCross.KeyPress
        Call SearchBarcodeParent(Me.C1FlexGrid_MaleCross, Me.TextBox_SearchMaleCross, e.KeyChar)
    End Sub

    'Search MaleCross KeyPress
    Private Sub TextBox_SearchValueCross_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_SearchValueCross.KeyPress
        Call SearchBarcodeValueCross(Me.C1FlexGrid_ValuesCross, Me.TextBox_SearchValueCross, e.KeyChar)
    End Sub

    'Harvest
    'Search Harvest KeyPress
    Private Sub TextBox_SearchHarvest_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_SearchHarvest.KeyPress
        Call SearchBarcodeCrossNew(Me.C1FlexGrid_Harvest, Me.TextBox_SearchHarvest, e.KeyChar, Me.Label_CountHarvest, Me.Button_LoadHarvest)
    End Sub

    'Maceration
    'Search Maceration KeyPress
    Private Sub TextBox_SearchMaceration_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_SearchMaceration.KeyPress
        If chkPrintHarvestGeneral.Checked = True Then
            Call SetResumeMac(Me.TextBox_SearchMaceration, Me.TextBox_SearchMaceration.Text, e.KeyChar)
        Else
            Call SearchBarcodeCrossNew(Me.C1FlexGrid_Maceration, Me.TextBox_SearchMaceration, e.KeyChar, Me.Label_CountMaceration, Me.Button_LoadMaceration)
        End If
    End Sub

    'Search Barcode Cross
    Sub SearchBarcodeCross(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef objText As TextBox, ByVal strChar As Char, ByRef objLabel As Label, ByRef objButton As Button)
        Dim arrayValues() As String

        If strChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            arrayValues = objText.Text.Split("x")
            If arrayValues.Length > 1 Then
                'If UBound(arrayValues) > -1 Then
                Call SearchCross(objGrid, objText.Text.ToUpper(), objLabel, objButton, "Nuevo")
                objText.SelectAll()
            End If
        End If
    End Sub

    'Search Barcode Cross New
    Sub SearchBarcodeCrossNew(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef objText As TextBox, ByVal strChar As Char, ByRef objLabel As Label, ByRef objButton As Button)
        Dim bolFound As Boolean
        Dim strValue As String

        If strChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            strValue = Trim(objText.Text)
            If strValue <> "" Then
                bolFound = SearchCrossNew(objGrid, strValue)
                If bolFound = True Then
                    objText.SelectAll()
                Else
                    MsgBox("ERROR: Dato de codigo barra no encontrado en la lista: " + strValue, MsgBoxStyle.Critical)
                End If
            End If
        End If
    End Sub

    'Search Barcode Cross Value
    Sub SearchBarcodeValueCross(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef objText As TextBox, ByVal strChar As Char)
        If strChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If cboBarcodeForCross.SelectedIndex = 0 Then
                SearchValueCross(objGrid, Trim(objText.Text.ToUpper().Replace("�", " ")))
            ElseIf cboBarcodeForCross.SelectedIndex = 1 Then
                Dim arrayValues() As String
                arrayValues = objText.Text.Split("_") ' cambios en camote: ; a _
                If arrayValues.Length > 1 Then
                    'If UBound(arrayValues) > -1 Then
                    Call SearchValueCross(objGrid, Trim(arrayValues(0).ToUpper().Replace("�", " ")))
                    ComboBox_GenFemale.Text = arrayValues(3) ' cambios en camote: 1 a 3
                    objText.Text = ""
                    'Else

                    'End If
                End If

            End If
            objText.SelectAll()
        End If
    End Sub

    'Search Barcode Harvest Fruits and Tuberculus
    Sub SearchBarcodeValueFruitsTuber(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef objText As TextBox, ByVal strChar As Char)
        If strChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If cboBarcodeForCross.SelectedIndex = 0 Then
                SearchValueCross(objGrid, objText.Text.ToUpper())
            ElseIf cboBarcodeForCross.SelectedIndex = 1 Then
                Dim arrayValues() As String
                arrayValues = objText.Text.Split("_") ' cambios en camote: ; a _
                If arrayValues.Length > 1 Then
                    'If UBound(arrayValues) > -1 Then
                    Call SearchValueCross(objGrid, arrayValues(0).ToUpper())
                    cboFemaleTuber.Text = arrayValues(3) ' cambios en camote: 1 a 3
                    objText.Text = ""
                End If
            End If
            objText.SelectAll()
            SelectHavTubFru()
        End If
    End Sub
    'Sub SetResumenTub(ByRef objText As TextBox, ByVal strChar As Char)
    '    If strChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
    '        Dim resumenTub As String = ""

    '        txtCosechaTuber.Text = ""
    '        objText.SelectAll()
    '    End If
    'End Sub

    'Button Close
    Private Sub Button_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Close.Click
        Me.Panel_Message.Visible = False
    End Sub

    Private Sub ComboBox_TypeHarvest_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'If Me.ComboBox_TypeHarvest.SelectedIndex = 0 Then
        '    lblNroPlant.Visible = False
        '    ComboBox_NroPlants.Visible = False
        '    lblNroTuber.Visible = False
        '    ComboBox_NroTubers.Visible = False
        '    cboProcedence.Visible = False
        '    lblProcedence.Visible = False
        '    Label_Fruits.Visible = True
        '    ComboBox_NroFruits.Visible = True
        '    lblNroFloresG.Visible = True
        '    ComboBox_TotalFlowers.Visible = True
        '    txtHarvest_Potato.Visible = True
        '    TextBox_Harvest.Visible = False
        'ElseIf Me.ComboBox_TypeHarvest.SelectedIndex = 1 Then
        '    lblNroPlant.Visible = True
        '    ComboBox_NroPlants.Visible = True
        '    lblNroTuber.Visible = True
        '    ComboBox_NroTubers.Visible = True
        '    cboProcedence.Visible = True
        '    lblProcedence.Visible = True
        '    Label_Fruits.Visible = False
        '    ComboBox_NroFruits.Visible = False
        '    lblNroFloresG.Visible = False
        '    ComboBox_TotalFlowers.Visible = False
        '    txtHarvest_Potato.Visible = False
        '    TextBox_Harvest.Visible = True
        'Else
        '    lblNroPlant.Visible = False
        '    ComboBox_NroPlants.Visible = False
        '    lblNroTuber.Visible = False
        '    cboProcedence.Visible = False
        '    lblProcedence.Visible = False
        '    ComboBox_NroTubers.Visible = False
        '    Label_Fruits.Visible = False
        '    ComboBox_NroFruits.Visible = False
        '    lblNroFloresG.Visible = False
        '    ComboBox_TotalFlowers.Visible = False
        '    txtHarvest_Potato.Visible = False
        '    TextBox_Harvest.Visible = False
        'End If
    End Sub

    Private Sub Button_Search_ValueCross_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Search_ValueCross.Click
        If Me.TextBox_SearchValueCross.Text <> "" Then
            Call SearchValueCross(Me.C1FlexGrid_ValuesCross, Me.TextBox_SearchValueCross.Text.ToUpper())
        End If
    End Sub

    Sub SearchValueCross(ByRef objGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal searchvalue As String)
        Dim intRow As Integer
        ' searchvalue = searchvalue.Replace("�", " ")

        For intRow = 0 To objGrid.Rows.Count - 1
            If searchvalue = Trim(objGrid(intRow, 0).ToString.ToUpper()) Then
                objGrid.Select(intRow, 0)
            End If
            If searchvalue = Trim(objGrid(intRow, 1).ToString.ToUpper()) Then
                objGrid.Select(intRow, 0)
            End If
            If searchvalue = Trim(objGrid(intRow, 2).ToString.ToUpper()) Then
                objGrid.Select(intRow, 0)
            End If
            If searchvalue = Trim(objGrid(intRow, 3).ToString.ToUpper()) Then
                objGrid.Select(intRow, 0)
            End If
        Next
    End Sub

    Private Sub TextBox_SearchValue_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_SearchValue.KeyPress
        If chkPrintHarvestGeneral.Checked = True Then
            Call SetResumeTub(Me.TextBox_SearchValue, Me.TextBox_SearchValue.Text, e.KeyChar)
        Else
            Call SearchBarcodeValueFruitsTuber(Me.C1FlexGrid_ValuesTubHarv, Me.TextBox_SearchValue, e.KeyChar)
        End If
    End Sub

    Private Sub btnSearchTuberculusHarvest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchTuberculusHarvest.Click
        If Me.TextBox_SearchValue.Text <> "" Then
            Call SearchValueCross(Me.C1FlexGrid_ValuesTubHarv, Me.TextBox_SearchValue.Text.ToUpper())
        End If
        'If chkPrintHarvestGeneral.Checked = True Then
        '    Call SetResumeTub2(Me.TextBox_SearchValue, Me.TextBox_SearchValue.Text, Microsoft.VisualBasic.ChrW(Keys.Return))
        'Else
        '    Call SearchBarcodeValueFruitsTuber2(Me.C1FlexGrid_ValuesTubHarv, Me.TextBox_SearchValue, Microsoft.VisualBasic.ChrW(Keys.Return))
        'End If
    End Sub

    Private Sub btnSelectTuberHarv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectTuberHarv.Click
        Call SelectHavTubFru()
    End Sub

    Private Sub SelectHavTubFru()
        Dim intRowSelectedFemale, intRowSelectedMale As Integer, FData1, MData1, OrderF, OrderM, FData2, MData2, FData3, MData3 As String
        Dim count As Integer = 0
        Dim intRow As Integer

        Try
            If Me.TextBox_CrossingPlan.Text <> "" Then

                Dim intRowSelectedValue As Integer, VData1, VData2, VData3, VData4 As String
                intRowSelectedValue = Me.C1FlexGrid_ValuesTubHarv.Selection.TopRow

                ' clear ComboBox_NroTubers and TextBox2 (Peso)

                Me.ComboBox_NroTubers.SelectedIndex = 0
                Me.TextBox2.Text = ""

                'Crossing
                VData1 = Me.C1FlexGrid_ValuesTubHarv.Item(intRowSelectedValue, 1).ToString

                'Order
                OrderF = Me.cboFemaleTuber.Text

                ''Data2
                VData2 = Me.C1FlexGrid_ValuesTubHarv.Item(intRowSelectedValue, 2).ToString

                'Data3
                VData3 = Me.C1FlexGrid_ValuesTubHarv.Item(intRowSelectedValue, 3).ToString

                'Data4
                VData4 = Me.C1FlexGrid_ValuesTubHarv.Item(intRowSelectedValue, 4).ToString

                If chkPrintHarvestGeneral.Checked = True Then
                    Call SetResumeTub2(VData1)
                Else
                    Me.txtCosechaTuber.Text = VData1 + "-" + VData2 + "-" + VData3 + "-" + VData4
                    'Me.TextBox_Order.Text = OrderF + " - " + OrderM
                    'Me.TextBox_Data2.Text = VData2 + " - " + VData2
                    'Me.TextBox_Data3.Text = VData3 + " - " + VData3
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnPrintTuberHarvest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintTuberHarvest.Click
        If MsgBox(strMessage24, MsgBoxStyle.OkCancel, "Mensaje") = MsgBoxResult.Ok Then
            Dim arrayHarvest() As String
            Dim arrayValues() As String
            ReDim arrayValues(13)
            Dim strError As String

            Dim strColector, strTipoCruza As String

            strError = ""

            If Me.txtCosechaTuber.Text <> "" Then
                If chkPrintHarvestGeneral.Checked = True Then
                    'strColector + "|" + strEspecie + "|" + strTotalTuberculos + "|" + strTotalGenotipos --------------+ "|" + ID
                    arrayHarvest = txtCosechaTuber.Text.Split("|")

                    arrayValues(0) = arrayHarvest(0) + "-" + arrayHarvest(1)
                    arrayValues(1) = ""
                    arrayValues(2) = ""
                    arrayValues(3) = ""
                    arrayValues(4) = ""
                    arrayValues(5) = Me.TextBox2.Text 'peso
                    arrayValues(6) = Me.cboProcedence.Text
                    arrayValues(7) = Me.DateTimePicker1.Text
                    arrayValues(8) = "" 'arrayHarvest(4)
                    arrayValues(11) = arrayHarvest(2)
                    arrayValues(12) = arrayHarvest(3)

                    Call PrintLabel(arrayValues, Me.ComboBox_BarcodeType.SelectedItem.ToString, Me.ComboBox_Port.SelectedItem.ToString, "Harvest Tuberculus", strError)
                    Me.TextBox_Cross.Text = ""
                    Me.TextBox_Order.Text = ""
                    Me.txtCosechaTuber.Text = ""

                Else

                    Dim ID As String = GenerarID()
                    'validate print harvest  
                    Dim value1 As String = Me.txtCosechaTuber.Text.Split("-")(0)
                    Dim value2 As String = Me.txtCosechaTuber.Text.Split("-")(2)
                    arrayValues(0) = value1 + "-" + value2
                    arrayValues(1) = Me.cboFemaleTuber.Text
                    arrayValues(2) = Me.txtCosechaTuber.Text.Split("-")(3)
                    arrayValues(3) = Me.cboProcedence.Text
                    arrayValues(4) = ""
                    arrayValues(5) = Me.TextBox2.Text
                    arrayValues(6) = Me.txtCosechaTuber.Text.Split("-")(1)
                    arrayValues(7) = Me.DateTimePicker1.Text
                    arrayValues(8) = ID
                    arrayValues(11) = Me.ComboBox_NroTubers.SelectedItem
                    arrayValues(12) = Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1

                    Dim intRow As Integer
                    Dim user2 As String

                    user2 = Me.ComboBox_Username.SelectedItem

                    Me.ListBox_Hide.Items.Clear()


                    Call PrintLabel(arrayValues, Me.ComboBox_BarcodeType.SelectedItem.ToString, Me.ComboBox_Port.SelectedItem.ToString, "Harvest Tuberculus", strError)

                    If boolImprimio = True Then
                        WriteHaverstBayasTuberculos(ID)
                        Me.TextBox_Cross.Text = ""
                        Me.TextBox_Order.Text = ""
                        Call GetView()
                    End If
                End If

            End If
        End If


    End Sub

    'Escribe los resultados en la BD
    Private Sub WriteHaverstBayasTuberculos(ByVal ID As String)
        Dim arrayValues() As String
        ReDim arrayValues(3)
        Dim intRow As Integer
        Dim valuesOrd(), valuesColN() As String

        Dim Plan, NroPlan, FDato1, FDato2, FDato3, FDato4, FDato5, MDato1, MDato2, MDato3, MDato4, MDato5, TipoCruzamiento, FOrder, MOrder, FloresPolinizadas, LocalidadCruce, NroRepeticiones, FechaImpresionCruce, UsuarioCruce, FechaCruce As String
        'Dim ResumenCosecha, TotalFloresPolinizadas, TotalFrutos, UsuarioCosecha, FechaCosecha, FechaImpresionCosecha As String
        Dim TotalTuberculos, ProcedenciaTub, FechaCosechaTub, LocalidadCosecha, LocalidadCosechaTub, UsuarioCosechaTub, FechaImpresionCosechaTub, Peso As String
        'Dim LocalidadMaceracion, FechaMaceracion, UsuarioMaceracion, LOTID, EstadoResultado As String

        'Asignacion de valor a las variables de cruzamiento.
        NroPlan = Me.ComboBox_SelectCrossingPlan.SelectedIndex + 1
        Plan = Me.TextBox_CrossingPlanSelectedHide.Text


        'TotalTuberculos ProcedenciaTub    
        'arrayValues(0) = Me.txtCosechaTuber.Text
        'arrayValues(1) = Me.cboFemaleTuber.Text
        UsuarioCosechaTub = Me.ComboBox_Username.SelectedItem
        LocalidadCosechaTub = Me.ComboBox_Location.SelectedItem
        TipoCruzamiento = Me.ComboBox_TypeCrossOption.SelectedItem
        TotalTuberculos = Me.ComboBox_NroTubers.SelectedItem
        FechaImpresionCosechaTub = Date.Now.ToString
        FechaCosechaTub = Me.DateTimePicker1.Text
        ProcedenciaTub = Me.cboProcedence.Text
        Peso = Me.TextBox2.Text
        FDato1 = Me.txtCosechaTuber.Text.Split("-")(0) '.Replace(" ", "").Split("-")(0)
        FDato2 = Me.txtCosechaTuber.Text.Split("-")(1) '.Replace(" ", "").Split("-")(1)
        FDato3 = Me.txtCosechaTuber.Text.Split("-")(2) '.Replace(" ", "").Split("-")(2)

        'Escribe un record de cruza en la BD
        Dim drv As DataRowView = DirectCast(Me.BindingSource1.AddNew(), DataRowView)
        Dim dr As Results_Dataset.resultsRow = DirectCast(drv.Row, Results_Dataset.resultsRow)

        dr.ID = ID
        dr.FDato1 = FDato1
        dr.FDato2 = FDato2
        dr.FDato3 = FDato3
        dr.FDato4 = ""
        dr.FDato5 = ""
        dr.MDato1 = ""
        dr.MDato2 = ""
        dr.MDato3 = ""
        dr.MDato4 = ""
        dr.MDato5 = ""
        dr.TipoCruzamiento = TipoCruzamiento
        dr.FOrder = Me.cboFemaleTuber.Text
        dr.MOrder = ""
        dr.FloresPolinizadas = 0
        dr.LocalidadCruce = ""
        dr.NroRepeticiones = 0
        dr.FechaImpresionCruce = ""
        dr.UsuarioCruce = ""
        dr.FechaCruce = ""
        dr.ResumenCosecha = ""
        dr.TotalFloresPolinizadas = ""
        dr.TotalFrutos = ""
        dr.UsuarioCosecha = ""
        dr.FechaCosecha = ""
        dr.FechaImpresionCosecha = ""
        dr.TotalTuberculos = TotalTuberculos
        dr.ProcedenciaTub = ProcedenciaTub
        dr.Peso = Peso
        dr.FechaCosechaTub = FechaCosechaTub
        dr.LocalidadCosecha = ""
        dr.LocalidadCosechaTub = LocalidadCosechaTub
        dr.UsuarioCosechaTub = UsuarioCosechaTub
        dr.FechaImpresionCosechaTub = FechaImpresionCosechaTub
        dr.LocalidadMaceracion = LocalidadCosechaTub
        dr.FechaMaceracion = ""
        dr.UsuarioMaceracion = ""
        dr.LOTID = ""
        dr.EstadoResultado = 0
        dr.Plan = Plan
        dr.NroPlan = NroPlan

        drv.EndEdit()
        Me.ResultsTableAdapter1.Update(Me.Results_DataSet1.results)

        'Me.ResultsTableAdapter1.ListarVista(Me.Results_DataSet1.results)

        'Me.Label_CountHM.Text = Me.C1FlexGrid_HM.Rows.Count - 1
        Me.ListBox_Hide.Items.Clear()
    End Sub

    Private Sub BindingSource1_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingSource1.CurrentChanged

    End Sub

    Function GenerarID() As String
        'Dim ID As String = ""
        Dim ID As String
        'Genera ID
        Dim ss As String
        Dim rn As New Random
        Dim n1, n2, n3 As Integer
        ss = Date.Now.ToString("ss")
        n1 = rn.Next(10, 99)
        n2 = rn.Next(10, 99)
        n3 = rn.Next(10, 99)
        ID = n1.ToString + n2.ToString + n3.ToString + ss

        Return ID
    End Function

#Region "Configuracion"

    'Parametros iniciales
    Private Sub HideParameters()
        'Hide Tabs
        Me.TabControl.TabPages(1).Enabled = False
        Me.TabControl.TabPages(2).Enabled = False
        Me.TabControl.TabPages(3).Enabled = False
        Me.TabControl.TabPages(4).Enabled = False
        Me.TabControl.TabPages(5).Enabled = False
        Me.TabControl.TabPages(6).Enabled = False
        Me.TabControl.TabPages(7).Enabled = False
        Me.TabControl.TabPages(8).Enabled = False

        'Hide Button Sign in
        Me.Button_SignIn.Enabled = False
        Me.Button_SelectPlan.Enabled = False

        ''Hide Search Harvest
        'Me.Button_LoadHarvest.Visible = False

        'Hide Search Maceration
        Me.Button_LoadMaceration.Visible = False
    End Sub

    Private Sub ColumnsWidth()
        'Columns Width
        '=============
        'CrossingPlan
        'Me.C1FlexGrid_CrossingPlan.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_CrossingPlan.Cols(0).Width = 45
        'Me.C1FlexGrid_CrossingPlan.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_CrossingPlan.Cols(1).Width = 137

        'Me.C1FlexGrid_Hide.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Hide.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter

        'Dim yellowColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Hide.Styles.Add("void")
        'yellowColor.BackColor = Drawing.Color.FromArgb(243, 247, 129)

        'Dim greenColor As C1.Win.C1FlexGrid.CellStyle = C1FlexGrid_Hide.Styles.Add("void")
        'greenColor.BackColor = Drawing.Color.FromArgb(129, 247, 129)

        'Me.C1FlexGrid_Hide.Cols(1).StyleNew.BackColor = greenColor.BackColor
        'Me.C1FlexGrid_Hide.Cols(2).StyleNew.BackColor = yellowColor.BackColor


        'Parentals
        'Female
        Me.C1FlexGrid_Female.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_Female.Cols(0).Width = 68
        Me.C1FlexGrid_Female.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Female.Cols(1).Width = 180
        Me.C1FlexGrid_Female.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Female.Cols(2).Width = 180
        Me.C1FlexGrid_Female.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Female.Cols(3).Width = 180

        'Male
        Me.C1FlexGrid_Male.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_Male.Cols(0).Width = 68
        Me.C1FlexGrid_Male.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Male.Cols(1).Width = 180
        Me.C1FlexGrid_Male.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Male.Cols(2).Width = 180
        Me.C1FlexGrid_Male.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Male.Cols(3).Width = 180

        'Crossing

        'Value Cross
        Me.C1FlexGrid_ValuesCross.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_ValuesCross.Cols(0).Width = 68
        Me.C1FlexGrid_ValuesCross.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_ValuesCross.Cols(1).Width = 180
        Me.C1FlexGrid_ValuesCross.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_ValuesCross.Cols(2).Width = 180
        Me.C1FlexGrid_ValuesCross.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_ValuesCross.Cols(3).Width = 180

        'Female Cross
        Me.C1FlexGrid_FemaleCross.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_FemaleCross.Cols(0).Width = 68
        Me.C1FlexGrid_FemaleCross.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_FemaleCross.Cols(1).Width = 180
        Me.C1FlexGrid_FemaleCross.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_FemaleCross.Cols(2).Width = 180
        Me.C1FlexGrid_FemaleCross.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_FemaleCross.Cols(3).Width = 180

        'Male Cross
        Me.C1FlexGrid_MaleCross.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_MaleCross.Cols(0).Width = 68
        Me.C1FlexGrid_MaleCross.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_MaleCross.Cols(1).Width = 180
        Me.C1FlexGrid_MaleCross.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_MaleCross.Cols(2).Width = 180
        Me.C1FlexGrid_MaleCross.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_MaleCross.Cols(3).Width = 180

        'Harvest
        'Me.C1FlexGrid_Harvest.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Harvest.Cols(0).Width = 68
        'Me.C1FlexGrid_Harvest.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Harvest.Cols(1).Width = 180
        'Me.C1FlexGrid_Harvest.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Harvest.Cols(2).Width = 0
        'Me.C1FlexGrid_Harvest.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Harvest.Cols(3).Width = 0
        'Me.C1FlexGrid_Harvest.Cols(4).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Harvest.Cols(4).Width = 68
        'Me.C1FlexGrid_Harvest.Cols(5).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Harvest.Cols(5).Width = 180
        'Me.C1FlexGrid_Harvest.Cols(6).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Harvest.Cols(6).Width = 0
        'Me.C1FlexGrid_Harvest.Cols(7).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Harvest.Cols(7).Width = 0
        'Me.C1FlexGrid_Harvest.Cols(8).Width = 120
        'Me.C1FlexGrid_Harvest.Cols(9).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Harvest.Cols(9).Width = 68
        'Me.C1FlexGrid_Harvest.Cols(10).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Harvest.Cols(10).Width = 108
        'Me.C1FlexGrid_Harvest.Cols(11).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Harvest.Cols(11).Width = 89
        'Me.C1FlexGrid_Harvest.Cols(12).Width = 180
        'Me.C1FlexGrid_Harvest.Cols(13).Width = 0
        'Me.C1FlexGrid_Harvest.Cols(14).Width = 150
        'Me.C1FlexGrid_Harvest.Cols(15).Width = 145
        'Me.C1FlexGrid_Harvest.Cols(16).Width = 175
        'Me.C1FlexGrid_Harvest.Cols(17).Width = 150
        'Me.C1FlexGrid_Harvest.Cols(18).Width = 150
        'Me.C1FlexGrid_Harvest.Cols(19).Width = 150
        'Me.C1FlexGrid_Harvest.Cols(20).Width = 200
        'Me.C1FlexGrid_Harvest.Cols(21).Width = 200
        'Me.C1FlexGrid_Harvest.Cols(22).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Harvest.Cols(22).Width = 88
        'Me.C1FlexGrid_Harvest.Cols(23).Width = 200
        'Me.C1FlexGrid_Harvest.Cols(24).Width = 200

        'Columnas de Cruzamiento
        Me.C1FlexGrid_Harvest.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_Harvest.Cols(0).Width = 68
        'Me.C1FlexGrid_Harvest.Cols(0).Visible = False
        Me.C1FlexGrid_Harvest.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(1).Width = 180
        Me.C1FlexGrid_Harvest.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(2).Width = 180
        Me.C1FlexGrid_Harvest.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(3).Width = 180
        Me.C1FlexGrid_Harvest.Cols(4).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(4).Width = 180
        Me.C1FlexGrid_Harvest.Cols(5).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(5).Width = 180
        Me.C1FlexGrid_Harvest.Cols(6).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(6).Width = 180
        Me.C1FlexGrid_Harvest.Cols(7).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(7).Width = 180
        Me.C1FlexGrid_Harvest.Cols(8).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(8).Width = 180
        Me.C1FlexGrid_Harvest.Cols(9).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(9).Width = 180
        Me.C1FlexGrid_Harvest.Cols(10).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Harvest.Cols(10).Width = 180
        Me.C1FlexGrid_Harvest.Cols(11).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_Harvest.Cols(11).Width = 108
        Me.C1FlexGrid_Harvest.Cols(12).Width = 80
        Me.C1FlexGrid_Harvest.Cols(13).Width = 80
        Me.C1FlexGrid_Harvest.Cols(14).Width = 80
        Me.C1FlexGrid_Harvest.Cols(15).Width = 180
        Me.C1FlexGrid_Harvest.Cols(16).Width = 80
        Me.C1FlexGrid_Harvest.Cols(17).Width = 150
        Me.C1FlexGrid_Harvest.Cols(18).Width = 150
        Me.C1FlexGrid_Harvest.Cols(19).Width = 150
        'Columnas de Cosecha
        Me.C1FlexGrid_Harvest.Cols(20).Width = 200
        Me.C1FlexGrid_Harvest.Cols(21).Width = 80
        Me.C1FlexGrid_Harvest.Cols(22).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_Harvest.Cols(22).Width = 80
        Me.C1FlexGrid_Harvest.Cols(23).Width = 150
        Me.C1FlexGrid_Harvest.Cols(24).Width = 150
        Me.C1FlexGrid_Harvest.Cols(25).Width = 150
        'Columnas de Tub�rculos
        Me.C1FlexGrid_Harvest.Cols(26).Width = 80
        Me.C1FlexGrid_Harvest.Cols(27).Width = 180
        Me.C1FlexGrid_Harvest.Cols(28).Width = 150
        Me.C1FlexGrid_Harvest.Cols(29).Width = 180
        Me.C1FlexGrid_Harvest.Cols(30).Width = 180
        Me.C1FlexGrid_Harvest.Cols(31).Width = 150
        Me.C1FlexGrid_Harvest.Cols(32).Width = 150
        'Columnas de Maceraci�n
        Me.C1FlexGrid_Harvest.Cols(33).Width = 180
        Me.C1FlexGrid_Harvest.Cols(34).Width = 150
        Me.C1FlexGrid_Harvest.Cols(35).Width = 150
        Me.C1FlexGrid_Harvest.Cols(36).Width = 150
        'Configuracion
        Me.C1FlexGrid_Harvest.Cols(37).Width = 50
        Me.C1FlexGrid_Harvest.Cols(38).Width = 200
        Me.C1FlexGrid_Harvest.Cols(39).Width = 50

        'Value Cross
        Me.C1FlexGrid_ValuesTubHarv.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_ValuesTubHarv.Cols(0).Width = 68
        Me.C1FlexGrid_ValuesTubHarv.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_ValuesTubHarv.Cols(1).Width = 180
        Me.C1FlexGrid_ValuesTubHarv.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_ValuesTubHarv.Cols(2).Width = 180
        Me.C1FlexGrid_ValuesTubHarv.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_ValuesTubHarv.Cols(3).Width = 180

        'Tuberculus Harvest
        Me.C1FlexGrid_ValuesTubHarv.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_ValuesTubHarv.Cols(0).Width = 68
        Me.C1FlexGrid_ValuesTubHarv.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_ValuesTubHarv.Cols(1).Width = 180
        Me.C1FlexGrid_ValuesTubHarv.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_ValuesTubHarv.Cols(2).Width = 180
        Me.C1FlexGrid_ValuesTubHarv.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_ValuesTubHarv.Cols(3).Width = 180

        'Maceration
        'Me.C1FlexGrid_Maceration.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Maceration.Cols(0).Width = 68
        'Me.C1FlexGrid_Maceration.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Maceration.Cols(1).Width = 180
        'Me.C1FlexGrid_Maceration.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Maceration.Cols(2).Width = 0
        'Me.C1FlexGrid_Maceration.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Maceration.Cols(3).Width = 0
        'Me.C1FlexGrid_Maceration.Cols(4).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Maceration.Cols(4).Width = 68
        'Me.C1FlexGrid_Maceration.Cols(5).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Maceration.Cols(5).Width = 180
        'Me.C1FlexGrid_Maceration.Cols(6).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Maceration.Cols(6).Width = 0
        'Me.C1FlexGrid_Maceration.Cols(7).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        'Me.C1FlexGrid_Maceration.Cols(7).Width = 0
        'Me.C1FlexGrid_Maceration.Cols(8).Width = 120
        'Me.C1FlexGrid_Maceration.Cols(9).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Maceration.Cols(9).Width = 68
        'Me.C1FlexGrid_Maceration.Cols(10).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Maceration.Cols(10).Width = 108
        'Me.C1FlexGrid_Maceration.Cols(11).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Maceration.Cols(11).Width = 89
        'Me.C1FlexGrid_Maceration.Cols(12).Width = 180
        'Me.C1FlexGrid_Maceration.Cols(13).Width = 0
        'Me.C1FlexGrid_Maceration.Cols(14).Width = 150
        'Me.C1FlexGrid_Maceration.Cols(15).Width = 145
        'Me.C1FlexGrid_Maceration.Cols(16).Width = 175
        'Me.C1FlexGrid_Maceration.Cols(17).Width = 150
        'Me.C1FlexGrid_Maceration.Cols(18).Width = 150
        'Me.C1FlexGrid_Maceration.Cols(19).Width = 150
        'Me.C1FlexGrid_Maceration.Cols(20).Width = 200
        'Me.C1FlexGrid_Maceration.Cols(21).Width = 200
        'Me.C1FlexGrid_Maceration.Cols(22).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        'Me.C1FlexGrid_Maceration.Cols(22).Width = 88
        'Me.C1FlexGrid_Maceration.Cols(23).Width = 200
        'Me.C1FlexGrid_Maceration.Cols(24).Width = 200

        'Columnas de Cruzamiento
        Me.C1FlexGrid_Maceration.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_Maceration.Cols(0).Width = 68
        'Me.C1FlexGrid_Harvest.Cols(0).Visible = False
        Me.C1FlexGrid_Maceration.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(1).Width = 180
        Me.C1FlexGrid_Maceration.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(2).Width = 180
        Me.C1FlexGrid_Maceration.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(3).Width = 180
        Me.C1FlexGrid_Maceration.Cols(4).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(4).Width = 180
        Me.C1FlexGrid_Maceration.Cols(5).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(5).Width = 180
        Me.C1FlexGrid_Maceration.Cols(6).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(6).Width = 180
        Me.C1FlexGrid_Maceration.Cols(7).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(7).Width = 180
        Me.C1FlexGrid_Maceration.Cols(8).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(8).Width = 180
        Me.C1FlexGrid_Maceration.Cols(9).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(9).Width = 180
        Me.C1FlexGrid_Maceration.Cols(10).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
        Me.C1FlexGrid_Maceration.Cols(10).Width = 180
        Me.C1FlexGrid_Maceration.Cols(11).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_Maceration.Cols(11).Width = 108
        Me.C1FlexGrid_Maceration.Cols(12).Width = 80
        Me.C1FlexGrid_Maceration.Cols(13).Width = 80
        Me.C1FlexGrid_Maceration.Cols(14).Width = 80
        Me.C1FlexGrid_Maceration.Cols(15).Width = 180
        Me.C1FlexGrid_Maceration.Cols(16).Width = 80
        Me.C1FlexGrid_Maceration.Cols(17).Width = 150
        Me.C1FlexGrid_Maceration.Cols(18).Width = 150
        Me.C1FlexGrid_Maceration.Cols(19).Width = 150
        'Columnas de Cosecha
        Me.C1FlexGrid_Maceration.Cols(20).Width = 200
        Me.C1FlexGrid_Maceration.Cols(21).Width = 80
        Me.C1FlexGrid_Maceration.Cols(22).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        Me.C1FlexGrid_Maceration.Cols(22).Width = 80
        Me.C1FlexGrid_Maceration.Cols(23).Width = 150
        Me.C1FlexGrid_Maceration.Cols(24).Width = 150
        Me.C1FlexGrid_Maceration.Cols(25).Width = 150
        'Columnas de Tub�rculos
        Me.C1FlexGrid_Maceration.Cols(26).Width = 80
        Me.C1FlexGrid_Maceration.Cols(27).Width = 180
        Me.C1FlexGrid_Maceration.Cols(28).Width = 150
        Me.C1FlexGrid_Maceration.Cols(29).Width = 180
        Me.C1FlexGrid_Maceration.Cols(30).Width = 180
        Me.C1FlexGrid_Maceration.Cols(31).Width = 150
        Me.C1FlexGrid_Maceration.Cols(32).Width = 150
        'Columnas de Maceraci�n
        Me.C1FlexGrid_Maceration.Cols(33).Width = 180
        Me.C1FlexGrid_Maceration.Cols(34).Width = 150
        Me.C1FlexGrid_Maceration.Cols(35).Width = 150
        Me.C1FlexGrid_Maceration.Cols(36).Width = 150
        'Configuracion
        Me.C1FlexGrid_Maceration.Cols(37).Width = 50
        Me.C1FlexGrid_Maceration.Cols(38).Width = 200
        Me.C1FlexGrid_Maceration.Cols(39).Width = 50
    End Sub

    'Inicializacion de ComboBox's
    Private Sub InitializationComboBoxAll()
        'Initialization
        Me.ComboBox_Language.SelectedIndex = 0
        Me.ComboBox_Port.SelectedIndex = 5
        Me.ComboBox_PrintType.SelectedIndex = 1
        Me.ComboBox_Flowers.SelectedIndex = 0
        Me.ComboBox_NroFruits.SelectedIndex = 0
        Me.ComboBox_NroTubers.SelectedIndex = 0
        Me.ComboBox_GenFemale.SelectedIndex = 0
        Me.ComboBox_GenMale.SelectedIndex = 0
        Me.ComboBox_TotalFlowers.SelectedIndex = 0
        Me.cboBarcodeForCross.SelectedIndex = 1
        Me.cboProcedence.SelectedIndex = 0
        Me.cboFemaleTuber.SelectedIndex = 0
        Me.ComboBox1.SelectedIndex = 0
        'Me.ChkDateHarvest.Checked = True
        'Me.ComboBox_Bayas.SelectedIndex = 0
        Me.ComboBox_Printer.SelectedIndex = 0
    End Sub

    'Limpiar controles y data
    Private Sub Clean()
        'User Login Clear
        Me.ListBox_Username.Items.Clear()
        Me.ListBox_Password.Items.Clear()
        Me.ListBox_FullName.Items.Clear()
        Me.ComboBox_Username.Items.Clear()
        Me.TextBox_Password.Text = ""
        Me.ComboBox_Location.Items.Clear()
        Me.TextBox_CrossingPlanSelected.Text = ""

        'CrossingPlan Clear
        'Me.C1FlexGrid_CrossingPlan.Clear()
        'Me.C1FlexGrid_CrossingPlan.Rows.Count = 2
        'Me.C1FlexGrid_CrossingPlan.Cols.Count = 2

        'Parentals Clear
        Me.C1FlexGrid_Female.Clear()
        Me.C1FlexGrid_Male.Clear()
        Me.C1FlexGrid_Female.Rows.Count = 1
        Me.C1FlexGrid_Male.Rows.Count = 1
        Me.ListBox_FemaleLabels.Items.Clear()
        Me.ListBox_MaleLabels.Items.Clear()
        Me.Label_CountFemales.Text = ""
        Me.Label_CountMales.Text = ""

        'Crossing Clear
        Me.C1FlexGrid_FemaleCross.Clear()
        Me.C1FlexGrid_MaleCross.Clear()
        Me.C1FlexGrid_ValuesCross.Clear()
        Me.C1FlexGrid_FemaleCross.Rows.Count = 1
        Me.C1FlexGrid_MaleCross.Rows.Count = 1
        Me.C1FlexGrid_ValuesCross.Rows.Count = 1
        Me.TextBox_Cross.Text = ""
        Me.TextBox_Order.Text = ""
        Me.TextBox_Data2.Text = ""
        Me.TextBox_Data3.Text = ""
        Me.ComboBox_TypeCross.Items.Clear()
        Me.Label_CountFemalesCross.Text = ""
        Me.Label_CountMalesCross.Text = ""

        'Harvest Clear
        Me.C1FlexGrid_Harvest.Clear()
        'Me.C1FlexGrid_Harvest.Rows.Count = 1
        Me.Label_CountHarvest.Text = ""
        Me.TextBox_Harvest.Text = ""
        Me.ComboBox_FruitSize.Items.Clear()
        Me.TextBox_IndexHarvest.Text = ""

        'Cosecha de tuberculos Clear
        Me.C1FlexGrid_ValuesTubHarv.Clear()
        Me.C1FlexGrid_ValuesTubHarv.Rows.Count = 1
        Me.txtCosechaTuber.Text = ""
        Me.cboProcedence.SelectedIndex = 0
        Me.ComboBox_NroTubers.SelectedIndex = 0
        Me.cboFemaleTuber.SelectedIndex = 0

        'Maceration Clear
        Me.C1FlexGrid_Maceration.Clear()
        Me.C1FlexGrid_Maceration.Rows.Count = 1
        Me.Label_CountMaceration.Text = ""
        Me.TextBox_Maceration.Text = ""
        Me.TextBox_IndexMaceration.Text = ""

        'Options Clear
        Me.ComboBox_TypeCrossOption.Items.Clear()
        Me.ListBox_BarcodeType.Items.Clear()
        Me.ListBox_BarcodeTypeID.Items.Clear()

    End Sub

    Private Sub OpenFile()
        Me.OpenFileDialog1.FileName = Nothing

        If Me.OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.TextBox_CrossingPlan.Text = Path.GetFileName(Me.OpenFileDialog1.FileName)
            'Verifica si es Excel
            Call CheckExcel()
        Else
            Me.TextBox_CrossingPlan.Text = ""
            Me.Button_SelectPlan.Enabled = False
            Me.Button_SignIn.Enabled = False
        End If
    End Sub

    'Verifica si es Excel
    Private Sub CheckExcel()
        Dim result As DataSet = Nothing

        If Path.GetExtension(Me.TextBox_CrossingPlan.Text).ToLower().Equals(".xls") Then
            'Lee el archivo Excel
            Call ReadExcelFile(result)

            'Verifica si es la plantilla correcta
            Call CheckTemplate(result)
        Else
            Me.TextBox_CrossingPlan.Text = ""
            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage1)
            Else
                Call ShowSystemMessage(strMessage1)
            End If
            Exit Sub
        End If
    End Sub

    'Lee el archivo Excel
    Private Sub ReadExcelFile(ByRef result As DataSet)
        Dim stream As FileStream = File.Open(Me.OpenFileDialog1.FileName, FileMode.Open, FileAccess.Read)
        Dim excelReader As IExcelDataReader

        excelReader = ExcelReaderFactory.CreateBinaryReader(stream)

        result = excelReader.AsDataSet()
        Return
    End Sub

    Private Sub CheckTemplate(ByVal result As DataSet)
        Dim objTable As DataTable

        Try
            objTable = result.Tables(0)
        Catch ex As Exception
            Me.TextBox_CrossingPlan.Text = ""

            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage2)
            Else
                Call ShowSystemMessage(strMessage2)
            End If
            Exit Sub
        End Try

        If objTable.Rows(2).ItemArray(0) = "TemplateCrossingPlan" Then
            'Carga parametros del plan de cruzamientos
            'Call LoadParameters(objTable, result)
            ''''''''''''''''''''''''''''''''''''''''
            Panel_SelectCrossPlan.Visible = True
            Panel_SelectCrossPlan.Height = Me.Height - 3
            Panel_SelectCrossPlan.Width = Me.Width - 2

            Dim i As Integer
            Me.ListBox_SelectPlan.Items.Clear()

            Try
                For i = 0 To result.Tables.Count - 4
                    Me.ListBox_SelectPlan.Items.Add(result.Tables.Add.TableName)
                    Me.ComboBox_SelectCrossingPlan.Items.Add(result.Tables(i).TableName)
                Next

                Me.Button_SelectPlan.Enabled = True
            Catch ex As Exception

            End Try
            ''''''''''''''''''''''''''''''''''''''''
        Else
            Me.TextBox_CrossingPlan.Text = ""

            If Me.ComboBox_Language.SelectedIndex = 0 Then
                Call ShowSystemMessage(strMessage2)
            Else
                Call ShowSystemMessage(strMessage2)
            End If
            Exit Sub
        End If

    End Sub

    Private Sub GridHeaderName()
        'Fields
        Dim posF1, posF2, posF3, posM1, posM2, posM3 As String

        'Females
        If Me.ListBox_FemaleLabels.Items.Item(0) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(1) Is DBNull.Value And Me.ListBox_FemaleLabels.Items.Item(2) Is DBNull.Value Then
            'Female
            posF1 = "F.Data1"
            posF2 = "F.Data2"
            posF3 = "F.Data3"
        Else
            'Female
            posF1 = Me.ListBox_FemaleLabels.Items.Item(0).ToString
            posF2 = Me.ListBox_FemaleLabels.Items.Item(1).ToString
            posF3 = Me.ListBox_FemaleLabels.Items.Item(2).ToString
        End If

        'Males
        If Me.ListBox_MaleLabels.Items.Item(0) Is DBNull.Value And Me.ListBox_MaleLabels.Items.Item(1) Is DBNull.Value And Me.ListBox_MaleLabels.Items.Item(2) Is DBNull.Value Then
            'Female
            posM1 = "M.Data1"
            posM2 = "M.Data2"
            posM3 = "M.Data3"
        Else
            'Female
            posM1 = Me.ListBox_MaleLabels.Items.Item(0).ToString
            posM2 = Me.ListBox_MaleLabels.Items.Item(1).ToString
            posM3 = Me.ListBox_MaleLabels.Items.Item(2).ToString
        End If

        'Columns Name
        'Haverts
        Me.C1FlexGrid_Harvest(0, 0) = "ID"
        'Datos de Planta
        Me.C1FlexGrid_Harvest(0, 1) = posF1
        Me.C1FlexGrid_Harvest(0, 2) = posF2
        Me.C1FlexGrid_Harvest(0, 3) = posF3
        Me.C1FlexGrid_Harvest(0, 4) = "F.Data4"
        Me.C1FlexGrid_Harvest(0, 5) = "F.Data5"
        Me.C1FlexGrid_Harvest(0, 6) = posM1
        Me.C1FlexGrid_Harvest(0, 7) = posM2
        Me.C1FlexGrid_Harvest(0, 8) = posM3
        Me.C1FlexGrid_Harvest(0, 9) = "M.Data4"
        Me.C1FlexGrid_Harvest(0, 10) = "M.Data5"
        'Cruzamiento
        Me.C1FlexGrid_Harvest(0, 11) = "Tipo Cruzamiento"
        Me.C1FlexGrid_Harvest(0, 12) = "FOrder"
        Me.C1FlexGrid_Harvest(0, 13) = "MOrder"
        Me.C1FlexGrid_Harvest(0, 14) = "T. Flores"
        Me.C1FlexGrid_Harvest(0, 15) = "Loc. Cruce"
        Me.C1FlexGrid_Harvest(0, 16) = "Repeticiones"
        Me.C1FlexGrid_Harvest(0, 17) = "Fecha Imp."
        Me.C1FlexGrid_Harvest(0, 18) = "Usuario Cruce"
        Me.C1FlexGrid_Harvest(0, 19) = "Fecha Cruce"
        'Cosecha
        Me.C1FlexGrid_Harvest(0, 20) = "Resumen Cosecha"
        Me.C1FlexGrid_Harvest(0, 21) = "Total Flores"
        Me.C1FlexGrid_Harvest(0, 22) = "Total Frutos"
        Me.C1FlexGrid_Harvest(0, 23) = "Usuario Cos"
        Me.C1FlexGrid_Harvest(0, 24) = "Fecha Cos"
        Me.C1FlexGrid_Harvest(0, 25) = "Fecha Imp"
        Me.C1FlexGrid_Harvest(0, 26) = "Total Tub."
        Me.C1FlexGrid_Harvest(0, 27) = "Procedencia Tub."
        Me.C1FlexGrid_Harvest(0, 28) = "Fecha Cos Tub."
        Me.C1FlexGrid_Harvest(0, 29) = "Loc. Cosecha"
        Me.C1FlexGrid_Harvest(0, 30) = "Loc. Cos Tub."
        Me.C1FlexGrid_Harvest(0, 31) = "Usuario Tub."
        Me.C1FlexGrid_Harvest(0, 32) = "Fecha Imp"
        'Maceracion
        Me.C1FlexGrid_Harvest(0, 33) = "Loc. Mac"
        Me.C1FlexGrid_Harvest(0, 34) = "Fecha Mac"
        Me.C1FlexGrid_Harvest(0, 35) = "Usuario Mac"
        Me.C1FlexGrid_Harvest(0, 36) = "LOTID"
        'Configuracion
        Me.C1FlexGrid_Harvest(0, 37) = "Est"
        Me.C1FlexGrid_Harvest(0, 38) = "NroPlan"
        Me.C1FlexGrid_Harvest(0, 39) = "PlanName"

        'Maceration
        Me.C1FlexGrid_Maceration(0, 0) = "ID"
        'Datos de Planta
        Me.C1FlexGrid_Maceration(0, 1) = posF1
        Me.C1FlexGrid_Maceration(0, 2) = posF2
        Me.C1FlexGrid_Maceration(0, 3) = posF3
        Me.C1FlexGrid_Maceration(0, 4) = "F.Data4"
        Me.C1FlexGrid_Maceration(0, 5) = "F.Data5"
        Me.C1FlexGrid_Maceration(0, 6) = posM1
        Me.C1FlexGrid_Maceration(0, 7) = posM2
        Me.C1FlexGrid_Maceration(0, 8) = posM3
        Me.C1FlexGrid_Maceration(0, 9) = "M.Data4"
        Me.C1FlexGrid_Maceration(0, 10) = "M.Data5"
        'Cruzamiento
        Me.C1FlexGrid_Maceration(0, 11) = "Tipo Cruzamiento"
        Me.C1FlexGrid_Maceration(0, 12) = "FOrder"
        Me.C1FlexGrid_Maceration(0, 13) = "MOrder"
        Me.C1FlexGrid_Maceration(0, 14) = "T. Flores"
        Me.C1FlexGrid_Maceration(0, 15) = "Loc. Cruce"
        Me.C1FlexGrid_Maceration(0, 16) = "Repeticiones"
        Me.C1FlexGrid_Maceration(0, 17) = "Fecha Imp."
        Me.C1FlexGrid_Maceration(0, 18) = "Usuario Cruce"
        Me.C1FlexGrid_Maceration(0, 19) = "Fecha Cruce"
        'Cosecha
        Me.C1FlexGrid_Maceration(0, 20) = "Resumen Cosecha"
        Me.C1FlexGrid_Maceration(0, 21) = "Total Flores"
        Me.C1FlexGrid_Maceration(0, 22) = "Total Frutos"
        Me.C1FlexGrid_Maceration(0, 23) = "Usuario Cos"
        Me.C1FlexGrid_Maceration(0, 24) = "Fecha Cos"
        Me.C1FlexGrid_Maceration(0, 25) = "Fecha Imp"
        Me.C1FlexGrid_Maceration(0, 26) = "Total Tub."
        Me.C1FlexGrid_Maceration(0, 27) = "Procedencia Tub."
        Me.C1FlexGrid_Maceration(0, 28) = "Fecha Cos Tub."
        Me.C1FlexGrid_Maceration(0, 29) = "Loc. Cosecha"
        Me.C1FlexGrid_Maceration(0, 30) = "Loc. Cos Tub."
        Me.C1FlexGrid_Maceration(0, 31) = "Usuario Tub."
        Me.C1FlexGrid_Maceration(0, 32) = "Fecha Imp"
        'Maceracion
        Me.C1FlexGrid_Maceration(0, 33) = "Loc. Mac"
        Me.C1FlexGrid_Maceration(0, 34) = "Fecha Mac"
        Me.C1FlexGrid_Maceration(0, 35) = "Usuario Mac"
        Me.C1FlexGrid_Maceration(0, 36) = "LOTID"
        'Configuracion
        Me.C1FlexGrid_Maceration(0, 37) = "Est"
        Me.C1FlexGrid_Maceration(0, 38) = "NroPlan"
        Me.C1FlexGrid_Maceration(0, 39) = "PlanName"

        'Me.C1FlexGrid_Maceration(0, 0) = "OrderF."
        'Me.C1FlexGrid_Maceration(0, 1) = posF1
        'Me.C1FlexGrid_Maceration(0, 2) = posF2
        'Me.C1FlexGrid_Maceration(0, 3) = posF3
        'Me.C1FlexGrid_Maceration(0, 4) = "OrderM."
        'Me.C1FlexGrid_Maceration(0, 5) = posM1
        'Me.C1FlexGrid_Maceration(0, 6) = posM2
        'Me.C1FlexGrid_Maceration(0, 7) = posM3
        'Me.C1FlexGrid_Maceration(0, 8) = "TypeCross"
        'Me.C1FlexGrid_Maceration(0, 9) = "Repetitions"
        'Me.C1FlexGrid_Maceration(0, 10) = "Flower(s)"

        'Me.C1FlexGrid_Maceration(0, 11) = "Harvest Resume"
        'Me.C1FlexGrid_Maceration(0, 12) = "Total Flowers"
        'Me.C1FlexGrid_Maceration(0, 13) = "Fruit(s)"
        'Me.C1FlexGrid_Maceration(0, 14) = "PrintingCrossDate"
        'Me.C1FlexGrid_Maceration(0, 15) = "CrossingDate"
        'Me.C1FlexGrid_Maceration(0, 16) = "HarvestDate"
        'Me.C1FlexGrid_Maceration(0, 17) = "MacerationDate"
        'Me.C1FlexGrid_Maceration(0, 18) = "User1"
        'Me.C1FlexGrid_Maceration(0, 19) = "User2"
        'Me.C1FlexGrid_Maceration(0, 20) = "User3"
        'Me.C1FlexGrid_Maceration(0, 21) = "Location1"
        'Me.C1FlexGrid_Maceration(0, 22) = "Location2"
        'Me.C1FlexGrid_Maceration(0, 23) = "NroPlan"
        'Me.C1FlexGrid_Maceration(0, 24) = "PlanName"
    End Sub

    '========================
    '===Show System Message==
    Sub ShowSystemMessage(ByVal strMessage As String)
        Panel_Message.Visible = True
        Panel_Message.Height = Me.Height - 3
        Panel_Message.Width = Me.Width - 2
        Label_Message.Text = strMessage
    End Sub

#End Region


#Region "Impresi�n"

#End Region

#Region "Validaci�n"

#End Region
    Private Sub SetResumeTub(ByVal objtext As TextBox, ByVal ID As String, ByVal strChar As Char)
        If strChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            Dim dtrReader As SqlCeDataReader
            Dim conn As New SqlCeConnection("Data Source=" & CurrentFolder & "\CIPCROSS_DB.sdf; ")
            Dim query As String = ""
            Dim strColector As String = ""
            Dim strEspecie As String = ""
            Dim strTotalGenotipos As String = ""
            Dim strProcedencia As String = ""
            'Obtener datos segun el ID.
            'query = "SELECT FDato1, FDato3 FROM results where ID = '" + ID + "'"
            query = "SELECT FDato1, FDato3, COUNT(*) AS TotalGenotipos FROM results where TotalTuberculos <> '' and FDato1 = '" + objtext.Text.Split(";")(0) + "' AND ProcedenciaTub = '" + Me.cboProcedence.Text + "' GROUP BY FDato1, FDato3"
            Dim objCmdselect As New SqlCeCommand(query, conn)

            conn.Open()
            objCmdselect.CommandText = query
            dtrReader = objCmdselect.ExecuteReader
            If dtrReader.Read() Then
                strColector = Trim(dtrReader(0))
                strEspecie = Trim(dtrReader(1))
                strTotalGenotipos = Trim(dtrReader(2))
                'strProcedencia = Trim(dtrReader(3))
            End If
            objCmdselect = Nothing
            dtrReader.Close()
            dtrReader = Nothing
            conn.Close()
            conn = Nothing

            'Setear el resumen de cosecha de tuberculos y prepararlos para mandar a imprimir.
            Dim strTotalTuberculos As String = ""
            If strColector <> "" Then
                Dim dtrReader2 As SqlCeDataReader
                Dim conn2 As New SqlCeConnection("Data Source=" & CurrentFolder & "\CIPCROSS_DB.sdf; ")
                query = "SELECT Sum(CAST(TotalTuberculos AS INT)) FROM results where TotalTuberculos <> '' and FDato1 = '" + strColector + "' and ProcedenciaTub = '" + Me.cboProcedence.Text + "'"
                Dim objCmdselect2 As New SqlCeCommand(query, conn2)

                conn2.Open()
                objCmdselect2.CommandText = query
                dtrReader2 = objCmdselect2.ExecuteReader
                If dtrReader2.Read() Then
                    strTotalTuberculos = Trim(dtrReader2(0))
                End If
                objCmdselect2 = Nothing
                dtrReader2.Close()
                dtrReader2 = Nothing
                conn2.Close()
                conn2 = Nothing

                'Asignar variables a textbox2
                'TextBox2.Text = strColector + "|" + strEspecie + "|" + strTotalTuberculos
                txtCosechaTuber.Text = strColector + "|" + strEspecie + "|" + strTotalTuberculos + "|" + strTotalGenotipos '+ "|" + ID
            End If
            objtext.Text = ""
        End If
    End Sub
    Private Sub SetResumeTub2(ByVal ID As String)
        Dim dtrReader As SqlCeDataReader
        Dim conn As New SqlCeConnection("Data Source=" & CurrentFolder & "\CIPCROSS_DB.sdf; ")
        Dim query As String = ""
        Dim strColector As String = ""
        Dim strEspecie As String = ""
        Dim strTotalGenotipos As String = ""
        Dim strProcedencia As String = ""
        'Obtener datos segun el ID.
        'query = "SELECT FDato1, FDato3 FROM results where ID = '" + ID + "'"
        query = "SELECT FDato1, FDato3, COUNT(*) AS TotalGenotipos FROM results where TotalTuberculos <> '' and FDato1 = '" + ID + "' AND ProcedenciaTub = '" + Me.cboProcedence.Text + "' GROUP BY FDato1, FDato3"
        Dim objCmdselect As New SqlCeCommand(query, conn)

        conn.Open()
        objCmdselect.CommandText = query
        dtrReader = objCmdselect.ExecuteReader
        If dtrReader.Read() Then
            strColector = Trim(dtrReader(0))
            strEspecie = Trim(dtrReader(1))
            strTotalGenotipos = Trim(dtrReader(2))
            'strProcedencia = Trim(dtrReader(3))
        End If
        objCmdselect = Nothing
        dtrReader.Close()
        dtrReader = Nothing
        conn.Close()
        conn = Nothing

        'Setear el resumen de cosecha de tuberculos y prepararlos para mandar a imprimir.
        Dim strTotalTuberculos As String = ""
        If strColector <> "" Then
            Dim dtrReader2 As SqlCeDataReader
            Dim conn2 As New SqlCeConnection("Data Source=" & CurrentFolder & "\CIPCROSS_DB.sdf; ")
            query = "SELECT Sum(CAST(TotalTuberculos AS INT)) FROM results where TotalTuberculos <> '' and FDato1 = '" + strColector + "' and ProcedenciaTub = '" + Me.cboProcedence.Text + "'"
            Dim objCmdselect2 As New SqlCeCommand(query, conn2)

            conn2.Open()
            objCmdselect2.CommandText = query
            dtrReader2 = objCmdselect2.ExecuteReader
            If dtrReader2.Read() Then
                strTotalTuberculos = Trim(dtrReader2(0))
            End If
            objCmdselect2 = Nothing
            dtrReader2.Close()
            dtrReader2 = Nothing
            conn2.Close()
            conn2 = Nothing

            'Asignar variables a textbox2
            'TextBox2.Text = strColector + "|" + strEspecie + "|" + strTotalTuberculos
            txtCosechaTuber.Text = strColector + "|" + strEspecie + "|" + strTotalTuberculos + "|" + strTotalGenotipos '+ "|" + ID
        End If
    End Sub
    Private Sub SetResumeMac(ByVal objtext As TextBox, ByVal ID As String, ByVal strChar As Char)
        If strChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            Dim dtrReader As SqlCeDataReader
            Dim conn As New SqlCeConnection("Data Source=" & CurrentFolder & "\CIPCROSS_DB.sdf; ")
            Dim query As String = ""
            Dim strColector As String = ""
            Dim strEspecie As String = ""
            Dim strTipoCruzamiento As String = ""

            'Obtener datos segun el ID.
            query = "SELECT FDato1, FDato3, TipoCruzamiento FROM results where ID = '" + ID + "'"
            Dim objCmdselect As New SqlCeCommand(query, conn)

            conn.Open()
            objCmdselect.CommandText = query
            dtrReader = objCmdselect.ExecuteReader
            If dtrReader.Read() Then
                strColector = Trim(dtrReader(0))
                strEspecie = Trim(dtrReader(1))
                strTipoCruzamiento = Trim(dtrReader(2))
            End If
            objCmdselect = Nothing
            dtrReader.Close()
            dtrReader = Nothing
            conn.Close()
            conn = Nothing

            'Setear el resumen de cosecha de tuberculos y prepararlos para mandar a imprimir.
            Dim strTotalFloresPolinizadas As String = ""
            Dim strTotalFrutos As String = ""

            If strColector <> "" Then
                Dim dtrReader2 As SqlCeDataReader
                Dim conn2 As New SqlCeConnection("Data Source=" & CurrentFolder & "\CIPCROSS_DB.sdf; ")
                query = "SELECT Sum(CAST(TotalFloresPolinizadas AS INT)), Sum(CAST(TotalFrutos AS INT)) FROM results where TotalFrutos <> '' and FDato1 = '" + strColector + "' and TipoCruzamiento = '" + strTipoCruzamiento + "'"
                Dim objCmdselect2 As New SqlCeCommand(query, conn2)

                conn2.Open()
                objCmdselect2.CommandText = query
                dtrReader2 = objCmdselect2.ExecuteReader

                If dtrReader2.Read() Then
                    strTotalFloresPolinizadas = Trim(dtrReader2(0))
                    strTotalFrutos = Trim(dtrReader2(1))
                End If

                objCmdselect2 = Nothing
                dtrReader2.Close()
                dtrReader2 = Nothing
                conn2.Close()
                conn2 = Nothing

                'Asignar variables a textbox2
                'TextBox2.Text = strColector + "|" + strEspecie + "|" + strTotalTuberculos
                TextBox_Maceration.Text = strColector + "|" + strTipoCruzamiento + "|" + strTotalFloresPolinizadas + "|" + strTotalFrutos + "|" + ID + "|" + strEspecie
                objtext.Text = ""
            End If
        End If
    End Sub

    Private Sub Button_ExportDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ExportDB.Click
        Dim i, j As Integer
        Dim result, resultColumn, curFile, cod As String


        cod = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
        cod = cod.Replace(":", "-")
        cod = cod.Replace(" ", "_")
        curFile = "\My Documents\DB_Results_" + cod + ".xls"

        Dim detailsNameWrite As String = curFile
        Dim objWriter As New System.IO.StreamWriter(detailsNameWrite)

        Dim conn As New SqlCeConnection("Data Source=" & CurrentFolder & "\CIPCROSS_DB.sdf; ")
        Dim query As String = ""
        query = "SELECT * FROM results"


        If query <> "" Then
            Dim cmd As New SqlCeCommand(query, conn)

            conn.Open()
            Dim rdr As SqlCeDataReader = cmd.ExecuteReader()
            Dim dt As New DataTable

            Try

                resultColumn = ""
                Do While rdr.Read
                    result = ""

                    If resultColumn = "" Then
                        For j = 0 To rdr.FieldCount - 1
                            resultColumn = resultColumn + rdr.GetName(j) + Chr(9)
                        Next
                        objWriter.WriteLine(resultColumn)
                    End If

                    For i = 0 To rdr.FieldCount - 1
                        'For j = 0 To grvFicticio.Cols.Count - 1
                        result = result + rdr.GetValue(i).ToString() + Chr(9)
                        'Next
                    Next
                    objWriter.WriteLine(result)
                Loop

            Finally

                ' Always call Close when done reading
                rdr.Close()

                ' Always call Close when done reading
                conn.Close()

            End Try
            MessageBox.Show("Archivo generado satisfactoriamente", "Mensaje de Alerta")
        End If

        'Exportar Gridview Ficticio
        'For i = 0 To grvFicticio.Rows.Count - 1
        '    result = ""
        '    For j = 0 To grvFicticio.Cols.Count - 1
        '        result = result + grvFicticio(i, j).ToString() + Chr(9)
        '    Next
        '    objWriter.WriteLine(result)
        'Next

        objWriter.Close()

        'grvFicticio.DataSource = Nothing
    End Sub

    Private Sub TabPage_Harvest_Tuber_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabPage_Harvest_Tuber.GotFocus
        TextBox_SearchValue.Focus()
    End Sub

    Private Sub TabPage_Crossing_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabPage_Crossing.GotFocus
        TextBox_SearchValueCross.Focus()
    End Sub

    Private Sub TabPage_Harvest_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabPage_Harvest.GotFocus
        TextBox_SearchHarvest.Focus()
    End Sub

    Private Sub TabControl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl.SelectedIndexChanged
        If TabControl.SelectedIndex = 2 Then
            TextBox_SearchValueCross.Focus()
        ElseIf TabControl.SelectedIndex = 4 Then
            TextBox_SearchValue.Focus()
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex > 0 Then
            'Listar Vista
            GetView()
        End If
    End Sub

    Private Sub Button_DeleteDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DeleteDB.Click

    End Sub

    Function ReplaceSpecialCharacters(ByVal strValue As String) As String
        Dim intCount As Integer

        'strValue = Replace(strValue, "�", " ", 1, -1, CompareMethod.Binary)
        'strValue = Replace(strValue, "�", " ", 1, -1, CompareMethod.Text)
        'strValue = Replace(strValue, "�", " ", 1, -1)
        'strValue = System.Text.RegularExpressions.Regex.Replace(strValue, "[^\u00C2-\u00C2]", String.Empty)
        strValue = System.Text.RegularExpressions.Regex.Replace(strValue, "[^\u0000-\u007F]", " ")
        strValue = Replace(strValue, "�", "n", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "�", "N", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "#", " ", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "'", " ", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "�", "o", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "�", "a", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "�", "e", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "�", "i", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "�", "u", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "�", "n", 1, -1, CompareMethod.Binary)
        strValue = Replace(strValue, "�", " ", 1, -1, CompareMethod.Binary)

        If strValue Is Nothing Then
            strValue = ""
        End If

        ReplaceSpecialCharacters = strValue
    End Function



    Private Sub ComboBox_Printer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_Printer.SelectedIndexChanged
        If ComboBox_Printer.SelectedIndex = 0 Then
            leftMarginOfLabel = "! 0 200 200 500 1" ' Default printer RW 220
        ElseIf ComboBox_Printer.SelectedIndex = 1 Then
            leftMarginOfLabel = "! 55 200 200 500 1" ' printer ZQ 510        
        End If
    End Sub

    Private Sub ComboBox_GenMale_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_GenMale.SelectedIndexChanged

    End Sub
End Class